from func.base_model import Base
from func import db, db_handle

class M_Log_Detail(Base):

    __tablename__ = 'log_details'

    detail         = db.Column(db.Text, nullable=True)
    type           = db.Column(db.String(256), nullable=True)
    tag            = db.Column(db.Text, nullable=True)
    source_message = db.Column(db.Text, nullable=True)

    def __init__(self):
        super(M_Log_Detail, self).__init__()

class M_Log_Detail_New(Base):
    __bind_key__  = 'newDB'
    __tablename__ = 'log_detail_backups'

    detail         = db.Column(db.Text, nullable=True)
    type           = db.Column(db.String(256), nullable=True)
    tag            = db.Column(db.Text, nullable=True)
    source_message = db.Column(db.Text, nullable=True)
    token_user     = db.Column(db.Text, nullable=True)
    params         = db.Column(db.Text, nullable=True)
    time_wait      = db.Column(db.Float, nullable=True)

    def __init__(self):
        super(M_Log_Detail_New, self).__init__()