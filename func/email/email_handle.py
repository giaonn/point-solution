#!/usr/bin/env python
# -*- coding: utf-8 -*-
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.header import Header
from email.utils import formataddr
from func.email import format_mail
from func.email import email_type_const

class EmailHandle:
    def send_email(self, send_email, password_send_email, email_host, email_port, email_variables, email_type):
        result = 'success'

        msg = MIMEMultipart()
        msg['From'] = formataddr((str(Header('イウンゴ事務局', 'utf-8')), send_email))
        msg['To'] = email_variables['email']

        if email_type == email_type_const.REGISTER_USER_ACCOUNT:
            msg['Subject'] = format_mail.SUBJECT_REGISTER_USER_ACCOUNT_MAIL
        elif email_type == email_type_const.SEND_RESET_PASSWORD_CODE:
            msg['Subject'] = format_mail.SUBJECT_CODE_RESET_PASSWORD_MAIL
        elif email_type == email_type_const.CONTACT_ADMIN:
            msg['Subject'] = format_mail.SUBJECT_CONTACT_ADMIN_MAIL
        elif email_type == email_type_const.CONTACT_USER:
            msg['Subject'] = format_mail.SUBJECT_CONTACT_USER_MAIL
        else:
            msg['Subject'] = 'Default'

        if email_type == email_type_const.REGISTER_USER_ACCOUNT:
            body = format_mail.content_register_user_account_mail(str(email_variables['username']), str(email_variables['email']))
        elif email_type == email_type_const.SEND_RESET_PASSWORD_CODE:
            body = format_mail.content_code_reset_password_mail(str(email_variables['code']))
        elif email_type == email_type_const.CONTACT_ADMIN:
            body = format_mail.content_contact_admin_mail(str(email_variables['sender']),
                                                          str(email_variables['email']),
                                                          str(email_variables['content']))
        elif email_type == email_type_const.CONTACT_USER:
            body = format_mail.content_contact_user_mail(str(email_variables['sender']), str(email_variables['content']))
        else:
            body = ''

        msg.attach(MIMEText(body, 'html'))
        try:
            mailer = smtplib.SMTP(str(email_host), int(email_port))
            mailer.ehlo()
            mailer.starttls()
            mailer.ehlo()
            mailer.login(send_email, password_send_email)
            mailer.sendmail(send_email, email_variables['email'], msg.as_string())
            mailer.quit()
        except smtplib.SMTPException:
            result = 'error'

        return result
