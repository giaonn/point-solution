#!/usr/bin/env python
# -*- coding: utf-8 -*-

# MAIL SEND WHEN REGISTER SUCCESS
SUBJECT_REGISTER_USER_ACCOUNT_MAIL = '[イウンゴ]アカウント登録完了のお知らせ'
SUBJECT_CODE_RESET_PASSWORD_MAIL = '[イウンゴ]利用パスワード初期化のご認証コード'
SUBJECT_CONTACT_ADMIN_MAIL = 'お問い合わせ'
SUBJECT_CONTACT_USER_MAIL = 'お問い合わせ返事メール'

def content_register_user_account_mail(login_id, email):
    mail = """
        <p>
            「イウンゴ」にアカウント登録いただき、ありがとうございます。
        </p>
        
        <p>
            アカウント登録が完了いたしました。
        </p>
        
        <p>
            イウンゴでは、セキュリティの観点から定期的に自動でログアウトが行われる場合がございます。
            再度ログインするためには、以下の情報が必要となりますので、大切に保管していただきますようお願いいたします。
        </p>
        
        <p>
            ■ログインID <br>
            """ + login_id + """ <br>
            ■メールアドレス <br>
            """ + email + """  <br>
        </p>
        
        <p>
            ■パスワード <br>
            お客様が登録されたパスワードです。
        </p>
        
        <p>
            ※このメールはイウンゴへアカウント登録いただいた方へお送りしています。<br>
            ※本メールは送信専用となっております。
        </p>
        
        <p>
            ------------------------------------------- <br>
            運営元： WSSJ株式会社 <br>
            http://www.wssj.co.jp <br>
            ------------------------------------------- <br>
        </p>
        """
    return str(mail)


def content_code_reset_password_mail(code):
    mail = """
        <p>
            「イウンゴ」にアカウント登録いただき、ありがとうございます。
        </p>

        <p>
            下記の6桁の認証コードをメール送信完了画面の認証コード入力欄に入力し、<br>
            〔認証する〕ボタンを押してください。
        </p>

        <p>
            ※12時間以内に再度アクセスしてください。
        </p>

        <p>
            認証コード【""" + code + """】
        </p>

        <p>
            ※このメールは送信専用です。このメールアドレス宛に返信しないようお願いいたします。
        </p>
        
        <p>
            ------------------------------------------- <br>
            運営元： WSSJ株式会社 <br>
            http://www.wssj.co.jp <br>
            ------------------------------------------- <br>
        </p>
        """
    return str(mail)

def content_contact_admin_mail(sender, email, content):
    mail = """
        <p>
            お問い合わせの内容です。
        </p>

        <p>
            ユーザ： """ + sender + """ <br>
            メールアドレス： """ + email + """ <br>
            お問い合わせ内容：<br>
            """ + content + """
        </p>

        <p>
            以上です。 <br>
            よろしくお願いします。
        </p>
        """
    return str(mail)

def content_contact_user_mail(sender, content):
    mail = """
        <p>
            """ + sender + """様、ご連絡有難う御座います。
        </p>

        <p>
            お問い合わせ内容を受付ましたので、<br>
            後程担当者よりご連絡差し上げますのでお待ち下さい。
        <p>

        <p>
            以下の内容が送信されました。<br>
            ------------------------------------------------------------------- <br>
            """ + content + """ <br>
            ------------------------------------------------------------------- <br>
        </p>
        
        <p>
            以上です。<br>
            よろしくお願いします。
        </p>
        """
    return str(mail)
