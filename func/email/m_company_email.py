from func.base_model import Base
from func import db, db_handle

class M_Company_Email(Base):
    __tablename__ = 'company_emails'

    email    = db.Column(db.String, nullable=True)
    password = db.Column(db.String, nullable=True)
    is_used  = db.Column(db.Integer, nullable=True)
    host     = db.Column(db.String, nullable=True)
    port     = db.Column(db.Integer, nullable=True)

    def __init__(self):
        super(M_Company_Email, self).__init__()
