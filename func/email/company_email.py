from func import db, table_name, db_handle
from datetime import datetime as dt
from func import config_const
import re
import hashlib, pytz
from sys import exit

class CompanyEmail:
    def get_company_email(self):
        self.check_company_email()
        sql = db_handle.SELECT + table_name.COMPANY_EMAIL + ' where is_used = 0 ' \
                                                            ' order by id ASC limit 1 '

        row = db_handle.executeCommand(sql)
        update_all_email = db_handle.UPDATE + table_name.COMPANY_EMAIL + ' SET is_used = 1 where id = %s' % row['id']
        db_handle.excuteCommandChangeRecord(update_all_email)
        return row

    def check_company_email(self):
        select_unused_email = db_handle.SELECT + table_name.COMPANY_EMAIL + ' where is_used = 0 '
        result = db_handle.executeCommand(select_unused_email)
        # no email
        if not result:
            update_email = db_handle.UPDATE + table_name.COMPANY_EMAIL + ' SET is_used = 0 where 1 '
            db_handle.excuteCommandChangeRecord(update_email)

        return ''