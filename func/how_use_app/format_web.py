#!/usr/bin/env python
# -*- coding: utf-8 -*-
class FormatWeb:

    @staticmethod
    def get_content_how_use_app():
        format_web = """
        <!doctype html>
        <html lang="ja">
            <head>
                <meta charset="utf-8">
                <title>How to use app</title>
            </head>
            <body>
			<p><span style="font-family: 'ＭＳ 明朝';">・アカウント登録について</span></p>
			<p><span style="font-family: 'ＭＳ 明朝';">　起動画面にて「新規登録」ボタンをクリックすると登録画面が表示します。</span></p>
<center><img src="https://s3-ap-northeast-1.amazonaws.com/pointsolution.dev/img_how_use_app/1.png" style="width:120px;height:200px;"></center>
			<p><span style="font-family: 'ＭＳ 明朝';">　各項目欄に入力し、年齢と性別を選択してから「登録」ボタンをクリックすると会員登録が完了します。</span></p>
<center><img src="https://s3-ap-northeast-1.amazonaws.com/pointsolution.dev/img_how_use_app/2.png" style="width:120px;height:200px;"></center>
			<p><span style="font-family: 'ＭＳ 明朝';">・パスワード変更について</span></p>
			<p><span style="font-family: 'ＭＳ 明朝';">　右上のメニューから「パスワード変更」メニューを選択してらパスワード変更画面が表示します。</span></p>
<center><img src="https://s3-ap-northeast-1.amazonaws.com/pointsolution.dev/img_how_use_app/3.png" style="width:120px;height:200px;"></center>
			<p><span style="font-family: 'ＭＳ 明朝';">　現パスワードと新パスワードを入力して「パスワード変更」ボタンを押下したらパスワード変更します。</span></p>
			<p><span style="font-family: 'ＭＳ 明朝';">・サービス利用について</span></p>
			<p><span style="font-family: 'ＭＳ 明朝';">　アプリのホーム画面から店</span>QR<span style="font-family: 'ＭＳ 明朝';">コードを読み込んで表示されたポップアップの「チェックイン」ボタンをタッチすると店にサービス利用するチェックインを行います。</span></p>
<center><img src="https://s3-ap-northeast-1.amazonaws.com/pointsolution.dev/img_how_use_app/4.png" style="width:120px;height:200px;"></center>
			<p><span style="font-family: 'ＭＳ 明朝';">　チェックイン後待ち中画面によりお客様の予約番号、お待ち人数、お呼出までの目安時間などが表示されます。</span></p>
<center><img src="https://s3-ap-northeast-1.amazonaws.com/pointsolution.dev/img_how_use_app/5.png" style="width:120px;height:200px;"></center>
			<p><span style="font-family: 'ＭＳ 明朝';">この画面でのアニメションはお店のスタッフに呼んでくれるまで動きます。</span></p>
			<p><span style="font-family: 'ＭＳ 明朝';">・自分の所有するスタンプカードの確認</span></p>
			<p><span style="font-family: 'ＭＳ 明朝';">　アプリ画面の左下にある「マイスタンプ」メニューをタッチすると獲得したサービススタンプが閲覧される画面に遷移します。この画面によりカードの説明とスタンプの履歴も記載されています。</span></p>
<center><img src="https://s3-ap-northeast-1.amazonaws.com/pointsolution.dev/img_how_use_app/6.png" style="width:120px;height:200px;"></center>
			<p><span style="font-family: 'ＭＳ 明朝';">スタップに記載される評価ボタンをタッチするとサービス画面に移動します。</span></p>
<center><img src="https://s3-ap-northeast-1.amazonaws.com/pointsolution.dev/img_how_use_app/7.png" style="width:120px;height:200px;"></center>
			<p><span style="font-family: 'ＭＳ 明朝';">星を選択したりコメントを記入したりできます。評価するボタンを押下して評価内容を該当の店舗へ共通します。</span></p>
			<p><span style="font-family: 'ＭＳ 明朝';">・要望記入について</span></p>
			<p><span style="font-family: 'ＭＳ 明朝';">マイスタンプ画面に「店舗の状況確認」ボタンを押すと店舗状態確認画面へ遷移します。</span></p>
<center><img src="https://s3-ap-northeast-1.amazonaws.com/pointsolution.dev/img_how_use_app/8.png" style="width:120px;height:200px;"></center>
			<p><span style="font-family: 'ＭＳ 明朝';">この画面によりスタンプを貰ったお店とそのお店の席状況を確認できます。</span></p>
			<p><span style="font-family: 'ＭＳ 明朝';">・要望記入について</span></p>
			<p><span style="font-family: 'ＭＳ 明朝';">右下の「要望」メニューを押すと該当サービスの各店舗へサービスに関する要望を伝えられます。</span></p>
<center><img src="https://s3-ap-northeast-1.amazonaws.com/pointsolution.dev/img_how_use_app/9.png" style="width:120px;height:200px;"></center>
			<p><span style="font-family: 'ＭＳ 明朝';">サービスに応じて適性な要望フォームが表示します。</span></p>
			<p><span style="font-family: 'ＭＳ 明朝';">内容を選択したり記入したりしてから「保存」ボタンを押下すると該当のお店へ自動連携します。</span></p>
            </body>
        </html>
        """
        return format_web