from func.base_model import Base
from func import db, db_handle

class M_Config_App_Store(Base):
    __tablename__ = 'config_app_stores'

    timeout = db.Column(db.Time, nullable=True)
    prefix  = db.Column(db.String(300), nullable=True)

    def __init__(self):
        super(M_Config_App_Store, self).__init__()

    def __repr__(self):
        return '<M_Config_App_Store %r>' % (self.id)