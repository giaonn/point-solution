from func.base_model import Base
from func import db, db_handle

class M_Version(Base):

    __tablename__ = 'versions'

    version_code    = db.Column(db.Integer, nullable=True)
    version_name    = db.Column(db.String, nullable=True)
    is_required     = db.Column(db.Integer, nullable=True)
    is_send_notify  = db.Column(db.Integer, nullable=True)
    change_log      = db.Column(db.Text, nullable=True)
    warning         = db.Column(db.Text, nullable=True)
    type            = db.Column(db.Integer, nullable=True)
    os_type            = db.Column(db.Integer, nullable=True)

    def __init__(self):
        super(M_Version, self).__init__()
