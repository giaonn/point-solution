from func.base_model import Base
from func import db, db_handle

class M_Encrypt_Store(Base):
    __tablename__ = 'encrypt_stores'

    store_id   = db.Column(db.Integer, nullable=True)
    code_store = db.Column(db.String, nullable=True)

    def __init__(self):
        super(M_Encrypt_Store, self).__init__()
