from func import db, table_name, db_handle
from datetime import datetime as dt
from func import config_const
import re
import hashlib, pytz

class Company:
    #get-list-company-by-user-id

    def cache_get_list_company_by(self):
        #get store actully
        sql = ' select S.service_company_id, S.name, S.logo ' \
              ' from stores S where id in (select store_id from management_users where app_id =3 ) ' \

        return db_handle.executeCommandMoreRow(sql)

    def get_list_company_new(self, user_id):
        sql = ' select S.service_company_id, S.name, S.logo ' \
              ' from stores S where id in (select store_id from info_of_user_from_stores where user_id =' + str(user_id) + ' ) '
        return db_handle.executeCommandMoreRow(sql)

    def get_list_company_by_user_id(self, user_id):
        sql = ' select SC.id as service_company_id, SC.card_name, SC.card_type, SC.company_id, CP.name, SC.logo, SC.service_id,' \
              ' S.name as service_name, sum(case when status =\'can_use\' then 1 else 0 end) as \'available_card\' ' \
              ' from ((select distinct(service_company_id) from stores S join info_of_user_from_stores IU ' \
              ' on S.id = IU.store_id where IU.user_id = ' + str(user_id) + ') as T1) ' \
              ' left join cards C on T1.service_company_id = C.service_company_id ' \
              ' join services_companies SC on SC.id = T1.service_company_id '\
              ' join companies CP on SC.company_id = CP.id ' \
              ' join services S on S.id = SC.service_id ' \
              ' where C.user_id = ' + str(user_id) + ' '\
              ' group by SC.id'

        return db_handle.executeCommandMoreRow(sql)

    def get_one_company_by_user_id(self, user_id):
        sql = ' select CP.logo ' \
              ' from ((select distinct(service_company_id) from stores S join info_of_user_from_stores IU ' \
              ' on S.id = IU.store_id where IU.user_id = ' + str(user_id) + ') as T1) ' \
              ' join cards C on T1.service_company_id = C.service_company_id ' \
              ' join services_companies SC on SC.id = T1.service_company_id '\
              ' join companies CP on SC.company_id = CP.id ' \
              ' join services S on S.id = SC.service_id where C.user_id = ' + str(user_id) + ' ' \
              ' group by SC.id limit 1'

        return db_handle.executeCommand(sql)

    def get_company_by_id(self, id):
        sql = db_handle.SELECT + table_name.COMPANY + ' where id = ' + str(id)
        return  db_handle.executeCommand(sql)