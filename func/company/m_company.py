from func.base_model import Base
from func import db, db_handle

class M_Company(Base):
    __tablename__ = 'companies'

    name                            = db.Column(db.String(255), nullable=True)
    email                           = db.Column(db.String(255), nullable=True)
    address                         = db.Column(db.String(200), nullable=True)
    phone                           = db.Column(db.String(100), nullable=True)
    information                     = db.Column(db.Text, nullable=True)
    logo                            = db.Column(db.String(300), nullable=True)
    current_number_of_notifications = db.Column(db.Integer, nullable=True)
    max_number_of_notifications     = db.Column(db.Integer, nullable=True)
    current_number_of_stores        = db.Column(db.Integer, nullable=True)
    max_number_of_stores            = db.Column(db.Integer, nullable=True)

    def __init__(self):
        super(M_Company, self).__init__()

    def __repr__(self):
        return '<M_Company %r>' % (self.id)