from func.base_model import Base
from func import db, db_handle

class M_Server_Information(Base):

    __tablename__ = 'server_informations'

    status = db.Column(db.String, nullable=True)
    is_maintain = db.Column(db.Integer, nullable=True)
    message = db.Column(db.Text, nullable=True)
    
    def __init__(self):
        super(M_Server_Information, self).__init__()
