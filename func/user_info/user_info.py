from func import db, table_name, db_handle
from datetime import datetime as dt
from func import config_const
import re
import hashlib, pytz

class UserInfo:
    def get_list_store_id(self, user_id):
        sql = 'select DISTINCT(store_id) from ' + table_name.USER_INFO + ' where user_id = ' + str(user_id)
        return db_handle.executeCommandMoreRow(sql)

    def get_user_info_by_store_id_and_user_id(self, store_id, user_id):
        sql = db_handle.SELECT + table_name.USER_INFO + ' where user_id = ' + str(user_id) + ' AND store_id = ' + str(store_id)
        return db_handle.executeCommand(sql)

    def get_user_from_list_store_id(self, list_store_str):
        sql = 'select distinct(user_id) from '+ table_name.USER_INFO + ' where store_id in (' + str(list_store_str) + ') '
        return db_handle.executeCommandMoreRow(sql)

    def create_user_info_by_store_id_and_user_id(self, store_id, user_id):
        created = modified = db_handle.get_full_current_jav_time()

        sql = db_handle.INSERT + table_name.USER_INFO + ' (store_id, user_id, check_in_amount, created, modified) ' \
                                                        ' values (%s , %s, %s, %s , %s)'
        params = (store_id, user_id, 0, created, modified)

        return db_handle.excuteCommandChangeRecordParams(sql, params)

    def create_user_info_by_store_id_and_user_id_contain_check_in_time(self, app_id, store_id, user_id):
        created  = db_handle.get_full_current_jav_time()
        modified = db_handle.subtract_5_year()

        sql = db_handle.INSERT + table_name.USER_INFO + ' (app_id, store_id, user_id, check_in_amount, last_check_in_time, created, modified) ' \
                                                        ' values (%s, %s , %s, %s , %s, %s , %s)'
        params = (app_id, store_id, user_id, 0, created, created, modified)

        return db_handle.excuteCommandChangeRecordParams(sql, params)

    def update_user_info_by_store_id_and_user_id(self, store_id, user_id, management_user_id, infomation_json):

        modified        = "'"  + db_handle.get_full_current_jav_time() + "'"
        infomation_json = "'" + infomation_json + "'"

        sql = db_handle.UPDATE + table_name.USER_INFO + ' SET information_json = ' \
              + str(infomation_json) + ', modified = '  + modified + ' , management_user_id = ' + str(management_user_id)\
              + ' where user_id = ' + str(user_id) + ' and store_id = ' + str(store_id)

        return db_handle.excuteCommandChangeRecord(sql)

    def update_user_info(self, store_id, user_id, check_in_amount):
        modified = "'" + db_handle.get_full_current_jav_time() + "'"

        sql = db_handle.UPDATE + table_name.USER_INFO + ' SET check_in_amount = ' + str(check_in_amount) + '' \
                    ', modified = ' + modified  + ', last_check_in_time = ' + modified + ' where store_id = ' + str(store_id) + ' and  user_id = ' + str(user_id)

        return db_handle.excuteCommandChangeRecord(sql)

    def update_store_user_memo(self, store_id, user_id, config_json, data_json):
        modified = "'" + db_handle.get_full_current_jav_time() + "'"
        sql = db_handle.UPDATE + table_name.USER_INFO + ' SET config_json = \'' + str(config_json).replace('\\','\\\\') + '\' ' \
            + ' , data_json = \'' + str(data_json).replace('\\','\\\\') + '\' , modified = ' + modified + ' WHERE ' \
            + ' store_id = ' + str(store_id) + ' AND user_id = ' + str(user_id)
        return db_handle.excuteCommandChangeRecord(sql)

    def update_store_user_memo_data(self, store_id, user_id, data_json):
        modified = "'" + db_handle.get_full_current_jav_time() + "'"
        sql = db_handle.UPDATE + table_name.USER_INFO + ' SET ' \
            + ' data_json = \'' + str(data_json).replace('\\','\\\\') + '\' , modified = ' + modified + ' WHERE ' \
            + ' store_id = ' + str(store_id) + ' AND user_id = ' + str(user_id)
        return db_handle.excuteCommandChangeRecord(sql)

