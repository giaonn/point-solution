from func.base_model import Base
from func import db, db_handle
import uuid

class M_User_Info(db.Model):

    __tablename__ = 'info_of_user_from_stores'

    store_id           = db.Column(db.Integer, nullable=True, primary_key=True)
    user_id            = db.Column(db.Integer, nullable=True, primary_key=True)
    app_id             = db.Column(db.Integer, nullable=True)
    information_json   = db.Column(db.Text, nullable=True)
    management_user_id = db.Column(db.Integer, nullable=True)
    data_json          = db.Column(db.Text, nullable=True)
    last_message       = db.Column(db.Text, nullable=True)
    config_json        = db.Column(db.Text, nullable=True)
    check_in_amount    = db.Column(db.Integer, nullable=True)
    last_check_in_time = db.Column(db.DateTime, nullable=True)
    key_gen            = db.Column('key_gen', db.String(256))
    created            = db.Column(db.DateTime, default=db_handle.get_full_current_jav_time())
    last_message_time  = db.Column(db.BigInteger)
    modified           = db.Column(db.DateTime, default=db_handle.get_full_current_jav_time()
                         , onupdate=db_handle.get_full_current_jav_time())

    def __init__(self):
        self.key_gen = uuid.uuid4()




