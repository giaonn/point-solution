from func.base_model import Base
from func import db, db_handle

class M_Token(Base):

    __tablename__ = 'user_auth_session'

    user_id            = db.Column(db.Integer, nullable=True)
    management_user_id = db.Column(db.Integer, nullable=True)
    auth_token         = db.Column(db.String, nullable=True)
    expire             = db.Column(db.Integer, nullable=True)

    def __init__(self):
        super(M_Token, self).__init__()




