from func import db, table_name, db_handle
from datetime import datetime as dt
from func import config_const
import re
import hashlib, pytz

class Token:
    def createTokenUser(self, user_id, token, expire):
        created = db_handle.get_full_current_jav_time()

        sql = db_handle.INSERT + table_name.USER_AUTH_SESSION + db_handle.LIST_USER_TOKEN_ATTRIBUTE + ' values ' \
                  '(%s , %s, %s, %s)'

        params = (user_id, token, created, expire)

        try:
            db.engine.execute(sql, params)
            return True
        except Exception, e:
            return False

    def createTokenManagerUser(self, user_id, token, expire):
        created = db_handle.get_full_current_jav_time()

        sql = db_handle.INSERT + table_name.USER_AUTH_SESSION + db_handle.LIST_MANAGER_TOKEN_ATTRIBUTE + ' values ' \
                  '(%s , %s, %s, %s)'

        params = (user_id, token, created, expire)

        try:
            db.engine.execute(sql, params)
            return True
        except Exception, e:
            return False

    def getLatestTokenUser(self, user_id):
        created = "'" + db_handle.get_date_current_jav_time() + "'"

        sql = ' select * from ' + table_name.USER_AUTH_SESSION + ' where user_id = ' + str(user_id) + ' ' \
         ' and DATE(created) = ' + created + ' order by created desc limit 1'

        return db_handle.executeCommand(sql)

    def getLatestTokenManagerUser(self, user_id):
        created = "'" + db_handle.get_date_current_jav_time() + "'"

        sql = ' select * from ' + table_name.USER_AUTH_SESSION + ' where management_user_id = ' + str(user_id) + ' ' \
         ' and DATE(created) = ' + created + ' order by created desc limit 1'

        return db_handle.executeCommand(sql)

    def getTokenByToken(self, token):
        token = "'" + token + "'"

        sql = ' select * from ' + table_name.USER_AUTH_SESSION + ' where auth_token = ' + str(token) + ' '
        return db_handle.executeCommand(sql)

    def deleteToken(self, token):
        token = "'" + token + "'"
        sql = db_handle.DELETE + table_name.USER_AUTH_SESSION + ' where auth_token = ' + str(token) + ' '
        return db_handle.excuteCommandChangeRecord(sql)

    def delete_all_token_by_user_id(self, user_id):
        user_id = "'" + user_id + "'"
        sql = db_handle.DELETE + table_name.USER_AUTH_SESSION + ' WHERE user_id = ' + str(user_id) + ' '
        return ''
