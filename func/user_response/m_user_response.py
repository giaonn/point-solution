from func.base_model import Base
from func import db, db_handle

class M_User_Response(Base):
    __tablename__ = 'use_responses'

    user_id           = db.Column(db.Integer, nullable=True)
    question_naire_id = db.Column(db.Integer, nullable=True)
    content           = db.Column(db.Text, nullable=True)

    def __init__(self):
        super(M_User_Response, self).__init__()


