from func.base_model import Base
from func import db, db_handle

class M_User_Push_Notifi(Base):

    __tablename__ = 'user_push_notifications'

    user_id              = db.Column(db.Integer, nullable=True)
    push_notification_id = db.Column(db.Integer, nullable=True)
    stamp_id             = db.Column(db.Integer, nullable=True, default=0)
    status               = db.Column(db.String, nullable=True, default=0)
    receive_time         = db.Column(db.DateTime, nullable=True)

    def __init__(self):
        super(M_User_Push_Notifi, self).__init__()




