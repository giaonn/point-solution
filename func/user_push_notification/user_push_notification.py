from func import db, table_name, db_handle
from datetime import datetime as dt
from func import config_const
import re
import hashlib, pytz

class User_Push_Notifi:
    def create_new_user_push(self, user_id, push_notification_id):
        sql = db_handle.INSERT + table_name.USER_PUSH_NOTIFI + ' (user_id, push_notification_id) ' + ' values (?, ?) '

        params = (user_id, push_notification_id)

        return db_handle.excuteCommandChangeRecordParams(sql, params)


    def dbCommit(self):

        return db_handle.excuteCommitManyCommandChangeRecordParams()


    def update_push_read(self, user_id, list_push_notification, type_read):

        sql = db_handle.UPDATE + table_name.USER_PUSH_NOTIFI + ' set status =  '+ str(type_read) + ' where user_id = '+\
              str(user_id) + ' and push_notification_id in (' + str(list_push_notification) + ')'

        return db_handle.excuteCommandChangeRecord(sql)


