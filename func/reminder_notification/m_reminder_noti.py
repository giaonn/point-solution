from func.base_model import Base
from func import db, db_handle

class M_Reminder_Noti(Base):

    __tablename__ = 'reminder_notifications'

    service_company_id   = db.Column(db.Integer, nullable=True)
    store_id             = db.Column(db.Integer, nullable=True)
    user_id              = db.Column(db.Integer, nullable=True)
    stamp_id             = db.Column(db.Integer, nullable=True)
    ask_to_review_status = db.Column(db.Integer, nullable=True)
    reminder_status      = db.Column(db.Integer, nullable=True)

    def __init__(self):
        super(M_Reminder_Noti, self).__init__()




