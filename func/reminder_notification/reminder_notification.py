from func import db, table_name, db_handle
from datetime import datetime as dt
from func import config_const
import re
import hashlib, pytz

class ReminderNotification:
    def create_reminder_notification(self, service_company_id, store_id, user_id, stamp_id, ask_to_review_status, reminder_status):
        created = db_handle.get_full_current_jav_time()
        sql = db_handle.INSERT + table_name.REMINDER_NOTIFI + db_handle.LIST_REMIND_NOTIFI_ATTRIBUTE + ' values ' \
            '(%s, %s, %s, %s, %s, %s, %s)'
        params = (service_company_id, store_id, user_id, stamp_id, ask_to_review_status, reminder_status, created)

        try:
            db.engine.execute(sql, params)
            return True
        except Exception, e:
            return False

    def get_last_reminder_notification_by_stamp_id(self, stamp_id):
        sql = db_handle.SELECT + table_name.REMINDER_NOTIFI + ' WHERE stamp_id = ' + stamp_id + \
              ' ORDER BY created DESC LIMIT 1'
        return db_handle.executeCommand(sql)

    def set_reviewed_status_by_id(self, id):
        sql = db_handle.UPDATE + table_name.REMINDER_NOTIFI + ' SET ask_to_review_status = 1 ' + \
              ' WHERE id = ' + str(id)
        return db_handle.excuteCommandChangeRecord(sql)
