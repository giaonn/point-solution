from func import db, table_name, db_handle
from datetime import datetime as dt
from func import config_const
import re
import hashlib

class Store_Push_Notifi:
    def create_new_store_push(self, store_id, push_notification_id):
        sql = db_handle.INSERT + table_name.STORE_PUSH_NOTIFI + ' (store_id, push_notification_id) ' + ' values (%s, %s) '

        params = (store_id, push_notification_id)

        return db_handle.excuteCommandChangeRecordParams(sql, params)