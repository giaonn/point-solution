from func.base_model import Base
from func import db, db_handle, json_utils

class M_Store_Push_Notifi(db.Model):

    __tablename__ = 'stores_push_notifications'

    store_id             = db.Column(db.Integer, primary_key=True)
    push_notification_id = db.Column(db.Integer, primary_key=True)

    created = db.Column(db.DateTime, default=db_handle.get_full_current_jav_time())
    modified = db.Column(db.DateTime, default=db_handle.get_full_current_jav_time()
                         , onupdate=db_handle.get_full_current_jav_time())

    def __init__(self):
        pass

    def to_dict(self, exclude = None):
        return json_utils.model2dict(self, self.__class__, exclude=exclude)

    def load_json(self, json):
        return json_utils.load_json(self, self.__class__, json)


