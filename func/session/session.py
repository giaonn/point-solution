from func import db, table_name, db_handle
from datetime import datetime as dt
from func import config_const
import re
import hashlib, pytz

class Session:

    def insertSession(self, store_id, user_id, user_name, check_in_code, expire, status, numerical_session):
        created = modified = db_handle.get_full_current_jav_time()

        sql = db_handle.INSERT + table_name.SESSION + db_handle.LIST_SESSION_ATTRIBUTE + ' values ' \
             '(%s, %s, %s, %s, %s, %s, %s, %s, %s)'

        params = (store_id, user_id, user_name, check_in_code, expire, status, numerical_session, created, modified)

        try:
            db.engine.execute(sql, params)
            return True
        except:
            return False

    def insert_session_with_key(self, store_id, user_id, user_name, check_in_code, expire, status, numerical_session, key_gen):
        created = modified = db_handle.get_full_current_jav_time()

        sql = db_handle.INSERT + table_name.SESSION + db_handle.LIST_SESSION_ATTRIBUTE_KEY + ' values ' \
             '(%s, %s, %s, %s, %s, %s, %s, %s, %s, %s)'

        params = (store_id, user_id, user_name, check_in_code, expire, status, numerical_session, key_gen, created, modified)

        try:
            db.engine.execute(sql, params)
            return True
        except Exception,e:
            return False

    def get_session_by_id(self, session_id):

        sql = db_handle.SELECT + table_name.SESSION + ' where id = ' + str(session_id) + ' '
        return db_handle.executeCommand(sql)

    def get_session_by_key(self, key_gen):
        key_gen = "'" + key_gen + "'"
        sql = db_handle.SELECT + table_name.SESSION + ' where key_gen = ' + str(key_gen) + ' '
        return db_handle.executeCommand(sql)

    def getSessionLatest(self, store_id):

        created = "'" + db_handle.get_date_current_jav_time() + "'"
        sql = ' select * from ' + table_name.SESSION + ' where store_id = ' + str(store_id) + ' ' \
                    ' and DATE(created) = ' + created + ' order by created desc limit 1'

        return db_handle.executeCommand(sql)

    def get_session_latest_by_store_id_and_user_id(self, store_id, user_id):

        created = "'" + db_handle.get_date_current_jav_time() + "'"
        sql = db_handle.SELECT + table_name.SESSION + ' where store_id = ' + str(store_id) + ' ' \
                    ' AND user_id = ' + str(user_id) + ' ' \
                    ' and DATE(created) = ' + created + ' order by created desc limit 1'

        return db_handle.executeCommand(sql)

    def getSessionByUser(self, user_id):

        sql = ' select * from ' + table_name.SESSION  + ' where user_id = ' + str(user_id) + ' ' \
                     ' limit 1'

        return db_handle.executeCommand(sql)

    def getSessionByUserForFeedback(self, user_id):

        created = "'" + db_handle.get_date_current_jav_time() + "'"
        sql = ' select * from ' + table_name.SESSION  + ' where user_id = ' + str(user_id) + ' '\
                    ' and DATE(created) = ' + created + ' order by created desc limit 1'

        return db_handle.executeCommand(sql)


    def get_number_people(self, store_id, status):
        created = db_handle.get_date_current_jav_time()
        created = "'" +created + "'"

        status  = "'" + status + "'"

        sql = ' select count(id) as num from ' + table_name.SESSION + ' where store_id = ' + str(store_id) \
              + ' and status = ' + str(status) + '' + ' and DATE(created) = ' + created

        res = db.engine.execute(sql)

        for row in res:
            return row['num']

        return 0

    def count_wait_confirm_session(self, store_id, status):
        status = "'" + status + "'"

        sql = ' select count(id) as num from ' + table_name.SESSION + ' where store_id = ' + str(store_id) \
              + ' and status = ' + str(status) + ''

        res = db.engine.execute(sql)

        for row in res:
            return row['num']

        return 0

    def get_list_session_by_store_and_status(self, store_id, status):
        status = "'" + status + "'"
        sql = ' select * from ' + table_name.SESSION + ' where store_id = ' + str(store_id) \
              + ' and status = ' + str(status)

        return db_handle.executeCommandMoreRow(sql)

    def get_list_session_by_store(self, store_id):
        sql = ' select * from ' + table_name.SESSION + ' where store_id = ' + str(store_id)

        return db_handle.executeCommandMoreRow(sql)


    def update_staff_of_session(self, staff, session_id):
        modified = "'" + db_handle.get_full_current_jav_time() + "'"
        sql = db_handle.UPDATE + table_name.SESSION + ' set staff = ' + str(staff) + ', modified = ' + modified +' ' \
                    ' where id = ' + str(session_id)

        return db_handle.excuteCommandChangeRecord(sql)

    def update_session_by_manager_user(self, store_id, seat_number, status, session_id):
        modified = "'" + db_handle.get_full_current_jav_time() + "'"
        status = "'" + status + "'"

        sql = db_handle.UPDATE + table_name.SESSION + ' set store_id = ' + str(store_id) + ', modified = ' \
                    + modified + ', seat_number = ' +  str(seat_number) + ', status = ' + str(status) \
                    + ' where id = ' + str(session_id)

        return db_handle.excuteCommandChangeRecord(sql)

    def get_list_seat_number(self, store_id):

        sql = 'select seat_number, staff from ' + table_name.SESSION + ' where store_id = ' + str(store_id) + ' '

        return db_handle.executeCommandMoreRow(sql)

    def delete_session(self, id):
        sql = db_handle.DELETE + table_name.SESSION + ' WHERE id = ' + str(id)
        return db_handle.excuteCommandChangeRecord(sql)

