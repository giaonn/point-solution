from func.base_model import Base
from func import db, db_handle

class M_Session(Base):

    __tablename__ = 'sessions'

    store_id          = db.Column(db.Integer, nullable=True)
    user_id           = db.Column(db.Integer, nullable=True)
    user_name         = db.Column(db.String, nullable=True)
    check_in_code     = db.Column(db.String, nullable=True)
    seat_number       = db.Column(db.Integer, nullable=True)
    expire            = db.Column(db.DateTime, nullable=True)
    status            = db.Column(db.String, nullable=True)
    numerical_session = db.Column(db.Integer, nullable=True)
    staff             = db.Column(db.Integer, nullable=True)
    check_in_time     = db.Column(db.DateTime, nullable=True)

    def __init__(self):
        super(M_Session, self).__init__()




