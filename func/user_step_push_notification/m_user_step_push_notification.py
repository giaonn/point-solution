from func.base_model import Base
from func import db, db_handle

class M_User_Step_Push_Notifi(Base):

    __tablename__ = 'user_step_push_notifications'

    user_id              = db.Column(db.Integer, nullable=True)
    push_notification_id = db.Column(db.Integer, nullable=True)
    send_time            = db.Column(db.DateTime, nullable=True)

    def __init__(self):
        super(M_User_Step_Push_Notifi, self).__init__()




