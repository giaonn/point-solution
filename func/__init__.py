from flask import Flask, Blueprint, abort, jsonify, request
from flask_sqlalchemy import SQLAlchemy
from email.email_handle import EmailHandle
import uuid
from flask_redis import FlaskRedis


app = Flask(__name__)

app.config.from_object('func.config')

redis_store = FlaskRedis(app)
db = SQLAlchemy(app)
count = 0

@app.route('/test-set-redis', methods=['GET'])
def set_redis():
    try:
        print 'set'
        redis_store.set('key1','hihi')
    except Exception,e:
        print e

    return jsonify({'message': 'success'})

@app.route('/test-get-redis', methods=['GET'])
def get_redis():
    try:
        print 'get'
        print redis_store.get("key1")
    except Exception,e:
        print e

    return jsonify({'message': 'success'})

@app.route('/flushall', methods=['GET'])
def flushall_list_push():
    redis_store.delete('list_push')
    return jsonify({'message': 'success'})

@app.route('/delete-cache', methods=['GET'])
def flushall():
    redis_store.flushall()
    return jsonify({'message': 'success'})

@app.route('/')
def hello_world():
    name = "'" + str(uuid.uuid4()) + "'"
    # sql = 'insert into companies(name) values( '+name+' )'
    #
    # db.engine.execute(sql)

    return 'Import company done!'

@app.route('/testJson', methods=['POST'])
def getApi():
    req_json = request.json if request.json else {}

    name       = req_json.get('name', 'noname')
    age        = req_json.get('age', 'unborn')
    hair_color = req_json.get('hair_color', 'nohair')

    result = [1]
    dict = {"name" : name}

    return jsonify(dict)



#Load Modules
from func.user_app.controller import mod_user
from func.store_app.controller import mod_store

app.register_blueprint(mod_user)
app.register_blueprint(mod_store)
