from func.base_model import Base
from func import db, db_handle

class M_Stamp(Base):

    __tablename__ = 'stamps'

    card_id            = db.Column(db.Integer, nullable=True)
    store_id           = db.Column(db.Integer, nullable=True)
    management_user_id = db.Column(db.Integer, nullable=True)
    number_in_card     = db.Column(db.Integer, nullable=True)
    point              = db.Column(db.Integer, nullable=True)
    store_name         = db.Column(db.String, nullable=True)
    star_review        = db.Column(db.Float, nullable=True)
    text_review        = db.Column(db.String, nullable=True)
    review_at          = db.Column(db.DateTime, nullable=True)

    def __init__(self):
        super(M_Stamp, self).__init__()




