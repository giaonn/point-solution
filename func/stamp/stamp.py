from func import db, table_name, db_handle
from datetime import datetime as dt
from func import config_const
import re
import hashlib, pytz

class Stamp:
    def getStamp(self, card_id):
        sql = 'select * from ' + table_name.STAMP + ' where card_id = ' + str(card_id) + ' '
        return db_handle.executeCommandMoreRow(sql)

    def stampExists(self, stamp_id):
        sql = 'select * from ' + table_name.STAMP + ' where id = ' + str(stamp_id) + ' limit 1'
        return db_handle.executeCommand(sql)

    def updateStamp(self, stamp_id, star_review, text_review):
        text_review = "'" + text_review + "'"
        review_at = "'" + db_handle.get_full_current_jav_time() + "'"
        sql = db_handle.UPDATE + table_name.STAMP + ' set star_review = ' + str(star_review) + ' ' \
            ' , text_review = ' + str(text_review) + ', review_at = ' + review_at +' where id = ' + str(stamp_id)

        return db_handle.excuteCommandChangeRecord(sql)

    def createStamp(self, card_id, store_id, management_user_id, store_name, star_review, text_review ):
        created = modified = db_handle.get_full_current_jav_time()

        sql = db_handle.INSERT + table_name.STAMP + db_handle.LIST_STAMP_ATTRIBUTE + ' values ' \
                  '(%s , %s, %s , %s, %s , %s, %s , %s)'

        params = (card_id, store_id, management_user_id, store_name, star_review, text_review, created, modified)

        return db_handle.excuteCommandChangeRecordParams(sql, params)

    def update_number_in_card(self, id, number_in_card):
        modified = "'" + db_handle.get_full_current_jav_time() + "'"

        sql = db_handle.UPDATE + table_name.STAMP + ' set number_in_card = ' + str(number_in_card) + \
                         ' , modified = ' + modified + ' where id = ' + str(id)
        return db_handle.excuteCommandChangeRecord(sql)

    def get_latest_stamp(self, card_id, store_id, management_user_id):
        sql = db_handle.SELECT + table_name.STAMP + ' where card_id = ' + str(card_id) + ' and store_id = ' + str(store_id) +'' \
                ' and management_user_id = ' + str(management_user_id) + ' order by created desc limit 1'

        return db_handle.executeCommand(sql)


