from func import db, table_name, db_handle
from datetime import datetime as dt
from func import config_const
import re
import hashlib, pytz

class User:

    def registerUser(self, username = None, password = None, name = None, email = None, age_avg = None, sex = None, app_id = None):
        created = modified = db_handle.get_full_current_jav_time()

        sql = db_handle.INSERT + table_name.USER + db_handle.LIST_USER_ATTRIBUTE + ' values ' \
              '(%s , %s, %s , %s, %s , %s, %s , %s, %s)'

        params = (username, password, name, email, age_avg, sex, created, modified, app_id)

        return db_handle.excuteCommandChangeRecordParams(sql, params)

    def checkUserInfo(self, username = None, password = None, name = None, email = None):
        if not username or not password or not name or not email:
            return config_const.INFO_NULL

        if not re.match('^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$', email):
            return config_const.FORMAT_EMAIL_WRONG

        if self.checkUserExists(username):
            return config_const.USER_EXISTS

        if self.checkEmailExist(email):
            return config_const.EMAIL_EXIST

    def check_mail_valid(self, email, user_id):
        if not re.match('^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$', email):
            return config_const.FORMAT_EMAIL_WRONG
        if self.check_mail_exists_case_update(email, user_id):
            return config_const.EMAIL_EXIST

    def checkUserExists(self, username):
        username = "'" + username + "'"
        sql = 'select * from ' + table_name.USER + ' where username = ' + username + ' AND is_deleted = 0'

        result = db.engine.execute(sql)

        count = 0

        for row in result:
            count+=1
        if count>0:
            return True
        return False

    def check_mail_exists_case_update(self, email, user_id):
        email = "'" + email + "'"
        sql = 'select * from ' + table_name.USER + ' where email = ' + email + ' AND is_deleted = 0 and id != ' + str(user_id)

        result = db.engine.execute(sql)

        count = 0

        for row in result:
            count += 1
        if count > 0:
            return True
        return False

    def checkEmailExist(self, email):
        email = "'" + email + "'"
        sql = 'select * from ' + table_name.USER + ' where email = ' + email + ' AND is_deleted = 0'

        result = db.engine.execute(sql)

        count = 0

        for row in result:
            count+=1
        if count>0:
            return True
        return False

    def loginSuccess(self, username, password):
        username = "'" + username + "'"
        password = "'" + password + "'"

        sql = 'select * from ' + table_name.USER + ' where username = ' + username +\
              ' and password =' + password + ' AND is_deleted = 0'

        return db_handle.executeCommand(sql)

    def getUser(self, user_id):
        sql = 'SELECT * FROM ' + table_name.USER + ' WHERE id = ' + str(user_id) + ' AND is_deleted = 0'
        return db_handle.executeCommand(sql)

    def get_user_by_username(self, username):
        sql = 'SELECT * FROM ' + table_name.USER + ' WHERE username = \'' + str(username) + '\' AND is_deleted = 0 LIMIT 1'
        return db_handle.executeCommand(sql)

    def get_user_by_email(self, email):
        sql = 'SELECT * FROM ' + table_name.USER + ' WHERE email = \'' + str(email) + '\' AND is_deleted = 0 LIMIT 1'
        return db_handle.executeCommand(sql)

    def check_password_user(self, id, password):
        password = self.encodePass(password)
        sql = 'SELECT id FROM ' + table_name.USER + ' WHERE password = \'' + str(password) + '\' AND id = ' + str(id) + ' AND is_deleted = 0 LIMIT 1'
        return db_handle.executeCommand(sql)

    def set_reset_password_code_user(self, id, code):
        sql = 'UPDATE ' + table_name.USER + ' SET reset_password_code = \'' + str(code) + '\' WHERE id = ' + str(id)
        return db_handle.excuteCommandChangeRecord(sql)

    def get_user_id_by_reset_password_code(self, code):
        sql = 'SELECT id FROM ' + table_name.USER + ' WHERE reset_password_code = \'' + str(code) + '\' AND is_deleted = 0 LIMIT 1'
        return db_handle.executeCommand(sql)

    def set_password_user(self, id, password):
        password = self.encodePass(password)
        sql = 'UPDATE ' + table_name.USER + ' SET password = \'' + str(password) + '\' WHERE id = ' + str(id)
        return db_handle.excuteCommandChangeRecord(sql)

    @staticmethod
    def encodePass(password):
        salt = "b50b1ffe2e7320b0d97062a9663d47a7adf1379392c58c66fd978171a2be7d65"
        return hashlib.md5(salt + password).hexdigest()







