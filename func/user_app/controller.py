from flask import Blueprint, jsonify, request
from func import message_const, token_const, config_const, db_handle, db, redis_store, count
#old Model
from func.user_app.user import User
from func.store_app.store import Store
from func.session.session import Session
from func.service.service import Service
from func.card.card import Card
from func.token.token import Token
from func.stamp.stamp import Stamp
from func.user_memo.user_memo import UserMemo
from func.email.email_handle import EmailHandle
from func.email.company_email import CompanyEmail
from func.email import email_type_const
from func.user_info.user_info import UserInfo
from func.how_use_app.format_web import FormatWeb
from func.policy.format_policy import FormatPolicy
from func.term_of_service.format_term import FormatTerm
from func.user_push_notification.user_push_notification import User_Push_Notifi
from func.push_notification.push_notification import Push_notifi
from func.question.question import Question
from func.company.company import Company
from func.user_contact.user_contact import UserContact
from func.device.device import Device
from func.service_company.service_company import Service_Company
from func.dict_handle import DictFail, DictSuccess, DictSession, LogHandle, LogOtherDB
from func.auth_utils import user_auth_required, user_auth_required_case_too_many_connection
from func.reminder_notification.reminder_notification import ReminderNotification
from func.email import admin_email
from sqlalchemy.orm import aliased
#new Model
from func.user_app.m_user import M_User
from func.manager_user.m_manager_user import M_Manager_User
from func.store_app.m_store import M_Store
from func.session.m_session import M_Session
from func.version.m_version import M_Version
from func.server_informations.m_server_informations import M_Server_Information
from func.service.m_service import M_Service
from func.card.m_card import M_Card
from func.token.m_token import M_Token
from func.stamp.m_stamp import M_Stamp
from func.user_memo.m_user_memo import M_User_Memo
from func.user_info.m_user_info import M_User_Info
from func.level.m_level import M_Level
from func.push_notification.m_push_notification import M_Push_Notification
from func.company.m_company import M_Company
from func.device.m_device import M_Device
from func.config_app_store.m_config_app_store import  M_Config_App_Store
from func.store_push_notification.m_store_push_notification import M_Store_Push_Notifi
from func.user_push_notification.m_user_push_notification import M_User_Push_Notifi
from func.service_company.m_service_company import M_Service_Company
from func.history_user.m_history_user import M_History_User
from func.reminder_notification.m_reminder_noti import M_Reminder_Noti
from func.contact.m_contact import M_Contact
from func.email.m_company_email import M_Company_Email
from func.qr_code_table_store.qr_code_table_store import M_Qr_Code_Table
from func.encrypt_url_codes.m_encrypt_url_codes import M_Encrypt_Url_Codes
from func.encrypt_question.m_encrypt_question import M_Encrypt_Question
from func.encrypt_store.m_encrypt_store import M_Encrypt_Store
from func.user_step_push_notification.m_user_step_push_notification import M_User_Step_Push_Notifi
from func.cache_user_push.m_cache_user_push import M_Cache_User_Push
from func.question_naire.m_questionnaire import M_Question_Naire
from func.user_coupons.m_user_coupon import M_User_Coupon
from func.coupons.m_coupon import M_Coupon
from func.rich_message.m_rich_message import M_Rich_Message
from func.apps.m_app import M_App
from func.user_response.m_user_response import M_User_Response

import os
from func.store_app import controller as StoreController
from datetime import datetime as dt, timedelta
from func.common import Common
import uuid
import time
import re
import logging, copy, math, json, pytz
#Timeline Model
from func.timeline_user_store.m_comment import M_Comment
from func.timeline_user_store.m_comment_count_like import M_Comment_Count_Like
from func.timeline_user_store.m_like import M_Like
from func.timeline_user_store.m_timeline import M_Timeline
from func.timeline_user_store.m_timeline_count_like import M_Timeline_Count_Like
from func.timeline_user_store.m_user_comment_like import M_User_Comment_Like
from func.timeline_user_store.m_user_timeline_like import M_User_Timeline_Like
from func.chat.m_chat import M_Chat

from sqlalchemy import distinct, func, or_
from oauth2client import client, crypt
import requests
from math import radians, cos, sin, asin, sqrt
from pyfcm import FCMNotification

import ssl
import urllib2


mod_user = Blueprint('users', __name__, url_prefix='/api/client/users')

@mod_user.route('/register_anonymous_user', methods = ['POST'])
def registerAnonymousUser():
    result = request.json if request.json else {}
    if result == {}:
        LogHandle(message_const.JSON_NONE, message_const.WARNING)
        db_handle.close_connect()
        dictFail = DictFail(message_const.JSON_NONE).response
        return jsonify(dictFail)

    store_key_gen = result.get('store_key_gen', '')

    m_store = M_Store.query.filter_by(key_gen = store_key_gen).first()
    #Todo will change if there is another store
    app_id = 3

    if not m_store:
        LogHandle(message_const.MES_NONE, message_const.WARNING)

        db_handle.close_connect()
        dictFail = DictFail(message_const.MES_NONE).response
        return jsonify(dictFail)

    _service_company = Service_Company()

    user_name_config = 'user_yuki'
    #Create user anonymous
    password = uuid.uuid4()
    m_user = M_User.query.filter_by(username = user_name_config).first()
    try:
        if m_user:
            m_user = M_User()
            m_user.username = user_name_config + db_handle.get_now_milis_add_one()
            m_user.password = db_handle.encodePass(str(password))
            m_user.email    = ''
            m_user.name     = message_const.NAME_ANONYMOUS_DEFAULT
            m_user.app_id   = app_id
        else:
            m_user = M_User()
            m_user.username = user_name_config + db_handle.get_now_milis()
            m_user.password = db_handle.encodePass(str(password))
            m_user.email    = ''
            m_user.name     = message_const.NAME_ANONYMOUS_DEFAULT
            m_user.app_id   = app_id

        db.session.add(m_user)
        db.session.commit()
        #Check in fast
        user_id  = m_user.id
        store_id = m_store.id

        service_company_id = m_store.service_company_id
        service_company = _service_company.get_service_from_service_company(service_company_id)

        func_check_in_and_check_out_now(user_id, store_id, service_company['card_type'])
        # Create card no stamp
        StoreController.get_card_after_create(service_company, user_id)
        #Create token
        data = create_auth_token_anonymous(m_user, password)

        if not data:
            dictFail = DictFail(message_const.LOGIN_FAIL).response
            return jsonify(dictFail)

        db_handle.close_connect()
        dict = DictSuccess(data, message_const.REGISTER_SUCCESS).response
        return jsonify(dict)

    except Exception, e:
        LogHandle(e, message_const.ERROR)
        db_handle.close_connect()
        db.session.rollback()
        dictFail = DictFail(message_const.MES_NONE).response
        return jsonify(dictFail)


def create_auth_token_anonymous(user, password):
    new_token = str(uuid.uuid4())

    user_id = user.id
    expire = db_handle.getExpireToken()

    try:
        m_token = M_Token()
        m_token.user_id = user_id
        m_token.auth_token = new_token
        m_token.expire = expire
        db.session.add(m_token)
        db.session.commit()

    except Exception, e:
        LogHandle(e, message_const.ERROR)
        db.session.rollback()
        db_handle.close_connect()
        return False

    data = {
        'user_id': int(user_id),
        'token': new_token,
        'expire_date': expire,
        'username': user.username.encode('utf-8'),
        'name': user.name.encode('utf-8'),
        'email': user.email.encode('utf-8'),
        'password': password,
        'img_user': user.img_url if user.img_url else ''
    }

    return data


@mod_user.route('/register', methods = ['POST'])
def registerUser():
    result = request.json if request.json else {}

    if result == {}:
        LogHandle(message_const.JSON_NONE, message_const.WARNING)
        dictFail = DictFail(message_const.JSON_NONE).response
        return jsonify(dictFail)

    token      = result.get('token','')
    app_id     = result.get('app_id', 3)
    type_login = result.get('type_login', config_const.LOGIN_NORMAL)


    #0-normal, 1-google, 2-facebook, 3-twitter

    #Login Google account
    if type_login == config_const.LOGIN_GG:

        m_user = get_user_login_google(token, app_id)

        if not m_user:
            dictFail = DictFail(message_const.INVALID_TOKEN).response
            return jsonify(dictFail)

        data = create_auth_token(m_user)

        if not data:
            dictFail = DictFail(message_const.LOGIN_FAIL).response
            return jsonify(dictFail)

        # Send email
        send_email_to_register_account(m_user.username, m_user.email)

        dict = DictSuccess(data, message_const.REGISTER_SUCCESS).response
        return jsonify(dict)

    #Login facebook account
    if type_login == config_const.LOGIN_FB:

        m_user = get_user_login_facebook(token, app_id)

        if not m_user:
            dictFail = DictFail(message_const.INVALID_TOKEN).response
            return jsonify(dictFail)

        data = create_auth_token(m_user)

        if not data:
            dictFail = DictFail(message_const.LOGIN_FAIL).response
            return jsonify(dictFail)

        # Send email
        send_email_to_register_account(m_user.username, m_user.email)

        dict = DictSuccess(data, message_const.REGISTER_SUCCESS).response
        return jsonify(dict)

    #Login normal
    username = result.get('username','')
    username = username.encode('utf-8')
    password = result.get('password','')
    name     = result.get('name','')
    email    = result.get('email','')
    age_avg  = result.get('age_avg', 10)
    sex      = result.get('sex', 1)

    user   = User()

    resultCheck = user.checkUserInfo(username, password, name, email)
    if resultCheck == config_const.INFO_NULL:
        dictFail = DictFail(message_const.MES_NULL_INFO_USER).response
        return jsonify(dictFail)
    if resultCheck == config_const.FORMAT_EMAIL_WRONG:
        dictFail = DictFail(message_const.FORMAT_EMAIL_WRONG).response
        return jsonify(dictFail)
    if resultCheck == config_const.USER_EXISTS:
        dictFail = DictFail(message_const.USER_EXISTS).response
        return jsonify(dictFail)
    if resultCheck == config_const.EMAIL_EXIST:
        dictFail = DictFail(message_const.EMAIL_EXIST).response
        return jsonify(dictFail)

    result = user.registerUser(username, password, name, email, age_avg, sex, app_id)

    if result:
        #Login
        m_user = M_User.query.filter_by(username = username).filter_by(password = password).filter_by(is_deleted = 0).first()

        if not m_user:
            dictFail = DictFail(message_const.LOGIN_FAIL).response
            return jsonify(dictFail)

        data = create_auth_token(m_user)

        if not data:
            LogHandle(message_const.LOGIN_FAIL, message_const.WARNING)
            dictFail = DictFail(message_const.LOGIN_FAIL).response
            return jsonify(dictFail)

        #Send email
        send_email_to_register_account(username, email)

        dict = DictSuccess(data, message_const.REGISTER_SUCCESS).response
        return jsonify(dict)

    LogHandle('register user fail', message_const.WARNING)
    dictFail = DictFail(message_const.MES_NONE).response
    return jsonify(dictFail)


def create_auth_token(user):

    new_token = str(uuid.uuid4())

    user_id = user.id
    expire  = db_handle.getExpireToken()

    try:
        m_token = M_Token()
        m_token.user_id    = user_id
        m_token.auth_token = new_token
        m_token.expire     = expire
        db.session.add(m_token)
        db.session.commit()

    except Exception, e:
        db.session.rollback()
        db_handle.close_connect()
        return False

    data = {
        'user_id'     : int(user_id),
        'token'       : new_token,
        'expire_date' : expire,
        'username'    : user.name.encode('utf-8'),
        'email'       : user.email.encode('utf-8'),
        'img_user'    : user.img_url if user.img_url else ''
    }

    return data

def send_email_to_register_account(username, email):
    try:
        _company_email = CompanyEmail()
        email_info = _company_email.get_company_email()

        email_variables = {
            'username': username,
            'email': email
        }
        # send email to register account
        _email_handle = EmailHandle()
        _email_handle.send_email(email_info['email'], email_info['password'],
                                 email_info['host'], email_info['port'],
                                 email_variables, email_type_const.REGISTER_USER_ACCOUNT)
        return True
    except:
        return False


def get_user_login_google(token, app_id):
    try:
        url_gg = config_const.URL_GOOGLE + str(token)

        response = requests.get(url_gg, verify=False)

        if response.status_code != 200:
            return False

        user_res = response.json()

    except Exception, e:
        return False
    try:
        user_exists = M_User.query.filter_by(username = str(user_res['sub'])).first()
        if user_exists:
            if app_id != user_res['app_id']:
                return False

            return user_exists

        m_user = M_User()
        m_user.username = user_res['sub']
        m_user.password = ''
        m_user.name     = user_res['name']
        m_user.email    = user_res['email']
        m_user.app_id   = app_id

        db.session.add(m_user)
        db.session.commit()

        return m_user
    except:
        db.session.rollback()
        return False


def get_user_login_facebook(token, app_id):
    try:
        url_fb = config_const.URL_FACEBOOK + str(token)

        response = requests.get(url_fb)

        if response.status_code != 200:
            return False

        user_res = response.json()

    except Exception, e:
        return False
    try:
        user_exists = M_User.query.filter_by(username = str(user_res['id'])).first()
        if user_exists:
            if app_id != user_res['app_id']:
                return False
            return user_exists

        m_user = M_User()
        m_user.username = user_res['id']
        m_user.password = ''
        m_user.name     = user_res['name']
        m_user.email    = user_res['email']
        m_user.app_id   = app_id

        db.session.add(m_user)
        db.session.commit()

        return m_user
    except:
        db.session.rollback()
        return False


@mod_user.route('/login', methods = ['POST'])
def loginUser():

    result = request.json if request.json else {}

    if result == {}:
        db_handle.close_connect()
        dictFail = DictFail(message_const.JSON_NONE).response
        return jsonify(dictFail)

    token      = result.get('token', '')
    app_id     = result.get('app_id', 3)
    type_login = result.get('type_login', config_const.LOGIN_NORMAL)

    # Login Google account
    if type_login == config_const.LOGIN_GG:

        m_user = get_user_login_google(token, app_id)

        if not m_user:
            db_handle.close_connect()
            dictFail = DictFail(message_const.INVALID_TOKEN).response
            return jsonify(dictFail)

        data = create_auth_token(m_user)

        if not data:
            db_handle.close_connect()
            dictFail = DictFail(message_const.LOGIN_FAIL).response
            return jsonify(dictFail)

        # Send email
        send_email_to_register_account(m_user.username, m_user.email)
        db_handle.close_connect()
        dict = DictSuccess(data, message_const.REGISTER_SUCCESS).response
        return jsonify(dict)

    # Login facebook account
    if type_login == config_const.LOGIN_FB:

        m_user = get_user_login_facebook(token, app_id)

        if not m_user:
            db_handle.close_connect()
            dictFail = DictFail(message_const.INVALID_TOKEN).response
            return jsonify(dictFail)

        data = create_auth_token(m_user)

        if not data:
            db_handle.close_connect()
            dictFail = DictFail(message_const.LOGIN_FAIL).response
            return jsonify(dictFail)

        # Send email
        send_email_to_register_account(m_user.username, m_user.email)
        db_handle.close_connect()
        dict = DictSuccess(data, message_const.REGISTER_SUCCESS).response
        return jsonify(dict)

    username = result.get('username', '')
    password = result.get('password', '')
    user   = User()
    _token = Token()

    resultLogin = user.loginSuccess(username, password)

    if not resultLogin:
        db_handle.close_connect()
        dictFail = DictFail(message_const.LOGIN_FAIL).response
        return jsonify(dictFail)

    if int(app_id) != int(resultLogin['app_id']):
        db_handle.close_connect()
        dictFail = DictFail(message_const.WRONG_APP_ID).response
        return jsonify(dictFail)

    newToken = str(uuid.uuid4())
    user_id  = resultLogin['id']
    expire   = db_handle.getExpireToken()

    res_create_token = _token.createTokenUser(user_id, newToken, expire)

    if not res_create_token:
        db_handle.close_connect()
        dictFail = DictFail(message_const.MES_NONE).response
        return jsonify(dictFail)

    data = {
        'user_id'     : int(user_id),
        'token'       : newToken,
        'expire_date' : expire,
        'username'    : resultLogin['name'],
        'email'       : resultLogin['email'],
        'img_user'    : resultLogin['img_url'] if resultLogin['img_url'] else ''
    }

    db_handle.close_connect()
    dict = DictSuccess(data, message_const.MES_NONE).response
    return jsonify(dict)


@mod_user.route('/send-email-contain-reset-password-code', methods = ['POST'])
def send_email_contain_reset_password():
    result = request.json if request.json else {}
    if result == {}:
        dict_fail = DictFail(message_const.JSON_NONE).response
        return jsonify(dict_fail)

    username_or_email = result.get('username_or_email', '')
    _user = User()
    _email_handle = EmailHandle()
    user = None

    # get email company
    _company_email = CompanyEmail()
    email_info = _company_email.get_company_email()

    # generate code
    random_code = db_handle.generate_random_code()
    success = False

    # check exist username or email
    is_exist_username = _user.checkUserExists(username_or_email)
    if bool(is_exist_username):
        user = _user.get_user_by_username(username_or_email)
        success = True
    else:
        is_exist_email = _user.checkEmailExist(username_or_email)
        if bool(is_exist_email):
            user = _user.get_user_by_email(username_or_email)
            success = True

    if success == True:
        # send email
        email_variables = {
            'email': user['email'],
            'code': random_code
        }
        send_email_result = _email_handle.send_email(email_info['email'], email_info['password'],
                                                     email_info['host'], email_info['port'],
                                                     email_variables, email_type_const.SEND_RESET_PASSWORD_CODE)

        if (send_email_result == 'success'):
            # update user in database set reset password code
            _user.set_reset_password_code_user(user['id'], random_code)
            data = None
            dict = DictSuccess(data, message_const.SUCCESS_SEND_EMAIL).response
        else:
            dict = DictFail(message_const.ERROR_SEND_EMAIL).response
    else:
        if not re.match('^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$', username_or_email):
            dict = DictFail(message_const.WRONG_USERNAME_RESET_PASSWORD).response
        else:
            dict = DictFail(message_const.WRONG_EMAIL_RESET_PASSWORD).response

    return jsonify(dict)

@mod_user.route('/change-password-by-code', methods = ['POST'])
def change_password_by_code():
    result = request.json if request.json else {}
    if result == {}:
        dict_fail = DictFail(message_const.JSON_NONE).response
        return jsonify(dict_fail)

    reset_password_code = result.get('code', '')
    new_password = result.get('password', '')

    _user = User()
    _token = Token()
    user_id = _user.get_user_id_by_reset_password_code(reset_password_code)

    if user_id:
        _user.set_password_user(str(user_id['id']), str(new_password))

        # delete old token
        _token.delete_all_token_by_user_id(str(user_id['id']))

        # login
        newToken = str(uuid.uuid4())
        expire = db_handle.getExpireToken()
        res_create_token = _token.createTokenUser(str(user_id['id']), newToken, expire)

        if not res_create_token:
            dictFail = DictFail(message_const.MES_NONE).response
            return jsonify(dictFail)

        data = {
            'token': newToken,
            'expire_date': expire
        }

        dict = DictSuccess(data, message_const.SUCCESS_CHANGE_PASSWORD).response
    else:
        dict = DictFail(message_const.ERROR_CHANGE_PASSWORD_BY_CODE).response

    return jsonify(dict)

@mod_user.route('/change-password', methods = ['POST'])
@user_auth_required()
def change_password(user = None):
    if not user:
        dict_fail = DictFail(message_const.TOKEN_EXPIRE, config_const.CODE_FAIL_AUTHORIZED).response
        return jsonify(dict_fail)

    user_id = user['id']
    result = request.json if request.json else {}
    if result == {}:
        dict_fail = DictFail(message_const.JSON_NONE).response
        return jsonify(dict_fail)

    old_password = result.get('old_password', '')
    new_password = result.get('new_password', '')

    _user = User()
    _token = Token()
    user_id = _user.check_password_user(str(user_id), str(old_password))

    if bool(user_id):
        _user.set_password_user(str(user_id['id']), str(new_password))

        # delete old token
        _token.delete_all_token_by_user_id(str(user_id['id']))

        # login
        newToken = str(uuid.uuid4())
        expire = db_handle.getExpireToken()
        res_create_token = _token.createTokenUser(str(user_id['id']), newToken, expire)

        if not res_create_token:
            dictFail = DictFail(message_const.MES_NONE).response
            return jsonify(dictFail)

        data = {
            'token': newToken,
            'expire_date': expire
        }

        dict = DictSuccess(data, message_const.SUCCESS_CHANGE_PASSWORD).response
    else:
        dict = DictFail(message_const.ERROR_CHANGE_PASSWORD).response

    return jsonify(dict)


def func_change_password(user_id, old_pass, new_pass):

    if not old_pass or old_pass == '':
        return True
    try:

        user = M_User.query.filter_by(id = user_id, password = old_pass).first()
        if not user:
            return False

        user.password = new_pass
        db.session.commit()
        return True

    except Exception, e:
        print e
        db.session.rollback()
        return False


@mod_user.route('/mapping-user-store', methods = ['POST'])
@user_auth_required()
def mapping_user_store(user = None):
    if not user:
        dictFail = DictFail(message_const.TOKEN_EXPIRE, config_const.CODE_FAIL_AUTHORIZED).response
        return jsonify(dictFail)

    user_id = user['id']

    result = request.json if request.json else {}

    if result == {}:
        dictFail = DictFail(message_const.MES_NONE).response
        return jsonify(dictFail)

    url_store = result.get('url_store', '')

    if not url_store:
        dictFail = DictFail(message_const.MES_NONE).response
        return jsonify(dictFail)

    store = M_Store.query.filter(M_Store.url_store == url_store).first()

    if not store:
        dictFail = DictFail(message_const.MES_NONE).response
        return jsonify(dictFail)
    try:
        m_user_info                    = M_User_Info()
        m_user_info.store_id           = store.id
        m_user_info.user_id            = user_id
        m_user_info.management_user_id = 0
        m_user_info.check_in_amount    = 0

        db.session.add(m_user_info)
        db.session.commit()

        dictSuccess = DictSuccess(None, message_const.MES_NONE).response
        return jsonify(dictSuccess)
    except Exception, e:
        LogHandle(e, message_const.ERROR)
        db.session.rollback()
        dictFail = DictFail(message_const.MES_NONE).response
        return jsonify(dictFail)

@mod_user.route('/feedback-ignore-session', methods = ['POST'])
@user_auth_required()
def feedback_ignore_session(user = None):
    if not user:
        db_handle.close_connect()
        dictFail = DictFail(message_const.TOKEN_EXPIRE, config_const.CODE_FAIL_AUTHORIZED).response
        return jsonify(dictFail)

    user_id = user['id']
    result = request.json if request.json else {}

    if result == {}:
        db_handle.close_connect()
        dictFail = DictFail(message_const.MES_NONE).response
        return jsonify(dictFail)

    code = result.get('code','')

    try:
        _user_info = UserInfo()
        m_encrypt = M_Encrypt_Url_Codes.query.filter_by(code = code).first()

        if not m_encrypt:
            db_handle.close_connect()
            dictFail = DictFail(message_const.MES_NONE).response
            return jsonify(dictFail)

        resultStore = M_Store.query.filter(M_Store.id == m_encrypt.store_id).first()

        if resultStore:
            store_id           = resultStore.id
            service_company_id = resultStore.service_company_id
        else:
            LogHandle(message_const.MES_NONE, message_const.ERROR)
            db_handle.close_connect()
            dict_fail = DictFail(message_const.MES_NONE).response
            return jsonify(dict_fail)
        #Mapping user vs store
        userInfo = _user_info.get_user_info_by_store_id_and_user_id(store_id, user_id)
        if not userInfo:

            app_id = func_check_user_store_mapping_app(user_id, store_id)
            if app_id:
                m_user_info = M_User_Info()
                m_user_info.user_id  = user_id
                m_user_info.store_id = store_id
                m_user_info.modified = db_handle.subtract_5_year()

                db.session.add(m_user_info)
                db.session.commit()

        lastest_session_id = 0
        StoreController.add_stamp_for_user(int(service_company_id), int(store_id), int(user_id), 0, 0
                                           , lastest_session_id, user['name'], 0, 1)
        #mapping user vs push
        # func_mapping_user_with_manager(int(user_id), int(store_id))

        data = { "push_id" : m_encrypt.push_notification_id}
        db_handle.close_connect()
        dictSuccess = DictSuccess(data, message_const.MES_NONE).response
        return jsonify(dictSuccess)

    except Exception, e:

        # LogHandle(e, message_const.ERROR)
        db.session.rollback()
        db_handle.close_connect()
        dict_fail = DictFail(message_const.MES_NONE).response
        return jsonify(dict_fail)

def func_check_user_store_mapping_app(user_id, store_id):
    m_manager = M_Manager_User.query.filter_by(store_id = store_id, level_id = 4).first()
    if not m_manager:
        return 0

    m_user = M_User.query.filter_by(id = user_id).first();

    if not m_user:
        return 0

    if m_manager.app_id != m_user.app_id:
        return 0

    return m_user.app_id


def func_check_in_and_check_out_now(user_id, store_id, card_type):
    _user_info = UserInfo()
    # update info user by store id and user id
    userInfo = _user_info.get_user_info_by_store_id_and_user_id(store_id, user_id)

    if not userInfo:
        app_id = func_check_user_store_mapping_app(user_id, store_id)
        if app_id:
            resCreate = _user_info.create_user_info_by_store_id_and_user_id_contain_check_in_time(app_id, store_id, user_id)

        # userInfo = _user_info.get_user_info_by_store_id_and_user_id(store_id, user_id)
        # mapping step push with user check in
        # func_create_step_push_user(user_id, store_id)
        #mapping user vs push accnounce when checkin first time
        # if card_type == 3:
            # func_create_push_accnoun_for_new_user(user_id, store_id)
            # func_mapping_user_with_manager(user_id, store_id)

    # check_in_amount = int(userInfo['check_in_amount']) + 1
    # _user_info.update_user_info(userInfo['store_id'], userInfo['user_id'], check_in_amount)

def func_change_status_create_push(user_id):
    m_user = M_User.query.filter_by(id = user_id, is_deleted = 0).first()

    if not m_user:
        return False
    try:
        m_user.is_create_push = 0
        db.session.commit()

        return True
    except Exception, e:
        print e
        db.session.rollback()
        db_handle.close_connect()

    return False

def func_mapping_user_with_manager(user_id, store_id):
    manager_id = get_manager_id_from_store_id(store_id)
    if not manager_id:
        return 0

    m_cache = M_Cache_User_Push.query.filter_by(user_id = user_id, manager_id = manager_id).first()
    if m_cache:
        return 0
    try:
        m_cache = M_Cache_User_Push()
        m_cache.user_id    = user_id
        m_cache.manager_id = manager_id
        db.session.add(m_cache)
        db.session.commit()
    except Exception, e:
        print e
        db.session.rollback()
    return 0


@mod_user.route('/check-in-fast', methods = ['POST'])
def check_in_fast(user = None):
    result = request.json if request.json else {}

    if result == {}:
        LogHandle(message_const.MES_NONE, message_const.WARNING)
        dictFail = DictFail(message_const.MES_NONE).response
        return jsonify(dictFail)

    if request.headers['Authorization']:
        token = request.headers['Authorization']
    else:
        token = ''

    code_store = result.get('code_store', '')

    _service_company = Service_Company()

    m_token = M_Token.query.filter_by(auth_token = token).filter(M_Token.user_id != None).first()

    if not m_token:
        LogHandle('not m_token', message_const.WARNING)
        dictFail = DictFail(message_const.MES_NONE).response
        return jsonify(dictFail)

    try:
        m_encrypt_store = M_Encrypt_Store.query.filter_by(code_store = code_store).first()

        if not m_encrypt_store:
            LogHandle('not m_encrypt_store', message_const.WARNING)
            dictFail = DictFail(message_const.MES_NONE).response
            return jsonify(dictFail)

        resultStore = M_Store.query.filter(M_Store.id == m_encrypt_store.store_id).first()

        service_company_id = resultStore.service_company_id
        service_company    = _service_company.get_service_from_service_company(service_company_id)

        func_check_in_and_check_out_now(m_token.user_id, m_encrypt_store.store_id, service_company['card_type'])

        # Create card no stamp
        StoreController.get_card_after_create(service_company, m_token.user_id)

        dictSuccess = DictSuccess(None, message_const.MES_NONE).response
        return jsonify(dictSuccess)

    except Exception, e:
        LogHandle(e, message_const.ERROR)
        dictFail = DictFail(message_const.MES_NONE).response
        return jsonify(dictFail)


def func_create_push_accnoun_for_new_user(user_id, store_id):

    try:
        m_manager = M_Manager_User.query.filter_by(store_id=store_id).first()

        if not m_manager:
            return False;

        m_list_push = M_Push_Notification.query.filter_by(management_user_id = m_manager.id, is_step = 0, status = 5).order_by(M_Push_Notification.created.desc()).all();
        for m_push in m_list_push:
            m_user_acc_push = M_User_Push_Notifi.query.filter_by(user_id=user_id, push_notification_id = m_push.id).first()

            if not m_user_acc_push:
                m_user_acc_push = M_User_Push_Notifi()

                m_user_acc_push.user_id              = user_id
                m_user_acc_push.push_notification_id = m_push.id

                db.session.add(m_user_acc_push)
        db.session.commit()

    except Exception,e:
        print e
        db.session.rollback()
        db_handle.close_connect()
        return False


def func_create_step_push_user(user_id, store_id):
    try:
        user_info = M_User_Info.query.filter_by(user_id = user_id, store_id = store_id).first()

        if not user_info:
            return False

        first_time_check_in = dt.date(user_info.created)

        m_manager = M_Manager_User.query.filter_by(store_id = store_id).first()

        if not m_manager:
            return False;

        m_list_push = M_Push_Notification.query.filter_by(management_user_id = m_manager.id, is_step = 1).all();

        for m_push in m_list_push:
            send_time = str(first_time_check_in + timedelta(days=m_push.number_day_after_check_in)) + ' ' + str(m_push.send_time_no_date)
            m_user_step_push = M_User_Step_Push_Notifi.query.filter_by(user_id = user_id, push_notification_id = m_push.id).first()

            if not m_user_step_push:
                m_user_step_push = M_User_Step_Push_Notifi()
                m_user_step_push.user_id              = user_id
                m_user_step_push.push_notification_id = m_push.id
                m_user_step_push.send_time            = send_time

                db.session.add(m_user_step_push)
                db.session.commit()
            else:
                m_user_step_push.send_time = send_time
                db.session.commit()

    except Exception,e:
        print e
        db.session.rollback()
        db_handle.close_connect()
        return False


@mod_user.route('/feedback-scan-qr-code-store', methods = ['POST'])
@user_auth_required()
def feedback_scan_qr_code_store(user = None):
    if not user:
        dictFail = DictFail(message_const.TOKEN_EXPIRE, config_const.CODE_FAIL_AUTHORIZED).response
        return jsonify(dictFail)

    user_id = user['id']
    result = request.json if request.json else {}

    if result == {}:
        dictFail = DictFail(message_const.MES_NONE).response
        return jsonify(dictFail)
    _service_company = Service_Company()

    currentDay = db_handle.get_current_day()

    qr_code = result.get('qr_code','')
    _store   = Store()
    _session = Session()

    if len(qr_code) > 4:
        # Case user check in and check out without history, stamp
        if qr_code[:4] == 'fast':
            qr_code = qr_code[5:]
            resultCheckQrCode = _store.check_qr_code(qr_code)
            if resultCheckQrCode == config_const.QR_CODE_NULL:
                dictFail = DictFail(message_const.WRONG_QR_CODE).response
                return jsonify(dictFail)
            if resultCheckQrCode == config_const.QR_CODE_NOT_EXISTS:
                dictFail = DictFail(message_const.WRONG_QR_CODE).response
                return jsonify(dictFail)

            result_check_session_exists = _session.getSessionByUserForFeedback(user_id)
            # check qr code string
            if result_check_session_exists:
                if result_check_session_exists['status'] == config_const.CHECKED_IN:
                    dictFail = DictFail(message_const.MES_CHECKED_IN).response
                    return jsonify(dictFail)
                dictFail = DictFail(message_const.MES_WAIT_CONFIMED).response
                return jsonify(dictFail)

            resultStore = M_Store.query.filter(M_Store.check_in_code == qr_code).first()
            store_id = resultStore.id

            service_company_id = resultStore.service_company_id
            service_company    = _service_company.get_service_from_service_company(service_company_id)

            #Check in and check out
            func_check_in_and_check_out_now(user_id, store_id, service_company['card_type'])

            # Create card no stamp
            StoreController.get_card_after_create(service_company, user_id)

            dictSuccess = DictSuccess(None, message_const.MES_NONE).response
            return jsonify(dictSuccess)

    _user            = User()
    _service_company = Service_Company()
    _user_info       = UserInfo()
    is_qr_code_son   = False

    #Check QR CODE
    resultCheckQrCode = _store.check_qr_code(qr_code)
    if resultCheckQrCode == config_const.QR_CODE_NULL:
        dictFail = DictFail(message_const.WRONG_QR_CODE).response
        return jsonify(dictFail)
    if resultCheckQrCode == config_const.QR_CODE_NOT_EXISTS:
        m_qr_table = M_Qr_Code_Table.query.filter(M_Qr_Code_Table.qr_code == qr_code).first()
        if not m_qr_table:
            dictFail = DictFail(message_const.WRONG_QR_CODE).response
            return jsonify(dictFail)
        is_qr_code_son = True
    #Check user check-out or not
    result_check_session_exists = _session.getSessionByUserForFeedback(user_id)

    # check qr code string
    if result_check_session_exists:
        if result_check_session_exists['status'] == config_const.CHECKED_IN:
            dictFail = DictFail(message_const.MES_CHECKED_IN).response
            return jsonify(dictFail)
        dictFail = DictFail(message_const.MES_WAIT_CONFIMED).response
        return jsonify(dictFail)

    #lay store
    if is_qr_code_son:
        store_id_from_son = m_qr_table.store_id
        resultStore = M_Store.query.filter(M_Store.id == store_id_from_son).first()
    else:
        resultStore = M_Store.query.filter(M_Store.check_in_code == qr_code).first()

    service_company_of_store = _service_company.get_service_from_service_company(resultStore.service_company_id)

    is_approved_check_in  = service_company_of_store['is_approved_check_in']
    is_approved_check_out = service_company_of_store['is_approved_check_out']

    if int(service_company_of_store['card_type']) == config_const.NONE_TYPE:
        is_approved_check_in  = 0
        is_approved_check_out = 0

    if is_approved_check_in != 1:

        if int(currentDay) != int(resultStore.code_create_day):
            dictFail = DictFail(message_const.QR_CODE_EXPIRE).response
            return jsonify(dictFail)

    if resultStore:
        store_id                 = resultStore.id
        service_company_id       = resultStore.service_company_id
        numerical_session_in_day = resultStore.numerical_session_in_day
        date_modified            = resultStore.modified
    else:
        dict_fail = DictFail(message_const.MES_NONE).response
        return jsonify(dict_fail)

    resultUser = _user.getUser(user_id)
    user_name  = str(resultUser['username'].encode('utf-8'))
    expire     = dt.now(tz=pytz.timezone('Asia/Tokyo')).strftime('%Y-%m-%d 23:59:59')

    if is_approved_check_in == 1 and is_approved_check_out == 1:
        status = config_const.WAIT_CONFIRM
    elif is_approved_check_in == 1 and is_approved_check_out == 0:
        status = config_const.WAIT_CONFIRM
    elif is_approved_check_in == 0 and is_approved_check_out == 1:
        status = config_const.CHECKED_IN
    elif is_approved_check_in == 0 and is_approved_check_out == 0:
        status = config_const.CHECKED_OUT
    else:
        dict_fail = DictFail(message_const.UNSETTING_APPROVED_CHECK_IN_OUT_STORE).response
        return jsonify(dict_fail)

    # guest wating
    number_people_wait = _session.get_number_people(str(store_id), config_const.WAIT_CONFIRM)

    #guest number in day
    numerical_session = _store.get_numerical_session_day(str(store_id))

    try:
        #update numerical_session_in_day in store
        _store.update_numerical_session(store_id, numerical_session)

        #Create session
        if is_qr_code_son:
            m_session                   = M_Session()
            m_session.store_id          = store_id
            m_session.user_id           = user_id
            m_session.user_name         = user_name
            m_session.check_in_code     = qr_code
            m_session.expire            = expire
            m_session.status            = status
            m_session.numerical_session = numerical_session
            m_session.seat_number       = int(qr_code[5])
            db.session.add(m_session)
            db.session.commit()

        else:
            _session.insertSession(store_id, user_id, user_name, qr_code, expire,
                                                       status, numerical_session)

    except:
        dict_fail = DictFail(message_const.MES_NONE).response
        return jsonify(dict_fail)

    # check in but not check out
    if (is_approved_check_in == 0 and is_approved_check_out == 1) or \
            (is_approved_check_in == 0 and is_approved_check_out == 0):
        # update info user by store id and user id
        userInfo = _user_info.get_user_info_by_store_id_and_user_id(store_id, user_id)

        if not userInfo:
            # Mapping user_step_push
            # func_create_step_push_user(user_id, store_id)
            # MAping user push accnounce(case None type)
            # if is_approved_check_in == 0 and is_approved_check_out == 0:
            #     func_create_push_accnoun_for_new_user(user_id, store_id)
            app_id = func_check_user_store_mapping_app(user_id, store_id)
            if app_id:
                resCreate = _user_info.create_user_info_by_store_id_and_user_id_contain_check_in_time(app_id, store_id, user_id)

            if not resCreate:
                dict_fail = DictFail(message_const.MES_NONE).response
                return jsonify(dict_fail)

            userInfo = _user_info.get_user_info_by_store_id_and_user_id(store_id, user_id)

        check_in_amount = int(userInfo['check_in_amount']) + 1
        _user_info.update_user_info(userInfo['store_id'], userInfo['user_id'], check_in_amount)

        lastest_session = _session.get_session_latest_by_store_id_and_user_id(store_id, user_id)

        M_Session.query.filter(M_Session.id == lastest_session['id']).update({
            M_Session.check_in_time: db_handle.get_full_current_jav_time()
        })
        db.session.commit()

        # _save_user_history
        save_history = StoreController.save_user_history(int(store_id), int(user_id), lastest_session['id'],
                                                          user_name, 0, 0, config_const.CHECKED_IN, ' ')

        if not save_history:
            dict_fail = DictFail(message_const.MES_NONE).response
            return jsonify(dict_fail)

    # Pass date to milisecond
    dt_obj = dt.strptime(expire, "%Y-%m-%d %H:%M:%S")
    d_in_ms = int(dt_obj.strftime('%s')) * 1000
    expire_time_milis = str(d_in_ms)

    if not (is_approved_check_in == 0 and is_approved_check_out == 0):
        resultServiceCompany = _service_company.get_service_from_service_company(service_company_id)
        if resultServiceCompany:
            service_id = resultServiceCompany['service_id']
            company_id = resultServiceCompany['company_id']

        resultSession = _session.getSessionLatest(store_id)
        m_service = M_Service.query.filter_by(id=service_id).first()
        service_name = m_service.name
        arrData = []
        arrData.append(resultSession['id'])
        arrData.append(status)
        arrData.append(store_id)
        arrData.append(service_company_id)
        arrData.append(service_id)
        arrData.append(company_id)
        arrData.append(number_people_wait)
        arrData.append(expire_time_milis)
        arrData.append(numerical_session)
        arrData.append(service_name)

        data = DictSession(arrData).response
        data['card_type'] = int(service_company_of_store['card_type'])

        dictSuccess = DictSuccess(data, message_const.MES_NONE).response
        return jsonify(dictSuccess)
    else:
        _save_history = StoreController.save_user_history(int(store_id), int(user_id), lastest_session['id'],
                                                          user_name, 0, 0, config_const.CHECKED_OUT, ' ')
        if not _save_history:
            dict_fail = DictFail(message_const.MES_NONE).response
            return jsonify(dict_fail)

        # delete session
        lastest_session_id = lastest_session['id']
        _session.delete_session(lastest_session['id'])

        resultServiceCompany = _service_company.get_service_from_service_company(service_company_id)
        if resultServiceCompany:
            service_id = resultServiceCompany['service_id']
            company_id = resultServiceCompany['company_id']

        StoreController.add_stamp_for_user(int(service_company_id), int(store_id), int(user_id), 0, 0
                                           , lastest_session_id, user['name'], 0, 1)
        m_service = M_Service.query.filter_by(id=service_id).first()
        service_name = m_service.name

        arrData = []
        arrData.append(lastest_session['id'])
        arrData.append(status)
        arrData.append(store_id)
        arrData.append(service_company_id)
        arrData.append(service_id)
        arrData.append(company_id)
        arrData.append(number_people_wait)
        arrData.append(expire_time_milis)
        arrData.append(numerical_session)
        arrData.append(service_name)

        data = DictSession(arrData).response
        data['card_type'] = int(service_company_of_store['card_type'])

        dictSuccess = DictSuccess(data, message_const.MES_NONE).response
        return jsonify(dictSuccess)

@mod_user.route('/get-check-in-status-by-user', methods = ['GET'])
@user_auth_required()
def get_check_in_status_by_user(user = None):
    if not user:
        dictFail = DictFail(message_const.TOKEN_EXPIRE, config_const.CODE_FAIL_AUTHORIZED).response
        return jsonify(dictFail)

    user_id = user['id']
    _session         = Session()
    _store           = Store()
    _service_company = Service_Company()
    _common          = Common()

    session_user = _session.getSessionByUser(user_id)

    if session_user:

        number_people_wait = _session.get_number_people(session_user['store_id'], config_const.WAIT_CONFIRM)

        if session_user['status'] == config_const.WAIT_CONFIRM:
            waiting_time = _common.get_waiting_time(int(session_user['id']), int(session_user['store_id']))
        else:
            waiting_time = 0

        resStore           = _store.getStoreById(session_user['store_id'])
        store_name         = resStore['name']
        service_company_id = resStore['service_company_id']

        resServiceCompany = _service_company.get_service_from_service_company(service_company_id)
        service_id        = resServiceCompany['service_id']
        m_service         = M_Service.query.filter_by(id = service_id).first()
        service_name      = m_service.name

        data = {}

        data['status_check_in']          = session_user['status']
        data['numerical_session']        = session_user['numerical_session']
        data['number_of_waiting_people'] = number_people_wait
        data['waiting_time']             = waiting_time
        data['store_name']               = store_name
        data['session_id']               = session_user['id']
        data['service_company_id']       = service_company_id
        data['service_id']               = service_id
        data['service_name']             = service_name

        # if status = cancel , xoa session
        if session_user['status'] == 'cancel':
            M_Session.query.filter(M_Session.id == session_user['id']).delete()
            db_handle.commit_obj()

        dictSucess = DictSuccess(data, message_const.MES_NONE).response
        return jsonify(dictSucess)


    m_session_died = M_Session.query.filter(M_Session.user_id == user_id).first()
    if m_session_died:

        M_History_User.query.filter(M_History_User.session == m_session_died.id)\
            .filter(M_History_User.action_code == config_const.CHECKED_IN).delete()

        db.session.delete(m_session_died)
        db.session.commit()

    dictFail = DictFail(message_const.MES_NONE).response
    return jsonify(dictFail)

@mod_user.route('/get-memo-user', methods = ['POST'])
@user_auth_required()
def get_memo_user(user = None):
    if not user:
        dict_fail = DictFail(message_const.TOKEN_EXPIRE, config_const.CODE_FAIL_AUTHORIZED).response
        return jsonify(dict_fail)

    user_id = user['id']
    _common = Common()

    post_data_json = request.json if request.json else {}

    if post_data_json == {}:
        dict_fail = DictFail(message_const.JSON_NONE).response
        return jsonify(dict_fail)

    service_id = post_data_json.get('service_id', '')

    user_memo_response = _common.get_user_memo(service_id, user_id)
    dict_success = DictSuccess(user_memo_response, message_const.MES_NONE).response
    return jsonify(dict_success)

@mod_user.route('/get-list-card-by-store-id', methods = ['POST'])
@user_auth_required()
def get_list_card_by_store_id(user = None):
    if not user:
        dictFail = DictFail(message_const.TOKEN_EXPIRE, config_const.CODE_FAIL_AUTHORIZED).response
        return jsonify(dictFail)

    user_id = user['id']
    page  = request.args.get('page', 1)
    limit = request.args.get('limit', 20)

    result = request.json if request.json else {}
    if result == {}:
        dictFail = DictFail(message_const.MES_NONE).response
        return jsonify(dictFail)

    store_id = result.get('store_id', '')

    _store = Store()
    _card  = Card()
    _stamp = Stamp()
    resStore = _store.getStoreById(store_id)

    if not resStore:
        dictFail = DictFail(message_const.MES_NONE).response
        return jsonify(dictFail)

    service_company_id = resStore['service_company_id']
    #Devide case to get all card or not

    # Card all
    getTotal = True
    result_number_of_cards = _card.get_card_by_service_company_and_user(service_company_id, user_id, page, limit, getTotal)
    total_number_of_cards = 0
    if not result_number_of_cards:
        dictFail = DictFail(message_const.MES_NONE).response
        return jsonify(dictFail)

    for row in result_number_of_cards:
        total_number_of_cards += 1

    #Card in page
    getTotal = False
    result_card_in_page = _card.get_card_by_service_company_and_user(service_company_id, user_id, page, limit, getTotal)
    number_of_cards_in_page = 0
    if not result_card_in_page:
        dictFail = DictFail(message_const.MES_NONE).response
        return jsonify(dictFail)

    data  = {}
    cards = []
    card  = {}

    for row in result_card_in_page:
        number_of_cards_in_page += 1
        stamps = []
        itemStamp = {}
        card['card_id'] = row['id']
        card['current_number_of_stamps'] = row['current_number_of_stamps']
        card['max_number_of_stamps'] = row['max_number_of_stamps']
        card['card_status'] = row['status']
        # Stamp of card
        lstStamp = _stamp.getStamp(card['card_id'])
        for item in lstStamp:
            itemStamp['stamp_id']           = item['id']
            itemStamp['management_user_id'] = item['management_user_id']
            itemStamp['number_in_card']     = item['number_in_card']
            itemStamp['store_name']         = item['store_name']
            itemStamp['created']            = db_handle.get_timestamp_milis_from_date(item['created'])
            itemStamp['star_review']        = item['star_review']
            itemStamp['text_review']        = item['text_review']
            stamps.append(copy.copy(itemStamp))

        card['stamp'] = stamps

        cards.append(copy.copy(card))

    total_page = int(math.ceil(int(total_number_of_cards) / float(int(limit))))
    #Set data
    data['cards']                   = cards
    data['page']                    = page
    data['total_page']              = total_page
    data['limit']                   = limit
    data['total_number_of_cards']   = total_number_of_cards
    data['number_of_cards_in_page'] = number_of_cards_in_page

    dictSucess = DictSuccess(data, message_const.MES_NONE).response
    return jsonify(dictSucess)


@mod_user.route('/set-memo-user', methods = ['POST'])
@user_auth_required()
def set_memo_user(user = None):
    if not user:
        dict_fail = DictFail(message_const.TOKEN_EXPIRE, config_const.CODE_FAIL_AUTHORIZED).response
        return jsonify(dict_fail)

    result = request.json if request.json else {}
    if result == {}:
        dict_fail = DictFail(message_const.MES_NONE).response
        return jsonify(dict_fail)

    user_id = int(user['id'])
    service_id = int(result.get('service_id', ''))
    user_memo_value = result.get('user_memo_value', '')
    _common = Common()

    if str(service_id) == '':
        dict_fail = DictFail(message_const.MES_NONE).response
        return jsonify(dict_fail)

    response = _common.set_user_memo(service_id, user_id, user_memo_value)

    if response:
        dict_success = DictSuccess(None, message_const.SET_MEMO_DONE).response
        return jsonify(dict_success)
    else:
        dict_fail = DictFail(message_const.MES_NONE).response
        return jsonify(dict_fail)

@mod_user.route('/get-list-company', methods = ['GET'])
@user_auth_required_case_too_many_connection()
def get_list_company(user = None, type_ex = None):

    if not user:
        db_handle.close_connect()
        if type_ex == 1:
            dictFail = DictFail(message_const.TOKEN_EXPIRE, config_const.CODE_FAIL_AUTHORIZED).response
            return jsonify(dictFail)

        dictFail = DictFail(message_const.TOO_MANY_CONNECTION, config_const.CODE_TOO_MANY_CONNECTION).response
        return jsonify(dictFail)

    user_id = user['id']
    app_id  = int(user['app_id'])
    _company = Company()
    _push_notifi = Push_notifi()

    data = {}
    companies = []
    company   = {}
    redis_key = "list_company"
    is_set_cache = False

    try:
        if redis_store.exists(redis_key) and app_id == 3:
            db_handle.close_connect()
            data = redis_store.get(redis_key)

            dictSucess = DictSuccess(json.loads(data), message_const.MES_NONE).response
            return jsonify(dictSucess)

        if app_id != 3 :
            # lst_company = _company.get_list_company_by_user_id(user_id)
            lst_company = _company.get_list_company_new(user_id)
        else:
            is_set_cache = True
            lst_company  = _company.cache_get_list_company_by()

        # lst_company = _company.get_list_company_by_user_id(user_id)
        if not lst_company:
            db_handle.close_connect()
            dictFail = DictFail(message_const.MES_NONE).response
            return jsonify(dictFail)

        # for row in lst_company:
        #     company['service_id']         = row['service_id']
        #     company['service_name']       = row['service_name']
        #     company['company_id']         = row['company_id']
        #     company['service_company_id'] = row['service_company_id']
        #     company['name']               = row['name']
        #     company['logo']               = row['logo']
        #     company['card_name']          = row['card_name'] if int(row['card_type']) !=3 else row['name']
        #     company['available_card']     = int(row['available_card'])
        #     company['service_id']         = row['service_id']
        #     company['card_type']          = row['card_type']
        #     company['unread_push'] = 0
        for row in lst_company:
            company['service_id']         = 0
            company['service_name']       = ''
            company['company_id']         = 0
            company['service_company_id'] = row['service_company_id']
            company['name']               = ''
            company['logo']               = row['logo'] if row['logo'] else ''
            company['card_name']          = row['name'] if row['name'] else ''
            company['available_card']     = 0
            company['service_id']         = 0
            company['card_type']          = 3
            company['unread_push'] = 0

            companies.append(copy.copy(company))

        data['company'] = companies

        if is_set_cache:
            redis_store.set(redis_key, json.dumps(data))

        db_handle.close_connect()
        dictSucess = DictSuccess(data, message_const.MES_NONE).response
        return jsonify(dictSucess)

    except Exception, e:
        LogHandle(e, message_const.ERROR)
        db.session.rollback()
        db_handle.close_connect()
        dictFail = DictFail(message_const.MES_NONE).response
        return jsonify(dictFail)


@mod_user.route('/get-list-card-by-service-company-id', methods = ['POST'])
@user_auth_required()
def get_list_card_by_service_company_id(user = None):
    if not user:
        dictFail = DictFail(message_const.TOKEN_EXPIRE, config_const.CODE_FAIL_AUTHORIZED).response
        return jsonify(dictFail)

    user_id = user['id']
    page  = request.args.get('page', 0)
    limit = request.args.get('limit', 0)

    result = request.json if request.json else {}
    if result == {}:
        dictFail = DictFail(message_const.MES_NONE).response
        return jsonify(dictFail)

    service_company_id = result.get('service_company_id', '')

    #Devide case to get all card or not
    _card = Card()
    _stamp = Stamp()
    # Card all
    getTotal = True
    result_number_of_cards = _card.get_card_by_service_company_and_user(service_company_id, user_id, page, limit, getTotal)
    total_number_of_cards = 0
    if not result_number_of_cards:
        dictFail = DictFail(message_const.MES_NONE).response
        return jsonify(dictFail)

    for row in result_number_of_cards:
        total_number_of_cards += 1

    #Card in page
    getTotal = False
    result_card_in_page = _card.get_card_by_service_company_and_user(service_company_id, user_id, page, limit, getTotal)
    number_of_cards_in_page = 0
    if not result_card_in_page:
        dictFail = DictFail(message_const.MES_NONE).response
        return jsonify(dictFail)

    data  = {}
    cards = []
    card  = {}

    # Get card_type
    m_service_company = M_Service_Company.query.filter_by(id=service_company_id).first()

    if not m_service_company:
        dictFail = DictFail(message_const.SERVICE_COMPANY_NOT_FOUND).response
        return jsonify(dictFail)

    card_type         = m_service_company.card_type
    explanation_stamp = m_service_company.explanation if m_service_company.explanation else ''

    for row in result_card_in_page:
        number_of_cards_in_page += 1
        stamps = []
        itemStamp = {}
        card['card_id'] = row['id']
        card['current_number_of_stamps'] = row['current_number_of_stamps'] if row['current_number_of_stamps'] else 0
        card['max_number_of_stamps'] = row['max_number_of_stamps'] if row['max_number_of_stamps'] else 0
        card['card_status'] = row['status'] if row['status'] else ''
        # Stamp of card
        lstStamp = _stamp.getStamp(card['card_id'])
        for item in lstStamp:
            itemStamp['stamp_id']           = item['id']
            itemStamp['management_user_id'] = item['management_user_id'] if item['management_user_id'] else 0
            itemStamp['number_in_card']     = item['number_in_card'] if item['number_in_card'] else 0
            itemStamp['store_name']         = item['store_name'] if item['store_name'] else ''
            itemStamp['created']            = db_handle.get_timestamp_milis_from_date(item['created']) if item['created'] else ''
            itemStamp['star_review']        = item['star_review'] if item['star_review'] else 0.0
            itemStamp['text_review']        = item['text_review'] if item['text_review'] else ''
            itemStamp['point']              = item['point'] if item['point'] else 0
            stamps.append(copy.copy(itemStamp))

        card['stamp'] = stamps

        cards.append(copy.copy(card))

    total_page = int(math.ceil(int(total_number_of_cards) / float(int(limit))))
    m_service_company = M_Service_Company.query.filter_by(id=service_company_id).first()
    #Set data
    data['cards']                    = cards
    data['card_type']                = card_type
    data['explanation_stamp']        = explanation_stamp
    data['page']                     = int(page)
    data['total_page']               = total_page
    data['limit']                    = limit
    data['total_number_of_cards']    = total_number_of_cards
    data['number_of_cards_in_page']  = number_of_cards_in_page
    data['number_of_stamps_in_card'] = m_service_company.number_of_stamps_in_card if m_service_company.number_of_stamps_in_card else 0

    dictSucess = DictSuccess(data, message_const.MES_NONE).response
    return jsonify(dictSucess)


@mod_user.route('/check-init-user-done', methods = ['GET'])
def check_init_user_done(user = None):

    data = {}

    data['status'] = 'done'
    dictSucess = DictSuccess(data, message_const.MES_NONE).response
    return jsonify(dictSucess)


@mod_user.route('/save-device-token', methods = ['POST'])
@user_auth_required_case_too_many_connection()
def save_device_token(user = None, type_ex = None):

    if not user:
        db_handle.close_connect()
        if type_ex == 1:
            dictFail = DictFail(message_const.TOKEN_EXPIRE, config_const.CODE_FAIL_AUTHORIZED).response
            return jsonify(dictFail)

        dictFail = DictFail(message_const.TOO_MANY_CONNECTION, config_const.CODE_TOO_MANY_CONNECTION).response
        return jsonify(dictFail)

    user_id = user['id']
    result = request.json if request.json else {}
    if result == {}:
        dictFail = DictFail(message_const.MES_NONE).response
        return jsonify(dictFail)

    device_token = result.get('device_token', '')
    device_id    = result.get('device_id', '')
    app_id       = result.get('app_id', 0)

    if not app_id:
        app_id = 3

    mDevice  = M_Device.query.filter(M_Device.device_id == str(device_id), M_Device.app_id == app_id).first()
    if mDevice:
        try:
            mDevice.device_token = device_token
            mDevice.user_id      = user_id
            db.session.commit()
            db_handle.close_connect()
            dictSucess = DictSuccess(None, message_const.MES_NONE).response
            return jsonify(dictSucess)

        except Exception, e:
            LogHandle(e, message_const.ERROR)
            db.session.rollback()
            db_handle.close_connect()
            dictFail = DictFail(message_const.MES_NONE).response
            return jsonify(dictFail)
    try:
        newDevice = M_Device()
        newDevice.device_id    = device_id
        newDevice.device_token = device_token
        newDevice.user_id      = user_id
        newDevice.app_id       = app_id
        db.session.add(newDevice)
        db.session.commit()

        db_handle.close_connect()

        dictSucess = DictSuccess(None, message_const.MES_NONE).response
        return jsonify(dictSucess)

    except Exception, e:
        LogHandle(e, message_const.ERROR)

        db.session.rollback()
        db_handle.close_connect()
        dictFail = DictFail(message_const.MES_NONE).response
        return jsonify(dictFail)


@mod_user.route('/get-service-list', methods = ['GET'])
@user_auth_required()
def get_service_list(user = None):

    if not user:
        dictFail = DictFail(message_const.TOKEN_EXPIRE, config_const.CODE_FAIL_AUTHORIZED).response
        return jsonify(dictFail)

    _service = Service()

    list_service = _service.get_all_service()

    if not list_service:
        dictFail = DictFail(message_const.MES_NONE)
        return jsonify(dictFail)

    data     = {}
    services = []
    service  = {}

    for row in list_service:
        service['id']   = row['id']
        service['name'] = row['name']
        services.append(copy.copy(service))

    data['service'] = services

    dictSucess = DictSuccess(data, message_const.MES_NONE).response
    return jsonify(dictSucess)

@mod_user.route('/get-store-list-of-company-which-user-has-used', methods = ['POST'])
@user_auth_required()
def get_store_list_of_company_which_user_has_used(user = None):

    if not user:
        dictFail = DictFail(message_const.TOKEN_EXPIRE, config_const.CODE_FAIL_AUTHORIZED).response
        return jsonify(dictFail)

    result = request.json if request.json else {}
    if result == {}:
        dictFail = DictFail(message_const.MES_NONE).response
        return jsonify(dictFail)

    latitude  = result.get('latitude', 0.0)
    longitude = result.get('longitude', 0.0)

    user_id            = user['id']
    service_company_id = request.args.get('service_company_id', 0)

    _store = Store()
    _common = Common()
    resStore = _store.get_store_list_of_company_which_user_has_used(user_id, service_company_id)
    if not resStore:
        dictFail = DictFail(message_const.MES_NONE)
        return jsonify(dictFail)

    stores = []
    store  = {}

    for row in resStore:
        store['name']                     = row['name']
        store['address']                  = row['address']
        store['waiting_time']             = int(row['number_of_waiting_people'])*600*1000 if row['number_of_waiting_people'] else 0
        store['number_of_waiting_people'] = int(row['number_of_waiting_people'])
        store['check_in_amount']          = row['check_in_amount']
        store['last_check_in_time']       = db_handle.get_timestamp_milis_from_date(row['last_check_in_time']) \
            if row['last_check_in_time'] else int(0)
        store['latitude']                 = float(row['latitude'])
        store['longitude']                = float(row['longitude'])
        store['distance']                 = count_distance_haversine_formula(float(longitude), float(latitude),
                                                                             float(row['longitude']), float(row['latitude']))

        stores.append(copy.copy(store))

    data = {}
    stores = sorted(stores, key=lambda k: k['distance'], reverse=False)
    data['store_list'] = stores

    dictSucess = DictSuccess(data, message_const.MES_NONE).response
    return jsonify(dictSucess)


def count_distance_haversine_formula(lon1, lat1, lon2, lat2):
    try:
        lon1, lat1, lon2, lat2 = map(radians, [lon1, lat1, lon2, lat2])
        dlon = lon2 - lon1
        dlat = lat2 - lat1
        a = sin(dlat / 2) ** 2 + cos(lat1) * cos(lat2) * sin(dlon / 2) ** 2
        c = 2 * asin(sqrt(a))
        r = config_const.RADIUS_KM
        return c * r
    except Exception, e:
        return 0

@mod_user.route('/review-service-by-stamp', methods = ['POST'])
@user_auth_required()
def review_service_by_stamp(user = None):
    if not user:
        dictFail = DictFail(message_const.TOKEN_EXPIRE, config_const.CODE_FAIL_AUTHORIZED).response
        return jsonify(dictFail)

    result = request.json if request.json else {}
    if result == {}:
        dictFail = DictFail(message_const.MES_NONE).response
        return jsonify(dictFail)

    stamp_id    = int(result.get('stamp_id', 0))
    star_review = float(result.get('star_review', 0.0))
    text_review = str(result.get('text_review', ''))

    _reminder_notification = ReminderNotification()
    _stamp = Stamp()

    if not _stamp.stampExists(stamp_id):
        dictFail = DictFail(message_const.MES_NONE).response
        return jsonify(dictFail)

    resUpdate = _stamp.updateStamp(stamp_id, star_review, text_review)

    # get reminder notification
    reminder_notification = _reminder_notification.get_last_reminder_notification_by_stamp_id(str(stamp_id))
    if reminder_notification:
        _reminder_notification.set_reviewed_status_by_id(reminder_notification['id'])

    if not resUpdate:
        dictFail = DictFail(message_const.MES_NONE).response
        return jsonify(dictFail)
    data = None
    dictSucess = DictSuccess(data, message_const.SUCCESS_MES).response
    return jsonify(dictSucess)

@mod_user.route('/get-info-user', methods = ['GET'])
@user_auth_required_case_too_many_connection()
def get_info_user(user = None, type_ex = None):
    if not user:
        db_handle.close_connect()
        if type_ex == 1:
            dictFail = DictFail(message_const.TOKEN_EXPIRE, config_const.CODE_FAIL_AUTHORIZED).response
            return jsonify(dictFail)

        dictFail = DictFail(message_const.TOO_MANY_CONNECTION, config_const.CODE_TOO_MANY_CONNECTION).response
        return jsonify(dictFail)

    user_id = user['id']

    m_user = M_User.query.filter_by(id = user_id).first()

    data = m_user.to_dict()
    data.pop('created', None)
    data.pop('is_create_push', None)
    data.pop('is_deleted', None)
    data.pop('key_gen', None)
    data.pop('modified', None)
    data.pop('password', None)
    data.pop('reset_password_code', None)
    data.pop('reset_password_required', None)
    data.pop('username', None)

    db_handle.close_connect()
    dictSucess = DictSuccess(data, message_const.SUCCESS_MES).response
    return jsonify(dictSucess)

@mod_user.route('/set-info-user', methods = ['POST'])
@user_auth_required_case_too_many_connection()
def set_info_user(user = None, type_ex = None):
    if not user:
        db_handle.close_connect()
        if type_ex == 1:
            dictFail = DictFail(message_const.TOKEN_EXPIRE, config_const.CODE_FAIL_AUTHORIZED).response
            return jsonify(dictFail)

        dictFail = DictFail(message_const.TOO_MANY_CONNECTION, config_const.CODE_TOO_MANY_CONNECTION).response
        return jsonify(dictFail)

    user_id = user['id']
    user = User()

    result = request.json if request.json else {}
    if result == {}:
        db_handle.close_connect()
        dictFail = DictFail(message_const.MES_NONE).response
        return jsonify(dictFail)

    name     = result.get('name', '')
    email    = result.get('email', '')
    age_avg  = result.get('age_avg', 10)
    sex      = result.get('sex', 1)
    old_pass = result.get('old_pass', '')
    new_pass = result.get('new_pass', '')
    img_url  = result.get('img_url', '')

    if email or email != '':
        resultCheck = user.check_mail_valid(email, user_id)
        if resultCheck == config_const.FORMAT_EMAIL_WRONG:
            db_handle.close_connect()
            dictFail = DictFail(message_const.FORMAT_EMAIL_WRONG).response
            return jsonify(dictFail)

        if resultCheck == config_const.EMAIL_EXIST:
            db_handle.close_connect()
            dictFail = DictFail(message_const.EMAIL_EXIST).response
            return jsonify(dictFail)

    result_change_pass = func_change_password(user_id, old_pass, new_pass)

    if not result_change_pass:
        db_handle.close_connect()
        dict = DictFail(message_const.ERROR_CHANGE_PASSWORD).response
        return jsonify(dict)
    try:
        m_user = M_User.query.filter_by(id = user_id).first()

        if name     : m_user.name    = name
        if email    : m_user.email   = email
        if age_avg  : m_user.age_avg = age_avg
        if sex!= '' : m_user.sex     = sex
        m_user.img_url = img_url

        db.session.commit()
        #Save cache name
        if name:
            redis_key = 'user_name_' + str(user_id)

            if redis_store.exists(redis_key):
                redis_store.delete(redis_key)

            redis_store.set(redis_key, name)


    except Exception, e:
        db.session.rollback()
        dict = DictFail(message_const.MES_NONE).response
        return jsonify(dict)

    finally:
        db_handle.close_connect()

    dictSucess = DictSuccess(None, message_const.SUCCESS_MES).response
    return jsonify(dictSucess)


@mod_user.route('/get-questionnaire-list-user', methods = ['POST'])
@user_auth_required_case_too_many_connection()
def get_questionnaire_list_user(user = None, type_ex = None):
    if not user:
        db_handle.close_connect()
        if type_ex == 1:
            dictFail = DictFail(message_const.TOKEN_EXPIRE, config_const.CODE_FAIL_AUTHORIZED).response
            return jsonify(dictFail)

        dictFail = DictFail(message_const.TOO_MANY_CONNECTION, config_const.CODE_TOO_MANY_CONNECTION).response
        return jsonify(dictFail)

    result = request.json if request.json else {}
    if result == {}:
        dictFail = DictFail(message_const.MES_NONE).response
        return jsonify(dictFail)

    user_id     = user['id']
    _pushNotifi = Push_notifi()
    _store = Store()

    last_user_push_id  = result.get('last_user_push_id', 0)
    limit              = result.get('limit', 5)
    is_search          = result.get('is_search', 0)
    text_search        = result.get('text_search', '')
    service_company_id = result.get('service_company_id', 0)

    try:
        if is_search:
            result_push_in_page = _pushNotifi.search_questionnaire(service_company_id, user_id, last_user_push_id, limit, text_search)
        else:
            result_push_in_page = _pushNotifi.get_questionnaire(service_company_id, user_id, last_user_push_id, limit)

        data  = {}
        pushs = []
        push  = {}

        storeMap = {}
        storeNameMap = {}
        for row in result_push_in_page:

            stores = []
            push['id']           = row['id']
            push['user_push_id'] = row['user_push_id']
            push['title']        = row['title']
            push['description']  = row['description']
            push['send_time']    = db_handle.get_timestamp_milis_from_date(row['send_push_time']) if row['send_push_time'] else 0
            push['content']      = row['content']
            push['stamp_id']     = 0


            type_push = 'action_question_naire'

            push['type'] = type_push
            push['status_read'] = 1

            push['send_time'] = db_handle.get_timestamp_milis_from_date(row['send_push_time']) if row[
                'send_push_time'] else 0

            managerId = row['management_user_id']
            logoStore = ''

            if not (managerId in storeMap):

                lstStore = _store.get_logo_store_by_manager(managerId)

                logoStore = lstStore.logo if lstStore.logo else ''
                storeMap[managerId] = logoStore

                stores.append(lstStore.name)
                storeNameMap[managerId] = stores

            else:
                logoStore = storeMap[managerId]
                stores = storeNameMap[managerId]

            push['logo'] = logoStore
            push['stores'] = stores

            pushs.append(copy.copy(push))

        data['push_notification'] = pushs
        db_handle.close_connect()
        dictSucess = DictSuccess(data, message_const.MES_NONE).response
        return jsonify(dictSucess)

    except Exception,e:
        LogHandle(e, message_const.ERROR)
        db.session.rollback()
        db_handle.close_connect()
        dictFail = DictFail(message_const.MES_NONE).response
        return jsonify(dictFail)

def try_get_user_token():
    if not request.headers['Authorization']:
        return None

    token = request.headers['Authorization']
    _token = Token()
    _user = User()

    # Get info token
    resToken = _token.getTokenByToken(token)

    if resToken:
        _time_now = db_handle.getMilisNow()
        if (_time_now < resToken['expire']):
            user = _user.getUser(resToken['user_id'])
        else:
            # Delete token:
            _token.deleteToken(token)
            return None
    else:
        return None

    return user

@mod_user.route('/get-notification-list-user', methods = ['POST'])
@user_auth_required_case_too_many_connection()
def get_notification_list_user(user = None, type_ex = None):

    if not user:
        db_handle.close_connect()
        if type_ex == 1:
            dictFail = DictFail(message_const.TOKEN_EXPIRE, config_const.CODE_FAIL_AUTHORIZED).response
            return jsonify(dictFail)

        dictFail = DictFail(message_const.TOO_MANY_CONNECTION, config_const.CODE_TOO_MANY_CONNECTION).response
        return jsonify(dictFail)

    result = request.json if request.json else {}

    if result == {}:
        LogHandle(message_const.JSON_NONE, message_const.WARNING)
        dictFail = DictFail(message_const.JSON_NONE).response
        return jsonify(dictFail)

    user_id = user['id']

    try:
        last_user_push_id = result.get('last_user_push_id', 0)
        is_read           = result.get('is_read', 1)
        limit             = result.get('limit', 5)
        is_search         = result.get('is_search', 0)
        text_search       = result.get('text_search', '')
        from_date         = result.get('fromdate', '')
        to_date           = result.get('todate', '')

    except Exception, e:
        push_id = 0
        is_read = 1
        limit   = 5

    _pushNotifi = Push_notifi()
    _store      = Store()

    app_id = int(user['app_id'])
    redis_key = "list_push"
    is_set_cache = False

    try:
        if redis_store.exists(redis_key) and not is_search and not from_date and not last_user_push_id and app_id == 3:
            data = redis_store.get(redis_key)

            db_handle.close_connect()
            dictSucess = DictSuccess(json.loads(data), message_const.MES_NONE).response
            return jsonify(dictSucess)

        get_unread = False if is_read else True

        if is_search:
            result_push_in_page = _pushNotifi.search_push_by_user_with_push_id(user_id, last_user_push_id, get_unread, limit, text_search)
        else:
            if from_date and to_date:
                result_push_in_page = _pushNotifi.get_push_by_user_with_push_id(user_id, last_user_push_id, get_unread, limit, from_date, to_date)
            else:
                if app_id == 3 and not last_user_push_id:
                    is_set_cache = True
                    result_push_in_page = _pushNotifi.get_list_push_cache()
                else:
                    result_push_in_page = _pushNotifi.get_push_by_user_with_push_id(user_id, last_user_push_id,
                                                                                    get_unread, limit, from_date,
                                                                                    to_date)

        if not result_push_in_page:
            db_handle.close_connect()
            LogHandle('not result_push_in_page', message_const.WARNING)
            dictFail = DictFail(message_const.MES_NONE).response
            return jsonify(dictFail)

        data  = {}
        pushs = []
        push  = {}
        # count_unread_push = _pushNotifi.count_unread_push(user_id)
        storeMap     = {}
        storeNameMap = {}
        for row in result_push_in_page:
            stores = []
            push['id']           = row['id']
            push['user_push_id'] = row['id']
            push['title']        = row['title']
            push['content']      = row['content']
            #Temple
            push['stamp_id']     = 0

            type_push = ''
            if int(row['type']) == 1:
                type_push = 'action_push'
            elif int(row['type']) == 2:
                type_push = 'action_remind'
            elif int(row['type']) == 3:
                type_push = 'action_request_review'
            elif int(row['type']) == 4:
                type_push = 'action_question_naire'

            push['type']        = type_push
            push['status_read'] = 1

            push['send_time'] = db_handle.get_timestamp_milis_from_date(row['send_push_time']) if row['send_push_time'] else 0

            managerId = row['management_user_id']
            logoStore = ''

            if not (managerId in storeMap):
                lstStore = _store.get_logo_store_by_manager(managerId)
                if not lstStore:
                    LogHandle('not lstStore', message_const.WARNING)
                    dictFail = DictFail(message_const.MES_NONE).response
                    return jsonify(dictFail)

                logoStore = lstStore.logo if lstStore.logo else ''
                storeMap[managerId] = logoStore

                stores.append(lstStore.name)
                storeNameMap[managerId] = stores

            else:
                logoStore = storeMap[managerId]
                stores = storeNameMap[managerId]

            push['logo']   = logoStore
            push['stores'] = stores

            pushs.append(copy.copy(push))

        data['unread_push']       = 0
        data['push_notification'] = pushs

        if is_set_cache:
            redis_store.set(redis_key, json.dumps(data))

        db_handle.close_connect()
        dictSucess = DictSuccess(data, message_const.MES_NONE).response
        return jsonify(dictSucess)

    except Exception, e:
        LogHandle(e, message_const.ERROR)
        db.session.rollback()
        db_handle.close_connect()
        dictFail = DictFail(message_const.MES_NONE).response
        return jsonify(dictFail)


@mod_user.route('/get-notification-list-user-old', methods = ['POST'])
@user_auth_required()
def get_notification_list_user_old(user = None):

    if not user:
        LogHandle(message_const.TOKEN_EXPIRE, message_const.WARNING)
        dictFail = DictFail(message_const.TOKEN_EXPIRE, config_const.CODE_FAIL_AUTHORIZED).response
        return jsonify(dictFail)

    result = request.json if request.json else {}
    if result == {}:
        LogHandle(message_const.JSON_NONE, message_const.WARNING)
        dictFail = DictFail(message_const.JSON_NONE).response
        return jsonify(dictFail)

    user_id = user['id']

    try:
        last_user_push_id = result.get('last_user_push_id', 0)
        is_read           = result.get('is_read', 1)
        limit             = result.get('limit', 5)
        is_search         = result.get('is_search', 0)
        text_search       = result.get('text_search', '')
        from_date         = result.get('fromdate', '')
        to_date           = result.get('todate', '')

    except Exception, e:
        push_id = 0
        is_read = 1
        limit   = 5

    _pushNotifi = Push_notifi()
    _store      = Store()

    # Push in page

    try:
        get_unread = False if is_read else True

        if is_search:
            result_push_in_page = _pushNotifi.search_push_by_user_with_push_id_old(user_id, last_user_push_id, get_unread, limit, text_search)
        else:
            result_push_in_page = _pushNotifi.get_push_by_user_with_push_id_old(user_id, last_user_push_id, get_unread, limit, from_date, to_date)

        if not result_push_in_page:
            LogHandle('not result_push_in_page', message_const.WARNING)
            dictFail = DictFail(message_const.MES_NONE).response
            return jsonify(dictFail)

        data  = {}
        pushs = []
        push  = {}
        count_unread_push = _pushNotifi.count_unread_push(user_id)

        storeMap     = {}
        storeNameMap = {}
        for row in result_push_in_page:
            stores = []
            push['id']           = row['id']
            push['user_push_id'] = row['user_push_id']
            push['title']        = row['title']
            push['content']      = row['content']
            push['stamp_id']     = row['stamp_id']

            type_push = ''
            if int(row['type']) == 1:
                type_push = 'action_push'
            elif int(row['type']) == 2:
                type_push = 'action_remind'
            elif int(row['type']) == 3:
                type_push = 'action_request_review'
            elif int(row['type']) == 4:
                type_push = 'action_question_naire'

            push['type']        = type_push
            push['status_read'] = int(row['status_read'])

            push['send_time'] = db_handle.get_timestamp_milis_from_date(row['send_push_time']) if row['send_push_time'] else 0

            managerId = row['management_user_id']
            logoStore = ''

            if not (managerId in storeMap):
                lstStore = _store.get_logo_store_by_manager(managerId)
                if not lstStore:
                    LogHandle('not lstStore', message_const.WARNING)
                    dictFail = DictFail(message_const.MES_NONE).response
                    return jsonify(dictFail)

                logoStore = lstStore.logo if lstStore.logo else ''
                storeMap[managerId] = logoStore

                stores.append(lstStore.name)
                storeNameMap[managerId] = stores

            else:
                logoStore = storeMap[managerId]
                stores = storeNameMap[managerId]

            push['logo']   = logoStore
            push['stores'] = stores

            pushs.append(copy.copy(push))

        data['unread_push']       = count_unread_push
        data['push_notification'] = pushs

        db_handle.close_connect()
        dictSucess = DictSuccess(data, message_const.MES_NONE).response
        return jsonify(dictSucess)

    except Exception, e:
        LogHandle(e, message_const.ERROR)
        db.session.rollback()
        db_handle.close_connect()
        dictFail = DictFail(message_const.MES_NONE).response
        return jsonify(dictFail)


@mod_user.route('/update-view-status-notification', methods = ['POST'])
@user_auth_required_case_too_many_connection()
def update_view_status_notification(user = None, type_ex = None):

    if not user:
        db_handle.close_connect()
        if type_ex == 1:
            dictFail = DictFail(message_const.TOKEN_EXPIRE, config_const.CODE_FAIL_AUTHORIZED).response
            return jsonify(dictFail)

        dictFail = DictFail(message_const.TOO_MANY_CONNECTION, config_const.CODE_TOO_MANY_CONNECTION).response
        return jsonify(dictFail)

    result = request.json if request.json else {}
    if result == {}:
        dictFail = DictFail(message_const.MES_NONE).response
        return jsonify(dictFail)

    try:
        pushIdList   = result.get('push_id_list', '')
        type_read    = result.get('type_read', config_const.SEEN)
        user_id      = user['id']
        list_push_id = db_handle.get_list_arr(pushIdList)
        _user_push   = User_Push_Notifi()

        if not list_push_id:
            db_handle.close_connect()
            dictFail = DictFail(message_const.MES_NONE).response
            return jsonify(dictFail)

        list_push_str = db_handle.get_string_from_arr(list_push_id)
        updateRes = _user_push.update_push_read(user_id, list_push_str, int(type_read))

        if not updateRes:
            db_handle.close_connect()
            dictFail = DictFail(message_const.MES_NONE).response
            return jsonify(dictFail)

        db_handle.close_connect()
        dictSucess = DictSuccess(None, message_const.MES_NONE).response
        return jsonify(dictSucess)

    except Exception, e:
        LogHandle(e, message_const.ERROR)
        db.session.rollback()
        db_handle.close_connect()
        dictFail = DictFail(message_const.MES_NONE).response
        return jsonify(dictFail)

@mod_user.route('/get-list-question', methods = ['POST'])
@user_auth_required()
def get_list_question(user = None):
    if not user:
        dictFail = DictFail(message_const.TOKEN_EXPIRE, config_const.CODE_FAIL_AUTHORIZED).response
        return jsonify(dictFail)

    try:
        page  = request.args.get('page', 1)
        limit = request.args.get('limit', 20)
    except:
        page  = 1
        limit = 20

    result = request.json if request.json else {}
    if result == {}:
        dictFail = DictFail(message_const.JSON_NONE).response
        return jsonify(dictFail)

    system = result.get('system', 1)

    _question = Question()

    # Question all
    getTotal = True
    result_number_of_ques = _question.get_question(page, limit, getTotal, system, 0)
    total_number_of_ques = 0
    if not result_number_of_ques:
        dictFail = DictFail(message_const.MES_NONE).response
        return jsonify(dictFail)

    for row in result_number_of_ques:
        total_number_of_ques += 1

    total_page = db_handle.get_total_page(total_number_of_ques, limit)

    # Question in page
    getTotal = False
    result_ques_in_page = _question.get_question(page, limit, getTotal, system, 0)
    number_of_ques_in_page = 0
    if not result_ques_in_page:
        dictFail = DictFail(message_const.MES_NONE).response
        return jsonify(dictFail)

    data   = {}
    titles = []
    title  = {}

    for row in result_ques_in_page:
        number_of_ques_in_page += 1
        title['title']  = row['title']  if row['title'] else ''
        title['answer'] = row['answer'] if row['answer'] else ''
        titles.append(copy.copy(title))

    data['page']                   = int(page)
    data['total_page']             = int(total_page)
    data['limit']                  = int(limit)
    data['total_number_of_ques']   = int(total_number_of_ques)
    data['number_of_ques_in_page'] = int(number_of_ques_in_page)
    data['list_titles']            = titles

    dictSucess = DictSuccess(data, message_const.MES_NONE).response
    return jsonify(dictSucess)


@mod_user.route('/save-user-contact', methods = ['POST'])
@user_auth_required()
def save_user_contact(user = None):
    if not user:
        dictFail = DictFail(message_const.TOKEN_EXPIRE, config_const.CODE_FAIL_AUTHORIZED).response
        return jsonify(dictFail)

    result = request.json if request.json else {}
    if result == {}:
        dictFail = DictFail(message_const.MES_NONE).response
        return jsonify(dictFail)

    info = result.get('info', '')

    user_id = user['id']
    user_name = str(user['name'])
    _user_contact = UserContact()
    _company_email = CompanyEmail()
    _email_handle = EmailHandle()

    is_have_line_break = info.find("\n")

    if is_have_line_break >= 0:
        info = info.replace("\n", "<br>")

    res = _user_contact.create_user_contact(user_id, info)

    if not res:
        dictFail = DictFail(message_const.MES_NONE).response
        return jsonify(dictFail)

    # get email company
    email_info = _company_email.get_company_email()

    # send email contact admin
    email_variables = {
        'email': admin_email.ADMIN_EMAIL,
        'sender': user_name,
        'content': str(info)
    }
    _email_handle.send_email(email_info['email'], email_info['password'],
                             email_info['host'], email_info['port'],
                             email_variables, email_type_const.CONTACT_ADMIN)

    email_variables = {
        'email': str(user['email']),
        'sender': user_name,
        'content': str(info)
    }
    _email_handle.send_email(email_info['email'], email_info['password'],
                             email_info['host'], email_info['port'],
                             email_variables, email_type_const.CONTACT_USER)

    dictSucess = DictSuccess(None, message_const.MES_NONE).response
    return jsonify(dictSucess)

@mod_user.route('/how-use-apps', methods = ['GET'])
@user_auth_required()
def how_use_app(user = None):

    if not user:
        dictFail = DictFail(message_const.TOKEN_EXPIRE, config_const.CODE_FAIL_AUTHORIZED).response
        return jsonify(dictFail)

    data = {}
    content = FormatWeb.get_content_how_use_app().replace('\n','').replace('\t','')
    data['content'] = content

    dictSucess = DictSuccess(data, message_const.MES_NONE).response
    return jsonify(dictSucess)

@mod_user.route('/get-policy', methods = ['GET'])
def get_policy(user = None):
    data = {}
    content = FormatPolicy.get_content_term_of_service().replace('\n', '').replace('\t', '')
    data['content'] = content

    dictSucess = DictSuccess(data, message_const.MES_NONE).response
    return jsonify(dictSucess)

@mod_user.route('/get-term-of-service', methods = ['GET'])
def get_term_of_service(user = None):

    data = {}
    content = FormatTerm.get_content_term_of_service().replace('\n','').replace('\t','')
    data['content'] = content

    dictSucess = DictSuccess(data, message_const.MES_NONE).response
    return jsonify(dictSucess)

@mod_user.route('/get-notification-list-user-unread', methods = ['GET'])
@user_auth_required_case_too_many_connection()
def get_notification_list_user_unread(user = None, type_ex = None):

    if not user:
        db_handle.close_connect()
        if type_ex == 1:
            #token expire
            dictFail = DictFail(message_const.TOKEN_EXPIRE, config_const.CODE_FAIL_AUTHORIZED).response
            return jsonify(dictFail)

        dictFail = DictFail(message_const.TOO_MANY_CONNECTION, config_const.CODE_TOO_MANY_CONNECTION).response
        return jsonify(dictFail)

    user_id = user['id']

    try:
        app_id = int(user['app_id'])
    except:
        app_id = 3

    try:
        page  = request.args.get('page', 1)
        limit = request.args.get('limit', 100)
    except:
        page  = 1
        limit = 100
    try:
        _pushNotifi = Push_notifi()
        _store      = Store()
        _company    = Company()
        # Push all
        result_number_of_pushs = _pushNotifi.get_push_by_user_unread(user_id, page, limit)
        total_number_of_pushs  = 0

        if not result_number_of_pushs:
            db_handle.close_connect()
            dictFail = DictFail(message_const.MES_NONE).response
            return jsonify(dictFail)

        data  = {}
        pushs = []
        push  = {}

        if app_id == 3:
            m_manager = M_Manager_User.query.filter_by(app_id = 3).first()
            if not m_manager:
                db_handle.close_connect()
                dictFail = DictFail(message_const.MES_NONE).response
                return jsonify(dictFail)
            m_store = M_Store.query.filter_by(id = m_manager.store_id).first()
            logo_store = m_store.logo if m_store.logo else ''

        for row in result_number_of_pushs:
            total_number_of_pushs += 1
            push['id']          = row['id']
            push['title']       = row['title']
            push['content']     = row['content']
            push['stamp_id']    = 0

            type_push = ''
            if int(row['type']) == 1:
                type_push = 'action_push'
            elif int(row['type']) == 2:
                type_push = 'action_remind'
            elif int(row['type']) == 3:
                type_push = 'action_request_review'
            elif int(row['type']) == 4:
                type_push = 'action_question_naire'

            push['type']        = type_push
            push['status_read'] = int(row['status_read'])
            push['send_time']   = db_handle.get_timestamp_milis_from_date(row['send_push_time']) if row['send_push_time'] else 0

            logo = ''
            if app_id != 3:
                lstStore = _store.get_store_by_push_id(row['id'])
                i = 0
                for item in lstStore:
                    if i == 0:
                        logo = item['logo'] if item['logo'] else ''
                    i = i + 1

                if int(row['type']) == 2 or int(row['type']) == 3:
                    service_company = M_Service_Company.query.filter_by(id = row['service_company_id']).first()
                    if not service_company:
                        logo = ''
                    else:
                        logo = service_company.logo

            push['logo']   = logo if app_id != 3 else logo_store
            push['stores'] = ''
            pushs.append(copy.copy(push))

        total_page = int(math.ceil(int(total_number_of_pushs) / float(int(limit))))

        data['page']                            = 1
        data['total_page']                      = 1
        data['limit']                           = 100
        data['total_number_of_notifications']   = 1
        data['number_of_notifications_in_page'] = 1
        data['unread_push']                     = int(total_number_of_pushs)
        data['push_notification']               = sorted(pushs, key=lambda k: k['send_time'], reverse=True)

        db_handle.close_connect()
        dictSucess = DictSuccess(data, message_const.MES_NONE).response
        return jsonify(dictSucess)

    except Exception, e:
        LogHandle(e, message_const.ERROR)
        db.session.rollback()
        db_handle.close_connect()
        dictFail = DictFail(message_const.MES_NONE).response
        return jsonify(dictFail)

@mod_user.route('/update-push-notification', methods = ['POST'])
@user_auth_required()
def update_push_notification(user = None):

    if not user:
        dictFail = DictFail(message_const.TOKEN_EXPIRE, config_const.CODE_FAIL_AUTHORIZED).response
        return jsonify(dictFail)

    result = request.json if request.json else {}
    if result == {}:
        dictFail = DictFail(message_const.MES_NONE).response
        return jsonify(dictFail)

    push_id = result.get('push_id', '')

    try:
        M_Push_Notification.query.filter(M_Push_Notification.id == push_id).update({M_Push_Notification.type: 1})
        db.session.commit()
        db_handle.close_connect()
        dictSucess = DictSuccess(None, message_const.MES_NONE).response
        return jsonify(dictSucess)
    except Exception, e:
        LogHandle(e, message_const.ERROR)
        dictFail = DictFail(message_const.MES_NONE).response
        return jsonify(dictFail)


@mod_user.route('/remove-device-token', methods = ['POST'])
@user_auth_required()
def remove_device_token(user=None):
    if not user:
        dict_fail = DictFail(message_const.TOKEN_EXPIRE, config_const.CODE_FAIL_AUTHORIZED).response
        return jsonify(dict_fail)

    result = request.json if request.json else {}
    if result == {}:
        dictFail = DictFail(message_const.JSON_NONE).response
        return jsonify(dictFail)

    device_id = str(result.get('device_id', ''))
    try:
        M_Device.query.filter(M_Device.device_id == device_id).delete()
        db.session.commit()
        db_handle.close_connect()

        dictSucess = DictSuccess(None, message_const.MES_NONE).response
        return jsonify(dictSucess)
    except Exception, e:
        LogHandle(e, message_const.ERROR)
        db.session.rollback()
        db_handle.close_connect()
        dictFail = DictFail(message_const.MES_NONE).response
        return jsonify(dictFail)


@mod_user.route('/check-version-apps-and-server-user', methods=['POST'])
def check_version_app_and_server():
    result = request.json if request.json else {}
    if result == {}:
        dict_fail = DictFail(message_const.JSON_NONE).response
        return jsonify(dict_fail)

    # get post data
    device_id = result.get('device_id')
    current_version = result.get('current_version')
    app_type = result.get('app_type')
    os_type = result.get('os_type')

    newest_version = M_Version.query.filter(M_Version.version_code > current_version, M_Version.type == app_type,
                                            M_Version.os_type == os_type)\
        .order_by(M_Version.version_code.desc()).first()

    server_information = M_Server_Information.query.order_by(M_Server_Information.id.asc()).first()

    if newest_version:
        version_info = {
            'version_code': newest_version.version_code,
            'version_name': newest_version.version_name,
            'is_required': True if newest_version.is_required == 1 else False,
            'is_send_notify': True if newest_version.is_send_notify == 1 else False,
            'change_log': newest_version.change_log,
            'warning': newest_version.warning
        }

        data = {
            'has_update': True
        }
    else:
        newest_version = M_Version.query.filter(M_Version.version_code == current_version, M_Version.type == app_type,
                                                M_Version.os_type == os_type)\
            .first()
        if not newest_version:
            dict_fail = DictFail(message_const.MES_NONE).response
            return jsonify(dict_fail)

        version_info = {
            'version_code': newest_version.version_code,
            'version_name': newest_version.version_name,
            'is_required': True if newest_version.is_required == 1 else False,
            'is_send_notify': True if newest_version.is_send_notify == 1 else False,
            'change_log': newest_version.change_log,
            'warning': newest_version.warning
        }

        data = {
            'has_update': False
        }

    server_info = {
        'status': server_information.status,
        'is_maintain': True if server_information.is_maintain == 1 else False,
        'message': server_information.message
    }

    data['version_info'] = version_info
    data['server_information'] = server_info

    dict_success = DictSuccess(data, message_const.MES_NONE).response
    return jsonify(dict_success)


@mod_user.route('/get-push-notification-by-id', methods = ['POST'])
@user_auth_required_case_too_many_connection()
def get_push_notification_by_id(user = None, type_ex = None):

    if not user:
        db_handle.close_connect()
        if type_ex == 1:
            dictFail = DictFail(message_const.TOKEN_EXPIRE, config_const.CODE_FAIL_AUTHORIZED).response
            return jsonify(dictFail)

        dictFail = DictFail(message_const.TOO_MANY_CONNECTION, config_const.CODE_TOO_MANY_CONNECTION).response
        return jsonify(dictFail)

    result = request.json if request.json else {}
    if result == {}:
        db_handle.close_connect()
        dictFail = DictFail(message_const.JSON_NONE).response
        return jsonify(dictFail)

    push_id = result.get('push_id', 0)

    #get cache
    redis_key = "push_detail_id={}".format(push_id)

    try:
        if redis_store.exists(redis_key):
            db_handle.close_connect()
            data = redis_store.get(redis_key)

            dict_success = DictSuccess(json.loads(data), message_const.MES_NONE).response
            return jsonify(dict_success)

        m_push = M_Push_Notification.query.filter_by(id=int(push_id)).first()

        if not m_push:
            db_handle.close_connect()

            LogHandle('no push ID', message_const.ERROR)
            dictFail = DictFail(message_const.MES_NONE).response
            return jsonify(dictFail)

        data = {}

        data['badge'] = m_push.badge
        data['content'] = m_push.content
        data['created'] = db_handle.get_timestamp_milis_from_date(m_push.created) if m_push.created else 0
        data['id'] = m_push.id
        data['modified'] = db_handle.get_timestamp_milis_from_date(m_push.modified) if m_push.created else 0
        data['number_day_after_check_in'] = m_push.number_day_after_check_in
        data['send_time'] = db_handle.get_timestamp_milis_from_date(m_push.send_time) if m_push.created else 0
        data['sort_description'] = m_push.sort_description if m_push.sort_description else ''
        data['management_user_id'] = m_push.management_user_id
        data['sound'] = m_push.sound
        data['status'] = m_push.status
        data['title'] = m_push.title
        type_push = ''
        type_push_int = int(m_push.type)

        if type_push_int == 1:
            type_push = 'action_push'
        elif type_push_int == 2:
            type_push = 'action_remind'
        elif type_push_int == 3:
            type_push = 'action_request_review'
        elif type_push_int == 4:
            type_push = 'action_question_naire'

        data['type'] = type_push

        #set cache
        redis_store.set(redis_key, json.dumps(data))

        db_handle.close_connect()

        dict_success = DictSuccess(data, message_const.MES_NONE).response
        return jsonify(dict_success)

    except Exception, e:
        LogHandle(e, message_const.ERROR)
        db.session.rollback()
        db_handle.close_connect()
        dictFail = DictFail(message_const.MES_NONE).response
        return jsonify(dictFail)

@mod_user.route('/get-list-push-notification-by-service-company-id', methods = ['POST'])
@user_auth_required_case_too_many_connection()
def get_list_push_notification_by_service_company_id(user = None, type_ex = None):

    if not user:
        if type_ex == 1:
            dictFail = DictFail(message_const.TOKEN_EXPIRE, config_const.CODE_FAIL_AUTHORIZED).response
            return jsonify(dictFail)

        dictFail = DictFail(message_const.TOO_MANY_CONNECTION, config_const.CODE_TOO_MANY_CONNECTION).response
        return jsonify(dictFail)

    _pushNotifi = Push_notifi()
    _store      = Store()

    user_id = user['id']
    result = request.json if request.json else {}
    if result == {}:
        dictFail = DictFail(message_const.JSON_NONE).response
        return jsonify(dictFail)

    service_company_id = result.get('service_company_id', 0)
    last_user_push_id  = result.get('last_user_push_id', 0)
    limit              = result.get('limit', 5)
    is_search          = result.get('is_search', 0)
    text_search        = result.get('text_search', '')
    from_date          = result.get('fromdate', '')
    to_date            = result.get('todate', '')

    # Push all
    try:
        if is_search:
            result_number_of_pushs = _pushNotifi.search_push_by_user_and_service_company_id(user_id, service_company_id,
                                                                                         last_user_push_id, limit, text_search)
        else:
            result_number_of_pushs = _pushNotifi.get_push_by_user_and_service_company_id(user_id, service_company_id, last_user_push_id, limit, from_date, to_date)

        if not result_number_of_pushs:
            LogHandle('not result_number_of_pushs', message_const.ERROR)
            dictFail = DictFail(message_const.MES_NONE).response
            return jsonify(dictFail)

        data  = {}
        pushs = []
        push  = {}
        # count_unread_push = _pushNotifi.count_unread_push(user_id)

        storeMap = {}
        storeNameMap = {}

        for row in result_number_of_pushs:
            stores = []
            push['id']           = row['id']
            push['user_push_id'] = row['id']
            push['title']        = row['title']
            push['content']      = row['content']
            push['stamp_id']     = 0

            type_push = ''
            if int(row['type']) == 1:
                type_push = 'action_push'
            elif int(row['type']) == 2:
                type_push = 'action_remind'
            elif int(row['type']) == 3:
                type_push = 'action_request_review'
            elif int(row['type']) == 4:
                type_push = 'action_question_naire'

            push['type'] = type_push
            push['status_read'] = 1

            push['send_time'] = db_handle.get_timestamp_milis_from_date(row['send_push_time']) if row['send_push_time'] else 0

            managerId = row['management_user_id']
            logoStore = ''

            if not (managerId in storeMap):

                lstStore = _store.get_logo_store_by_manager(managerId)

                logoStore = lstStore.logo if lstStore.logo else ''
                storeMap[managerId] = logoStore

                stores.append(lstStore.name)
                storeNameMap[managerId] = stores

            else:
                logoStore = storeMap[managerId]
                stores = storeNameMap[managerId]

            push['logo']   = logoStore
            push['stores'] = stores
            pushs.append(copy.copy(push))

        data['unread_push'] = 0
        data['push_notification'] = pushs

        dictSucess = DictSuccess(data, message_const.MES_NONE).response
        return jsonify(dictSucess)

    except Exception, e:
        LogHandle(e, message_const.ERROR)
        db.session.rollback()
        db_handle.close_connect()
        dictFail = DictFail(message_const.MES_NONE).response
        return jsonify(dictFail)


@mod_user.route('/get-list-push-notification-by-service-company-id-old', methods = ['POST'])
@user_auth_required()
def get_list_push_notification_by_service_company_id_old(user = None):
    if not user:
        dictFail = DictFail(message_const.USER_NOT_EXISTS).response
        return jsonify(dictFail)

    _pushNotifi = Push_notifi()
    _store      = Store()

    user_id = user['id']
    result = request.json if request.json else {}
    if result == {}:
        dictFail = DictFail(message_const.JSON_NONE).response
        return jsonify(dictFail)

    service_company_id = result.get('service_company_id', 0)
    last_user_push_id  = result.get('last_user_push_id', 0)
    limit              = result.get('limit', 5)
    is_search          = result.get('is_search', 0)
    text_search        = result.get('text_search', '')
    from_date          = result.get('fromdate', '')
    to_date            = result.get('todate', '')

    # Push all
    try:
        if is_search:
            result_number_of_pushs = _pushNotifi.search_push_by_user_and_service_company_id_old(user_id, service_company_id,
                                                                                         last_user_push_id, limit, text_search)
        else:
            result_number_of_pushs = _pushNotifi.get_push_by_user_and_service_company_id_old(user_id, service_company_id, last_user_push_id, limit, from_date, to_date)

        if not result_number_of_pushs:
            dictFail = DictFail(message_const.MES_NONE).response
            return jsonify(dictFail)

        data  = {}
        pushs = []
        push  = {}
        count_unread_push = _pushNotifi.count_unread_push(user_id)

        storeMap = {}
        storeNameMap = {}

        for row in result_number_of_pushs:
            stores = []
            push['id']           = row['id']
            push['user_push_id'] = row['user_push_id']
            push['title']        = row['title']
            push['content']      = row['content']
            push['stamp_id']     = row['stamp_id']

            type_push = ''
            if int(row['type']) == 1:
                type_push = 'action_push'
            elif int(row['type']) == 2:
                type_push = 'action_remind'
            elif int(row['type']) == 3:
                type_push = 'action_request_review'
            elif int(row['type']) == 4:
                type_push = 'action_question_naire'

            push['type'] = type_push
            push['status_read'] = int(row['status_read'])

            push['send_time'] = db_handle.get_timestamp_milis_from_date(row['send_push_time']) if row['send_push_time'] else 0

            managerId = row['management_user_id']
            logoStore = ''

            if not (managerId in storeMap):

                lstStore = _store.get_logo_store_by_manager(managerId)

                logoStore = lstStore.logo if lstStore.logo else ''
                storeMap[managerId] = logoStore

                stores.append(lstStore.name)
                storeNameMap[managerId] = stores

            else:
                logoStore = storeMap[managerId]
                stores = storeNameMap[managerId]

            push['logo']   = logoStore
            push['stores'] = stores
            pushs.append(copy.copy(push))

        data['unread_push'] = count_unread_push
        data['push_notification'] = pushs

        dictSucess = DictSuccess(data, message_const.MES_NONE).response
        return jsonify(dictSucess)

    except Exception, e:
        LogHandle(e, message_const.ERROR)
        db.session.rollback()
        db_handle.close_connect()
        dictFail = DictFail(message_const.MES_NONE).response
        return jsonify(dictFail)


@mod_user.route('/get-code-to-survey', methods = ['POST'])
@user_auth_required()
def get_code_to_survey(user = None):
    if not user:
        LogHandle(message_const.USER_NOT_EXISTS, message_const.WARNING)
        db_handle.close_connect()
        dictFail = DictFail(message_const.USER_NOT_EXISTS).response
        return jsonify(dictFail)

    user_id = user['id']
    result = request.json if request.json else {}
    if result == {}:
        db_handle.close_connect()
        dictFail = DictFail(message_const.JSON_NONE).response
        return jsonify(dictFail)

    try:
        push_id = result.get('push_id', 0)
        m_push = M_Push_Notification.query.filter_by(id = push_id).first()

        if not m_push:
            LogHandle('no push ID', message_const.WARNING)
            db_handle.close_connect()
            dictFail = DictFail(message_const.MES_NONE).response
            return jsonify(dictFail)

        question_naire_id = m_push.question_naires_id
        server_type = os.environ.get('Server_Type')
        data = {}

        _question_naire = M_Question_Naire()

        question_naire = _question_naire.get_question_naire_still_effect(question_naire_id)

        if not question_naire:
            code = 'code_expire_cmnr'

            if server_type == 'DEV':
                url = 'http://54.92.40.147/get-opinion?code=' + code +''
            elif server_type == 'PROD':
                url = 'http://13.113.185.172/get-opinion?code=' + code + ''
            else:
                url = 'http://localhost:8765/get-opinion?code=' + code + ''

            data['code'] = url
            db_handle.close_connect()
            dictSucess = DictSuccess(data, message_const.MES_NONE).response
            return jsonify(dictSucess)

        # check user rate questionaire or not
        m_user_response = M_User_Response.query.filter_by(user_id = user_id, question_naire_id = question_naire_id).first()

        if not m_user_response:
            data['reply'] = 0
        else:
            data['reply'] = 1
        #Get coupon Id
        if question_naire['coupon_id'] and question_naire['coupon_id'] is not None and question_naire['coupon_id'] != '':
            data['coupon_id'] = question_naire['coupon_id']
        else:
            data['coupon_id'] = 0

        encrypt_ques = M_Encrypt_Question.query.filter_by(user_id = user_id, question_naire_id = question_naire_id).first()
        if not encrypt_ques:
            code = str(uuid.uuid4().get_hex().upper()[0:15])

            encrypt_ques = M_Encrypt_Question()

            encrypt_ques.question_naire_id = question_naire_id
            encrypt_ques.user_id           = user_id
            encrypt_ques.code              = code

            db.session.add(encrypt_ques)
            db.session.commit()

            if server_type == 'DEV':
                url = 'http://54.92.40.147/get-opinion?code=' + code +''
            elif server_type == 'PROD':
                url = 'http://13.113.185.172/get-opinion?code=' + code + ''
            else:
                url = 'http://localhost:8765/get-opinion?code=' + code + ''

            data['code'] = url
            db_handle.close_connect()
            dictSucess = DictSuccess(data, message_const.MES_NONE).response
            return jsonify(dictSucess)

        code = encrypt_ques.code

        if server_type == 'DEV':
            url = 'http://54.92.40.147/get-opinion?code=' + code + ''
        elif server_type == 'PROD':
            url = 'http://13.113.185.172/get-opinion?code=' + code + ''
        else:
            url = 'http://localhost:8765/get-opinion?code=' + code + ''

        data['code'] = url
        db_handle.close_connect()
        dictSucess = DictSuccess(data, message_const.MES_NONE).response
        return jsonify(dictSucess)

    except Exception, e:
        LogHandle(e, message_const.ERROR)
        db.session.rollback()
        db_handle.close_connect()
        dictFail = DictFail(message_const.MES_NONE).response
        return jsonify(dictFail)


#TIMELINE USER
@mod_user.route('/get-list-timeline-by-user', methods = ['POST'])
@user_auth_required_case_too_many_connection()
def get_list_timeline_by_user(user = None, type_ex = None):
    if not user:
        db_handle.close_connect()
        if type_ex == 1:
            dictFail = DictFail(message_const.TOKEN_EXPIRE, config_const.CODE_FAIL_AUTHORIZED).response
            return jsonify(dictFail)

        dictFail = DictFail(message_const.TOO_MANY_CONNECTION, config_const.CODE_TOO_MANY_CONNECTION).response
        return jsonify(dictFail)

    user_id = user['id']
    app_id  = int(user['app_id'])

    result = request.json if request.json else {}
    if result == {}:
        db_handle.close_connect()
        dictFail = DictFail(message_const.JSON_NONE).response
        return jsonify(dictFail)

    last_timeline_id = result.get('last_timeline_id', 0)
    limit_timeline   = result.get('limit_timeline', 5)

    list_timeline = []
    data = {}

    redis_key = "list_timeline"
    is_set_cache = False
    try:
        if redis_store.exists(redis_key) and not last_timeline_id and app_id == 3:
            data = redis_store.get(redis_key)

            db_handle.close_connect()
            dictSucess = DictSuccess(json.loads(data), message_const.MES_NONE).response
            return jsonify(dictSucess)

        if app_id != 3 or last_timeline_id:
            lst_store_user_check_in = M_User_Info.query.with_entities(distinct(M_User_Info.store_id).label('store_id'))\
                .filter_by(user_id = user_id).subquery()

            lst_boss = M_Manager_User.query.with_entities(distinct(M_Manager_User.id).label('manager_id')).filter(M_Manager_User.store_id.in_(lst_store_user_check_in)).subquery()

            if last_timeline_id:
                lst_timeline = M_Timeline.query.filter(M_Timeline.management_user_id.in_(lst_boss), M_Timeline.is_deleted == 0, M_Timeline.id < last_timeline_id)\
                    .order_by(M_Timeline.id.desc()).limit(limit_timeline)
            else:
                lst_timeline = M_Timeline.query.filter(M_Timeline.management_user_id.in_(lst_boss), M_Timeline.is_deleted == 0)\
                    .order_by(M_Timeline.id.desc()).limit(limit_timeline)
        else:
            is_set_cache = True
            m_manager = M_Manager_User.query.filter(M_Manager_User.app_id == 3).first()
            lst_timeline = M_Timeline.query.filter_by(management_user_id = m_manager.id).order_by(M_Timeline.id.desc()).limit(limit_timeline)

        one_timeline = {}
        is_normal_user = True
        for timeline in lst_timeline:
            #GET INFO STORE
            manager_name = ''
            img_store    = ''
            m_manager_user = M_Manager_User.query.filter_by(id=timeline.management_user_id).first()
            if m_manager_user:
                m_store = M_Store.query.filter_by(id=m_manager_user.store_id).first()
                if m_store:
                    img_store = m_store.logo if m_store.logo else ''
                    manager_name = m_store.name if m_store.name else ''

            result = StoreController.func_list_like_info_of_timeline_id(timeline.id);
            one_timeline_json = timeline.to_dict()
            one_timeline_json['manager_name'] = manager_name
            one_timeline_json['img_store']    = img_store
            one_timeline_json['number_like']  = result[0]
            # Check like type of current store
            one_timeline_json['self_like_id'] = StoreController.func_get_like_type_of_current_user(user_id, timeline.id, is_normal_user)

            one_timeline['likes'] = result[1]

            one_timeline['timeline'] = one_timeline_json

            list_timeline.append(copy.copy(one_timeline))

        data['list_timeline'] = list_timeline

        if is_set_cache:
            redis_store.set(redis_key, json.dumps(data))

        db_handle.close_connect()
        dictSucess = DictSuccess(data, message_const.MES_NONE).response
        return jsonify(dictSucess)

    except Exception, e:
        LogHandle(e, message_const.ERROR)
        db.session.rollback()
        db_handle.close_connect()
        dictFail = DictFail(message_const.MES_NONE).response
        return jsonify(dictFail)

@mod_user.route('/user-get-list-timeline-by-store-id', methods = ['POST'])
@user_auth_required()
def user_get_list_timeline_by_store_id(user = None):
    if not user:
        db_handle.close_connect()
        dictFail = DictFail(message_const.USER_NOT_EXISTS).response
        return jsonify(dictFail)

    user_id = user['id']
    result = request.json if request.json else {}

    if result == {}:
        db_handle.close_connect()
        dictFail = DictFail(message_const.JSON_NONE).response
        return jsonify(dictFail)

    manager_id       = result.get('manager_id', 0)
    last_timeline_id = result.get('last_timeline_id', 0)
    limit_timeline   = result.get('limit_timeline', 5)

    m_manager = M_Manager_User.query.filter_by(id = manager_id, is_deleted = 0, level_id = 4).first()

    if not m_manager:
        db_handle.close_connect()
        dictFail = DictFail(message_const.MES_NONE).response
        return jsonify(dictFail)

    store_id = m_manager.store_id

    data = {}
    is_normal_user = True

    try:
        data = StoreController.func_get_timeline_by_store(last_timeline_id, limit_timeline, manager_id, user_id, store_id, is_normal_user)
        db_handle.close_connect()
        dict_success = DictSuccess(data, message_const.MES_NONE).response
        return jsonify(dict_success)

    except Exception, e:
        LogHandle(e, message_const.ERROR)
        db.session.rollback()
        db_handle.close_connect()
        dictFail = DictFail(message_const.MES_NONE).response
        return jsonify(dictFail)


@mod_user.route('/get-store-profile', methods=['POST'])
@user_auth_required_case_too_many_connection()
def get_store_profile(user = None, type_ex = None):
    if not user:
        db_handle.close_connect()
        if type_ex == 1:
            dictFail = DictFail(message_const.TOKEN_EXPIRE, config_const.CODE_FAIL_AUTHORIZED).response
            return jsonify(dictFail)

        dictFail = DictFail(message_const.TOO_MANY_CONNECTION, config_const.CODE_TOO_MANY_CONNECTION).response
        return jsonify(dictFail)

    result = request.json if request.json else {}

    if result == {}:
        db_handle.close_connect()
        dictFail = DictFail(message_const.JSON_NONE).response
        return jsonify(dictFail)

    store_id   = result.get('store_id', 0)
    manager_id = result.get('manager_id', 0)

    if manager_id:
        m_manager = M_Manager_User.query.filter_by(id = manager_id, is_deleted = 0, level_id = 4).first()

        if not m_manager:
            db_handle.close_connect()
            dictFail = DictFail(message_const.MES_NONE).response
            return jsonify(dictFail)

        m_store = M_Store.query.filter_by(id = m_manager.store_id).first()

        if not m_store:
            db_handle.close_connect()
            dictFail = DictFail(message_const.MES_NONE).response
            return jsonify(dictFail)

        data = m_store.to_dict()
        data['manager_id'] = manager_id

        db_handle.close_connect()
        dict_success = DictSuccess(data, message_const.MES_NONE).response
        return jsonify(dict_success)

    m_store = M_Store.query.filter_by(id=store_id).first()

    if not m_store:
        db_handle.close_connect()
        dictFail = DictFail(message_const.MES_NONE).response
        return jsonify(dictFail)

    m_manager = M_Manager_User.query.filter_by(store_id=store_id, is_deleted=0, level_id=4).first()

    if not m_manager:
        db_handle.close_connect()
        dictFail = DictFail(message_const.MES_NONE).response
        return jsonify(dictFail)

    data = m_store.to_dict()
    data['manager_id'] = m_manager.id

    db_handle.close_connect()
    dict_success = DictSuccess(data, message_const.MES_NONE).response
    return jsonify(dict_success)


#USER LIKE TIMELINE
@mod_user.route('/user-like-timeline', methods = ['POST'])
@user_auth_required()
def user_like_timeline(user = None):
    if not user:
        db_handle.close_connect()
        dictFail = DictFail(message_const.USER_NOT_EXISTS).response
        return jsonify(dictFail)

    user_id = user['id']
    result = request.json if request.json else {}

    if result == {}:
        dictFail = DictFail(message_const.JSON_NONE).response
        return jsonify(dictFail)

    like_id     = result.get('like_id', 0)
    old_like_id = result.get('old_like_id', 0)
    timeline_id = result.get('timeline_id', 0)
    type_like   = result.get('type_like', 0)

    try:
        normal_user = True
        #Like or unlike
        StoreController.func_like_timeline(type_like, timeline_id, user_id, like_id, normal_user, old_like_id)
        # get timeline
        m_timeline = M_Timeline.query.filter_by(id=timeline_id).first()

        if not m_timeline:
            db_handle.close_connect()
            dictSucess = DictSuccess(None, message_const.MES_NONE).response
            return jsonify(dictSucess)

        # get firebase_api_key
        if m_timeline.app_id is not None and m_timeline.app_id != '':
            m_app = M_App.query.filter_by(id=m_timeline.app_id).first()

            if not m_app:
                db_handle.close_connect()
                dictSucess = DictSuccess(None, message_const.MES_NONE).response
                return jsonify(dictSucess)
            firebase_api_key = m_app.firebase_api_key
        else:
            firebase_api_key = m_timeline.firebase_api_key

        # get store post timeline
        m_manager = M_Manager_User.query.filter_by(id=m_timeline.management_user_id).first()

        if not m_manager:
            db_handle.close_connect()
            dictSucess = DictSuccess(None, message_const.MES_NONE).response
            return jsonify(dictSucess)

        # get token store
        list_device_token_store = StoreController.func_get_list_store_token_by_manager_id(m_manager.id)
        StoreController.send_push_firebase(False, firebase_api_key, list_device_token_store, 'action_like',
                                           timeline_id, user['name'], '', 0)

        dictSucess = DictSuccess(None, message_const.MES_NONE).response
        return jsonify(dictSucess)

    except Exception, e:
        LogHandle(e, message_const.ERROR)
        db.session.rollback()
        db_handle.close_connect()
        dictSucess = DictSuccess(None, message_const.MES_NONE).response
        return jsonify(dictSucess)

#USER COMMENT
@mod_user.route('/user-add-comment', methods = ['POST'])
@user_auth_required_case_too_many_connection()
def user_add_comment(user = None, type_ex = None):
    if not user:
        db_handle.close_connect()
        if type_ex == 1:
            dictFail = DictFail(message_const.TOKEN_EXPIRE, config_const.CODE_FAIL_AUTHORIZED).response
            return jsonify(dictFail)

        dictFail = DictFail(message_const.TOO_MANY_CONNECTION, config_const.CODE_TOO_MANY_CONNECTION).response
        return jsonify(dictFail)

    user_id = user['id']
    result = request.json if request.json else {}

    if result == {}:
        dictFail = DictFail(message_const.JSON_NONE).response
        return jsonify(dictFail)

    content     = result.get('content', '')
    timeline_id = result.get('timeline_id', 0)

    normal_user = True

    #Comment
    try:
        # get timeline
        m_timeline = M_Timeline.query.filter_by(id=timeline_id).first()

        if not m_timeline:
            db_handle.close_connect()
            dictSucess = DictSuccess(None, message_const.MES_NONE).response
            return jsonify(dictSucess)

        # get firebase_api_key
        if m_timeline.app_id is not None and m_timeline.app_id != '':
            m_app = M_App.query.filter_by(id=m_timeline.app_id).first()

            if not m_app:
                db_handle.close_connect()
                dictSucess = DictSuccess(None, message_const.MES_NONE).response
                return jsonify(dictSucess)
            firebase_api_key = m_app.firebase_api_key
        else:
            firebase_api_key = m_timeline.firebase_api_key

        # get store post timeline
        m_manager = M_Manager_User.query.filter_by(id=m_timeline.management_user_id).first()

        if not m_manager:
            db_handle.close_connect()
            dictSucess = DictSuccess(None, message_const.MES_NONE).response
            return jsonify(dictSucess)

        # get token store
        list_device_token_store = StoreController.func_get_list_store_token_by_manager_id(m_manager.id)

        manager_post_timeline = m_manager.name if m_manager.name else message_const.ANONYMOUS.decode('utf-8')

        m_comment = StoreController.func_add_comment(timeline_id, user_id, content, normal_user)

        comment_id = m_comment.id if m_comment.id else 0
        user_comment = m_comment.user_name if m_comment.user_name else message_const.ANONYMOUS.decode('utf-8')
        is_send_for_user = True

        if comment_id :
            list_device_token = StoreController.func_get_list_user_receive_notifi_when_comment(user_id, timeline_id, normal_user)
            # send push user
            StoreController.send_push_firebase(is_send_for_user, firebase_api_key, list_device_token, 'action_comment', int(timeline_id), user_comment, manager_post_timeline, 0)
            StoreController.send_push_firebase(False, firebase_api_key, list_device_token_store, 'action_comment', int(timeline_id), user_comment, manager_post_timeline, 0)

        dictSucess = DictSuccess(None, message_const.MES_NONE).response
        return jsonify(dictSucess)

    except Exception, e:
        LogHandle(e, message_const.ERROR)
        db.session.rollback()
        db_handle.close_connect()
        dictSucess = DictSuccess(None, message_const.MES_NONE).response
        return jsonify(dictSucess)


@mod_user.route('/delete-comment', methods = ['POST'])
def user_delete_comment(user = None):

    result = request.json if request.json else {}

    if result == {}:
        dictFail = DictFail(message_const.JSON_NONE).response
        return jsonify(dictFail)

    comment_id = result.get('comment_id', 0)

    #update count comment
    m_comment = M_Comment.query.filter(M_Comment.id == comment_id).first()
    if not m_comment:
        dictSucess = DictSuccess(None, message_const.MES_NONE).response
        return jsonify(dictSucess)

    timeline_id = m_comment.timeline_id
    m_timeline = M_Timeline.query.filter(M_Timeline.id == timeline_id).first()
    now_count_comment = m_timeline.comment_number - 1

    M_Timeline.query.filter(M_Timeline.id == timeline_id).update({M_Timeline.comment_number : now_count_comment})

    #Comment
    try:
        M_Comment.query.filter(M_Comment.id == comment_id).update({M_Comment.is_deleted : 1})
        db.session.commit()
    except Exception, e:
        LogHandle(e, message_const.ERROR)
        db.session.rollback()
        dictFail = DictFail(message_const.MES_NONE).response
        return jsonify(dictFail)

    dictSucess = DictSuccess(None, message_const.MES_NONE).response
    return jsonify(dictSucess)


@mod_user.route('/user-like-comment', methods = ['POST'])
@user_auth_required()
def user_like_comment(user = None):
    if not user:
        dictFail = DictFail(message_const.USER_NOT_EXISTS).response
        return jsonify(dictFail)

    user_id = user['id']
    result = request.json if request.json else {}

    if result == {}:
        dictFail = DictFail(message_const.JSON_NONE).response
        return jsonify(dictFail)

    like_id     = result.get('like_id', 0)
    comment_id  = result.get('comment_id', 0)
    old_like_id = result.get('old_like_id', 0)
    type_like   = result.get('type_like', 0)

    normal_user = True
    #Like or unlike
    try:
        StoreController.func_like_comment(type_like, comment_id, user_id, like_id, normal_user, old_like_id)
    except Exception, e:
        db.session.rollback()
        db_handle.close_connect()

        dictFail = DictFail(message_const.MES_NONE).response
        return jsonify(dictFail)

    dictSucess = DictSuccess(None, message_const.MES_NONE).response
    return jsonify(dictSucess)


@mod_user.route('/user-get-list-comment-by-timeline-id', methods = ['POST'])
@user_auth_required_case_too_many_connection()
def user_get_list_comment_by_timeline_id(user = None, type_ex = None):
    if not user:
        db_handle.close_connect()
        if type_ex == 1:
            dictFail = DictFail(message_const.TOKEN_EXPIRE, config_const.CODE_FAIL_AUTHORIZED).response
            return jsonify(dictFail)

        dictFail = DictFail(message_const.TOO_MANY_CONNECTION, config_const.CODE_TOO_MANY_CONNECTION).response
        return jsonify(dictFail)

    user_id = user['id']

    result = request.json if request.json else {}
    if result == {}:
        db_handle.close_connect()
        dictFail = DictFail(message_const.MES_NONE).response
        return jsonify(dictFail)

    timeline_id = result.get('timeline_id', 0)
    data = {}
    one_comment = {}
    list_comment = []

    is_normal_user = True

    try:
        comments = M_Comment.query.filter_by(timeline_id = timeline_id, is_deleted = 0).order_by(M_Comment.created.desc()).all()
        for comment in comments:

            result = StoreController.func_list_like_info_of_comment_id(comment.id)
            one_comment_json = comment.to_dict()
            if not comment.management_user_id:
                one_comment_json['management_user_id'] = 0
            if not comment.user_id:
                one_comment_json['user_id'] = 0
            one_comment_json['number_like'] = result[0]
            one_comment_json['img_avatar'] = ''
            if int(one_comment_json['management_user_id']) != 0:
                m_manager = M_Manager_User.query.filter_by(id=one_comment_json['management_user_id']).first()
                if m_manager:
                    m_store = M_Store.query.filter_by(id=m_manager.store_id).first()
                    if m_store:
                        one_comment_json['img_avatar'] = m_store.logo if m_store.logo else ''

            if int(one_comment_json['user_id']) != 0:
                m_user = M_User.query.filter_by(id=one_comment_json['user_id']).first()
                if m_user:
                    one_comment_json['img_avatar'] = m_user.img_url if m_user.img_url else ''
                    one_comment_json['user_name']  = m_user.name if m_user.name else ''

            one_comment['comment'] = one_comment_json
            one_comment['likes'] = result[1]
            one_comment['self_like_id'] = StoreController.func_get_like_type_of_current_user_for_comment(user_id, comment.id, is_normal_user)

            list_comment.append(copy.copy(one_comment))

        data['list_comment'] = list_comment
        db_handle.close_connect()
        dict_success = DictSuccess(data, message_const.MES_NONE).response
        return jsonify(dict_success)

    except Exception, e:
        LogHandle(e, message_const.ERROR)
        db.session.rollback()
        db_handle.close_connect()
        dictFail = DictFail(message_const.MES_NONE).response
        return jsonify(dictFail)


#USER CHAT
@mod_user.route('/user-send-chat', methods = ['POST'])
@user_auth_required()
def user_send_chat(user = None):
    if not user:
        dictFail = DictFail(message_const.USER_NOT_EXISTS).response
        return jsonify(dictFail)

    normal_user_id = user['id']
    result = request.json if request.json else {}

    if result == {}:
        dictFail = DictFail(message_const.JSON_NONE).response
        return jsonify(dictFail)

    store_id  = result.get('store_id', 0)
    chat_info = result.get('chat_info', '')
    img_html  = result.get('img_html', '')
    img       = result.get('img', '')

    is_user_send = 0 # user send
    manager_user_id = StoreController.func_get_manager_user_id_from_store_id(store_id)

    try:
        StoreController.func_create_chat(normal_user_id, manager_user_id,
                                         store_id, chat_info, is_user_send, img_html, img)

        dictSucess = DictSuccess(None, message_const.MES_NONE).response
        db_handle.close_connect()

        return jsonify(dictSucess)

    except Exception, e:
        LogHandle(e, message_const.ERROR)
        db.session.rollback()
        db_handle.close_connect()
        dictFail = DictFail(message_const.MES_NONE).response
        return jsonify(dictFail)

@mod_user.route('/user-send-notifi-chat', methods = ['POST'])
@user_auth_required_case_too_many_connection()
def user_send_notifi_chat(user = None, type_ex=None):
    if not user:
        db_handle.close_connect()
        if type_ex == 1:
            dictFail = DictFail(message_const.TOKEN_EXPIRE, config_const.CODE_FAIL_AUTHORIZED).response
            return jsonify(dictFail)

        dictFail = DictFail(message_const.TOO_MANY_CONNECTION, config_const.CODE_TOO_MANY_CONNECTION).response
        return jsonify(dictFail)
    result = request.json if request.json else {}
    if result == {}:
        dictFail = DictFail(message_const.JSON_NONE).response
        return jsonify(dictFail)

    store_id = result.get('store_id', 0)
    chat_id  = result.get('chat_id', 0)
    try:
        m_manager = M_Manager_User.query.filter_by(store_id=store_id, level_id=4).first()

        if not m_manager:
            LogHandle('no manager', message_const.ERROR)
            dictSucess = DictSuccess(None, message_const.MES_NONE).response
            return jsonify(dictSucess)

        # get token store
        list_device_token_store = StoreController.func_get_list_store_token_by_manager_id(m_manager.id)
        StoreController.send_push_firebase(False, m_manager.firebase_api_key, list_device_token_store, 'action_chat',
                                           chat_id, user['name'], '', int(user['id']))

        dictSucess = DictSuccess(None, message_const.MES_NONE).response
        db_handle.close_connect()

        return jsonify(dictSucess)

    except Exception, e:
        LogHandle(e, message_const.ERROR)
        db.session.rollback()
        db_handle.close_connect()
        dictSucess = DictSuccess(None, message_const.MES_NONE).response
        db_handle.close_connect()

        return jsonify(dictSucess)

@mod_user.route('/get-list-store-following-default', methods = ['GET'])
@user_auth_required_case_too_many_connection()
def get_list_store_following_default(user = None, type_ex = None):
    if not user:
        db_handle.close_connect()
        if type_ex == 1:
            dictFail = DictFail(message_const.TOKEN_EXPIRE, config_const.CODE_FAIL_AUTHORIZED).response
            return jsonify(dictFail)

        dictFail = DictFail(message_const.TOO_MANY_CONNECTION, config_const.CODE_TOO_MANY_CONNECTION).response
        return jsonify(dictFail)

    normal_user_id = user['id']
    app_id         = int(user['app_id'])

    list_store_following = []
    data = {}
    one_store_json = {}

    redis_key    = "list_store_following_default"
    is_set_cache = False

    try:
        if redis_store.exists(redis_key) and app_id == 3:

            data = redis_store.get(redis_key)

            db_handle.close_connect()
            dictSucess = DictSuccess(json.loads(data), message_const.MES_NONE).response
            return jsonify(dictSucess)

        if app_id != 3:
            m_lst_store_id_following = M_User_Info.query.with_entities(distinct(M_User_Info.store_id).label('store_id')) \
                .filter_by(user_id=normal_user_id).order_by(M_User_Info.modified.desc()).all()

            for m_store in m_lst_store_id_following:

                store = M_Store.query.filter_by(id = m_store.store_id, is_deleted = 0).first()
                if store:
                    manager_user_id = StoreController.func_get_manager_user_id_from_store_id(store.id)
                    one_store_json['manager_id']         = manager_user_id
                    one_store_json['id']                 = store.id
                    one_store_json['name']               = store.name
                    one_store_json['service_company_id'] = store.service_company_id
                    one_store_json['logo']               = store.logo

                    list_store_following.append(copy.copy(one_store_json))

        else:
            is_set_cache = True
            list_store_id = M_Manager_User.query.with_entities(distinct(M_Manager_User.store_id).label('store_id')).filter(M_Manager_User.app_id == 3).subquery()

            m_lst_store_id_following = M_Store.query.filter(M_Store.id.in_(list_store_id)).all()

            for store in m_lst_store_id_following:
                manager_user_id = StoreController.func_get_manager_user_id_from_store_id(store.id)

                one_store_json['manager_id']         = manager_user_id
                one_store_json['id']                 = store.id
                one_store_json['name']               = store.name
                one_store_json['service_company_id'] = store.service_company_id
                one_store_json['logo']               = store.logo

                list_store_following.append(copy.copy(one_store_json))

        data['stores_follow'] = list_store_following

        if is_set_cache:
            redis_store.set(redis_key, json.dumps(data))

        db_handle.close_connect()
        dictSucess = DictSuccess(data, message_const.MES_NONE).response
        return jsonify(dictSucess)

    except Exception, e:
        LogHandle(e, message_const.ERROR)
        db.session.rollback()
        db_handle.close_connect()
        dictFail = DictFail(message_const.MES_NONE).response
        return jsonify(dictFail)


@mod_user.route('/get-list-store-following', methods = ['GET'])
@user_auth_required_case_too_many_connection()
def get_list_store_following(user = None, type_ex = None):

    if not user:
        db_handle.close_connect()
        if type_ex == 1:
            dictFail = DictFail(message_const.TOKEN_EXPIRE, config_const.CODE_FAIL_AUTHORIZED).response
            return jsonify(dictFail)

        dictFail = DictFail(message_const.TOO_MANY_CONNECTION, config_const.CODE_TOO_MANY_CONNECTION).response
        return jsonify(dictFail)

    normal_user_id     = user['id']
    app_id             = int(user['app_id'])

    list_store_following = []
    data = {}
    one_store_json = {}

    redis_key    = "list_store_following"
    is_set_cache = False

    try:
        if redis_store.exists(redis_key) and app_id == 3:

            data = redis_store.get(redis_key)

            db_handle.close_connect()
            dictSucess = DictSuccess(json.loads(data), message_const.MES_NONE).response
            return jsonify(dictSucess)

        if app_id != 3:

            m_lst_store_id_following = M_User_Info.query.with_entities(distinct(M_User_Info.store_id).label('store_id'))\
                .filter_by(user_id = normal_user_id).order_by(M_User_Info.modified.desc()).all()

            for m_store in m_lst_store_id_following:

                store = M_Store.query.filter_by(id = m_store.store_id, is_deleted = 0).first()
                if store:
                    one_store_json['id']   = store.id
                    one_store_json['name'] = store.name
                    one_store_json['img_store'] = store.logo if store.logo else ""
                    arr = StoreController.func_get_last_message(normal_user_id, store.id)
                    one_store_json['last_message']      = arr[0]
                    one_store_json['last_message_time'] = arr[1]

                    list_store_following.append(copy.copy(one_store_json))

        else:
            is_set_cache = True
            list_store_id = M_Manager_User.query.with_entities(distinct(M_Manager_User.store_id).label('store_id')).filter(M_Manager_User.app_id == 3).subquery()

            m_lst_store_id_following = M_Store.query.filter(M_Store.id.in_(list_store_id)).all()

            for store in m_lst_store_id_following:
                one_store_json['id'] = store.id
                one_store_json['name'] = store.name
                one_store_json['img_store'] = store.logo if store.logo else ""
                arr = StoreController.func_get_last_message(normal_user_id, store.id)
                one_store_json['last_message'] = arr[0]
                one_store_json['last_message_time'] = arr[1]

                list_store_following.append(copy.copy(one_store_json))

        data['stores_follow'] = list_store_following

        if is_set_cache:
            redis_store.set(redis_key, json.dumps(data))

        db_handle.close_connect()
        dictSucess = DictSuccess(data, message_const.MES_NONE).response
        return jsonify(dictSucess)

    except Exception, e:
        LogHandle(e, message_const.ERROR)
        db.session.rollback()
        db_handle.close_connect()
        dictFail = DictFail(message_const.MES_NONE).response
        return jsonify(dictFail)

@mod_user.route('/get-list-chat-history-for-user', methods = ['POST'])
@user_auth_required()
def get_list_chat_history_for_user(user = None):
    if not user:
        db_handle.close_connect()
        dictFail = DictFail(message_const.USER_NOT_EXISTS).response
        return jsonify(dictFail)

    normal_user_id = user['id']

    result = request.json if request.json else {}
    data = {}

    list_message = []

    if result == {}:
        db_handle.close_connect()
        dictFail = DictFail(message_const.JSON_NONE).response
        return jsonify(dictFail)

    store_id     = result.get('store_id', 0)
    last_chat_id = result.get('last_chat_id', 0)
    limit        = result.get('limit', 5)

    data['store_id'] = int(store_id)
    data['user_id']  = int(normal_user_id)
    management_user_id = 0

    try:
        if not last_chat_id:
            lst_chat = M_Chat.query.filter_by(user_id = normal_user_id, store_id = store_id)\
                .order_by(M_Chat.id.desc()).limit(limit).all()
        else:
            lst_chat = M_Chat.query.filter(M_Chat.user_id == normal_user_id, M_Chat.store_id == store_id, M_Chat.id < last_chat_id) \
                .order_by(M_Chat.id.desc()).limit(limit).all()

        if not lst_chat:
            data['list_message'] = list_message
            data['management_user_id'] = management_user_id

            db_handle.close_connect()
            dictSucess = DictSuccess(data, message_const.MES_NONE).response
            return jsonify(dictSucess)

        for chat in lst_chat:
            management_user_id = chat.management_user_id if chat.management_user_id else 0
            chat_json = chat.to_dict()
            list_message.append(copy.copy(chat_json))

        data['list_message']       = list_message
        data['management_user_id'] = management_user_id

        db_handle.close_connect()
        dictSucess = DictSuccess(data, message_const.MES_NONE).response
        return jsonify(dictSucess)

    except Exception, e:
        LogHandle(e, message_const.ERROR)
        db.session.rollback()
        db_handle.close_connect()
        dictFail = DictFail(message_const.MES_NONE).response
        return jsonify(dictFail)


#Coupon
@mod_user.route('/get-list-coupon-by-user', methods = ['POST'])
@user_auth_required()
def get_list_coupon_by_user(user = None):
    if not user:
        dictFail = DictFail(message_const.USER_NOT_EXISTS).response
        return jsonify(dictFail)

    normal_user_id = user['id']
    result = request.json if request.json else {}
    data            = {}
    list_coupon_arr = []

    if result == {}:
        dictFail = DictFail(message_const.JSON_NONE).response
        return jsonify(dictFail)

    last_coupon_id = result.get('last_coupon_id', 0)
    limit          = result.get('limit', 5)
    is_used        = result.get('is_used', 0)  #0- chua dung, 1- da dung

    try:
        if last_coupon_id:
            if is_used:
                list_coupon = M_User_Coupon.query.filter(M_User_Coupon.user_id == normal_user_id,
                            M_User_Coupon.id < last_coupon_id, or_(M_User_Coupon.is_used == is_used,
                                                                   M_User_Coupon.time_end_coupon < db_handle.get_full_current_jav_time()))\
                            .order_by(M_User_Coupon.id.desc()).limit(limit).all()
            else:
                list_coupon = M_User_Coupon.query.filter(M_User_Coupon.user_id == normal_user_id,
                                                         M_User_Coupon.id < last_coupon_id,
                                                         M_User_Coupon.is_used == is_used,
                                                         M_User_Coupon.time_end_coupon >= db_handle.get_full_current_jav_time()) \
                    .order_by(M_User_Coupon.id.desc()).limit(limit).all()

        else:
            if is_used:
                list_coupon = M_User_Coupon.query.filter(M_User_Coupon.user_id == normal_user_id,
                                                         or_(M_User_Coupon.is_used == is_used,
                                                             M_User_Coupon.time_end_coupon < db_handle.get_full_current_jav_time()))\
                    .order_by(M_User_Coupon.id.desc()).limit(limit).all()
            else:
                list_coupon = M_User_Coupon.query.filter(M_User_Coupon.user_id == normal_user_id,
                                                         M_User_Coupon.is_used == is_used,
                                                         M_User_Coupon.time_end_coupon >= db_handle.get_full_current_jav_time()) \
                    .order_by(M_User_Coupon.id.desc()).limit(limit).all()

        for m_user_coupon in list_coupon:
            m_coupon_json = m_user_coupon.to_dict()

            m_coupon =  M_Coupon.query.filter_by(id = m_user_coupon.coupon_id).first()
            m_coupon_json['time_start_coupon'] = db_handle.get_timestamp_milis_from_date(m_coupon.time_start_coupon)

            list_coupon_arr.append(copy.copy(m_coupon_json))

        data['list_coupons'] = list_coupon_arr

        dictSucess = DictSuccess(data, message_const.MES_NONE).response
        return jsonify(dictSucess)

    except Exception, e:
        LogHandle(e, message_const.ERROR)
        db.session.rollback()
        db_handle.close_connect()
        dictFail = DictFail(message_const.MES_NONE).response
        return jsonify(dictFail)

@mod_user.route('/use-coupon', methods = ['POST'])
@user_auth_required()
def use_coupon(user = None):
    if not user:
        dictFail = DictFail(message_const.USER_NOT_EXISTS).response
        return jsonify(dictFail)

    normal_user_id = user['id']

    result = request.json if request.json else {}

    if result == {}:
        dictFail = DictFail(message_const.JSON_NONE).response
        return jsonify(dictFail)

    coupon_id    = result.get('coupon_id', 0)
    is_use_again = result.get('is_use_again', 0)

    m_user_coupon = M_User_Coupon.query.filter_by(user_id = normal_user_id, coupon_id = coupon_id).first()

    if not m_user_coupon:
        dictSucess = DictSuccess(None, message_const.MES_NONE).response
        return jsonify(dictSucess)

    try:
        if not is_use_again:
            m_user_coupon.is_used = 1
        db.session.commit()
        func_update_number_use_coupon(coupon_id)

    except Exception, e:
        LogHandle(e, message_const.ERROR)
        db.session.rollback()
    finally:
        db_handle.close_connect()

    dictSucess = DictSuccess(None, message_const.MES_NONE).response
    return jsonify(dictSucess)

def func_update_number_use_coupon(coupon_id):
    count_use = M_User_Coupon.query.filter_by(coupon_id = coupon_id, is_used = 1).count()

    m_coupon = M_Coupon.query.filter_by(id = coupon_id).first();

    if not m_coupon:
        return

    m_coupon.number_use_coupon = count_use
    db.session.commit()

#view-coupon
@mod_user.route('/view-coupon', methods = ['POST'])
@user_auth_required()
def view_coupon(user = None):
    if not user:
        dictFail = DictFail(message_const.USER_NOT_EXISTS).response
        return jsonify(dictFail)

    normal_user_id = user['id']

    result = request.json if request.json else {}

    if result == {}:
        dictFail = DictFail(message_const.JSON_NONE).response
        return jsonify(dictFail)

    coupon_id = result.get('coupon_id', 0)

    try:
        m_coupon = M_Coupon.query.filter_by(id = coupon_id).first()

        if not m_coupon:
            LogHandle('not coupon', message_const.ERROR)
            dictSucess = DictSuccess(None, message_const.MES_NONE).response
            return jsonify(dictSucess)

        try:
            m_coupon.number_view_coupon = m_coupon.number_view_coupon + 1
            db.session.commit()
        except Exception, e:
            print e
            db.session.rollback()
        finally:
            db_handle.close_connect()

        dictSucess = DictSuccess(None, message_const.MES_NONE).response
        return jsonify(dictSucess)

    except Exception, e:
        LogHandle(e, message_const.ERROR)
        db.session.rollback()
        db_handle.close_connect()
        dictFail = DictFail(message_const.MES_NONE).response
        return jsonify(dictFail)

#count-user-has-receive-coupon
#web-admin

@mod_user.route('/insert-list-push', methods = ['POST'])
def insert_list_push(user = None):
    result    = request.json if request.json else {}
    store_id  = result.get('store_id', 0)
    data_json = result.get('data_json', '')

    manager_id = get_manager_id_from_store_id(store_id)

    if not manager_id:
        dictFail = DictFail('Store does not exists').response
        return jsonify(dictFail)
    try:
        for push in data_json:
            push_id = create_new_push(push['old_push_id'], push['title'], push['content'], push['sort_description'], push['send_time'], manager_id)
            create_store_push(store_id, push_id)
        db.session.commit()

        dictSucess = DictSuccess('Insert done', message_const.MES_NONE).response
        return jsonify(dictSucess)

    except Exception, e:
        print e
        db.session.rollback()
        db_handle.close_connect()
        dictFail = DictFail(message_const.MES_NONE).response
        return jsonify(dictFail)

    return 'Done'


@mod_user.route('/get-list-push-by-list-old', methods = ['POST'])
def get_list_push_by_list_old(user = None):
    result = request.json if request.json else {}
    data = {}

    list_message = []

    if result == {}:
        dictFail = DictFail(message_const.JSON_NONE).response
        return jsonify(dictFail)

    pushIdList = result.get('push_id_list', '')

    data  = {}
    pushs = []
    push  = {}

    list_push_id = db_handle.get_list_arr(pushIdList)
    for old_push_id in list_push_id:
        m_push = M_Push_Notification.query.filter_by(old_push_id = old_push_id).first()
        if not m_push:
            continue
        pushs.append(copy.copy(m_push.to_dict()))

    data['push_notification'] = pushs

    dictSucess = DictSuccess(data, message_const.MES_NONE).response
    return jsonify(dictSucess)


def create_store_push(store_id, push_id):
    m_store_push = M_Store_Push_Notifi()
    m_store_push.push_notification_id  = push_id
    m_store_push.store_id              = store_id
    db.session.add(m_store_push)


@mod_user.route('/test', methods = ['POST'])
def test(user = None):
    return 0

def create_new_push(old_push_id = None, title = None, content = None, sort_description = None, send_time = None, manager_id = None):
    m_push = M_Push_Notification()

    m_push.send_time          = send_time
    m_push.title              = title
    m_push.content            = content
    m_push.sort_description   = sort_description
    m_push.management_user_id = manager_id
    m_push.old_push_id        = old_push_id
    m_push.status             = 5

    db.session.add(m_push)
    db.session.commit()
    return m_push.id

def get_manager_id_from_store_id(store_id):
    m_manager = M_Manager_User.query.filter_by(store_id = store_id, is_deleted = 0, level_id = 4).first()

    if not m_manager:
        return None

    return m_manager.id

aaa = 1

@mod_user.route('/set_global', methods = ['GET'])
def set_global():
    global aaa
    aaa = 10
    return 'done'

@mod_user.route('/get_global', methods = ['GET'])
def get_global():
    print aaa
    return 'done'

@mod_user.route('/test-connection-sss', methods = ['GET'])
# @user_auth_required()
def test_connection(user = None):
    try:
        i = 0
        m_user = M_User.query.all()
        return 'done'

    except Exception,e:
        print e
        LogHandle(e, message_const.ERROR)
        return 'faillll'

@mod_user.route('/test-cache-token', methods = ['GET'])
@user_auth_required_case_too_many_connection()
def test_cache_token(user = None, type_ex= None):
    return 'done'

@mod_user.route('/test-connection', methods = ['GET'])
def test_new_connection(user = None):
    try:
        global count
        count += 1
        # m_user = M_User.query.first()

        sql = 'select * from users limit 1'
        db_handle.executeCommand(sql)
        db_handle.close_connect()



        # dbconfig = {
        #     "host": "point.crygr0uwyp5o.ap-northeast-1.rds.amazonaws.com",
        #     "database": "point",
        #     "user": "point",
        #     "password": "VN201501"
        # }
        # cnxpool = mysql.connector.connect(pool_name="pool_db_2",
        #                                                       pool_size=10,
        #                                                       **dbconfig)

        # cnxpool = mysql.connector.pooling.MySQLConnectionPool(pool_name = "pool_db",
        #                                                       pool_size = 20,
        #                                                       **dbconfig)
        # cnx1 = cnxpool.get_connection()

        # c = cnxpool.cursor()
        # sql = 'select * from users'
        # c.execute(sql)
        #
        #
        # result = c.fetchall()

        # cnxpool.close()


        return str(count)

    except Exception,e:
        print  e
        LogHandle(e, message_const.ERROR)
        return 'fail'


