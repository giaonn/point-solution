from func.base_model import Base
from func import db, db_handle

class M_User(Base):

    __tablename__ = 'users'

    username                = db.Column(db.String(255), nullable=True)
    email                   = db.Column(db.String(255), nullable=True)
    password                = db.Column(db.String(255), nullable=True)
    name                    = db.Column(db.String(255), nullable=True)
    reset_password_required = db.Column(db.Integer, nullable=True, default=0)
    app_id                  = db.Column(db.Integer, nullable=True)
    is_create_push          = db.Column(db.Integer, nullable=True, default=1)
    is_deleted              = db.Column(db.Integer, nullable=True, default=0)
    age_avg                 = db.Column(db.Integer, nullable=True, default=10)
    sex                     = db.Column(db.Integer, nullable=True, default=1)
    reset_password_code     = db.Column(db.String(50), nullable=True)
    img_url                 = db.Column(db.String(512), nullable=True)

    def to_dict(self, exclude = None, expand = False):
        user_dict = super(M_User, self).to_dict(exclude=exclude)

        if expand:
            user_dict['devices'] = []
            for device in  self.devices:
                device_dict = device.to_dict()
                user_dict['devices'].append(device_dict)

        return user_dict


    def __init__(self):
        super(M_User, self).__init__()



