from func import db, table_name, db_handle
from datetime import datetime as dt
from func import config_const
import re
import hashlib, pytz


class ManagerUser:

    def registerUser(self, username = None, password = None, name = None, email = None):
        created = modified = db_handle.get_full_current_jav_time()

        sql = db_handle.INSERT + table_name.MANAGEMENT_USER + db_handle.LIST_MANAGER_USER_ATTRIBUTE + ' values ' \
              '(%s , %s, %s , %s, %s , %s, %s, %s, %s, %s, %s)'

        params = (username, password, name, email, created, modified, 3, 1, 1, 1, 0)

        try:
            db.engine.execute(sql, params)
            return True
        except:
            return False

    def checkUserInfo(self, username = None, password = None, name = None, email = None):
        if not username or not password or not name or not email:
            return config_const.INFO_NULL

        if len(password)<8 or len(password)>14:
            return config_const.LENGTH_PASS_WRONG

        if not re.match('^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$', email):
            return config_const.FORMAT_EMAIL_WRONG

        if self.checkUserExists(username):
            return config_const.USER_EXISTS

    def checkUserExists(self, username):
        username = "'" + username + "'"
        sql = 'select * from ' + table_name.MANAGEMENT_USER + ' where username = ' + username

        result = db.engine.execute(sql)

        count = 0

        for row in result:
            count+=1
        if count>0:
            return True
        return False

    def loginSuccess(self, username, password):
        username = "'" + username + "'"
        password = "'" + password + "'"

        sql = 'select * from ' + table_name.MANAGEMENT_USER + ' where username = ' + username +\
              ' and password =' + password + ' and is_deleted = 0 and (level_id = 4 or level_id = 5)'

        return db_handle.executeCommand(sql)

    def getUser(self, user_id):
        sql = 'select * from ' + table_name.MANAGEMENT_USER + ' where id = ' + str(user_id) + ' AND is_deleted = 0'
        return db_handle.executeCommand(sql)

    @staticmethod
    def encodePass(password):
        salt = "b50b1ffe2e7320b0d97062a9663d47a7adf1379392c58c66fd978171a2be7d65"
        return hashlib.md5(salt + password).hexdigest()

    def get_manager_user_by_store(self, store_id, level_id):
        sql = db_handle.SELECT + table_name.MANAGEMENT_USER + ' where store_id = ' \
              + str(store_id) + ' and level_id = ' + str(level_id)
        return db_handle.executeCommandMoreRow(sql)

    def get_staff_name_by_id(self, id):
        sql = 'SELECT name from ' + table_name.MANAGEMENT_USER + ' WHERE id = ' + \
              str(id) + ' AND level_id = 5 AND is_deleted = 0'
        return db_handle.executeCommand(sql)

