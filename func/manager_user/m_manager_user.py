from func.base_model import Base
from func import db, db_handle

class M_Manager_User(Base):

    __tablename__ = 'management_users'

    username                  = db.Column(db.String, nullable=True)
    email                     = db.Column(db.String, nullable=True)
    password                  = db.Column(db.String, nullable=True)
    name                      = db.Column(db.String, nullable=True)
    phone                     = db.Column(db.String, nullable=True)
    level_id                  = db.Column(db.Integer, nullable=True)
    store_id                  = db.Column(db.Integer, nullable=True)
    company_id                = db.Column(db.Integer, nullable=True)
    service_company_id        = db.Column(db.Integer, nullable=True)
    parent_management_user_id = db.Column(db.Integer, nullable=True)
    is_deleted                = db.Column(db.Integer, nullable=True)
    last_login                = db.Column(db.DateTime, nullable=True)
    app_id                    = db.Column(db.Integer, nullable=True)
    firebase_api_key          = db.Column(db.Text, nullable=True)

    def __init__(self):
        super(M_Manager_User, self).__init__()




