from func import db, table_name, db_handle
from datetime import datetime as dt
from func import config_const

class Question():
    def get_question(self, page, limit, getTotal, system, manager_type):
        startPoint = db_handle.getStartPoint(page, limit)

        if not getTotal:
            sql = db_handle.SELECT + table_name.QUESTION_USER + ' where manager_type = ' + str(manager_type) \
                  + ' and (system = 1 or system = ' + str(system) + ') order by created asc '\
                  ' limit ' + str(startPoint) + ', ' + str(limit)
        else:
            sql = db_handle.SELECT + table_name.QUESTION_USER + ' where manager_type = ' + str(manager_type) \
                  + ' and (system = 1 or system = ' + str(system) + ') order by created asc '

        return db_handle.executeCommandMoreRow(sql)