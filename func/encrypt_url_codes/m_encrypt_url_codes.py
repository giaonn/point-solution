from func.base_model import Base
from func import db, db_handle

class M_Encrypt_Url_Codes(Base):
    __tablename__ = 'encrypt_url_codes'

    store_id             = db.Column(db.Integer, nullable=True)
    push_notification_id = db.Column(db.Integer, nullable=True)
    code                 = db.Column(db.String, nullable=True)

    def __init__(self):
        super(M_Encrypt_Url_Codes, self).__init__()
