from func.base_model import Base
from func import db, db_handle

class M_Rich_Message(Base):

    __tablename__ = 'rich_messages'

    img_rich           = db.Column(db.Text, nullable=True)
    title              = db.Column(db.Text, nullable=True)
    img_html           = db.Column(db.Text, nullable=True)
    type_format        = db.Column(db.Integer, nullable=True, default=1)
    json_content       = db.Column(db.Text, nullable=True)
    management_user_id = db.Column(db.Integer, nullable=True)


    def __init__(self):
        super(M_Rich_Message, self).__init__()


