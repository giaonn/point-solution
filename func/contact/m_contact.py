from func.base_model import Base
from func import db, db_handle

class M_Contact(Base):
    __tablename__ = 'contacts'

    user_id  = db.Column(db.Integer, nullable=True)
    store_id = db.Column(db.Integer, nullable=True)
    info     = db.Column(db.Text, nullable=True)

    def __init__(self):
        super(M_Contact, self).__init__()

    def __repr__(self):
        return '<M_Contact %r>' % (self.id)


