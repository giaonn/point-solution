from func import db, table_name, db_handle
from datetime import datetime as dt
from func import config_const
import re
import hashlib, pytz

class Service_Company:

    def get_service_from_service_company(self, service_company_id):
        sql = 'select * from ' + table_name.SERVICE_COMPANY + ' where id = ' + str(service_company_id)
        return db_handle.executeCommand(sql)

    def set_ask_to_review_info_by_service_company_id(self, service_company_id, ask_to_review_title, ask_to_review_content, ask_to_review_day_send_push):
        sql = db_handle.UPDATE + table_name.SERVICE_COMPANY + ' SET ask_to_review_title = \'' + str(ask_to_review_title) + \
              '\'' + ', ask_to_review_content = \'' + str(ask_to_review_content) + '\', ask_to_review_day_send_push = \'' + \
              ask_to_review_day_send_push + '\' WHERE id = ' + service_company_id
        return db_handle.excuteCommandChangeRecord(sql)

    def set_reminder_info_by_service_company_id(self, service_company_id, reminder_title, reminder_content, reminder_day_send_push):
        sql = db_handle.UPDATE + table_name.SERVICE_COMPANY + ' SET reminder_title = \'' + str(reminder_title) + \
              '\'' + ', reminder_content = \'' + str(reminder_content) + '\', reminder_day_send_push = \'' + \
              reminder_day_send_push + '\' WHERE id = ' + service_company_id
        return db_handle.excuteCommandChangeRecord(sql)

    def set_approve_check_in_out(self, service_company_id, is_approved_check_in, is_approved_check_out):
        modified = "'" + db_handle.get_full_current_jav_time() + "'"
        sql = db_handle.UPDATE + table_name.SERVICE_COMPANY + ' SET is_approved_check_in = ' + \
              str(is_approved_check_in) + ', is_approved_check_out = ' + str(is_approved_check_out) +\
              ', modified = ' + modified + ' WHERE id = ' + str(service_company_id)
        return db_handle.excuteCommandChangeRecord(sql)
