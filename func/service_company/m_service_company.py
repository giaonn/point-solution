from func.base_model import Base
from func import db, db_handle

class M_Service_Company(Base):

    __tablename__ = 'services_companies'

    service_id                      = db.Column(db.Integer, nullable=True)
    company_id                      = db.Column(db.Integer, nullable=True)
    card_name                       = db.Column(db.String, nullable=True)
    number_of_stamps_in_card        = db.Column(db.Integer, nullable=True)
    current_number_of_notifications = db.Column(db.Integer, nullable=True)
    max_number_of_notifications     = db.Column(db.Integer, nullable=True)
    current_number_of_stores        = db.Column(db.Integer, nullable=True)
    max_number_of_stores            = db.Column(db.Integer, nullable=True)
    logo                            = db.Column(db.String, nullable=True)
    ip                              = db.Column(db.String, nullable=True)
    ask_to_review_title             = db.Column(db.String, nullable=True)
    ask_to_review_content           = db.Column(db.Text, nullable=True)
    ask_to_review_sort_description  = db.Column(db.Text, nullable=True)
    ask_to_review_day_send_push     = db.Column(db.Integer, nullable=True)
    reminder_title                  = db.Column(db.String, nullable=True)
    reminder_content                = db.Column(db.Text, nullable=True)
    reminder_sort_description       = db.Column(db.Text, nullable=True)
    reminder_day_send_push          = db.Column(db.Integer, nullable=True)
    is_approved_check_in            = db.Column(db.Integer, nullable=True)
    is_approved_check_out           = db.Column(db.Integer, nullable=True)
    card_type                       = db.Column(db.Integer, nullable=True)
    explanation                     = db.Column(db.Text, nullable=True)
    def __init__(self):
        super(M_Service_Company, self).__init__()




