from func import config
#STATUS CODE
CODE_FAIL_AUTHORIZED     = 401
CODE_TOO_MANY_CONNECTION = 429

#REGISTER
INFO_NULL          = 1
LENGTH_PASS_WRONG  = 2
FORMAT_EMAIL_WRONG = 3
USER_EXISTS        = 4
EMAIL_EXIST        = 6
#LOGIN
LOGIN_FAIL         = 5
#SESSION_STATUS
CHECKED_OUT  = 'check_out'
CHECKED_IN   = 'checked_in'
ADD_STAMP    = 'add_stamp'
WAIT_CONFIRM = 'wait_confirm'
WAIT_CONFIRM_CHECK_OUT = "wait_confirm_check_out"
CHECKED_IN_AND_OUT = 'check in out luon'
#CARD STATUS
POINT_CAN_USE = 'point_can_use'
CAN_USE       = 'can_use'
UNUSED        = 'unused'
USED          = 'used'
#CARD HISTORY STATUS
USE_CARD      = 'use_card'

#feedback-scan-qr-code-store
QR_CODE_NULL       = 1
QR_CODE_NOT_EXISTS = 2

#name_database
# DB_DEV      = 'point' if config.IS_SERVER_DEV else 'waterkuru'
# DB_STAGGING = 'point_stagging' if config.IS_SERVER_DEV else 'waterkuru'
#
#
#LOGIN AUTHEN WITH GOOGLE, FACEBOOK
LOGIN_NORMAL = 0
LOGIN_GG     = 1
LOGIN_FB     = 2
LOGIN_TW     = 3

CLIENT_ID_GOOGLE = '610695143021-kh9svl3nvsuh31pn6mt3elvv3tlfbmrm.apps.googleusercontent.com'
URL_GOOGLE       = 'https://www.googleapis.com/oauth2/v3/tokeninfo?id_token='
URL_FACEBOOK     = 'https://graph.facebook.com/me?fields=email,name,location,locale,link,gender,cover,birthday,about&access_token='

#CONFIG FAIL AUTHEN
FAIL_AUTHEN = 0
USER_AUTHEN_EXISTS = 1

RADIUS_KM = 6371

#CONFIG TYPE CARD_TYPE
CARD_TYPE  = 1
POINT_TYPE = 2
NONE_TYPE  = 3

#CONFIG READ PUSH
UNREAD = 0
READ   = 1
SEEN   = 2










