import os
BASE_DIR = os.path.abspath(os.path.dirname(os.path.dirname(__file__)))
SQLALCHEMY_DATABASE_URI = os.environ.get(
 	'SQLALCHEMY_DATABASE_URI',
 	'mysql://point:VN201501@point.crygr0uwyp5o.ap-northeast-1.rds.amazonaws.com:3306/point')

# SQLALCHEMY_DATABASE_URI = "mysql://point:VN201501@point.crygr0uwyp5o.ap-northeast-1.rds.amazonaws.com:3306/point"
# SQLALCHEMY_DATABASE_URI = "mysql://root:VN201501@waterkurudbinstance.cltokztxmxeu.ap-northeast-1.rds.amazonaws.com:3306/waterkuru"
# SQLALCHEMY_DATABASE_URI = "mysql://root:VN201501@iungo.cltokztxmxeu.ap-northeast-1.rds.amazonaws.com:3306/waterkuru"

SQLALCHEMY_BINDS = {
    'newDB' : 'mysql://point:VN201501@point.crygr0uwyp5o.ap-northeast-1.rds.amazonaws.com:3306/point'
}
Server_Type = os.environ.get('Server_Type')

SQLALCHEMY_TRACK_MODIFICATIONS = True
DATABASE_CONNECT_OPTIONS = {}
JSON_AS_ASCII = False

REDIS_URL = os.environ.get('REDIS_URL', 'redis://localhost:6379/0')

#REMEMER BEFORE DEPLOY
#Server for dev or not
# IS_SERVER_DEV = False
# #database for dev or not
# FOR_DEV = False
#
# if IS_SERVER_DEV:
#     if FOR_DEV:
#         SQLALCHEMY_DATABASE_URI = "mysql://point:VN201501@point.crygr0uwyp5o.ap-northeast-1.rds.amazonaws.com:3306/point"
#     else:
#         SQLALCHEMY_DATABASE_URI = "mysql://point:VN201501@point.crygr0uwyp5o.ap-northeast-1.rds.amazonaws.com:3306/point_stagging"
# else:
#     SQLALCHEMY_DATABASE_URI = "mysql://root:VN201501@waterkurudbinstance.cltokztxmxeu.ap-northeast-1.rds.amazonaws.com:3306/waterkuru"
