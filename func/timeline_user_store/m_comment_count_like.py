from func.base_model import Base
from func import db, db_handle

class M_Comment_Count_Like(Base):

    __tablename__ = 'commentcountlikes'

    comment_id = db.Column(db.Integer, nullable=True)
    like_id    = db.Column(db.Integer, nullable=True)
    count_like = db.Column(db.Integer, nullable=True)

    def __init__(self):
        super(M_Comment_Count_Like, self).__init__()




