from func.base_model import Base
from func import db, db_handle

class M_Comment(Base):

    __tablename__ = 'comments'

    timeline_id        = db.Column(db.Integer, nullable=True)
    user_id            = db.Column(db.Integer, nullable=True)
    management_user_id = db.Column(db.Integer, nullable=True)
    content            = db.Column(db.Text, nullable=True)
    is_deleted         = db.Column(db.Integer, nullable=True, default=0)
    user_name          = db.Column(db.String, nullable=True)

    def __init__(self):
        super(M_Comment, self).__init__()




