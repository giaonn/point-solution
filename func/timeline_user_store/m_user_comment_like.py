from func.base_model import Base
from func import db, db_handle

class M_User_Comment_Like(Base):

    __tablename__ = 'user_comment_likes'

    comment_id         = db.Column(db.Integer, nullable=True)
    like_id            = db.Column(db.Integer, nullable=True)
    user_id            = db.Column(db.Integer, nullable=True)
    management_user_id = db.Column(db.Integer, nullable=True)
    user_name          = db.Column(db.String, nullable=True)
    is_deleted         = db.Column(db.Integer, nullable=True, default=0)

    def __init__(self):
        super(M_User_Comment_Like, self).__init__()




