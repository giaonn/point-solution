from func.base_model import Base
from func import db, db_handle

class M_Timeline_Count_Like(Base):

    __tablename__ = 'timelinecountlikes'

    timeline_id  = db.Column(db.Integer, nullable=True)
    like_id      = db.Column(db.Integer, nullable=True)
    count_like   = db.Column(db.Integer, nullable=True, default=0)

    def __init__(self):
        super(M_Timeline_Count_Like, self).__init__()




