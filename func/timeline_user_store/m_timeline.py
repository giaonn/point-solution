from func.base_model import Base
from func import db, db_handle

class M_Timeline(Base):

    __tablename__ = 'timelines'

    images             = db.Column(db.Text, nullable=True)
    messages           = db.Column(db.Text, nullable=True)
    img_html           = db.Column(db.Text, nullable=True)
    firebase_api_key   = db.Column(db.Text, nullable=True)
    comment_number     = db.Column(db.Integer, nullable=True, default=0)
    management_user_id = db.Column(db.Integer, nullable=True, default=0)
    is_deleted         = db.Column(db.Integer, nullable=True, default=0)
    app_id             = db.Column(db.Integer, nullable=True, default=0)

    def __init__(self):
        super(M_Timeline, self).__init__()




