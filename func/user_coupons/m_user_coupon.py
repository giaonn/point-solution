from func.base_model import Base
from func import db, db_handle

class M_User_Coupon(Base):
    __tablename__ = 'user_coupons'

    user_id          = db.Column(db.Integer, nullable=True)
    coupon_id        = db.Column(db.Integer, nullable=True)
    name_coupon      = db.Column(db.Text, nullable=True)
    img_coupon       = db.Column(db.Text, nullable=True)
    type_coupon_name = db.Column(db.Text, nullable=True)
    time_end_coupon  = db.Column(db.DateTime, nullable=True)
    code_coupon      = db.Column(db.Text, nullable=True)
    is_use_again     = db.Column(db.Integer, nullable=True)
    policy           = db.Column(db.Text, nullable=True)
    store_name       = db.Column(db.Text, nullable=True)
    store_img        = db.Column(db.Text, nullable=True)
    is_used          = db.Column(db.Integer, nullable=True)

    def __init__(self):
        super(M_User_Coupon, self).__init__()


