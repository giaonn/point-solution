from func.base_model import Base
from func import db, db_handle

class M_Push_Notification(Base):

    __tablename__ = 'push_notifications'

    management_user_id        = db.Column(db.Integer, nullable=True)
    question_naires_id        = db.Column(db.Integer, nullable=True)
    stamp_bonus               = db.Column(db.Integer, nullable=True)
    point_bonus               = db.Column(db.Integer, nullable=True)
    old_push_id               = db.Column(db.Integer, nullable=True)
    title                     = db.Column(db.String, nullable=True)
    content                   = db.Column(db.Text, nullable=True)
    send_time                 = db.Column(db.DateTime, nullable=True)
    sound                     = db.Column(db.String, nullable=True)
    badge                     = db.Column(db.String, nullable=True)
    status                    = db.Column(db.Integer, nullable=True)
    type                      = db.Column(db.Integer, nullable=True, default=1)
    number_day_after_check_in = db.Column(db.Integer, nullable=True, default=0)
    is_step                   = db.Column(db.Integer, nullable=True, default=0)
    send_time_no_date         = db.Column(db.Time, nullable=True)
    sort_description          = db.Column(db.Text, nullable=True)

    def __init__(self):
        super(M_Push_Notification, self).__init__()




