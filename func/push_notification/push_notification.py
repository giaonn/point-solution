from func import db, table_name, db_handle
from datetime import datetime as dt
from func import config_const
import re
import hashlib, pytz

class Push_notifi:
    def count_unread_push_by_service_company_id(self, service_company_id, user_id):
        sql = ' select count(*) as number_push_unread from ' \
              ' (select PN.id from push_notifications PN ' \
              ' join user_push_notifications UPN on PN.id = UPN.push_notification_id' \
              ' where PN.id in' \
              ' (' \
              ' select PN.id' \
              ' from push_notifications PN ' \
              ' join (' \
              ' select distinct(SP.push_notification_id) as pn_id from stores_push_notifications SP ' \
              ' where SP.store_id  in (select id from stores S where S.service_company_id = ' + str(service_company_id) + ')' \
              ' ) as T  ' \
              ' on PN.id = T.pn_id) ' \
              ' and PN.status = 5 ' \
              ' and (UPN.status = 0 or UPN.status = 2) and UPN.user_id = ' + str(user_id) + ' ' \
              ' group by PN.id ' \
              ' ) as T '

        res = db.engine.execute(sql)
        for row in res:
            return int(row['number_push_unread'])
        return 0

    def count_unread_push(self, user_id):
        sql = ' select count(*) as number_push_unread from (select PN.id from push_notifications PN ' \
              ' join user_push_notifications UPN on PN.id = UPN.push_notification_id ' \
              ' where user_id = ' + str(user_id) + ' and PN.status = 5 and (UPN.status = 0 or UPN.status = 2) group by PN.id) as T'

        res = db.engine.execute(sql)
        for row in res:
            return int(row['number_push_unread'])
        return 0

    def get_push_by_user(self, user_id, page, limit, getTotal):
        startPoint = db_handle.getStartPoint(page, limit)
        if not getTotal:
            sql = ' select PN.*, UPN.receive_time, UPN.status as status_read, UPN.stamp_id as stamp_id from push_notifications PN ' \
                  ' join user_push_notifications UPN on PN.id = UPN.push_notification_id ' \
                  ' where user_id = ' + str(user_id) + ' and PN.status = 5 group by PN.id order by created desc ' \
                  ' limit ' + str(startPoint) + ', ' + str(limit)
        else:
            sql = ' select PN.*, UPN.receive_time, UPN.status as status_read, UPN.stamp_id as stamp_id from push_notifications PN ' \
                  ' join user_push_notifications UPN on PN.id = UPN.push_notification_id ' \
                  ' where user_id = ' + str(user_id) + ' and PN.status = 5 group by PN.id order by created desc '

        return db_handle.executeCommandMoreRow(sql)


    def get_questionnaire(self, service_company_id, user_id, last_user_push_id, limit):
        if service_company_id:

            if last_user_push_id:
                sql = ' select PN.management_user_id, QN.description, PN.type, PN.id as user_push_id, PN.id as id,  PN.title, PN.content, PN.send_time as send_push_time from push_notifications PN ' \
                      ' join (select distinct(SP.push_notification_id) as pn_id from stores_push_notifications SP where SP.store_id ' \
                      ' in (select id from stores S where S.service_company_id = ' + str(service_company_id) + ' ' \
                      ' )) as T on PN.id = T.pn_id join question_naires QN on PN.question_naires_id = QN.id where PN.status = 5 and PN.id < ' + str(last_user_push_id) + \
                      ' order by PN.id desc limit ' + str(limit)
            else:
                sql = ' select PN.management_user_id, QN.description, PN.type, PN.id as user_push_id, PN.id as id,  PN.title, PN.content, PN.send_time as send_push_time from push_notifications PN ' \
                      ' join (select distinct(SP.push_notification_id) as pn_id from stores_push_notifications SP where SP.store_id ' \
                      ' in (select id from stores S where S.service_company_id = ' + str(service_company_id) + ' ' \
                      ' )) as T on PN.id = T.pn_id join question_naires QN on PN.question_naires_id = QN.id where PN.status = 5 ' + \
                      ' order by PN.id desc limit ' + str(limit)
        else:
            # case get all
            if last_user_push_id:
                sql = ' select PN.management_user_id, QN.description, PN.type, PN.id as user_push_id, PN.id as id,  PN.title, PN.content, PN.send_time as send_push_time from push_notifications PN ' \
                      ' join (select distinct(SP.push_notification_id) as pn_id from stores_push_notifications SP where SP.store_id ' \
                      ' in (select store_id from info_of_user_from_stores UI where UI.user_id = ' + str(user_id) + ' ' \
                      ' )) as T on PN.id = T.pn_id join question_naires QN on PN.question_naires_id = QN.id where PN.status = 5 and PN.id < ' + str(last_user_push_id) + \
                      ' order by PN.id desc limit ' + str(limit)
            else:
                sql = ' select PN.management_user_id, QN.description, PN.type, PN.id as user_push_id, PN.id as id,  PN.title, PN.content, PN.send_time as send_push_time from push_notifications PN ' \
                      ' join (select distinct(SP.push_notification_id) as pn_id from stores_push_notifications SP where SP.store_id ' \
                      ' in (select store_id from info_of_user_from_stores UI where UI.user_id = ' + str(user_id) + ' ' \
                      ' )) as T on PN.id = T.pn_id join question_naires QN on PN.question_naires_id = QN.id where PN.status = 5 ' + \
                      ' order by PN.id desc limit ' + str(limit)

        return db_handle.executeCommandMoreRow(sql)


    def search_questionnaire(self, service_company_id, user_id, last_user_push_id, limit, text_search):
        text_search = '%' + text_search + '%'
        if  service_company_id:

            if last_user_push_id:
                sql = ' select PN.management_user_id, QN.description, PN.type, PN.id as user_push_id, PN.id as id,  PN.title, PN.content, PN.send_time as send_push_time from push_notifications PN ' \
                      ' join (select distinct(SP.push_notification_id) as pn_id from stores_push_notifications SP where SP.store_id ' \
                      ' in (select id from stores S where S.service_company_id = ' + str(service_company_id) + ' ' \
                      ' )) as T on PN.id = T.pn_id join question_naires QN on PN.question_naires_id = QN.id where PN.status = 5 ' + ' and (QN.title like ' + '%s' + ' or QN.description like ' + '%s' + ') and PN.id < ' + str(last_user_push_id) + \
                      ' order by PN.id desc limit ' + str(limit)
            else:
                sql = ' select PN.management_user_id, QN.description, PN.type, PN.id as user_push_id, PN.id as id,  PN.title, PN.content, PN.send_time as send_push_time from push_notifications PN ' \
                      ' join (select distinct(SP.push_notification_id) as pn_id from stores_push_notifications SP where SP.store_id ' \
                      ' in (select id from stores S where S.service_company_id = ' + str(service_company_id) + ' ' \
                      ' )) as T on PN.id = T.pn_id join question_naires QN on PN.question_naires_id = QN.id where PN.status = 5 ' + ' and (QN.title like '+ '%s' + ' or QN.description like ' + '%s' + ') ' + \
                      ' order by PN.id desc limit ' + str(limit)
        else:
            # case get all
            if last_user_push_id:
                sql = ' select PN.management_user_id, QN.description, PN.type, PN.id as user_push_id, PN.id as id,  PN.title, PN.content, PN.send_time as send_push_time from push_notifications PN ' \
                      ' join (select distinct(SP.push_notification_id) as pn_id from stores_push_notifications SP where SP.store_id ' \
                      ' in (select store_id from info_of_user_from_stores UI where UI.user_id = ' + str(user_id) + ' ' \
                      ' )) as T on PN.id = T.pn_id join question_naires QN on PN.question_naires_id = QN.id where PN.status = 5 ' + ' and (QN.title like ' + '%s' + ' or QN.description like ' + '%s' + ') and PN.id < ' + str(last_user_push_id) + \
                      ' order by PN.id desc limit ' + str(limit)
            else:
                sql = ' select PN.management_user_id, QN.description, PN.type, PN.id as user_push_id, PN.id as id,  PN.title, PN.content, PN.send_time as send_push_time from push_notifications PN ' \
                      ' join (select distinct(SP.push_notification_id) as pn_id from stores_push_notifications SP where SP.store_id ' \
                      ' in (select store_id from info_of_user_from_stores UI where UI.user_id = ' + str(user_id) + ' ' \
                      ' )) as T on PN.id = T.pn_id join question_naires QN on PN.question_naires_id = QN.id where PN.status = 5 ' + ' and (QN.title like '+ '%s' + ' or QN.description like ' + '%s' + ') ' + \
                      ' order by PN.id desc limit ' + str(limit)

        params = (text_search, text_search)

        return db_handle.executeCommandMoreRowWithParams(sql, params)

    def get_list_push_cache(self):

        sql = ' select PN.management_user_id, PN.type, PN.id, PN.title, PN.content, PN.send_time as send_push_time from push_notifications PN ' \
              ' where PN.status = 5 and PN.management_user_id in (select id from management_users where app_id = 3) order by PN.id desc limit 20'

        return db_handle.executeCommandMoreRow(sql)

    def get_push_by_user_with_push_id(self, user_id, push_id, get_unread, limit, from_date, to_date):

        if from_date:
            from_date = "'" + str(from_date) + "'"
            to_date   = "'" + str(to_date)   + "'"
            query_time = ' and  PN.send_time > ' + str(from_date) + ' and PN.send_time < ' + str(to_date) + ' '
        else:
            query_time = ' '

        if push_id:
            sql =  ' select PN.management_user_id, PN.type, PN.id, PN.title, PN.content, PN.send_time as send_push_time from push_notifications PN ' \
                   ' join (select distinct(SP.push_notification_id) as pn_id from stores_push_notifications SP where SP.store_id ' \
                   ' in (select store_id from info_of_user_from_stores UI where UI.user_id = ' + str(user_id) + ' ' \
                   ' )) as T on PN.id = T.pn_id where PN.status = 5 and PN.id < ' + str(push_id) + query_time + \
                   ' order by PN.id desc limit ' + str(limit)
        else:
            sql = ' select PN.management_user_id, PN.type, PN.id, PN.title, PN.content, PN.send_time as send_push_time from push_notifications PN ' \
                  ' join (select distinct(SP.push_notification_id) as pn_id from stores_push_notifications SP where SP.store_id ' \
                  ' in (select store_id from info_of_user_from_stores UI where UI.user_id = ' + str(user_id) + ' ' \
                  ' )) as T on PN.id = T.pn_id where PN.status = 5 ' + query_time + \
                  ' order by PN.id desc limit ' + str(limit)

        return db_handle.executeCommandMoreRow(sql)

    def search_push_by_user_with_push_id(self, user_id, push_id, get_unread, limit, text_search):
        text_search = '%' + text_search + '%'

        if push_id:
            sql = ' select PN.management_user_id, PN.type, PN.id, PN.title, PN.content, PN.send_time as send_push_time from push_notifications PN ' \
                  ' join (select distinct(SP.push_notification_id) as pn_id from stores_push_notifications SP where SP.store_id ' \
                  ' in (select store_id from info_of_user_from_stores UI where UI.user_id = ' + str(user_id) + ' ' \
                  ' )) as T on PN.id = T.pn_id where PN.status = 5 and PN.id < ' + str(push_id) + ' and (PN.title like '+ '%s' + ' or PN.content like ' + '%s' + ') ' + \
                  ' order by PN.id desc limit ' + str(limit)
        else:
            sql = ' select PN.management_user_id, PN.type, PN.id, PN.title, PN.content, PN.send_time as send_push_time from push_notifications PN ' \
                  ' join (select distinct(SP.push_notification_id) as pn_id from stores_push_notifications SP where SP.store_id ' \
                  ' in (select store_id from info_of_user_from_stores UI where UI.user_id = ' + str(user_id) + ' ' \
                  ' )) as T on PN.id = T.pn_id where PN.status = 5 ' + ' and (PN.title like '+ '%s' + ' or PN.content like ' + '%s' + ') ' + \
                  ' order by PN.id desc limit ' + str(limit)
        params = (text_search, text_search)

        return db_handle.executeCommandMoreRowWithParams(sql, params)

    #--------------------------------------------------------------------------------------------------

    def get_push_by_user_with_push_id_old(self, user_id, push_id, get_unread, limit, from_date, to_date):

        if from_date:
            from_date = "'" + str(from_date) + "'"
            to_date   = "'" + str(to_date)   + "'"
            query_time = ' and  UPN.created > ' + str(from_date) + ' and UPN.created < ' + str(to_date) + ' '
        else:
            query_time = ' '

        if push_id:
            if not get_unread:
                sql = ' select PN.*, UPN.receive_time, UPN.status as status_read, UPN.id as user_push_id, UPN.stamp_id as stamp_id, UPN.created as send_push_time from push_notifications PN ' \
                      ' join user_push_notifications UPN on PN.id = UPN.push_notification_id ' \
                      ' where user_id = ' + str(user_id) + ' and UPN.id < ' + str(push_id) + \
                       query_time + \
                      ' group by PN.id order by UPN.id desc limit ' + str(limit)

            else:
                sql = ' select PN.*, UPN.receive_time, UPN.status as status_read, UPN.id as user_push_id,  UPN.stamp_id as stamp_id, UPN.created as send_push_time from push_notifications PN ' \
                      ' join user_push_notifications UPN on PN.id = UPN.push_notification_id ' \
                      ' where user_id = ' + str(user_id) + ' and (UPN.status = 0 or UPN.status = 2) and UPN.id < ' + str(push_id) + \
                       query_time + \
                      ' group by PN.id order by UPN.id desc limit ' + str(limit)
        else:
            if not get_unread:
                sql = ' select PN.*, UPN.receive_time, UPN.status as status_read, UPN.id as user_push_id, UPN.stamp_id as stamp_id, UPN.created as send_push_time from push_notifications PN ' \
                      ' join user_push_notifications UPN on PN.id = UPN.push_notification_id ' \
                      ' where user_id = ' + str(user_id) + \
                       query_time + \
                      ' group by PN.id order by UPN.id desc limit ' + str(limit)

            else:
                sql = ' select PN.*, UPN.receive_time, UPN.status as status_read, UPN.id as user_push_id, UPN.stamp_id as stamp_id, UPN.created as send_push_time from push_notifications PN ' \
                      ' join user_push_notifications UPN on PN.id = UPN.push_notification_id ' \
                      ' where user_id = ' + str(user_id) + ' and (UPN.status = 0 or UPN.status = 2) ' +\
                       query_time + \
                      ' group by PN.id order by UPN.id desc limit '+ str(limit)

        return db_handle.executeCommandMoreRow(sql)

    def search_push_by_user_with_push_id_old(self, user_id, push_id, get_unread, limit, text_search):
        text_search = '%' + text_search + '%'
        if push_id:
            if not get_unread:
                sql = ' select PN.*, UPN.receive_time, UPN.status as status_read, UPN.id as user_push_id, UPN.stamp_id as stamp_id, UPN.created as send_push_time from push_notifications PN ' \
                      ' join user_push_notifications UPN on PN.id = UPN.push_notification_id ' \
                      ' where user_id = ' + str(user_id) + ' and UPN.id < ' + str(push_id) + ' and (PN.title like '+ '%s' + ' or PN.content like ' + '%s' + ') ' +  \
                      ' group by PN.id order by UPN.id desc limit ' + str(limit)

            else:
                sql = ' select PN.*, UPN.receive_time, UPN.status as status_read, UPN.id as user_push_id,  UPN.stamp_id as stamp_id, UPN.created as send_push_time from push_notifications PN ' \
                      ' join user_push_notifications UPN on PN.id = UPN.push_notification_id ' \
                      ' where user_id = ' + str(user_id) + ' and (UPN.status = 0 or UPN.status = 2) and UPN.id < ' + str(push_id) + \
                      ' group by PN.id order by UPN.id desc limit ' + str(limit)
        else:
            if not get_unread:
                sql = ' select PN.*, UPN.receive_time, UPN.status as status_read, UPN.id as user_push_id, UPN.stamp_id as stamp_id, UPN.created as send_push_time from push_notifications PN ' \
                      ' join user_push_notifications UPN on PN.id = UPN.push_notification_id ' \
                      ' where user_id = ' + str(user_id)  + ' and (PN.title like '+ '%s' + ' or PN.content like ' + '%s' + ') ' +  \
                      ' group by PN.id order by UPN.id desc limit ' + str(limit)
            else:
                sql = ' select PN.*, UPN.receive_time, UPN.status as status_read, UPN.id as user_push_id, UPN.stamp_id as stamp_id, UPN.created as send_push_time from push_notifications PN ' \
                      ' join user_push_notifications UPN on PN.id = UPN.push_notification_id ' \
                      ' where user_id = ' + str(user_id) + ' and (UPN.status = 0 or UPN.status = 2) group by PN.id order by UPN.id desc limit '+ str(limit)

        params = (text_search, text_search)

        return db_handle.executeCommandMoreRowWithParams(sql, params)


    def get_push_by_user_unread(self, user_id, page, limit):
        sql = ' select PN.*, UPN.receive_time, UPN.status as status_read,UPN.created as send_push_time from push_notifications PN ' \
              ' join user_push_notifications UPN on PN.id = UPN.push_notification_id ' \
              ' where user_id = ' + str(user_id) + ' and (UPN.status = 0 or UPN.status = 2) group by PN.id order by created desc ' \
              ' limit ' + str(limit)

        return db_handle.executeCommandMoreRow(sql)


    def get_push_by_user_and_service_company_id(self, user_id, service_company_id, push_id, limit, from_date, to_date):

        if from_date:
            from_date = "'" + str(from_date) + "'"
            to_date   = "'" + str(to_date)   + "'"
            query_time = ' and  PN.send_time > ' + str(from_date) + ' and PN.send_time < ' + str(to_date) + ' '
        else:
            query_time = ' '

        if push_id:
            sql = ' select PN.management_user_id, PN.type, PN.id, PN.title, PN.content, PN.send_time as send_push_time from push_notifications PN ' \
                  ' join (select distinct(SP.push_notification_id) as pn_id from stores_push_notifications SP where SP.store_id ' \
                  ' in (select id from stores S where S.service_company_id = ' + str(service_company_id) + ')) ' \
                  ' as T on PN.id = T.pn_id where PN.status = 5 and PN.id < ' + str(push_id) + query_time + \
                  ' order by PN.id desc limit ' + str(limit)
        else:
            sql = ' select PN.management_user_id, PN.type, PN.id, PN.title, PN.content, PN.send_time as send_push_time from push_notifications PN ' \
                  ' join (select distinct(SP.push_notification_id) as pn_id from stores_push_notifications SP where SP.store_id ' \
                  ' in (select id from stores S where S.service_company_id = ' + str(service_company_id) + ')) ' \
                  ' as T on PN.id = T.pn_id where PN.status = 5 ' + query_time + \
                  ' order by PN.id desc limit ' + str(limit)

        return db_handle.executeCommandMoreRow(sql)


    def search_push_by_user_and_service_company_id(self, user_id, service_company_id, push_id, limit, text_search):
        text_search = '%' + text_search + '%'

        if push_id:
            sql = ' select PN.management_user_id, PN.type, PN.id, PN.title, PN.content, PN.send_time as send_push_time from push_notifications PN ' \
                  ' join (select distinct(SP.push_notification_id) as pn_id from stores_push_notifications SP where SP.store_id ' \
                  ' in (select id from stores S where S.service_company_id = ' + str(service_company_id) + ')) ' \
                  ' as T on PN.id = T.pn_id where PN.status = 5 and PN.id < ' + str(push_id) + ' and (PN.title like '+ '%s' + ' or PN.content like ' + '%s' + ') ' + \
                  ' order by PN.id desc limit ' + str(limit)
        else:
            sql = ' select PN.management_user_id, PN.type, PN.id, PN.title, PN.content, PN.send_time as send_push_time from push_notifications PN ' \
                  ' join (select distinct(SP.push_notification_id) as pn_id from stores_push_notifications SP where SP.store_id ' \
                  ' in (select id from stores S where S.service_company_id = ' + str(service_company_id) + ')) ' \
                  ' as T on PN.id = T.pn_id where PN.status = 5 ' + ' and (PN.title like '+ '%s' + ' or PN.content like ' + '%s' + ') ' + \
                  ' order by PN.id desc limit ' + str(limit)

        params = (text_search, text_search)

        return db_handle.executeCommandMoreRowWithParams(sql, params)

    #-----------------------------------------------------------------
    def get_push_by_user_and_service_company_id_old(self, user_id, service_company_id, last_user_push_id, limit, from_date,
                                                to_date):

        if from_date:
            from_date = "'" + str(from_date) + "'"
            to_date = "'" + str(to_date) + "'"
            query_time = ' and  UPN.created > ' + str(from_date) + ' and UPN.created < ' + str(to_date) + ' '
        else:
            query_time = ' '

        if last_user_push_id:
            sql = ' select PN.*, UPN.id as user_push_id, UPN.created as send_push_time, UPN.status as status_read, UPN.stamp_id as stamp_id from push_notifications PN  ' \
                  ' join user_push_notifications UPN on PN.id = UPN.push_notification_id ' \
                  ' where PN.status = 5 and PN.id in (select PNN.id from push_notifications PNN join ' \
                  ' (select distinct(SP.push_notification_id) as pn_id from stores_push_notifications SP where SP.store_id in ' \
                  ' (select id from stores S where S.service_company_id = ' + str(
                service_company_id) + ')) as T on PNN.id = T.pn_id) ' \
                                      ' and UPN.id < ' + str(last_user_push_id) + ' and UPN.user_id = ' + str(user_id) + \
                  query_time + \
                  ' group by PN.id order by UPN.id desc limit ' + str(limit)
        else:
            sql = ' select PN.*, UPN.id as user_push_id, UPN.created as send_push_time, UPN.status as status_read, UPN.stamp_id as stamp_id from push_notifications PN  ' \
                  ' join user_push_notifications UPN on PN.id = UPN.push_notification_id ' \
                  ' where PN.status = 5 and PN.id in (select PNN.id from push_notifications PNN join ' \
                  ' (select distinct(SP.push_notification_id) as pn_id from stores_push_notifications SP where SP.store_id in ' \
                  ' (select id from stores S where S.service_company_id = ' + str(
                service_company_id) + ')) as T on PNN.id = T.pn_id) ' \
                                      ' and UPN.user_id = ' + str(user_id) + \
                  query_time + \
                  ' group by PN.id order by UPN.id desc limit ' + str(limit)

        return db_handle.executeCommandMoreRow(sql)

    def search_push_by_user_and_service_company_id_old(self, user_id, service_company_id, last_user_push_id, limit,
                                                   text_search):
        text_search = '%' + text_search + '%'

        if last_user_push_id:
            sql = ' select PN.*, UPN.id as user_push_id, UPN.created as send_push_time, UPN.status as status_read, UPN.stamp_id as stamp_id from push_notifications PN  ' \
                  ' join user_push_notifications UPN on PN.id = UPN.push_notification_id ' \
                  ' where PN.status = 5 and PN.id in (select PNN.id from push_notifications PNN join ' \
                  ' (select distinct(SP.push_notification_id) as pn_id from stores_push_notifications SP where SP.store_id in ' \
                  ' (select id from stores S where S.service_company_id = ' + str(
                service_company_id) + ')) as T on PNN.id = T.pn_id) ' \
                                      ' and UPN.id < ' + str(last_user_push_id) + ' and UPN.user_id = ' + str(user_id) + \
                  ' and (PN.title like ' + '%s' + ' or PN.content like ' + '%s' + ') ' + \
                  ' group by PN.id order by UPN.id desc limit ' + str(limit)
        else:
            sql = ' select PN.*, UPN.id as user_push_id, UPN.created as send_push_time, UPN.status as status_read, UPN.stamp_id as stamp_id from push_notifications PN  ' \
                  ' join user_push_notifications UPN on PN.id = UPN.push_notification_id ' \
                  ' where PN.status = 5 and PN.id in (select PNN.id from push_notifications PNN join ' \
                  ' (select distinct(SP.push_notification_id) as pn_id from stores_push_notifications SP where SP.store_id in ' \
                  ' (select id from stores S where S.service_company_id = ' + str(
                service_company_id) + ')) as T on PNN.id = T.pn_id) ' \
                                      ' and (PN.title like ' + '%s' + ' or PN.content like ' + '%s' + ') ' + \
                  ' and UPN.user_id = ' + str(user_id) + \
                  ' group by PN.id order by UPN.id desc limit ' + str(limit)

        params = (text_search, text_search)

        return db_handle.executeCommandMoreRowWithParams(sql, params)

    def get_push_by_user_info(self, user_id, page, limit, getTotal):
        startPoint = db_handle.getStartPoint(page, limit)
        if not getTotal:
            sql = ' select PN.id, PN.title, PN.content, PN.send_time from push_notifications PN ' \
                  ' join (select distinct(SP.push_notification_id) as pn_id from stores_push_notifications SP where SP.store_id ' \
                  ' in (select store_id from info_of_user_from_stores UI where UI.user_id = ' + str(user_id) + ' ' \
                  ' )) as T on PN.id = T.pn_id where PN.status = 5 ' \
                  ' limit ' + str(startPoint) + ', ' + str(limit)
        else:
            sql = ' select PN.id, PN.title, PN.content, PN.send_time from push_notifications PN ' \
                  ' join (select distinct(SP.push_notification_id) as pn_id from stores_push_notifications SP where SP.store_id ' \
                  ' in (select store_id from info_of_user_from_stores UI where UI.user_id = ' + str(user_id) + ' ' \
                  ' )) as T on PN.id = T.pn_id where PN.status = 5 '

        return db_handle.executeCommandMoreRow(sql)


    def get_push_by_user_lv3(self, service_company_id, page, limit, getTotal):
        startPoint = db_handle.getStartPoint(page, limit)
        if not getTotal:
            sql = ' select PN.id, PN.title, PN.content, PN.send_time, PN.management_user_id, PN.sound, PN.badge, ' \
                  ' PN.status, PN.created, PN.modified from push_notifications PN ' \
                  ' join (select distinct(SP.push_notification_id) as pn_id from stores_push_notifications SP where SP.store_id ' \
                  ' in (select id from stores S where S.service_company_id = ' + str(service_company_id) + ' ' \
                  ' )) as T on PN.id = T.pn_id order by PN.modified DESC ' \
                  ' limit ' + str(startPoint) + ', ' + str(limit)
        else:
            sql = ' select PN.id, PN.title, PN.content, PN.send_time, PN.management_user_id, PN.sound, PN.badge, ' \
                  ' PN.status, PN.created, PN.modified from push_notifications PN ' \
                  ' join (select distinct(SP.push_notification_id) as pn_id from stores_push_notifications SP where SP.store_id ' \
                  ' in (select id from stores S where S.service_company_id = ' + str(service_company_id) + ' ' \
                  ' )) as T on PN.id = T.pn_id order by modified DESC '

        return db_handle.executeCommandMoreRow(sql)

    def get_push_by_user_lv4(self, store_id, page, limit, getTotal):
        startPoint = db_handle.getStartPoint(page, limit)
        if not getTotal:
            sql = ' select PN.id, PN.title, PN.content, PN.send_time, PN.management_user_id, PN.sound, PN.badge, PN.status, PN.created, PN.modified, PN.sort_description from push_notifications PN ' \
                  ' join (select distinct(SP.push_notification_id) as pn_id from stores_push_notifications SP where SP.store_id = ' + str(store_id) + '' \
                  ' ) as T on PN.id = T.pn_id where PN.is_step = 0 order by PN.send_time DESC ' \
                  ' limit ' + str(startPoint) + ', ' + str(limit)
        else:
            sql = ' select PN.id, PN.title, PN.content, PN.send_time from push_notifications PN ' \
                  ' join (select distinct(SP.push_notification_id) as pn_id from stores_push_notifications SP where SP.store_id = ' + str(store_id) + '' \
                  ' ) as T on PN.id = T.pn_id where PN.is_step = 0 order by PN.send_time DESC ' \

        return db_handle.executeCommandMoreRow(sql)

    def create_push_notifi(self, key_gen, management_user_id, title, content, send_time, sound, badge, status, sort_description, app_id, firebase_api_key):
        created = modified = db_handle.get_full_current_jav_time()

        sql = db_handle.INSERT + table_name.PUSH_NOTIFI + db_handle.LIST_PUSH_ATTRIBUTE + ' ' \
                                        ' values (%s, %s , %s, %s , %s, %s , %s, %s, %s, %s, %s, %s, %s)'
        params = (key_gen, management_user_id, title, content, send_time, sound, badge, status, created, modified, sort_description, app_id, firebase_api_key)

        return db_handle.excuteCommandChangeRecordParams(sql, params)

    def get_push_by_key_push(self, key_gen):
        key_gen = "'" + key_gen + "'"
        sql =  db_handle.SELECT + table_name.PUSH_NOTIFI + ' where key_gen = ' + key_gen
        return db_handle.executeCommand(sql)
