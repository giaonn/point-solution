from functools import wraps
from flask import  request
from func.token.token import Token
from func.user_app.user import User
from func.manager_user.manager_user import ManagerUser

from func.user_app.m_user import M_User
from func.manager_user.m_manager_user import M_Manager_User
from func import db_handle, redis_store
from func.dict_handle import LogHandle, LogOtherDB
import time, json

def try_get_user_token():
    if not request.headers['Authorization']:
        return None
    token  = request.headers['Authorization']

    _token = Token()
    _user  = User()

    redis_key = "token_{}".format(token)
    _time_now = db_handle.getMilisNow()
    if redis_store.exists(redis_key):
        cache_user = redis_store.get(redis_key)
        cache_user = json.loads(cache_user)

        if (_time_now < int(cache_user['expire'])):
            cache_user_id = cache_user['id']
            redis_key_name_user = 'user_name_' + str(cache_user_id)

            if redis_store.exists(redis_key_name_user):
                cache_user['name'] = redis_store.get(redis_key_name_user)

            return cache_user
        else:
            redis_store.delete(redis_key)
            return None

    # Get info token
    resToken = _token.getTokenByToken(token)
    if resToken:
        if (_time_now < resToken['expire']):
            user = _user.getUser(resToken['user_id'])

            cache_user = {}
            cache_user['id']     = user['id']
            cache_user['app_id'] = user['app_id']
            cache_user['name']   = user['name']
            cache_user['expire'] = int(resToken['expire'])

        else:
            # Delete token:
            _token.deleteToken(token)
            return None
    else:
        return None

    redis_store.set(redis_key, json.dumps(cache_user))
    return user

def user_auth_required_case_too_many_connection():
    def decorator(func):
        @wraps(func)
        def with_auth_wrapper(*args, **kwargs):
            user = None
            time_wait  = 1
            total_wait = 0
            is_first_time = True

            while (total_wait < 10 and not user):
                # try get token
                if is_first_time:
                    time_wait = 0
                    is_first_time = False

                time.sleep(time_wait)

                total_wait += time_wait
                time_wait = 0.1
                try:
                    user = try_get_user_token()

                    if total_wait > 0:
                        LogOtherDB('Too many connection but still running', 'Warning', total_wait)

                    return func(user=user, type_ex=1, *args, **kwargs)

                except Exception, e:
                    print e
                    continue
            #Fail
            LogOtherDB('Too many connection', 'Error', total_wait)
            return func(user=user, type_ex=2, *args, **kwargs)

        return with_auth_wrapper
    return decorator

def user_auth_required():
    def decorator(func):
        @wraps(func)
        def with_auth_wrapper(*args, **kwargs):
            try:
                user = try_get_user_token()
                return func(user = user, *args, **kwargs)

            except Exception,e :
                LogOtherDB(e, 'Error')
                return func(user=None, *args, **kwargs)

        return with_auth_wrapper
    return decorator

def manager_user_auth_required():
    def decorator(func):
        @wraps(func)
        def with_auth_wrapper(*args, **kwargs):
            try:
                if request.headers['Authorization']:
                    token = request.headers['Authorization']
                else:
                    return func(user=None, *args, **kwargs)

                _token = Token()
                _user  = ManagerUser()
                #Get info token
                resToken = _token.getTokenByToken(token)
                #Todo check isNormalUser here by column user_id in token table

                if not resToken['management_user_id']:
                    return func(user=None, *args, **kwargs)

                if resToken:
                    _time_now = db_handle.getMilisNow()
                    if(_time_now < resToken['expire']):
                        user = _user.getUser(resToken['management_user_id'])
                    else:
                        #Delete token:
                        _token.deleteToken(token)
                        return func(user=None, *args, **kwargs)
                else:
                    return func(user=None, *args, **kwargs)

                return func(user = user, *args, **kwargs)

            except Exception,e :
                LogOtherDB(e, 'Error')
                return func(user=None, *args, **kwargs)

        return with_auth_wrapper
    return decorator