from func import db, table_name, db_handle
from datetime import datetime as dt
from func import config_const
import re
import hashlib, pytz

class Service:
    def get_service_from_user_id(self, service_company_id):
        # sql = 'select * from ' + table_name.SERVICE + ' where service_company_id = '+ service_company_id
        # return db_handle.executeCommand(sql)

        return ''

    def get_all_service(self):
        sql = 'select * from ' + table_name.SERVICE + ' where user_memo != \'\' and user_memo is not null'
        return db_handle.executeCommandMoreRow(sql)

    def get_service_by_id(self, id):
        sql = db_handle.SELECT + table_name.SERVICE + ' WHERE id = ' + str(id)
        return db_handle.executeCommand(sql)

    def set_check_in_out_setting_by_service_id(self, is_approved_check_in, is_approved_check_out, service):
        sql = db_handle.UPDATE + table_name.SERVICE + ' SET is_approved_check_in = ' + \
              str(is_approved_check_in) + ' , is_approved_check_out = ' + str(is_approved_check_out) + ' WHERE ' + \
              ' id = ' + str(service)
        return db_handle.excuteCommandChangeRecord(sql)
