from func.base_model import Base
from func import db, db_handle

class M_Service(Base):

    __tablename__ = 'services'

    name                  = db.Column(db.String, nullable=True)
    user_memo             = db.Column(db.Text, nullable=True)
    is_have_user_memo     = db.Column(db.Integer, nullable=True)

    def __init__(self):
        super(M_Service, self).__init__()




