from func.base_model import Base
from func import db, db_handle

class M_Coupon(Base):
    __tablename__ = 'coupons'

    name_coupon        = db.Column(db.Text, nullable=True)
    img                = db.Column(db.Text, nullable=True)
    type_coupon        = db.Column(db.Integer, nullable=True)
    time_start_coupon  = db.Column(db.DateTime, nullable=True)
    time_end_coupon    = db.Column(db.DateTime, nullable=True)
    code_coupon        = db.Column(db.Text, nullable=True)
    is_use_again       = db.Column(db.Integer, nullable=True)
    policy             = db.Column(db.Text, nullable=True)
    management_user_id = db.Column(db.Integer, nullable=True)
    number_view_coupon = db.Column(db.Integer, nullable=True)
    number_has_coupon  = db.Column(db.Integer, nullable=True)
    number_use_coupon  = db.Column(db.Integer, nullable=True)

    def __init__(self):
        super(M_Coupon, self).__init__()


