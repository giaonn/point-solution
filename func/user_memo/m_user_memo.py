from func.base_model import Base
from func import db, db_handle

class M_User_Memo(Base):

    __tablename__ = 'user_memo_to_services'

    service_id        = db.Column(db.Integer, nullable=True)
    user_id           = db.Column(db.Integer, nullable=True)
    information_json  = db.Column(db.Text, nullable=True)
    data_json         = db.Column(db.Text, nullable=True)
    config_json       = db.Column(db.Text, nullable=True)
    first_modified    = db.Column(db.DateTime, nullable=True)

    def __init__(self):
        super(M_User_Memo, self).__init__()




