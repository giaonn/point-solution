from func import db, table_name, db_handle
from datetime import datetime as dt
from func import config_const
import re
import hashlib, pytz

class UserMemo:
    def get_memo_by_user_service(self, user_id, service_id):
        sql = ' select * from ' + table_name.USER_MEMO + ' where user_id = ' + str(user_id) + '' \
                                ' and service_id = ' + str(service_id) + ' limit 1'
        return db_handle.executeCommand(sql)

    def create_memo_user(self, user_id, service_id):
        created = modified = db_handle.get_full_current_jav_time()

        sql = db_handle.INSERT + table_name.USER_MEMO + db_handle.LIST_MEMO_ATTRIBUTE + \
                                                        ' values (%s , %s, %s , %s) '
        params = (user_id, service_id, created, modified)

        return db_handle.excuteCommandChangeRecordParams(sql, params)

    def update_user_memo_infomation(self, user_id, service_id, config_json, data_json, first_time):
        modified = "'" + db_handle.get_full_current_jav_time() + "'"
        if not first_time:
            sql = db_handle.UPDATE + table_name.USER_MEMO + ' set config_json = \'' + str(config_json).replace('\\','\\\\') + \
                  '\', data_json = \'' + str(data_json).replace('\\','\\\\') + '\', modified = ' \
                  ' ' + modified + ' where user_id = ' + str(user_id) + ' and service_id = ' + str(service_id)
        else:
            sql = db_handle.UPDATE + table_name.USER_MEMO + ' set config_json = \'' + str(config_json).replace('\\','\\\\') + \
                  '\', data_json = \'' + str(data_json).replace('\\','\\\\') + '\', modified = ' \
                  ' ' + modified + ' , first_modified = ' + modified + ' where user_id = ' \
                  + str(user_id) + ' and service_id = ' + str(service_id)

        return db_handle.excuteCommandChangeRecord(sql)

    def update_user_memo_config(self, user_id, service_id, config_json):
        modified = "'" + db_handle.get_full_current_jav_time() + "'"
        sql = db_handle.UPDATE + table_name.USER_MEMO + ' set config_json = \'' + str(config_json).replace('\\','\\\\') + \
            '\', modified = ' + modified + ' where user_id = ' + str(user_id) + ' and service_id = ' + str(service_id)

        return db_handle.excuteCommandChangeRecord(sql)

    def update_user_memo_value(self, user_id, service_id, data_json):
        modified = "'" + db_handle.get_full_current_jav_time() + "'"
        sql = db_handle.UPDATE + table_name.USER_MEMO + ' set data_json = \'' + str(data_json).replace('\\','\\\\') + \
            '\', modified = ' + modified + ' where user_id = ' + str(user_id) + ' and service_id = ' + str(service_id)

        return db_handle.excuteCommandChangeRecord(sql)

    def update_first_modified_date(self, user_id, service_id):
        modified = "'" + db_handle.get_full_current_jav_time() + "'"
        sql = db_handle.UPDATE + table_name.USER_MEMO + ' SET first_modified = ' + modified + \
            ' , is_first_modified = 0, modified = ' + modified + ' WHERE user_id = ' +\
            str(user_id) + ' AND service_id = ' + str(service_id)
        return db_handle.excuteCommandChangeRecord(sql)
