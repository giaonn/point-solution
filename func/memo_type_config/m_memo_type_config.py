from func.base_model import Base
from func import db, db_handle

class M_Memo_Type_Config(Base):

    __tablename__ = 'memo_type_config'

    type = db.Column(db.String, nullable=True)

    def __init__(self):
        super(M_Memo_Type_Config, self).__init__()




