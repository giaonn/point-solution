from func.service.service import Service
from func.service_company.service_company import Service_Company
from func.store_app.store import Store
from func.manager_user.manager_user import ManagerUser
from func.user_app.user import User
from func.user_memo.user_memo import UserMemo
from func.user_info.user_info import UserInfo
from func.memo_type_config.memo_type_config import MemoTypeConfig
from func.session.m_session import M_Session
from func.store_app.m_store import M_Store
from func import db_handle
from func import config_const
import copy, json, datetime
from sqlalchemy import distinct, func

class Common:
    def handle_memo_config(self, memo):
        _memo_type_config = MemoTypeConfig()
        this_config = _memo_type_config.get_config_by_type(str(memo['type']))

        response_memo = {}
        config = {}

        response_memo['id'] = str(memo['id'])
        response_memo['type'] = int(this_config['id'])
        response_memo['title'] = memo['title']
        if str(memo['type']) == 'short_text':

            config['length'] = int(memo['length'])
            config['placeholder'] = memo['placeholder']
            response_memo['config'] = config
            return response_memo

        elif memo['type'] == 'long_text':

            config['length'] = int(memo['length'])
            config['placeholder'] = memo['placeholder']
            response_memo['config'] = config
            return response_memo

        elif memo['type'] == 'image':

            config['number_of_image'] = int(memo['number_of_image'])
            response_memo['config'] = config
            return response_memo

        elif memo['type'] == 'combo':

            combo_list = []
            for j in memo['item']:
                combo_list.append(j)

            config['combo_list'] = combo_list
            response_memo['config'] = config
            return response_memo

        elif memo['type'] == 'radio':
            if str(memo['status']) == 'on':
                config['status'] = 'on'
            else:
                config['status'] = 'off'

            response_memo['config'] = config
            return response_memo

        elif memo['type'] == 'level':
            config['level'] = memo['level']
            response_memo['config'] = config
            return response_memo
        else:
            return {}

    def check_memo_config(self, memo):
        _memo_type_config = MemoTypeConfig()
        this_config = _memo_type_config.get_config_by_type(str(memo['type']))

        response_memo = {}
        config = {}

        response_memo['id'] = str(memo['id'])
        response_memo['type'] = int(this_config['id'])
        response_memo['title'] = str(memo['title'].encode('utf-8'))
        if str(memo['type']) == 'short_text':

            config['length'] = int(memo['length'])
            config['placeholder'] = str(memo['placeholder'].encode('utf-8'))
            response_memo['config'] = config
            return response_memo

        elif memo['type'] == 'long_text':

            config['length'] = int(memo['length'])
            config['placeholder'] = str(memo['placeholder'].encode('utf-8'))
            response_memo['config'] = config
            return response_memo

        elif memo['type'] == 'image':

            config['number_of_image'] = int(memo['number_of_image'])
            response_memo['config'] = config
            return response_memo

        elif memo['type'] == 'combo':

            combo_list = []
            for j in memo['item']:
                combo_list.append(str(j.encode('utf-8')))

            config['combo_list'] = combo_list
            response_memo['config'] = config
            return response_memo

        elif memo['type'] == 'radio':

            response_memo['config'] = None
            return response_memo

        elif memo['type'] == 'level':
            config['level'] = str(memo['level'].encode('utf-8'))
            response_memo['config'] = config
            return response_memo
        else:
            return {}

    def handle_null_memo_value(self, memo):
        _memo_type_config = MemoTypeConfig()
        this_config = _memo_type_config.get_config_by_type(str(memo['type']))

        response_memo = {}
        value = {}

        response_memo['id'] = str(memo['id'])
        response_memo['type'] = int(this_config['id'])
        if str(memo['type']) == 'short_text':

            value['value'] = ''
            response_memo['value'] = value
            return response_memo

        elif memo['type'] == 'long_text':

            value['value'] = ''
            response_memo['value'] = value
            return response_memo

        elif memo['type'] == 'image':

            number_of_image = int(memo['number_of_image'])
            photo_obj = {}
            photo = []
            for i in range(0, number_of_image):
                photo_item = {}
                photo_item['id'] = 'photo_' + str(i)
                photo_item['value'] = ''
                photo.append(copy.copy(photo_item))

            photo_obj['images'] = photo
            response_memo['value'] = photo_obj
            return response_memo

        elif memo['type'] == 'combo':

            value['selected_position'] = 0
            response_memo['value'] = value
            return response_memo

        elif memo['type'] == 'radio':

            if str(memo['status']) == 'on':
                value['status'] = True
            else:
                value['status'] = False

            response_memo['value'] = value
            return response_memo

        elif memo['type'] == 'level':

            value['value'] = ''
            response_memo['value'] = value
            return response_memo
        else:
            return {}

    def get_user_memo(self, service_id, user_id):
        _service = Service()
        _user_memo = UserMemo()

        if not service_id or not user_id:
            return {
                'memo_config': None,
                'memo_value': None
            }

        service_user_memo_data = _user_memo.get_memo_by_user_service(user_id, service_id)
        service = _service.get_service_by_id(int(service_id))

        if not service_user_memo_data:
            _user_memo.create_memo_user(user_id, service_id)
            service_user_memo_data = _user_memo.get_memo_by_user_service(user_id, service_id)

        user_memo_config = service_user_memo_data['config_json']
        user_memo_data   = service_user_memo_data['data_json']

        if (not user_memo_config) or (not user_memo_data):
            if service['user_memo']:
                service_user_config = service['user_memo']
                service_user_config = json.loads(service_user_config)
                config_item_array = []
                value_item_array = []
                for item in service_user_config:
                    response_config_item = self.handle_memo_config(item)
                    config_item_array.append(copy.copy(response_config_item))
                    response_value_item = self.handle_null_memo_value(item)
                    value_item_array.append(copy.copy(response_value_item))

                config_json = json.dumps(config_item_array)
                data_json = json.dumps(value_item_array)

                _user_memo.update_user_memo_infomation(user_id, service_id, config_json, data_json, True)

                user_memo_response = {
                    'memo_config': config_item_array,
                    'memo_value': value_item_array
                }

            else:
                user_memo_response = {
                    'memo_config': None,
                    'memo_value': None
                }

        else:
            user_memo_config = json.loads(user_memo_config)
            user_memo_data = json.loads(user_memo_data)

            if service['user_memo']:
                service_user_config = service['user_memo']
                service_user_config = json.loads(service_user_config)
                config_item_array = []
                value_item_array = user_memo_data
                for item in service_user_config:
                    response_config_item = self.handle_memo_config(item)
                    config_item_array.append(copy.copy(response_config_item))

                config_json = json.dumps(config_item_array)
                data_json = json.dumps(value_item_array)
                _user_memo.update_user_memo_infomation(user_id, service_id, config_json, data_json, False)

                user_memo_response = {
                    'memo_config': config_item_array,
                    'memo_value': value_item_array
                }

            else:
                user_memo_response = {
                    'memo_config': None,
                    'memo_value': None
                }

        service_user_memo_data = _user_memo.get_memo_by_user_service(user_id, service_id)
        if service_user_memo_data['first_modified']:
            user_memo_response['created'] = db_handle.get_timestamp_from_date(str(service_user_memo_data['first_modified'])) \
                                            * 1000
        else:
            user_memo_response['created'] = 0

        return user_memo_response

    def set_user_memo(self, service_id, user_id, user_memo_value):
        _user_memo = UserMemo()

        user_memo = _user_memo.get_memo_by_user_service(user_id, service_id)

        if not user_memo:
            _user_memo.create_memo_user(user_id, service_id)
            user_memo = _user_memo.get_memo_by_user_service(user_id, service_id)

        if int(user_memo['is_first_modified']) == 1:
            _user_memo.update_first_modified_date(user_id, service_id)
            user_memo = _user_memo.get_memo_by_user_service(user_id, service_id)

        user_memo_string = json.dumps(user_memo_value)
        response = _user_memo.update_user_memo_value(user_id, service_id, user_memo_string)

        return response

    def get_store_memo(self, store_id, user_id):
        _store = Store()
        _user_info = UserInfo()
        _management_user = ManagerUser()
        if store_id == 0 or user_id == 0:
            return {
                    'memo_config': None,
                    'memo_value': None
                }

        store_memo_data = _user_info.get_user_info_by_store_id_and_user_id(store_id, user_id)
        store = _store.getStoreById(store_id)

        if not store_memo_data:
            _user_info.create_user_info_by_store_id_and_user_id(store_id, user_id)
            store_memo_data = _user_info.get_user_info_by_store_id_and_user_id(store_id, user_id)

        store_memo_config = store_memo_data['config_json']
        store_memo_data = store_memo_data['data_json']

        if (not store_memo_config) or (not store_memo_data):
            if store['config_memo']:
                store_user_config = store['config_memo']
                store_user_config = json.loads(store_user_config)
                config_item_array = []
                value_item_array = []
                for item in store_user_config:
                    response_config_item = self.handle_memo_config(item)
                    config_item_array.append(copy.copy(response_config_item))
                    response_value_item = self.handle_null_memo_value(item)
                    value_item_array.append(copy.copy(response_value_item))

                config_json = json.dumps(config_item_array)
                data_json = json.dumps(value_item_array)

                _user_info.update_store_user_memo(store_id, user_id, config_json, data_json)

                store_memo_response = {
                    'memo_config': config_item_array,
                    'memo_value': value_item_array
                }
            else:
                store_memo_response = {
                    'memo_config': None,
                    'memo_value': None
                }
        else:
            try:
                store_memo_config = json.loads(store_memo_config)
                store_memo_data   = json.loads(store_memo_data)
            except:
                return None

            if store['config_memo']:
                store_user_config = store['config_memo']
                store_user_config = json.loads(store_user_config)
                config_item_array = []
                value_item_array  = store_memo_data

                for item in store_user_config:
                    response_config_item = self.handle_memo_config(item)
                    config_item_array.append(copy.copy(response_config_item))

                config_json = json.dumps(config_item_array)
                data_json = json.dumps(value_item_array)

                _user_info.update_store_user_memo(store_id, user_id, config_json, data_json)

                store_memo_response = {
                    'memo_config': config_item_array,
                    'memo_value': value_item_array
                }

            else:
                store_memo_response = {
                    'memo_config': None,
                    'memo_value': None
                }

        store_memo_data = _user_info.get_user_info_by_store_id_and_user_id(store_id, user_id)

        if not store_memo_data['management_user_id']:
            store_memo_response['management_user'] = ''
        elif int(store_memo_data['management_user_id']) > 0:
            management_user = _management_user.getUser(int(store_memo_data['management_user_id']))
            store_memo_response['management_user'] = str(management_user['name'])
        else:
            store_memo_response['management_user'] = ''

        store_memo_response['modified_time'] = db_handle.get_timestamp_from_date(str(store_memo_data['modified'])) \
                                               * 1000

        return store_memo_response

    def set_store_memo(self, store_id, user_id, store_memo_value):
        _user_info = UserInfo()

        store_memo = _user_info.get_user_info_by_store_id_and_user_id(store_id, user_id)

        if not store_memo:
            _user_info.create_user_info_by_store_id_and_user_id(store_id, user_id)
            store_memo = _user_info.get_user_info_by_store_id_and_user_id(store_id, user_id)

        store_memo_string = json.dumps(store_memo_value)
        response = _user_info.update_store_user_memo_data(store_id, user_id, store_memo_string)

        return response

    def get_waiting_time(self, session_id, store_id):

        waiting_time = 0
        current_timestamp = db_handle.get_timestamp_now()
        # get list wait confirm before this session
        date_now = db_handle.get_date_current_jav_time()

        wait_confirm_user_count = M_Session.query.filter(M_Session.status == config_const.WAIT_CONFIRM,
                                                        M_Session.store_id == store_id,
                                                        M_Session.id < session_id, func.DATE(M_Session.created) == str(date_now)).count()
        if wait_confirm_user_count:

            waiting_time = wait_confirm_user_count * 600

        return waiting_time
