from func.base_model import Base
from func import db, db_handle

class M_Qr_Code_Table(Base):

    __tablename__ = 'qr_code_table_stores'

    store_id = db.Column(db.Integer, nullable=True)
    qr_code  = db.Column(db.String, nullable=True)

    def __init__(self):
        super(M_Qr_Code_Table, self).__init__()




