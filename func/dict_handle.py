from func.log_detail.m_log_details import M_Log_Detail, M_Log_Detail_New
from func import db
from flask import  request

class DictFail:
    def __init__(self, message, status_code = None):

        self.response = {}
        self.response["success"]     = False
        self.response["data"]        = None
        self.response["message"]     = message
        self.response["status_code"] = status_code

class DictSuccess:
    def __init__(self, data, message):
        self.response = {}
        self.response["success"] = True
        self.response["data"]    = data
        self.response["message"] = message


class DictSession:
    def __init__(self, arr):
        self.response = {}
        self.response['session_id']               = arr[0]
        self.response['status_check_in']          = arr[1]
        self.response['store_id']                 = arr[2]
        self.response['service_company_id']       = arr[3]
        self.response['service_id']               = arr[4]
        self.response['company_id']               = arr[5]
        self.response['number_of_waiting_people'] = arr[6]
        self.response['expire_time']              = int(arr[7])
        self.response['numerical_session']        = arr[8]
        self.response['service_name']             = arr[9]

class LogHandle:
    def __init__(self, detail = None, type = None):
        try:
            log = M_Log_Detail()
            log.detail = detail
            log.type   = type
            log.tag    = str(request.url_rule)

            db.session.add(log)
            db.session.commit()
        except Exception, e:
            print e
            db.session.rollback()
            db.session.close()

class LogOtherDB:
    def __init__(self, detail = None, type = None, time_wait = None):

        try:
            try:
                token_user = request.headers['Authorization'] if request.headers['Authorization'] else ''
                params = request.json if request.json else {}
            except Exception, e:
                token_user = ''
                params     = ''

            log = M_Log_Detail_New()
            log.detail     = detail
            log.type       = type
            log.tag        = str(request.url_rule)
            log.time_wait  = time_wait
            log.token_user = token_user
            log.params     = str(params)

            db.session.add(log)
            db.session.commit()
        except Exception, e:
            db.session.rollback()
            db.session.close()
