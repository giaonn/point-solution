import uuid
from func import db, db_handle, json_utils

class Base(db.Model):
    __abstract__ = True

    id       = db.Column(db.Integer, primary_key=True)
    key_gen  = db.Column('key_gen', db.String(256))
    created  = db.Column(db.DateTime, default=db_handle.get_full_current_jav_time())
    modified = db.Column(db.DateTime, default=db_handle.get_full_current_jav_time()
                         , onupdate=db_handle.get_full_current_jav_time())

    def __init__(self):
        self.created  = db_handle.get_full_current_jav_time()
        self.modified = db_handle.get_full_current_jav_time()

        self.key_gen = uuid.uuid4()

    def to_dict(self, exclude = None):
        return json_utils.model2dict(self, self.__class__, exclude=exclude)

    def load_json(self, json):
        return json_utils.load_json(self, self.__class__, json)

