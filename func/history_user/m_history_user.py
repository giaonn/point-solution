from func.base_model import Base
from func import db, db_handle

class M_History_User(Base):

    __tablename__ = db_handle.get_name_table_history_now()

    store_id    = db.Column(db.Integer, nullable=True)
    stamp_id    = db.Column(db.Integer, nullable=True)
    user        = db.Column(db.Integer, nullable=True)
    user_name   = db.Column(db.String, nullable=True)
    session     = db.Column(db.Integer, nullable=True)
    seat_number = db.Column(db.Integer, nullable=True)
    staff       = db.Column(db.Integer, nullable=True)
    action_code = db.Column(db.String, nullable=True)
    note        = db.Column(db.Text, nullable=True)
    current_day = db.Column(db.Integer, nullable=True)

    def __init__(self):
        super(M_History_User, self).__init__()




