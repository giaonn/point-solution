from func import db, table_name, db_handle
from datetime import datetime as dt
from func import config_const
import re
import hashlib, pytz

class HistoryUser:
    def create_user_history(self, table_name, store_id, user, user_name, session,
                            seat_number, staff, action_code, note, current_day):

        modified = created =  db_handle.get_full_current_jav_time()

        sql = db_handle.INSERT + table_name + db_handle.LIST_USER_HISTORY_ATTRIBUTE + ' values ' \
                 ' (%s , %s, %s , %s, %s , %s, %s, %s, %s, %s, %s)'

        params = (store_id, user, user_name, session, seat_number, staff, action_code, note, current_day, created, modified)

        return db_handle.excuteCommandChangeRecordParams(sql, params)

    def get_history_user_check_out(self, table_name, store_id, user, action_code):

        action_code = "'" + action_code + "'"
        sql = db_handle.SELECT + table_name + ' where store_id = ' + str(store_id) + ' and user = ' + str(user) + ' ' \
                ' and action_code = ' + action_code + ' order by created desc limit 1 '

        return db_handle.executeCommand(sql)

    def get_last_history_user_by_session(self, table_name, store_id, user, session, action_code):

        action_code = "'" + action_code + "'"
        sql = db_handle.SELECT + table_name + ' where store_id = ' + str(store_id) + ' and user = ' + str(user) + ' ' \
                'and action_code = ' + action_code + ' and session = '+ str(session) +' order by created desc limit 1'
        return db_handle.executeCommand(sql)

    def count_history_check_out(self, table_name, store_id, action_code, page, limit, getTotal):
        action_code = "'" + action_code + "'"
        created = "'" + db_handle.get_date_current_jav_time() + "'"
        startPoint = db_handle.getStartPoint(page, limit)
        if not getTotal:
            sql = ' select count(*)  as count_his from (select *  from ' + table_name + ' where store_id = ' + str(store_id) + \
                  ' and action_code = ' + action_code + ' and DATE(created) = ' + created + \
                  ' limit ' + str(startPoint) + ', ' + str(limit) + ' ) as T'
        else:
            sql = ' select count(*) as count_his from ' + table_name + ' where store_id = ' + str(store_id) + \
                  ' and action_code = ' + action_code + ' and DATE(created) = ' + created
        res = db.engine.execute(sql)
        for row in res:
            return row['count_his']

    def get_history_check_out_by_store(self, table_name, store_id, action_code, page, limit):
        action_code = "'" + action_code + "'"
        created = "'" + db_handle.get_date_current_jav_time() + "'"
        startPoint = db_handle.getStartPoint(page, limit)
        sql = ' select * from ' + table_name + ' where store_id = ' + str(store_id) + \
              ' and action_code = ' + action_code + ' and DATE(created) = ' + created + \
              ' limit ' + str(startPoint) + ', ' + str(limit)

        return db_handle.executeCommandMoreRow(sql)

    def get_history_by_id(self, table_name, id):
        sql = 'select * from ' + table_name + ' where id = ' + str(id) + ''
        return db_handle.executeCommand(sql)

    def get_history_user_by_action_code(self, table_name, store_id, user, session, action_code, current_day):
        action_code = "'" + action_code + "'"

        sql = ' select * from ' + table_name + \
              ' where store_id = '  + str(store_id) + ' '\
              ' and user = '        + str(user) + ' '\
              ' and session = '     + str(session) + ' '\
              ' and action_code = ' + str(action_code) + ' '\
              ' and current_day = ' + str(current_day) + ' limit 1'

        return db_handle.executeCommand(sql)

    def delete_history_user_not_session(self, table_name, store_id, user, action_code, current_day):
        action_code = "'" + action_code + "'"

        sql = ' select * from ' + table_name + \
              ' where store_id = '  + str(store_id) + ' '\
              ' and user = '        + str(user) + ' '\
              ' and action_code = ' + str(action_code) + ' '\
              ' and current_day = ' + str(current_day) + ' order by created desc limit 1'

        return db_handle.executeCommand(sql)
