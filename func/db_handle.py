from func import db, table_name
from datetime import datetime as dt
import time
import string
import random, math
import uuid, pytz
import hashlib
from dateutil.relativedelta import relativedelta

INSERT = ' Insert into '
UPDATE = ' Update '
DELETE = ' Delete from '
SELECT = ' Select * from '

LIST_MANAGER_USER_ATTRIBUTE  = ' (username, password, name, email, created, modified, level_id, store_id, company_id, service_company_id, is_deleted) '

LIST_USER_ATTRIBUTE          = ' (username, password, name, email, age_avg, sex, created, modified, app_id) '

LIST_SESSION_ATTRIBUTE       = ' (store_id, user_id, user_name, check_in_code, expire, status, numerical_session, created, modified) '

LIST_SESSION_ATTRIBUTE_KEY   = ' (store_id, user_id, user_name, check_in_code, expire, status, numerical_session, key_gen, created, modified) '

LIST_USER_TOKEN_ATTRIBUTE    = ' (user_id, auth_token, created, expire) '

LIST_MANAGER_TOKEN_ATTRIBUTE = ' (management_user_id, auth_token, created, expire) '

LIST_DEVICE_ATTRIBUTE        = ' (device_id, device_token, user_id, created)'

LIST_USER_HISTORY_ATTRIBUTE  = ' (store_id, user, user_name, session, seat_number, staff, action_code, note, current_day, created, modified) '

LIST_CARD_ATTRIBUTE          = ' (service_company_id, user_id, current_number_of_stamps, max_number_of_stamps, can_be_used, is_used, status, created, modified) '

LIST_STAMP_ATTRIBUTE         = ' (card_id, store_id, management_user_id, store_name, star_review, text_review, created, modified) '

LIST_MEMO_ATTRIBUTE          = ' (user_id, service_id, created, modified) '

LIST_PUSH_ATTRIBUTE          = ' (key_gen, management_user_id, title, content, send_time, sound, badge, status, created, modified, sort_description, app_id, firebase_api_key) '

LIST_REMIND_NOTIFI_ATTRIBUTE = ' (service_company_id, store_id, user_id, stamp_id, ask_to_review_status, reminder_status, created) '

LIST_USER_CONTACT_ATTRIBUTE  = ' (user_id, info, created, modified) '

def excuteCommandChangeRecord(sql):

    db.engine.execute(sql)
    return True


def excuteCommandChangeRecordParams(sql, params):

    db.engine.execute(sql, params)
    return True


def excuteManyCommandChangeRecordParams(sql, params):

    db.engine.executemany(sql, params)
    return True


def excuteCommitManyCommandChangeRecordParams():

    db.engine.commit()
    return True


def executeCommand(sql):
    result = db.engine.execute(sql)
    for row in result:
        return row

    return None

def executeCommandMoreRow(sql):
    result = db.engine.execute(sql)
    return result

def executeCommandMoreRowWithParams(sql, params):

    result = db.engine.execute(sql, params)
    return result


def isTodayFirstCheckIn(date_modified):

    dt_obj = dt.strptime(str(date_modified), "%Y-%m-%d %H:%M:%S")

    date_modified = str(dt_obj.strftime('%Y-%m-%d'))

    date_today    = str(get_date_current_jav_time())

    if date_modified == date_today:
        return False

    return True

def get_timestamp_milis_from_date(date):

    dt_obj = dt.strptime(str(date), "%Y-%m-%d %H:%M:%S")
    _time = int(time.mktime(dt_obj.timetuple())) * 1000

    return _time

def get_timestamp_from_date(date):

    dt_obj = dt.strptime(str(date), "%Y-%m-%d %H:%M:%S")
    _time = int(time.mktime(dt_obj.timetuple()))

    return _time

def get_date_from_timestamp(time_stamp):
    return dt.fromtimestamp(int(time_stamp)).strftime('%Y-%m-%d %H:%M:%S')

def get_midnight_expire_time_milis():
    expire = dt.now(tz=pytz.timezone('Asia/Tokyo')).strftime('%Y-%m-%d 23:59:59')
    dt_obj = dt.strptime(expire, "%Y-%m-%d %H:%M:%S")
    d_in_ms = int(dt_obj.strftime('%s')) * 1000
    expire_time_milis = str(d_in_ms)

    return expire_time_milis

def get_now_milis():
    time_now = get_full_current_jav_time()
    dt_obj = dt.strptime(time_now, "%Y-%m-%d %H:%M:%S")
    d_in_ms = int(dt_obj.strftime('%s')) * 1000
    time_milis = str(d_in_ms)

    return time_milis

def get_now_milis_add_one():
    time_now = get_full_current_jav_time()
    dt_obj = dt.strptime(time_now, "%Y-%m-%d %H:%M:%S")
    d_in_ms = int(dt_obj.strftime('%s')) * 1000 + 1
    time_milis = str(d_in_ms)

    return time_milis

def get_timestamp_now():
    d_in_ms = dt.now(tz=pytz.timezone('Asia/Tokyo')).strftime('%s')
    time_milis = str(d_in_ms)

    return time_milis

def getExpireToken():
    expire_time = (int(time.mktime(dt.now(tz=pytz.timezone('Asia/Tokyo')).timetuple())) + 3600*24*7*4)*1000

    return expire_time

def getMilisNow():
    _time = int(time.mktime(dt.now(tz=pytz.timezone('Asia/Tokyo')).timetuple())) * 1000
    return _time

#for page, limit
def getStartPoint(page, limit):
    startPoint = 0
    if int(page) > 1:
        startPoint = int(page) * int(limit) - int(limit)

    return  startPoint

def getPrefix():
    sql = SELECT + table_name.CONFIG_APP_STORE + ' limit 1 '
    return executeCommand(sql)

def check_table_exists(table_name, name_db):

    table_name = "'" + table_name + "'"

    sql = 'SELECT * FROM information_schema.tables WHERE table_name = ' + table_name + ' '

    return executeCommand(sql)

def create_table_history(table_name):
    sql = 'create table ' + table_name + '(' \
            ' id int(11) PRIMARY KEY NOT NULL AUTO_INCREMENT, ' \
            ' store_id int(11), ' \
            ' stamp_id int(11), ' \
            ' user int(11), ' \
            ' user_name varchar(255) COLLATE utf8_unicode_ci, ' \
            ' session int(11), ' \
            ' seat_number int(11), ' \
            ' staff int(11), ' \
            ' action_code varchar(255) COLLATE utf8_unicode_ci, ' \
            ' note text COLLATE utf8_unicode_ci, ' \
            ' current_day int(11), ' \
            ' created datetime, ' \
            ' modified datetime, ' \
            ' key_gen varchar(256) COLLATE utf8_unicode_ci ' \
            ' )' \

    return excuteCommandChangeRecord(sql)

def get_name_table_history_now():
    return dt.now(tz=pytz.timezone('Asia/Tokyo')).strftime('%Y_%m') + '_user_history'

def generate_random_code(size=6, chars=string.ascii_uppercase + string.digits):
    return ''.join(random.choice(chars) for _ in range(size))

def generate_random_id(size=10, chars=string.ascii_lowercase + string.digits):
    return ''.join(random.choice(chars) for _ in range(size))

def get_string_from_arr(arr):
    return ','.join(map(str, arr))

def get_list_arr(list_get):
    if not list_get:
        return None
    list = list_get.split(',')
    listInt = []
    for item in list:
        listInt.append(int(item))
    return listInt

def get_current_day():
    return int(dt.now(tz=pytz.timezone('Asia/Tokyo')).strftime('%d'))

def get_total_page(total_number, limit):
    return int(math.ceil(int(total_number) / float(int(limit))))

def create_key():
    key = str(uuid.uuid4())
    return key

def get_full_current_jav_time():
    return dt.now(tz=pytz.timezone('Asia/Tokyo')).strftime('%Y-%m-%d %H:%M:%S')

def get_date_current_jav_time():
    return dt.now(tz=pytz.timezone('Asia/Tokyo')).strftime('%Y-%m-%d')

def get_day_current_jav_time():
    return dt.now(tz=pytz.timezone('Asia/Tokyo')).strftime('%d')

def commit_obj():
    db.session.commit()

def subtract_5_year():
    five_year_ago = dt.now(tz=pytz.timezone('Asia/Tokyo')) - relativedelta(years=5)
    return five_year_ago.strftime('%Y-%m-%d %H:%M:%S')

def encodePass(password):
    salt = "b50b1ffe2e7320b0d97062a9663d47a7adf1379392c58c66fd978171a2be7d65"
    return hashlib.md5(salt + password).hexdigest()

def close_connect():
    db.session.close()
    db.engine.dispose()