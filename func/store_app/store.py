from func import db, table_name, db_handle
from datetime import datetime as dt
from func import config_const
import re
import hashlib, pytz
from func.store_app.m_store import M_Store
from func.manager_user.m_manager_user import M_Manager_User

class Store:

    def check_qr_code(self, qr_code):
        if not qr_code:
            return config_const.QR_CODE_NULL
        if not self.check_qr_exists(qr_code):
            return config_const.QR_CODE_NOT_EXISTS

    def check_qr_exists(self, qr_code):

        qr_code = "'" + qr_code + "'"
        sql = 'select id from ' + table_name.STORE + ' where check_in_code = ' + qr_code
        result = db.engine.execute(sql)
        count = 0

        for row in result:
            count += 1
        if count > 0:
            return True
        return False

    def getStore(self, qr_code):

        qr_code = "'" + qr_code + "'"
        sql = ' select * from ' + table_name.STORE + ' where check_in_code = ' + qr_code

        return db_handle.executeCommand(sql)

    def getStoreById(self, store_id):

        sql = ' select * from ' + table_name.STORE + ' where id = ' + str(store_id)

        return db_handle.executeCommand(sql)

    def update_numerical_session_and_today(self, store_id, numerical_session, today):
        modified = "'" + db_handle.get_full_current_jav_time() + "'"
        sql = ' update ' + table_name.STORE + ' set numerical_session_in_day = ' + str(numerical_session) + ' '+ \
                            ', modified = ' + str(modified) + ', today = ' + str(today) + ' ' \
                            ' where id = '  + str(store_id) + ' '
        try:
            db.engine.execute(sql)
            return True
        except Exception, e:
            return False

    def update_numerical_session(self, store_id, numerical_session):

        modified = "'" + db_handle.get_full_current_jav_time() + "'"
        sql = db_handle.UPDATE + table_name.STORE + ' set numerical_session_in_day = ' + str(numerical_session) + ' ' \
                            ', modified = ' + modified + ' '\
                            ' where id = '  + str(store_id)
        try:
            db.engine.execute(sql)
            return True
        except Exception, e:
            return False



    #get-store-list-of-company-which-user-has-used
    def get_store_list_of_company_which_user_has_used(self, user_id, service_company_id):

        sql = ' select S.id, S.name, S.address, sum(case when status =\'wait_confirm\' then 1 else 0 end) as \'number_of_waiting_people\' ' \
              ' , UI.check_in_amount, UI.last_check_in_time, S.latitude, S.longitude ' \
              ' from stores S ' \
              ' join info_of_user_from_stores UI on S.id = UI.store_id ' \
              ' left join sessions SS on SS.store_id = S.id ' \
              ' where UI.user_id = ' + str(user_id) + ' and S.service_company_id = ' + str(service_company_id) + ' ' \
              ' group by S.id '

        return db_handle.executeCommandMoreRow(sql)


    def get_store_by_push_id(self, push_id):

        sql = ' select S.name, S.logo from stores S join stores_push_notifications SP on S.id = SP.store_id ' \
              ' where SP.push_notification_id = ' + str(push_id) + ''



        return db_handle.executeCommandMoreRow(sql)

    def get_logo_store_by_manager(self, manager_id):
        m_manager = M_Manager_User.query.filter_by(id=manager_id).first()
        m_store = M_Store.query.filter_by(id = m_manager.store_id).first()
        return  m_store if m_store else None

    def get_store_by_service_company_id(self, service_company_id):

        sql = db_handle.SELECT + table_name.STORE + ' WHERE service_company_id = ' + str(service_company_id) + ' '

        return db_handle.executeCommandMoreRow(sql)

    def get_store_by_manager_user_id(self, user_id):

        sql = db_handle.SELECT + table_name.STORE  + ' as S join ' + table_name.MANAGEMENT_USER + ' as MU on S.id = MU.store_id ' \
                ' where MU.id = ' + str(user_id)

        return db_handle.executeCommand(sql)

    def update_store_after_create_qr_code(self, store_id, check_in_code, dayUpdate):
        modified = "'" + db_handle.get_full_current_jav_time() + "'"
        check_in_code = "'" + check_in_code + "'"

        sql = ' update ' + table_name.STORE + ' set check_in_code = ' + str(check_in_code) + ' ' \
                                                ', modified = ' + modified + ' ' \
                                                ', code_create_day = ' + str(dayUpdate) +' ' \
                                                ' where id = ' + str(store_id)

        return db_handle.excuteCommandChangeRecord(sql)

    def update_seat_manager_of_store(self, store_id, seat_manager):
        modified = "'" + db_handle.get_full_current_jav_time() + "'"

        sql = db_handle.UPDATE + table_name.STORE + ' SET seat_manager = \'' + str(seat_manager) + '\'' \
                    ' , modified = ' + modified  + ' where id = ' + str(store_id) + ' '

        return db_handle.excuteCommandChangeRecord(sql)

    def get_numerical_session_day(self, id):
        sql = db_handle.SELECT + table_name.STORE + ' WHERE id = ' + str(id)
        date_today = int(db_handle.get_day_current_jav_time())

        store = db_handle.executeCommand(sql)

        if date_today == int(store['today']):
            return int(store['numerical_session_in_day']) + 1
        else:
            modified = "'" + db_handle.get_full_current_jav_time() + "'"
            sql = db_handle.UPDATE + table_name.STORE + ' SET today = ' + str(date_today) + ' ' \
                    ' , numerical_session_in_day = 1 ' + \
                    ' , modified = ' + modified + ' WHERE id = ' + str(id) + ' '
            db_handle.excuteCommandChangeRecord(sql)
        return 1



















