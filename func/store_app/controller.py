#!/usr/bin/env python
# -*- coding: utf-8 -*-
from flask import Blueprint, jsonify, request
from func import message_const, token_const, config_const, db_handle, db, config, redis_store
#old Model
from func.user_app.user import User
from func.manager_user.manager_user import ManagerUser
from func.store_app.store import Store
from func.session.session import Session
from func.service.service import Service
from func.card.card import Card
from func.token.token import Token
from func.stamp.stamp import Stamp
from func.user_memo.user_memo import UserMemo
from func.user_info.user_info import UserInfo
from func.level.level import Level
from func.push_notification.push_notification import Push_notifi
from func.company.company import Company
from func.device.device import Device
from func.config_app_store.config_app_store import  Config_App_Store
from func.store_push_notification.store_push_notification import Store_Push_Notifi
from func.user_push_notification.user_push_notification import User_Push_Notifi
from func.service_company.service_company import Service_Company
from func.history_user.history_user import HistoryUser
from func.reminder_notification.reminder_notification import ReminderNotification
from func.question.question import Question
#new Model
from func.contact.m_contact import M_Contact
from func.user_app.m_user import M_User
from func.manager_user.m_manager_user import M_Manager_User
from func.store_app.m_store import M_Store
from func.session.m_session import M_Session
from func.service.m_service import M_Service
from func.card.m_card import M_Card
from func.token.m_token import M_Token
from func.stamp.m_stamp import M_Stamp
from func.user_memo.m_user_memo import M_User_Memo
from func.user_info.m_user_info import M_User_Info
from func.level.m_level import M_Level
from func.push_notification.m_push_notification import M_Push_Notification
from func.company.m_company import M_Company
from func.device.m_device import M_Device
from func.config_app_store.m_config_app_store import  M_Config_App_Store
from func.store_push_notification.m_store_push_notification import M_Store_Push_Notifi
from func.user_push_notification.m_user_push_notification import M_User_Push_Notifi
from func.service_company.m_service_company import M_Service_Company
from func.history_user.m_history_user import M_History_User
from func.reminder_notification.m_reminder_noti import M_Reminder_Noti
from sqlalchemy.sql.expression import func
from func.qr_code_table_store.qr_code_table_store import M_Qr_Code_Table
from func.version.m_version import M_Version
from func.server_informations.m_server_informations import M_Server_Information
#Timeline Model
from func.timeline_user_store.m_comment import M_Comment
from func.timeline_user_store.m_comment_count_like import M_Comment_Count_Like
from func.timeline_user_store.m_like import M_Like
from func.timeline_user_store.m_timeline import M_Timeline
from func.timeline_user_store.m_timeline_count_like import M_Timeline_Count_Like
from func.timeline_user_store.m_user_comment_like import M_User_Comment_Like
from func.timeline_user_store.m_user_timeline_like import M_User_Timeline_Like
from func.chat.m_chat import M_Chat
from func.rich_message.m_rich_message import M_Rich_Message
from func.apps.m_app import M_App
from func.encrypt_store.m_encrypt_store import M_Encrypt_Store

from func.dict_handle import DictFail, DictSuccess, DictSession, LogHandle
from func.auth_utils import  manager_user_auth_required
from datetime import datetime as dt, timedelta
from func.common import Common
import time, logging, copy, math, json, uuid, pytz
from dateutil.relativedelta import relativedelta
from func.how_use_app.format_web import FormatWeb
from func.email.email_handle import EmailHandle
from func.email.company_email import CompanyEmail
from func.email import email_type_const
from func.email import admin_email
from sqlalchemy import distinct, func
import os
import requests
import string, random

from pyfcm import FCMNotification

mod_store = Blueprint('stores', __name__, url_prefix='/api/storeclient/management-users')

@mod_store.route('/register', methods = ['POST'])
def registerManagerUser():
    result = request.json if request.json else {}

    if result == {}:
        dictFail = DictFail(message_const.JSON_NONE).response
        return jsonify(dictFail)

    username = result.get('username', '')
    password = result.get('password', '')
    name     = result.get('name', '')
    email    = result.get('email', '')

    user = ManagerUser()
    resultCheck = user.checkUserInfo(username, password, name, email)

    if resultCheck == config_const.INFO_NULL:
        LogHandle(message_const.MES_NULL_INFO_USER, message_const.WARNING)
        dictFail = DictFail(message_const.MES_NULL_INFO_USER).response
        return jsonify(dictFail)
    if resultCheck == config_const.LENGTH_PASS_WRONG:
        LogHandle(message_const.LENGTH_PASS_WRONG, message_const.WARNING)
        dictFail = DictFail(message_const.LENGTH_PASS_WRONG).response
        return jsonify(dictFail)
    if resultCheck == config_const.FORMAT_EMAIL_WRONG:
        LogHandle(message_const.FORMAT_EMAIL_WRONG, message_const.WARNING)
        dictFail = DictFail(message_const.FORMAT_EMAIL_WRONG).response
        return jsonify(dictFail)
    if resultCheck == config_const.USER_EXISTS:
        LogHandle(message_const.USER_EXISTS, message_const.WARNING)
        dictFail = DictFail(message_const.USER_EXISTS).response
        return jsonify(dictFail)

    result = user.registerUser(username, User.encodePass(password), name, email)

    if result:
        data = {
            'token': 'tokentest',
            'expire_date': token_const.EXPIRE_DATE
        }

        dict = DictSuccess(data, message_const.REGISTER_SUCCESS).response
        return jsonify(dict)

    LogHandle(message_const.IMPORT_FAIL, message_const.ERROR)
    dictFail = DictFail(message_const.IMPORT_FAIL).response
    return jsonify(dictFail)

@mod_store.route('/get_service_list', methods = ['GET'])
def get_service_list():
    all_service = M_Service.query.filter().all()
    data = {}
    service_list = []
    for row in all_service:
        service = {
            'id': row.id,
            'name': row.name
        }
        service_list.append(copy.copy(service))

    data['service_list'] = service_list

    dict = DictSuccess(data, message_const.MES_NONE).response
    return jsonify(dict)

@mod_store.route('/register-store-account', methods = ['POST'])
def register_store_account():
    result = request.json if request.json else {}

    if result == {}:
        dict_fail = DictFail(message_const.JSON_NONE).response
        return jsonify(dict_fail)

    # get data
    username = result.get('username', '')
    password = result.get('password', '')
    email = result.get('email', '')
    name = result.get('name', '')
    store_name = result.get('store_name', '')
    service_id = result.get('service_id', '')
    address = result.get('address', '')
    address2 = result.get('address2', '')
    phone = result.get('phone', '')
    information = result.get('information', '')
    card_name = result.get('card_name', '')
    number_of_stamps = result.get('number_of_stamps', '')
    logo = result.get('logo', '')
    number_of_seats = result.get('number_of_seats', '')
    max_number_of_staff = result.get('max_number_of_staff', '')
    latitude = result.get('latitude', '')
    longitude = result.get('longitude', '')
    postal_code = result.get('postal_code', '')

    check_exist_account = M_Manager_User.query.filter_by(level_id=4, username=username, is_deleted=0).first()
    if (check_exist_account):
        dict_fail = DictFail(message_const.USER_EXISTS).response
        return jsonify(dict_fail)

    check_exist_account = M_Manager_User.query.filter_by(level_id=4, email=email, is_deleted=0).first()
    if (check_exist_account):
        dict_fail = DictFail(message_const.EMAIL_EXIST).response
        return jsonify(dict_fail)

    # add company
    m_company = M_Company()
    m_company.name = name
    m_company.email = email
    m_company.address = address
    m_company.phone = phone
    m_company.information = information
    m_company.logo = logo
    m_company.current_number_of_notifications = 0
    m_company.max_number_of_notifications = 1
    m_company.current_number_of_stores = 1
    m_company.max_number_of_stores = 1
    db.session.add(m_company)
    db.session.commit()

    # add service company
    m_service_company = M_Service_Company()
    m_service_company.service_id = service_id
    m_service_company.company_id = m_company.id
    m_service_company.card_name = card_name
    m_service_company.number_of_stamps_in_card = number_of_stamps
    m_service_company.current_number_of_notifications = 0
    m_service_company.max_number_of_notifications = 1
    m_service_company.current_number_of_stores = 1
    m_service_company.max_number_of_stores = 1
    m_service_company.logo = logo
    m_service_company.ip = None
    m_service_company.ask_to_review_title = '{store-name}サービス評価依頼'
    ask_to_review_content = '{user-name}様 <br/>' \
                            '<br/>'\
                            '大変お世話になっております。<br/>'\
                            '{store-name}の{admin-name}でございます。<br/>'\
                            '<br/>'\
                            'この度は{service-name}をご利用いただき、<br/>'\
                            '誠にありがとうございます。<br/>'\
                            '<br/>'\
                            '弊社のサービスに満足できますでしょうか <br/>'\
                            'お客様から頂戴した評価・コメントを楽しみに、 <br/>'\
                            'そして励みにして日々営業いたしております！<br/>'\
                            'スタッフ一同、心待ちにしておりますので <br/>'\
                            'お手数おかけいたしますが、ご評価のご協力をお願<br/>'\
                            'い申上げます。 <br/>'\
                            '<br/>'\
                            'よろしくお願いたします。'
    m_service_company.ask_to_review_content = ask_to_review_content
    m_service_company.ask_to_review_day_send_push = 10
    m_service_company.ask_to_review_sort_description = ''

    m_service_company.reminder_title = '長期間未来店お客様へ'
    m_service_company.reminder_content = '{user-name}様 <br/>' \
                                         '<br/>' \
                                         '大変お世話になっております。 <br/>' \
                                         '{store-name}の{admin-name}でございます。<br/>' \
                                         '<br/>' \
                                         'この度は{service-name}をご利用いただき、<br/>' \
                                         '誠にありがとうございます。<br/>' \
                                         '<br/>'\
                                         '{内容挿入} <br/>'\
                                         '<br/>'\
                                         'よろしくお願いたします。'
    m_service_company.reminder_day_send_push = 20
    m_service_company.reminder_sort_description = ''

    m_service_company.is_approved_check_in = 0
    m_service_company.is_approved_check_out = 1
    db.session.add(m_service_company)
    db.session.commit()

    # add store
    m_store = M_Store()
    m_store.service_company_id = m_service_company.id
    m_store.name = store_name
    m_store.address = address
    m_store.address2 = address2
    m_store.phone = phone
    m_store.check_in_code = None
    m_store.number_of_seats = number_of_seats
    m_store.seat_manager = None
    m_store.current_number_of_staff = 0
    m_store.max_number_of_staff = max_number_of_staff
    m_store.current_number_of_notifications = 0
    m_store.max_number_of_notifications = 1
    m_store.is_applied = 1
    m_store.information = information
    m_store.review = None
    m_store.review_rating = None
    m_store.notification_fee_per_people = None
    m_store.working_time = None
    m_store.road = None
    m_store.day_off = None
    m_store.way_to_pay = None
    m_store.have_parking = 0
    m_store.is_smoking = 0
    m_store.logo = logo
    m_store.info_of_user = None
    m_store.is_deleted = 0
    m_store.code_create_day = None
    m_store.numerical_session_in_day = 0
    m_store.today = 0
    m_store.latitude = latitude
    m_store.longitude = longitude
    m_store.secret_code = None
    m_store.note_json = None
    m_store.postal_code = postal_code
    db.session.add(m_store)
    db.session.commit()

    # get admin account
    admin_account = M_Manager_User.query.filter_by(level_id=1).first()
    # admin_company = M_Company.query.filter_by(id=admin_account.company_id).first()
    # admin_company.current_number_of_notifications = admin_company.current_number_of_notifications + 1
    # admin_company.current_number_of_stores = admin_company.current_number_of_stores + 1
    db.session.commit()

    # add management user
    m_user = M_Manager_User()
    _management_user = ManagerUser()
    m_user.username = username
    m_user.email = email
    m_user.password = _management_user.encodePass(password)
    m_user.name = name
    m_user.phone = phone
    m_user.level_id = 4
    m_user.store_id = m_store.id
    m_user.company_id = m_company.id
    m_user.service_company_id = m_service_company.id
    m_user.parent_management_user_id = admin_account.id
    m_user.is_deleted = 0
    m_user.last_login = None
    db.session.add(m_user)
    db.session.commit()

    data = create_auth_token(m_user)

    dict = DictSuccess(data, message_const.REGISTER_SUCCESS).response
    return jsonify(dict)

@mod_store.route('/login', methods = ['POST'])
def loginUser():
    result = request.json if request.json else {}

    if result == {}:
        dictFail = DictFail(message_const.JSON_NONE).response
        return jsonify(dictFail)

    username = result.get('username', '')
    password = result.get('password', '')
    _user = ManagerUser()
    _token = Token()
    _level = Level()

    user = _user.loginSuccess(username, password)

    if not user:
        LogHandle(message_const.LOGIN_FAIL, message_const.WARNING)
        dictFail = DictFail(message_const.LOGIN_FAIL).response
        return jsonify(dictFail)

    m_store = M_Store.query.filter_by(id=user['store_id']).first()

    if not m_store:
        LogHandle(message_const.LOGIN_FAIL, message_const.WARNING)
        dictFail = DictFail(message_const.LOGIN_FAIL).response
        return jsonify(dictFail)

    newToken = str(uuid.uuid4())
    user_id = user['id']
    expire = db_handle.getExpireToken()

    res_create_token = _token.createTokenManagerUser(user_id, newToken, expire)

    if not res_create_token:
        LogHandle(message_const.CREATE_TOKEN_FAIL, message_const.WARNING)
        dictFail = DictFail(message_const.CREATE_TOKEN_FAIL).response
        return jsonify(dictFail)

    level = _level.get_level_by_id(user['level_id'])

    data = {
        'token': newToken,
        'expire_date': expire,
        'level': user['level_id'],
        'level_name': level['name'],
        'username': user['name'],
        'email': user['email'],
        'img_store' : m_store.logo if m_store.logo else ''
    }

    dict = DictSuccess(data, message_const.LOGIN_SUCCESS).response
    return jsonify(dict)

@mod_store.route('/generate-string-for-code-store', methods = ['GET'])
@manager_user_auth_required()
def generate_string_for_code_store(user = None):
    if not user:
        LogHandle(message_const.TOKEN_EXPIRE, message_const.WARNING)
        db_handle.close_connect()
        dictFail = DictFail(message_const.TOKEN_EXPIRE).response
        return jsonify(dictFail)

    user_id = user['id']
    data = {}

    _store = Store()
    store  = _store.get_store_by_manager_user_id(user_id)
    if not store:
        LogHandle(message_const.STORE_NOT_FOUND, message_const.WARNING)
        db_handle.close_connect()
        dictFail = DictFail(message_const.STORE_NOT_FOUND).response
        return jsonify(dictFail)

    res_config_app = db_handle.getPrefix()
    prefix = res_config_app['prefix']
    dayToday = db_handle.get_current_day()
    service_company = M_Service_Company.query.filter(M_Service_Company.id == store['service_company_id']).first()

    if service_company.is_approved_check_in == 1:
        data['expire'] = "9999999999999"

        if not store['check_in_code']:
            check_in_code = str(prefix) + '.' + str(user_id) + db_handle.get_timestamp_now()
            _store.update_store_after_create_qr_code(store['store_id'], check_in_code, dayToday)
        else:
            check_in_code = store['check_in_code']

        data['check_in_code'] = check_in_code
    else:

        data['expire'] = db_handle.get_midnight_expire_time_milis()

        if store['check_in_code'] or store['code_create_day']:
            if dayToday == store['code_create_day']:
                data['check_in_code'] = store['check_in_code']
            else:
                check_in_code = str(prefix) + '.' + str(user_id) + db_handle.get_timestamp_now()
                data['check_in_code'] = str(check_in_code)
                _store.update_store_after_create_qr_code(store['store_id'], check_in_code, dayToday)
        else:
            check_in_code = str(prefix) + '.' + str(user_id) + db_handle.get_timestamp_now()
            data['check_in_code'] = str(check_in_code)
            _store.update_store_after_create_qr_code(store['store_id'], check_in_code, dayToday)
    try:
        server_type = os.environ.get('Server_Type')
        m_encrypt_store = M_Encrypt_Store.query.filter_by(store_id=user['store_id']).first()

        if not m_encrypt_store:
            code_store = ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(10))
            m_encrypt_store = M_Encrypt_Store()
            m_encrypt_store.code_store = code_store
            m_encrypt_store.store_id   = user['store_id']

            db.session.add(m_encrypt_store)
            db.session.commit()

        if server_type == 'DEV':
            data['url_checkin'] = 'http://54.92.40.147/fh/' + m_encrypt_store.code_store + ''
        elif server_type == 'PROD':
            data['url_checkin'] = 'http://13.113.185.172/fh/' + m_encrypt_store.code_store + ''
        else:
            data['url_checkin'] = 'http://localhost:8765/fh/' + m_encrypt_store.code_store + ''

    except Exception, e:
        data['url_checkin'] = ''

    db_handle.close_connect()
    dict = DictSuccess(data, message_const.SUCCESS_MES).response
    return jsonify(dict)


@mod_store.route('/confirm-check-in', methods = ['POST'])
@manager_user_auth_required()
def confirm_check_in(user = None):
    if not user:
        dictFail = DictFail(message_const.TOKEN_EXPIRE).response
        return jsonify(dictFail)

    result = request.json if request.json else {}

    if result == {}:
        dictFail = DictFail(message_const.JSON_NONE).response
        return jsonify(dictFail)

    session_id  = result.get('session_id', '')
    seat_number = result.get('seat_number', '')

    _session = Session()
    _store = Store()
    _service_company = Service_Company()
    _user_info = UserInfo()

    session = _session.get_session_by_id(session_id)
    store = _store.getStoreById(str(session['store_id']))
    service_company = _service_company.get_service_from_service_company(str(store['service_company_id']))
    is_approved_check_in = service_company['is_approved_check_in']
    is_approved_check_out = service_company['is_approved_check_out']

    user_id = str(session['user_id'])
    if int(user_id) == 0:
        user_name = str(session['user_name'].encode('utf-8'))
    else:
        m_user    = M_User.query.filter_by(id = user_id).first()
        user_name = m_user.name if m_user else ''
    data = {}
    if not session:
        dict_fail = DictFail(message_const.UNDEFINED_SESSION).response
        return jsonify(dict_fail)

    if session['status'] == config_const.CHECKED_IN:
        data['status'] = config_const.CHECKED_IN
        dict = DictSuccess(data, message_const.CHECKED_IN).response
        return jsonify(dict)

    #__setStoreUser
    updateRes = _session.update_session_by_manager_user(user['store_id'], seat_number,
                                                        config_const.CHECKED_IN, session_id)

    if not updateRes:
        dictFail = DictFail(message_const.UNDEFINED_SESSION).response
        return jsonify(dictFail)

    if int(user_id) > 0:
        userInfo = _user_info.get_user_info_by_store_id_and_user_id(session['store_id'], session['user_id'])

        if not userInfo:
            resCreate = _user_info.create_user_info_by_store_id_and_user_id_contain_check_in_time(session['store_id'], session['user_id'])
            if not resCreate:
                dictFail = DictFail(message_const.MES_NONE).response
                return jsonify(dictFail)

            userInfo = _user_info.get_user_info_by_store_id_and_user_id(session['store_id'], session['user_id'])

        check_in_amount = int(userInfo['check_in_amount']) + 1
        _user_info.update_user_info(userInfo['store_id'], userInfo['user_id'], check_in_amount)

    #_save_user_history
    _save_history = save_user_history(int(session['store_id']), int(session['user_id']), int(session['id']),
                                      user_name, int(session['staff']), int(seat_number),
                                      config_const.CHECKED_IN, ' ')

    M_Session.query.filter(M_Session.id == session['id']).update({
        M_Session.check_in_time: db_handle.get_full_current_jav_time()
    })
    db.session.commit()

    if not _save_history:
        dictFail = DictFail(message_const.MES_NONE).response
        return jsonify(dictFail)

    if is_approved_check_out == 0:
        save_user_history(int(session['store_id']), int(session['user_id']), int(session['id']),
                                          user_name, int(session['staff']), int(seat_number),
                                          config_const.CHECKED_OUT, ' ')

        data['status'] = config_const.CHECKED_OUT

        _session.delete_session(str(session['id']))

        if int(user_id) > 0:
            add_stamp_for_user(int(service_company['id']), int(session['store_id']), int(session['user_id']), int(user['id'])
                               , int(seat_number), session_id, user_name, 0, 1)

        dict = DictSuccess(data, config_const.CHECKED_OUT).response
        return jsonify(dict)

    data['status'] = config_const.CHECKED_IN

    dict = DictSuccess(data, str(user_name) + message_const.MATCH_CHECK_IN).response
    return jsonify(dict)

def save_add_stamp_history(stamp_id, user_id, session_id, username, staff, seatNumber, actionCode, note):
    name_history_table = db_handle.get_name_table_history_now()
    try:
        checkTableExists = db_handle.check_table_exists(name_history_table, '')

        if not checkTableExists:
            db_handle.create_table_history(name_history_table)

        currentDay = db_handle.get_current_day()

        m_history_user = M_History_User()
        m_history_user.stamp_id    = stamp_id
        m_history_user.user        = user_id
        m_history_user.user_name   = username
        m_history_user.session     = session_id
        m_history_user.seat_number = seatNumber
        m_history_user.staff       = staff
        m_history_user.action_code = actionCode
        m_history_user.note        = note
        m_history_user.current_day = currentDay

        db.session.add(m_history_user)
        db.session.commit()
        return True
    except Exception, e:
        return False


def save_user_history(store_id, user_id, session_id, username, staff, seatNumber, actionCode, note):

    if actionCode == config_const.CHECKED_IN:
        response = get_list_seat_of_store(store_id)

        if response['success']:
            seatArr = response['data']
            staff = 0

            for seat in seatArr:
                if seatNumber == seat['number']:
                    staff = seat['staff_id']
                    break

        else:
            staff = 0

        _session = Session()
        _session.update_staff_of_session(staff, session_id)

    name_history_table = db_handle.get_name_table_history_now()

    checkTableExists = db_handle.check_table_exists(name_history_table, '')

    if not checkTableExists:
        res_create_table = db_handle.create_table_history(name_history_table)
        if not res_create_table:
            return False

    _history_user = HistoryUser()
    currentDay    = db_handle.get_current_day()

    res_create_history_user = _history_user.create_user_history(name_history_table, store_id, user_id,
                                                                username, session_id, seatNumber,
                                                                staff, actionCode, note, currentDay)

    if not res_create_history_user:
        return False

    return True

def get_list_seat_for_info_user(store_id):
    m_store = M_Store.query.filter_by(id = store_id).first()
    info = {}
    seatArr = []
    try:
        if m_store.number_of_seats:
            count = 1
            while(count <= int(m_store.number_of_seats)):
                info['number'] = count
                info['status'] = 0
                seatArr.append(copy.copy(info))
                count += 1

            session_list = M_Session.query.filter(M_Session.store_id == store_id).all()

            for session in session_list:
                for seat in seatArr:
                    if session.seat_number == seat['number']:
                        seat['status'] = 1
            success = True
            message = ''
            data    = seatArr
        else:
            success = False
            message = message_const.NUMBER_SEAT_NULL
            data = []
    except:
        success = False
        message = message_const.NUMBER_SEAT_NULL
        data = []

    response = {}
    response['success'] = success
    response['message'] = message
    response['data'] = data

    return response

def get_list_seat_of_store(store_id):
    _store = Store()

    store = _store.getStoreById(store_id)

    if store['number_of_seats']:

        if store['seat_manager']:
            seatArr = json.loads(store['seat_manager'])
            if(len(seatArr) !=  store['number_of_seats']):
                seatArr = []
                info = {}
                count = 1
                while count <= store['number_of_seats']:
                    info['number'] = count
                    info['status'] = 0
                    info['staff_id'] = 0
                    info['staff_name'] = ''
                    seatArr.append(copy.copy(info))
                    count += 1
                _store.update_seat_manager_of_store(store_id, str(json.dumps(seatArr)))

        else:
            seatArr = []
            info = {}
            count = 1
            while count <= store['number_of_seats']:
                info['number'] = count
                info['status'] = 0
                info['staff_id'] = 0
                info['staff_name'] = ''
                seatArr.append(copy.copy(info))
                count += 1
            _store.update_seat_manager_of_store(store_id, str(json.dumps(seatArr)))

        success = True
        message = ''
        data    = seatArr

    else:
        success = False
        message = message_const.NUMBER_SEAT_NULL
        data    = []

    response = {}
    response['success'] = success
    response['message'] = message
    response['data']    = data

    return response


@mod_store.route('/get-store-detail', methods = ['GET'])
@manager_user_auth_required()
def get_store_detail(user = None):
    if not user:
        db_handle.close_connect()
        dictFail = DictFail(message_const.USER_NOT_EXISTS).response
        return jsonify(dictFail)

    manager_id = user['id']

    m_manager = M_Manager_User.query.filter_by(id = manager_id).first()
    data =  {}
    if not m_manager:
        db_handle.close_connect()
        dictFail = DictFail(message_const.JSON_NONE).response
        return jsonify(dictFail)

    m_store = M_Store.query.filter_by(id=m_manager.store_id).first()
    data['name'] = m_store.name
    data['store_id'] = m_store.id
    data['sort_description'] = m_store.sort_description
    data['logo'] = m_store.logo

    db_handle.close_connect()
    dictSucess = DictSuccess(data, message_const.SUCCESS_MES).response
    return jsonify(dictSucess)


@mod_store.route('/get-store-info', methods = ['GET'])
@manager_user_auth_required()
def get_store_info(user = None):
    if not user:
        db_handle.close_connect()
        dictFail = DictFail(message_const.USER_NOT_EXISTS).response
        return jsonify(dictFail)

    manager_id = user['id']

    m_manager = M_Manager_User.query.filter_by(id = manager_id).first()
    data = m_manager.to_dict()

    data.pop('created', None)
    data.pop('username', None)
    data.pop('firebase_api_key', None)
    data.pop('last_login', None)
    data.pop('modified', None)
    data.pop('parent_management_user_id', None)
    data.pop('password', None)
    data.pop('username', None)
    data.pop('is_deleted', None)
    data.pop('key_gen', None)

    db_handle.close_connect()
    dictSucess = DictSuccess(data, message_const.SUCCESS_MES).response
    return jsonify(dictSucess)


@mod_store.route('/get-store-information-by-code', methods = ['POST'])
def get_store_information_by_code(user = None):

    result = request.json if request.json else {}
    if result == {}:
        dictFail = DictFail(message_const.JSON_NONE).response
        return jsonify(dictFail)

    qr_code = result.get('qr_code', '')

    _store = Store()
    is_qr_code_table = False

    if qr_code[:4] == 'fast':
        qr_code = qr_code[5:]

    result_check_qr_code = _store.check_qr_code(qr_code)
    if result_check_qr_code == config_const.QR_CODE_NULL:
        dict_fail = DictFail(message_const.WRONG_QR_CODE).response
        return jsonify(dict_fail)
    if result_check_qr_code == config_const.QR_CODE_NOT_EXISTS:
        m_qr_table = M_Qr_Code_Table.query.filter(M_Qr_Code_Table.qr_code == qr_code).first()
        if not m_qr_table:
            dict_fail = DictFail(message_const.WRONG_QR_CODE).response
            return jsonify(dict_fail)
        is_qr_code_table = True

    # qr code table
    if is_qr_code_table == True:
        store = _store.getStoreById(m_qr_table.store_id)
    else:  # qr code normal
        store = _store.getStore(qr_code)
        service_company = M_Service_Company.query.filter(M_Service_Company.id == store['service_company_id']).first()
        if service_company.is_approved_check_in != 1:
            code_create_day = str(store['code_create_day'])
            currentDay = dt.now(tz=pytz.timezone('Asia/Tokyo')).strftime('%d')

            if int(code_create_day) != int(currentDay):
                dictFail = DictFail(message_const.QR_CODE_EXPIRE).response
                return jsonify(dictFail)

    prefix = ''
    data = {}

    logo = str(store['logo']) if store['logo'] else ''

    data['store_id']    = store['id']
    data['store_name']  = store['name']
    data['address']     = store['address']
    data['phone']       = store['phone']
    data['information'] = store['information']
    data['logo']        = prefix + logo

    dictSucess = DictSuccess(data, message_const.SUCCESS_MES).response
    return jsonify(dictSucess)


@mod_store.route('/get-check-in-status-by-session-id', methods = ['POST'])
@manager_user_auth_required()
def get_check_in_status_by_session_id(user = None):
    if not user:
        dictFail = DictFail(message_const.USER_NOT_EXISTS).response
        return jsonify(dictFail)

    result = request.json if request.json else {}
    if result == {}:
        dictFail = DictFail(message_const.JSON_NONE).response
        return jsonify(dictFail)

    session_id = result.get('session_id', 0)
    _session = Session()

    session = _session.get_session_by_id(session_id)

    if not session:
        dictFail = DictFail(message_const.UNDEFINED_SESSION).response
        return jsonify(dictFail)

    data ={}
    data['status_check_in'] = session['status']

    dictSucess = DictSuccess(data, message_const.MES_NONE).response
    return jsonify(dictSucess)

@mod_store.route('/get-seat-number', methods = ['POST'])
@manager_user_auth_required()
def get_seat_number(user = None):
    if not user:
        dictFail = DictFail(message_const.USER_NOT_EXISTS).response
        return jsonify(dictFail)

    result = request.json if request.json else {}
    if result == {}:
        dictFail = DictFail(message_const.JSON_NONE).response
        return jsonify(dictFail)

    session_id = result.get('session_id', 0)
    _session = Session()

    session = _session.get_session_by_id(session_id)

    if not session:
        dictFail = DictFail(message_const.UNDEFINED_SESSION).response
        return jsonify(dictFail)

    _store = Store()
    store = _store.getStoreById(session['store_id'])
    _management_user = ManagerUser()

    if not store['number_of_seats']:
        dictFail = DictFail(message_const.NUMBER_SEAT_NULL).response
        return jsonify(dictFail)

    #Create seat_manager
    if store['seat_manager']:
        seatArr = json.loads(store['seat_manager'])
        if (len(seatArr) != store['number_of_seats']):
            seatArr = []
            info = {}
            count = 1
            while count <= store['number_of_seats']:
                info['number'] = count
                info['status'] = 0
                info['staff_id'] = 0
                info['staff_name'] = ''
                seatArr.append(copy.copy(info))
                count += 1
            _store.update_seat_manager_of_store(session['store_id'], str(seatArr))

    else:
        seatArr = []
        info = {}
        count = 1
        while count <= store['number_of_seats']:
            info['number'] = count
            info['status'] = 0
            info['staff_id'] = 0
            info['staff_name'] = ''
            seatArr.append(copy.copy(info))
            count += 1
        _store.update_seat_manager_of_store(session['store_id'], str(seatArr))

    sessionList = _session.get_list_seat_number(session['store_id'])

    array_seat_response = []
    seat_response = {}
    for item in seatArr:
        seat_response['number'] = item['number']
        seat_response['status'] = item['status']
        seat_response['staff_id'] = item['staff_id']
        seat_response['staff_name'] = str(item['staff_name'])

        array_seat_response.append(copy.copy(seat_response))

    for item_session in sessionList:
        for item_seat_response in array_seat_response:
            if int(item_seat_response['number']) == int(item_session['seat_number']):
                item_seat_response['status'] = 1
                if item_session['staff'] > 0:
                    # select staff name
                    staff = _management_user.get_staff_name_by_id(item_session['staff'])
                    if staff:
                        item_seat_response['staff_id'] = item_session['staff']
                        item_seat_response['staff_name'] = staff['name']

    data = {}
    data['seat_number'] = session['seat_number']
    data['seat_list']   = array_seat_response
    dictSucess = DictSuccess(data, message_const.MES_NONE).response
    return jsonify(dictSucess)

@mod_store.route('/get-info-management-user', methods = ['GET'])
@manager_user_auth_required()
def get_info_management_user(user = None):
    if not user:
        dictFail = DictFail(message_const.USER_NOT_EXISTS).response
        return jsonify(dictFail)
    _company = Company()
    _store   = Store()
    _level   = Level()

    company = _company.get_company_by_id(user['company_id'])
    store   = _store.getStoreById(user['store_id'])
    level   = _level.get_level_by_id(user['level_id'])

    data = {}
    info = {}
    info['username']   = user['username']
    info['name']       = user['name']
    info['email']      = user['email']
    info['level']      = level['id']
    info['level_name'] = level['name']
    info['store']      = store['name']
    info['company']    = company['name']

    data['users'] = info

    dictSucess = DictSuccess(data, message_const.MES_NONE).response
    return jsonify(dictSucess)

@mod_store.route('/get-list-user-by-status', methods = ['POST'])
@manager_user_auth_required()
def get_list_user_by_status(user = None):
    if not user:
        dictFail = DictFail(message_const.USER_NOT_EXISTS).response
        return jsonify(dictFail)

    result = request.json if request.json else {}
    if result == {}:
        dictFail = DictFail(message_const.JSON_NONE).response
        return jsonify(dictFail)

    status = result.get('status', '')

    _session      = Session()
    _user         = User()
    _card         = Card()
    _manager_user = ManagerUser()
    _history_user = HistoryUser()

    #Check current has history?
    name_history_table = db_handle.get_name_table_history_now()

    checkTableExists = db_handle.check_table_exists(name_history_table, '')

    sessionList = _session.get_list_session_by_store_and_status(user['store_id'], status)

    m_store = M_Store.query.filter_by(id = user['store_id']).first()

    data = {}
    users = []
    for session in sessionList:
        #Start foreach
        if session['staff']:
            staff = _manager_user.getUser(session['staff'])
            if staff:
                staffName = staff['name']
            else:
                staffName = ''
        else:
            staffName = ''

        if session['user_id']:
            #Start if session
            normal_user = _user.getUser(session['user_id'])

            resCard = _card.get_card_amount(m_store.service_company_id, session['user_id'], config_const.CAN_USE)
            card_amount = resCard['card_amount']
            if checkTableExists:
                #Start if check table
                last_check_out_user = _history_user.get_history_user_check_out(
                    name_history_table, user['store_id'], normal_user['id'], config_const.CHECKED_OUT)
                if last_check_out_user:
                    lastCheckOutTime = db_handle.get_timestamp_milis_from_date(last_check_out_user['created']) \
                        if last_check_out_user['created'] else 0
                else:
                    lastCheckOutTime = 0

                count = 1
                while(count < 4):
                    table_name_history_before = get_name_table_after_subtract_month(count)

                    checkTableExists = db_handle.check_table_exists(table_name_history_before, '')

                    if checkTableExists:
                        last_check_out_user_before = _history_user.get_history_user_check_out(
                            table_name_history_before, user['store_id'], normal_user['id'], config_const.CHECKED_OUT)
                        if last_check_out_user_before:
                            lastCheckOutTime = db_handle.get_timestamp_milis_from_date(last_check_out_user_before['created']) \
                                if last_check_out_user_before['created'] else 0
                    else:
                        break
                    count+=1

                #End if check table
            else:
                lastCheckOutTime = 0

            userItem = {}
            userItem['session_id'] = session['id']
            userItem['name'] = normal_user['name']
            userItem['seat_number'] = session['seat_number']
            userItem['staff_id'] = session['staff']
            userItem['staff_name'] = staffName
            userItem['numerical_session'] = session['numerical_session']
            userItem['card_amount'] = card_amount
            userItem['time_wait_confirm'] = db_handle.get_timestamp_milis_from_date(session['created']) if session['created'] else 0
            userItem['time_check_in'] = db_handle.get_timestamp_milis_from_date(session['check_in_time']) if session['check_in_time'] else 0
            userItem['last_check_out_time'] = lastCheckOutTime
            users.append(copy.copy(userItem))
            #End if session
        else:
            userItem = {}
            userItem['session_id']          = session['id']
            userItem['name']                = session['user_name']
            userItem['seat_number']         = session['seat_number']
            userItem['staff_id']            = session['staff']
            userItem['staff_name']          = staffName
            userItem['numerical_session']   = session['numerical_session']
            userItem['card_amount']         = 0
            userItem['time_wait_confirm']   = db_handle.get_timestamp_milis_from_date(session['created']) if session['created'] else 0
            userItem['time_check_in']       = db_handle.get_timestamp_milis_from_date(session['check_in_time']) if session['check_in_time'] else 0
            userItem['last_check_out_time'] = 0
            users.append(copy.copy(userItem))
        #End foreach
    data['users'] = users

    dictSucess = DictSuccess(data, message_const.GET_LIST_USER_CHECK_IN_SUCCESS).response
    return jsonify(dictSucess)

@mod_store.route('/get-info-of-user-from-store', methods = ['POST'])
@manager_user_auth_required()
def get_info_user_from_store(user = None):
    if not user:
        dictFail = DictFail(message_const.USER_NOT_EXISTS).response
        return jsonify(dictFail)

    result = request.json if request.json else {}
    if result == {}:
        dictFail = DictFail(message_const.JSON_NONE).response
        return jsonify(dictFail)

    session_id = result.get('session_id', 0)

    _session = Session()
    _common = Common()
    session = _session.get_session_by_id(session_id)

    if not session:
        dictFail = DictFail(message_const.UNDEFINED_SESSION).response
        return jsonify(dictFail)

    store_memo_response = _common.get_store_memo(int(session['store_id']), int(session['user_id']))

    if not store_memo_response:
        dictFail = DictFail(message_const.MES_NONE).response
        return jsonify(dictFail)
    dict_success = DictSuccess(store_memo_response, message_const.MES_NONE).response
    return jsonify(dict_success)

def _get_info_user_from_store(store_id, user_id):
    data = {}
    photo = {}
    if not store_id and not user_id:
        data['note'] = ''

        photo['photo_1'] = ''
        photo['photo_2'] = ''
        photo['photo_3'] = ''
        photo['photo_4'] = ''

        data['photo'] = photo

        data['management_user'] = ''
        data['modified_time'] = 0

        return data
    _info_user = UserInfo()
    _manager_user = ManagerUser()

    infoUser = _info_user.get_user_info_by_store_id_and_user_id(store_id, user_id)

    if not infoUser:
        _info_user.create_user_info_by_store_id_and_user_id(store_id, user_id)
        infoUser = _info_user.get_user_info_by_store_id_and_user_id(store_id, user_id)

    if infoUser['information_json']:
        info = json.loads(infoUser['information_json'])

        data['note'] = info['note'] if info['note'] else ''
        photo['photo_1'] = str(info['photo']['photo_1']) if info['photo']['photo_1'] else ''
        photo['photo_2'] = str(info['photo']['photo_2']) if info['photo']['photo_2'] else ''
        photo['photo_3'] = str(info['photo']['photo_3']) if info['photo']['photo_3'] else ''
        photo['photo_4'] = str(info['photo']['photo_4']) if info['photo']['photo_4'] else ''
        data['photo'] = photo
    else:
        data['note'] = ''
        photo['photo_1'] = ''
        photo['photo_2'] = ''
        photo['photo_3'] = ''
        photo['photo_4'] = ''
        data['photo'] = photo

    if infoUser['management_user_id']:
        managementUserUpdateInfo = _manager_user.getUser(infoUser['management_user_id'])
        managementUserName = managementUserUpdateInfo['name']
    else:
        managementUserName = ''
    time_modifi = db_handle.get_timestamp_milis_from_date(infoUser['modified']) if infoUser['modified'] else 0
    data['management_user'] = managementUserName
    data['modified_time'] = time_modifi if managementUserName != '' else 0
    return data

@mod_store.route('/check-out-session', methods = ['POST'])
@manager_user_auth_required()
def check_out_session(user = None):
    if not user:
        dict_fail = DictFail(message_const.USER_NOT_EXISTS).response
        return jsonify(dict_fail)

    result = request.json if request.json else {}
    if result == {}:
        dict_fail = DictFail(message_const.JSON_NONE).response
        return jsonify(dict_fail)

    session_id = result.get('session_id', 0)

    _session      = Session()
    _history_user = HistoryUser()
    session = _session.get_session_by_id(session_id)

    if not session:
        dict_fail = DictFail(message_const.UNDEFINED_SESSION).response
        return jsonify(dict_fail)

    if str(session['status']) != 'checked_in':
        dict_fail = DictFail(message_const.UNCHECK_IN_USER).response
        return jsonify(dict_fail)

    data = {}

    this_user = M_User.query.filter(M_User.username == session['user_name'], M_User.is_deleted == 0).first()
    if this_user:
        this_session_username = this_user.name
    else:
        this_session_username = session['user_name']

    session_id          = session['id']
    session_store_id    = session['store_id']
    session_user_id     = session['user_id']
    session_seat_number = session['seat_number']
    session_username    = str(this_session_username.encode('utf-8'))
    session_staff       = session['staff']

    delSession = _session.delete_session(session_id)
    if not delSession:
        dictFail = DictFail(message_const.DELETE_FAIL).response
        return jsonify(dictFail)

    save_user_history(session_store_id, session_user_id, session_id, session_username, session_staff, session_seat_number,
                      config_const.CHECKED_OUT, '-')

    table_name = db_handle.get_name_table_history_now()
    history_last_check_out = _history_user.get_last_history_user_by_session(table_name, session_store_id, session_user_id,
                                                                            session_id, config_const.CHECKED_OUT)

    if history_last_check_out:
        lastCheckOutTime = db_handle.get_timestamp_milis_from_date(history_last_check_out['created'])\
            if history_last_check_out['created'] else 0
    else:
        lastCheckOutTime = 0

    history_last_check_in = _history_user.get_last_history_user_by_session(table_name, session_store_id, session_user_id,
                                                                           session_id, config_const.CHECKED_IN)

    if history_last_check_in:
        lastCheckInTime = db_handle.get_timestamp_milis_from_date(history_last_check_in['created']) \
            if history_last_check_in['created'] else 0
    else:
        lastCheckInTime = 0

    user = {}
    user['name']           = session_username
    user['seat_number']    = int(session_seat_number)
    user['time_check_in']  = lastCheckInTime
    user['time_check_out'] = lastCheckOutTime

    data['users'] = user

    dictSucess = DictSuccess(data, message_const.CHECK_OUT_SUCCESS).response
    return jsonify(dictSucess)

def get_card_after_create(service_company, user_id):

    _card = Card()
    type_create_coin   = int(service_company['card_type'])
    service_company_id = service_company['id']

    if type_create_coin == config_const.CARD_TYPE:
        card = _card.get_card_by_service_company_id_and_user(service_company_id, user_id,
                                                             config_const.UNUSED)
        if not card:
            _card.create_new_card(
                service_company_id,
                user_id,
                0,
                service_company['number_of_stamps_in_card'],
                0,
                0,
                config_const.UNUSED
            )
            card = _card.get_card_by_service_company_id_and_user(service_company_id, user_id,
                                                                 config_const.UNUSED)
        return card

    elif type_create_coin == config_const.POINT_TYPE:
        card = _card.get_card_by_service_company_id_and_user(service_company_id, user_id,
                                                             config_const.POINT_CAN_USE)
        if not card:
            _card.create_new_card(
                service_company_id,
                user_id,
                0,
                0,
                0,
                0,
                config_const.POINT_CAN_USE
            )
            card = _card.get_card_by_service_company_id_and_user(service_company_id, user_id,
                                                                 config_const.POINT_CAN_USE)
        return card

    elif type_create_coin == config_const.NONE_TYPE:
        card = _card.get_card_by_service_company_id_and_user(service_company_id, user_id,
                                                             config_const.UNUSED)
        if not card:
            _card.create_new_card(
                service_company_id,
                user_id,
                0,
                service_company['number_of_stamps_in_card'],
                0,
                0,
                config_const.UNUSED
            )
            card = _card.get_card_by_service_company_id_and_user(service_company_id, user_id,
                                                                 config_const.UNUSED)
        return card

    return None


@mod_store.route('/add-stamp-card-for-user', methods = ['POST'])
@manager_user_auth_required()
def add_stamp_card_for_user(user = None):
    if not user:
        dictFail = DictFail(message_const.USER_NOT_EXISTS).response
        return jsonify(dictFail)

    result = request.json if request.json else {}
    if result == {}:
        dictFail = DictFail(message_const.JSON_NONE).response
        return jsonify(dictFail)
    try:
        session_id       = result.get('session_id', 0)
        point            = result.get('point', 0)
        number_stamp_add = result.get('number_stamp_add', 1)
    except:
        point = 0

    _session         = Session()
    _store           = Store()
    _service_company = Service_Company()

    session  = _session.get_session_by_id(session_id)

    if not session:
        dictFail = DictFail(message_const.UNDEFINED_SESSION).response
        return jsonify(dictFail)

    if not session['store_id'] and not session['user_id']:
        dictFail = DictFail(message_const.UNDEFINED_STORE_ID_AND_USER_ID).response
        return jsonify(dictFail)

    if not session['user_id']:
        dictFail = DictFail(message_const.GUEST).response
        return jsonify(dictFail)

    if session['status'] == config_const.WAIT_CONFIRM:
        dictFail = DictFail(message_const.UNCHECK_IN_USER).response
        return jsonify(dictFail)

    store = _store.getStoreById(session['store_id'])

    service_company = _service_company.get_service_from_service_company(store['service_company_id'])

    m_user = M_User.query.filter_by(id=session['user_id']).first()

    if m_user:
        username = m_user.name
    else:
        username = session['user_name']

    added = add_stamp_for_user(int(service_company['id']), int(session['store_id']), int(session['user_id']), int(user['id'])
                       , int(session['seat_number']), session_id, username, point, number_stamp_add)

    if not added:
        dictFail = DictFail(message_const.UPDATE_FAIL).response
        return jsonify(dictFail)

    dictSucess = DictSuccess(None, message_const.MES_NONE).response
    return jsonify(dictSucess)


@mod_store.route('/use-card', methods = ['POST'])
@manager_user_auth_required()
def use_card(user = None):
    if not user:
        dictFail = DictFail(message_const.USER_NOT_EXISTS).response
        return jsonify(dictFail)

    result = request.json if request.json else {}
    if result == {}:
        dictFail = DictFail(message_const.JSON_NONE).response
        return jsonify(dictFail)

    session_id    = result.get('session_id', 0)
    card_id_list  = result.get('card_id_list', '')
    type_discount = result.get('type_discount', config_const.CARD_TYPE)
    point         = result.get('point', 0)

    _card    = Card()
    _session = Session()

    if type_discount == config_const.POINT_TYPE:
        list_card = db_handle.get_list_arr(card_id_list)
        card_id   = list_card[0]

        card = M_Card.query.filter(M_Card.id == card_id).first()
        if not card:
            dictFail = DictFail(message_const.WRONG_CARD_ID).response
            return jsonify(dictFail)

        m_stamp = M_Stamp.query.filter(M_Stamp.card_id == card_id).first()
        if not m_stamp:
            dictFail = DictFail(message_const.WRONG_STAMP_ID).response
            return jsonify(dictFail)

        old_point = int(m_stamp.point) if m_stamp.point else 0
        if old_point < int(point):
            dictFail = DictFail(message_const.POINT_NOT_ENOUGH).response
            return jsonify(dictFail)
        try:
            m_stamp.point = old_point - int(point)
            db.session.commit()

            dictSucess = DictSuccess(None, message_const.MES_NONE).response
            return jsonify(dictSucess)
        except:
            db.session.rollback()
            dictFail = DictFail(message_const.MES_NONE).response
            return jsonify(dictFail)


    list_card = db_handle.get_list_arr(card_id_list)
    session   = _session.get_session_by_id(session_id)

    if not list_card:
        dictFail = DictFail(message_const.MES_NONE).response
        return jsonify(dictFail)

    if len(list_card)>1:
        for card_id in list_card:
            card = _card.get_card_by_id(card_id)
            if not card:
                dictFail = DictFail(message_const.WRONG_CARD_ID).response
                return jsonify(dictFail)
            if int(card['current_number_of_stamps']) != int(card['max_number_of_stamps']):
                dictFail = DictFail(message_const.CARD_NOT_FULL).response
                return jsonify(dictFail)
            user_id = card['user_id']

        is_used = 1
        status  = config_const.USED
        for card_id in list_card:
            _card.udpate_card_used(card_id, is_used, status)

        note = {}
        note['number_of_used_card'] = len(list_card)
        note['card_id']             = list_card

        note = json.dumps(note)
        res = save_use_card_history(user['store_id'], user_id, session_id, user['id'], session['seat_number'], str(note))

        if not res:
            dictFail = DictFail(message_const.MES_NONE).response
            return jsonify(dictFail)
    else:
        card_id = list_card[0]
        card = _card.get_card_by_id(card_id)

        if not card:
            dictFail = DictFail(message_const.WRONG_CARD_ID).response
            return jsonify(dictFail)

        if int(card['current_number_of_stamps']) != int(card['max_number_of_stamps']):
            dictFail = DictFail(message_const.CARD_NOT_FULL).response
            return jsonify(dictFail)

        user_id = card['user_id']
        is_used = 1
        status = config_const.USED
        _card.udpate_card_used(card_id, is_used, status)

        note = {}
        note['number_of_used_card'] = 1
        note['card_id'] = list_card
        try:
            note = json.dumps(note)
        except:
            note = {}
        res = save_use_card_history(user['store_id'], user_id, session_id, user['id'], session['seat_number'],
                                    str(note))

        if not res:
            dictFail = DictFail(message_const.MES_NONE).response
            return jsonify(dictFail)

    dictSucess = DictSuccess(None, message_const.MES_NONE).response
    return jsonify(dictSucess)

def save_use_card_history(store_id, user_id, session_id, staff, seat_number, note):

    _user = User()

    user = _user.getUser(user_id)
    if not user:
        return False

    name_history_table = db_handle.get_name_table_history_now()

    checkTableExists = db_handle.check_table_exists(name_history_table, '')

    if not checkTableExists:
        res_create_table = db_handle.create_table_history(name_history_table)
        if not res_create_table:
            return False

    _history_user = HistoryUser()
    currentDay    = db_handle.get_current_day()

    res_create_history_user = _history_user.create_user_history(name_history_table, store_id, user_id, user['name'],
                                                                session_id, seat_number,
                                                                staff, config_const.USE_CARD, note, currentDay)
    if not res_create_history_user:
        return False

    return True

@mod_store.route('/set-info-of-user-from-store', methods = ['POST'])
@manager_user_auth_required()
def set_info_of_user_from_store(user = None):
    if not user:
        dictFail = DictFail(message_const.USER_NOT_EXISTS).response
        return jsonify(dictFail)

    result = request.json if request.json else {}
    if result == {}:
        dictFail = DictFail(message_const.JSON_NONE).response
        return jsonify(dictFail)

    session_id = result.get('session_id', 0)
    store_memo_value = result.get('store_memo_value', '')

    _session = Session()
    _common = Common()

    session = _session.get_session_by_id(session_id)

    if not session:
        dict_fail = DictFail(message_const.UNDEFINED_SESSION).response
        return jsonify(dict_fail)

    response = _common.set_store_memo(int(session['store_id']), int(session['user_id']), store_memo_value)

    if response:
        dict_success = DictSuccess(None, message_const.SET_INFO_DONE).response
        return jsonify(dict_success)

    dict_fail = DictFail(message_const.MES_NONE).response
    return jsonify(dict_fail)


@mod_store.route('/get-list-card-of-user-by-session-id', methods = ['POST'])
@manager_user_auth_required()
def get_list_card_of_user_by_session_id(user = None):
    if not user:
        dictFail = DictFail(message_const.USER_NOT_EXISTS).response
        return jsonify(dictFail)

    result = request.json if request.json else {}
    if result == {}:
        dictFail = DictFail(message_const.JSON_NONE).response
        return jsonify(dictFail)

    session_id = result.get('session_id', 0)
    try:
        page = request.args.get('page', 1)
        limit = request.args.get('limit', 20)
    except:
        page  = 1
        limit = 20

    _session = Session()

    session = _session.get_session_by_id(session_id)

    if not session:
        dictFail = DictFail(message_const.UNDEFINED_SESSION).response
        return jsonify(dictFail)

    _card  = Card()
    _stamp = Stamp()
    _store = Store()
    data   = {}

    if not session['store_id'] and not session['user_id']:
        data['number_of_cards'] = 0
        dictSucess = DictSuccess(data, message_const.UNDEFINED_STORE_ID_AND_USER_ID).response
        return jsonify(dictSucess)

    if not session['user_id']:
        data['number_of_cards'] = 0
        dictSucess = DictSuccess(data, message_const.GUEST).response
        return jsonify(dictSucess)

    if session['status'] != config_const.CHECKED_IN:
        dictFail = DictFail(message_const.UNCHECK_IN_USER).response
        return jsonify(dictFail)

    store = _store.getStoreById(session['store_id'])

    if not store:
        dictFail = DictFail(message_const.STORE_NOT_FOUND).response
        return jsonify(dictFail)

    #Get card_type
    m_service_company = M_Service_Company.query.filter_by(id = store['service_company_id']).first()

    if not m_service_company:
        dictFail = DictFail(message_const.SERVICE_COMPANY_NOT_FOUND).response
        return jsonify(dictFail)

    card_type = m_service_company.card_type

    # Card all
    getTotal = True
    result_number_of_cards = _card.get_card_can_use_of_user(store['service_company_id'], session['user_id'], page, limit,
                                                                        getTotal)
    total_number_of_cards = 0
    if not result_number_of_cards:
        dictFail = DictFail(message_const.FAIL_GET_CARD).response
        return jsonify(dictFail)

    for row in result_number_of_cards:
        total_number_of_cards += 1

    #Card in page
    getTotal = False
    result_card_in_page = _card.get_card_can_use_of_user(store['service_company_id'], session['user_id'],
                                                                     page, limit, getTotal)
    number_of_cards_in_page = 0
    if not result_card_in_page:
        dictFail = DictFail(message_const.FAIL_GET_CARD).response
        return jsonify(dictFail)

    data = {}
    cards = []
    card = {}

    for row in result_card_in_page:
        number_of_cards_in_page += 1
        stamps = []
        itemStamp = {}
        card['card_id'] = row['id']
        card['current_number_of_stamps'] = row['current_number_of_stamps'] if row['current_number_of_stamps'] else 0
        card['max_number_of_stamps']  = row['max_number_of_stamps'] if row['max_number_of_stamps'] else 0
        card['card_status'] = row['status'] if row['status'] else ''
        # Stamp of card
        lstStamp = _stamp.getStamp(card['card_id'])
        for item in lstStamp:
            itemStamp['stamp_id']           = item['id']
            itemStamp['management_user_id'] = item['management_user_id'] if item['management_user_id'] else 0
            itemStamp['number_in_card']     = item['number_in_card'] if item['number_in_card'] else 0
            itemStamp['store_name']         = item['store_name'] if item['store_name'] else ''
            itemStamp['created']            = db_handle.get_timestamp_milis_from_date(item['created']) if item['created'] else ''
            itemStamp['star_review']        = item['star_review'] if item['star_review'] else 0.0
            itemStamp['text_review']        = item['text_review'] if item['text_review'] else ''
            stamps.append(copy.copy(itemStamp))

        card['stamp'] = stamps

        cards.append(copy.copy(card))

    total_page = int(math.ceil(int(total_number_of_cards) / float(int(limit))))
    #Set data
    data['cards']                   = cards
    data['card_type']               = card_type
    data['page']                    = int(page)
    data['total_page']              = total_page
    data['limit']                   = limit
    data['total_number_of_cards']   = total_number_of_cards
    data['number_of_cards_in_page'] = number_of_cards_in_page

    dictSucess = DictSuccess(data, message_const.SUCCESS_MES).response
    return jsonify(dictSucess)

@mod_store.route('/add-push-notification', methods = ['POST'])
@manager_user_auth_required()
def add_push_notification(user = None):
    if not user:
        dictFail = DictFail(message_const.USER_NOT_EXISTS).response
        return jsonify(dictFail)

    result = request.json if request.json else {}
    if result == {}:
        dictFail = DictFail(message_const.JSON_NONE).response
        return jsonify(dictFail)
    _push_notifi       = Push_notifi()
    _store_push_notifi = Store_Push_Notifi()
    _user_push_notifi  = User_Push_Notifi()
    _user_info         = UserInfo()
    _store             = Store()
    data               = {}

    title_push        = result.get('title', '')
    contentPush       = result.get('content', '')
    sendTimeTimestamp = result.get('send_time', 0)
    sound             = result.get('sound', '')
    badge             = result.get('badge', '')
    sort_description  = result.get('sort_description', '')

    app_id           = user['app_id']
    firebase_api_key = user['firebase_api_key']

    if not app_id:
        app_id = 0

    if not firebase_api_key:
        firebase_api_key = ''

    try:
        sendTime = db_handle.get_date_from_timestamp(int(sendTimeTimestamp)/1000)
    except:
        dictFail = DictFail(message_const.MES_NONE).response
        return jsonify(dictFail)

    if int(user['level_id']) != 4:
        dictFail = DictFail(message_const.PERMISSION_DENIED).response
        return jsonify(dictFail)
    try:
        m_store = M_Store.query.filter(M_Store.id == user['store_id']).first()
        current_number_of_notifications = 0
    except:
        dictFail = DictFail(message_const.MES_NONE).response
        return jsonify(dictFail)

    #level_id = 4
    try:
        key_gen = str(uuid.uuid4())
        push_notifi = _push_notifi.create_push_notifi(key_gen, user['id'], title_push, contentPush, sendTime, sound, badge, 2, sort_description, app_id, firebase_api_key)
        if not push_notifi:
            dictFail = DictFail(message_const.MES_NONE).response
            return jsonify(dictFail)

        new_push = _push_notifi.get_push_by_key_push(key_gen)
        _store_push_notifi.create_new_store_push(user['store_id'], new_push['id'])
        # create_user_push

        list_store_str = str(user['store_id'])

        m_store.current_number_of_notifications = current_number_of_notifications
        db.session.commit()

    except:
        dictFail = DictFail(message_const.MES_NONE).response
        return jsonify(dictFail)

    dictSucess = DictSuccess(data, message_const.MES_NONE).response
    return jsonify(dictSucess)


@mod_store.route('/get-list-push-notification', methods = ['GET'])
@manager_user_auth_required()
def get_list_push_notification(user = None):
    if not user:
        db_handle.close_connect()
        dictFail = DictFail(message_const.USER_NOT_EXISTS).response
        return jsonify(dictFail)

    page  = request.args.get('page', 1)
    limit = request.args.get('limit', 20)

    _push_notifi = Push_notifi()
    _store       = Store()

    store = _store.getStoreById(user['store_id'])

    if not store:
        db_handle.close_connect()
        dictFail = DictFail(message_const.MES_NONE).response
        return jsonify(dictFail)

    store_id = store['id']
    # service_company_id = store['service_company_id']
    if int(user['level_id']) != 4:
        db_handle.close_connect()
        dictFail = DictFail(message_const.PERMISSION_DENIED).response
        return jsonify(dictFail)
    data  = {}
    pushs = []
    push  = {}

    # level_id = 4
    # Push all
    getTotal = True
    result_number_of_pushs = _push_notifi.get_push_by_user_lv4(store_id, page, limit, getTotal)
    total_number_of_pushs = 0
    for row in result_number_of_pushs:
        total_number_of_pushs += 1
    # Push in page
    getTotal = False
    result_push_in_page = _push_notifi.get_push_by_user_lv4(store_id, page, limit, getTotal)
    number_of_pushs_in_page = 0
    if not result_push_in_page:
        dictFail = DictFail(message_const.FAIL_GET_PUSH).response
        return jsonify(dictFail)
    for row in result_push_in_page:

        number_of_pushs_in_page += 1
        stores = []
        push['management_user_id'] = row['management_user_id']
        push['title']              = row['title'] if row['title'] else ''
        push['content']            = row['content'] if row['content'] else ''
        push['send_time']          = db_handle.get_timestamp_milis_from_date(row['send_time']) if row['send_time'] else 0
        push['sound']              = row['sound'] if row['sound'] else ''
        push['badge']              = row['badge'] if row['badge'] else ''
        push['status']             = row['status'] if row['status'] else 1
        push['created']            = db_handle.get_timestamp_milis_from_date(row['created']) if row['created'] else 0
        push['modified']           = db_handle.get_timestamp_milis_from_date(row['modified']) if row['modified'] else 0
        push['sort_description']   = row['sort_description'] if row['sort_description'] else ""

        # users_get_push = db.session.query(distinct(M_User_Push_Notifi.user_id)).filter_by(push_notification_id = row['id'])
        # push['number_device_send'] = users_get_push.count()
        # push['number_user_view']   = M_User_Push_Notifi.query.filter_by(push_notification_id = row['id']).filter_by(status = config_const.SEEN).count()
        # push['number_user_read']   = M_User_Push_Notifi.query.filter_by(push_notification_id = row['id']).filter_by(status = config_const.READ).count()

        push['number_device_send'] = 0
        push['number_user_view']   = 0
        push['number_user_read']   = 0

        push['id'] = row['id']
        stores.append(store['name'])
        push['stores'] = stores
        pushs.append(copy.copy(push))

    total_page = int(math.ceil(int(total_number_of_pushs) / float(int(limit))))
    data['page']                            = page
    data['total_page']                      = total_page
    data['limit']                           = limit
    data['total_number_of_notifications']   = total_number_of_pushs
    data['number_of_notifications_in_page'] = number_of_pushs_in_page
    data['push_notification']               = pushs
    db_handle.close_connect()

    dictSucess = DictSuccess(data, message_const.MES_NONE).response
    return jsonify(dictSucess)


@mod_store.route('/get-list-managed-store', methods = ['GET'])
@manager_user_auth_required()
def get_list_managed_store(user = None):
    if not user:
        dictFail = DictFail(message_const.TOKEN_EXPIRE).response
        return jsonify(dictFail)

    _store = Store()

    if not (int(user['level_id']) == 3 or int(user['level_id']) == 4):
        dictFail = DictFail(message_const.PERMISSION_DENIED).response
        return jsonify(dictFail)

    data   = {}
    stores = []
    store  = {}

    if int(user['level_id']) == 3:
        lstStore = _store.get_store_by_service_company_id(user['service_company_id'])

        for oneStore in lstStore:
            store['store_id']           = oneStore['id']
            store['service_company_id'] = oneStore['service_company_id']
            store['name']               = oneStore['name']
            store['logo']               = oneStore['logo']
            stores.append(copy.copy(store))

        data['store'] = stores

        dictSucess = DictSuccess(data, message_const.MES_NONE).response
        return jsonify(dictSucess)
    elif int(user['level_id']) == 4:
        # level_id = 4
        try:
            oneStore = _store.getStoreById(user['store_id'])

            store['store_id'] = oneStore['id']
            store['service_company_id'] = oneStore['service_company_id']
            store['name'] = oneStore['name']
            store['logo'] = oneStore['logo']
            stores.append(store)
            data['store'] = stores

            dictSucess = DictSuccess(data, message_const.MES_NONE).response
            return jsonify(dictSucess)
        except:
            dictFail = DictFail(message_const.MES_NONE).response
            return jsonify(dictFail)

@mod_store.route('/get-ask-to-review-info', methods = ['GET'])
@manager_user_auth_required()
def get_ask_to_review_info(user=None):
    if not user:
        dict_fail = DictFail(message_const.USER_NOT_EXISTS).response
        return jsonify(dict_fail)

    if int(user['level_id']) != 4:
        dict_fail = DictFail(message_const.PERMISSION_DENIED).response
        return jsonify(dict_fail)

    _service_company = Service_Company()
    if user['service_company_id']:
        service_company = _service_company.get_service_from_service_company(str(user['service_company_id']))
        if service_company:
            data = {
                'ask_to_review_title'            : str(service_company['ask_to_review_title'].encode('utf-8')) if service_company['ask_to_review_title'] else '',
                'ask_to_review_content'          : str(service_company['ask_to_review_content'].encode('utf-8')) if service_company['ask_to_review_content'] else '',
                'ask_to_review_sort_description' : str(service_company['ask_to_review_sort_description'].encode('utf-8')) if service_company['ask_to_review_sort_description'] else '',
                'ask_to_review_day_send_push'    : int(service_company['ask_to_review_day_send_push']) if service_company['ask_to_review_day_send_push'] else 0
            }
            dict_success = DictSuccess(data, message_const.SUCCESS_MES).response
            return jsonify(dict_success)
        else:
            dict_fail = DictFail(message_const.UNKNOWN_SERVICE_COMPANY).response
            return jsonify(dict_fail)

    else:
        dict_fail = DictFail(message_const.UNKNOWN_SERVICE_COMPANY_ID).response
        return jsonify(dict_fail)

@mod_store.route('/set-ask-to-review-info', methods = ['POST'])
@manager_user_auth_required()
def set_ask_to_review_info(user=None):
    if not user:
        dict_fail = DictFail(message_const.USER_NOT_EXISTS).response
        return jsonify(dict_fail)

    result = request.json if request.json else {}
    if result == {}:
        dictFail = DictFail(message_const.JSON_NONE).response
        return jsonify(dictFail)

    if int(user['level_id']) != 4:
        dict_fail = DictFail(message_const.PERMISSION_DENIED).response
        return jsonify(dict_fail)

    ask_to_review_title            = result.get('ask_to_review_title', '')
    ask_to_review_content          = result.get('ask_to_review_content', '')
    ask_to_review_day_send_push    = result.get('ask_to_review_day_send_push', '')
    ask_to_review_sort_description = result.get('ask_to_review_sort_description', '')
    _service_company = Service_Company()

    if user['service_company_id']:

        try:
            service_company = M_Service_Company.query.filter_by(id = user['service_company_id']).first()

            service_company.ask_to_review_title            = str(ask_to_review_title.encode('utf-8'))
            service_company.ask_to_review_content          = str(ask_to_review_content.encode('utf-8'))
            service_company.ask_to_review_day_send_push    = int(ask_to_review_day_send_push)
            service_company.ask_to_review_sort_description = str(ask_to_review_sort_description.encode('utf-8'))

            db.session.commit()
            dict_success = DictSuccess(None, message_const.SUCCESS_MES).response
            return jsonify(dict_success)

        except:
            db.session.rollback()
            db.session.close()
            dict_fail = DictFail(message_const.MES_NONE).response
            return jsonify(dict_fail)


    else:
        dict_fail = DictFail(message_const.UNKNOWN_SERVICE_COMPANY_ID).response
        return jsonify(dict_fail)

@mod_store.route('/get-reminder-info', methods = ['GET'])
@manager_user_auth_required()
def get_reminder_info(user=None):
    if not user:
        dict_fail = DictFail(message_const.USER_NOT_EXISTS).response
        return jsonify(dict_fail)

    if int(user['level_id']) != 4:
        dict_fail = DictFail(message_const.PERMISSION_DENIED).response
        return jsonify(dict_fail)

    _service_company = Service_Company()
    if user['service_company_id']:
        service_company = _service_company.get_service_from_service_company(str(user['service_company_id']))
        if service_company:
            data = {
                'reminder_title'            : str(service_company['reminder_title'].encode('utf-8')) if service_company['reminder_title'] else '',
                'reminder_content'          : str(service_company['reminder_content'].encode('utf-8')) if service_company['reminder_content'] else '',
                'reminder_sort_description' : str(service_company['reminder_sort_description'].encode('utf-8')) if service_company['reminder_sort_description'] else '',
                'reminder_day_send_push'    : int(service_company['reminder_day_send_push']) if service_company['reminder_day_send_push'] else 0
            }
            dict_success = DictSuccess(data, message_const.SUCCESS_MES).response
            return jsonify(dict_success)
        else:
            dict_fail = DictFail(message_const.UNKNOWN_SERVICE_COMPANY).response
            return jsonify(dict_fail)

    else:
        dict_fail = DictFail(message_const.UNKNOWN_SERVICE_COMPANY_ID).response
        return jsonify(dict_fail)

@mod_store.route('/set-reminder-info', methods = ['POST'])
@manager_user_auth_required()
def set_reminder_info(user=None):
    if not user:
        dict_fail = DictFail(message_const.USER_NOT_EXISTS).response
        return jsonify(dict_fail)

    result = request.json if request.json else {}
    if result == {}:
        dictFail = DictFail(message_const.JSON_NONE).response
        return jsonify(dictFail)

    if int(user['level_id']) != 4:
        dict_fail = DictFail(message_const.PERMISSION_DENIED).response
        return jsonify(dict_fail)

    reminder_title            = result.get('reminder_title', '')
    reminder_content          = result.get('reminder_content', '')
    reminder_day_send_push    = result.get('reminder_day_send_push', '')
    reminder_sort_description = result.get('reminder_sort_description', '')

    _service_company = Service_Company()

    if user['service_company_id']:
        try:
            service_company = M_Service_Company.query.filter_by(id = user['service_company_id']).first()

            service_company.reminder_title            = str(reminder_title.encode('utf-8'))
            service_company.reminder_content          = str(reminder_content.encode('utf-8'))
            service_company.reminder_day_send_push    = int(reminder_day_send_push)
            service_company.reminder_sort_description = str(reminder_sort_description.encode('utf-8'))

            db.session.commit()
            dict_success = DictSuccess(None, message_const.SUCCESS_MES).response
            return jsonify(dict_success)

        except:
            db.session.rollback()
            db.session.close()
            dict_fail = DictFail(message_const.MES_NONE).response
            return jsonify(dict_fail)

    else:
        dict_fail = DictFail(message_const.UNKNOWN_SERVICE_COMPANY_ID).response
        return jsonify(dict_fail)

@mod_store.route('/get-information-user-by-session-id', methods=['POST'])
@manager_user_auth_required()
def get_information_user_by_session_id(user=None):
    if not user:
        dict_fail = DictFail(message_const.USER_NOT_EXISTS).response
        return jsonify(dict_fail)

    result = request.json if request.json else {}
    if result == {}:
        dict_fail = DictFail(message_const.JSON_NONE).response
        return jsonify(dict_fail)

    session_id = result.get('session_id', 0)
    _session = Session()
    _store = Store()
    _service_company = Service_Company()
    _common = Common()
    _user = User()
    _card = Card()
    data = {}
    name_history_table = db_handle.get_name_table_history_now()
    _history_user = HistoryUser()

    session = _session.get_session_by_id(session_id)

    if not session:
        dict_fail = DictFail(message_const.UNDEFINED_SESSION).response
        return jsonify(dict_fail)

    if not session['store_id'] and not session['user_id']:
        dict_fail = DictFail(message_const.UNDEFINED_STORE_ID_AND_USER_ID).response
        return jsonify(dict_fail)

    # get seat info
    store = _store.getStoreById(session['store_id'])

    if not store['number_of_seats']:
        dict_fail = DictFail(message_const.NUMBER_SEAT_NULL).response
        return jsonify(dict_fail)

    list_seat = get_list_seat_for_info_user(int(session['store_id']))

    if (list_seat['success']):
        list_seat = list_seat['data']
    else:
        dict_fail = DictFail(message_const.FAIL_MES).response
        return jsonify(dict_fail)

    # case Guest
    if not session['user_id']:
        data['user_id']                     = 0
        data['name']                        = session['user_name']
        data['last_check_out_time']         = 0
        data['current_seat_number']         = str(session['seat_number'])
        data['seat_list']                   = list_seat
        data['user_information_from_store'] = None
        data['user_memo']                   = None
        data['number_of_cards']             = 0
        data['cards']                       = []
        dict_success = DictSuccess(data, message_const.SUCCESS_MES).response
        return jsonify(dict_success)

    # get user information from store
    info_user_from_store = _common.get_store_memo(str(session['store_id']), str(session['user_id']))
    # get memo user
    service_company = _service_company.get_service_from_service_company(str(store['service_company_id']))
    memo_user = _common.get_user_memo(str(service_company['service_id']), str(session['user_id']))

    # get card info of user can use
    card_info = get_list_card_and_stamp_of_user(str(service_company['id']), str(session['user_id']))

    # get last check out time
    checkTableExists = db_handle.check_table_exists(name_history_table, '')
    lastCheckOutTime = 0
    if checkTableExists:
        # Start if check table

        last_check_out_user = _history_user.get_history_user_check_out(
            name_history_table, user['store_id'], session['user_id'], config_const.CHECKED_OUT)
        if last_check_out_user:

            lastCheckOutTime = db_handle.get_timestamp_milis_from_date(last_check_out_user['created']) \
                if last_check_out_user['created'] else 0
        else:
            count = 1
            while (count < 4):
                table_name_history_before = get_name_table_after_subtract_month(count)

                checkTableExists = db_handle.check_table_exists(table_name_history_before, '')

                if checkTableExists:
                    last_check_out_user_before = _history_user.get_history_user_check_out(
                        table_name_history_before, user['store_id'], session['user_id'], config_const.CHECKED_OUT)
                    if last_check_out_user_before:
                        lastCheckOutTime = db_handle.get_timestamp_milis_from_date(
                            last_check_out_user_before['created']) \
                            if last_check_out_user_before['created'] else 0
                else:
                    break
                count += 1

    else:
        count = 1

        while (count < 4):
            table_name_history_before = get_name_table_after_subtract_month(count)

            checkTableExists = db_handle.check_table_exists(table_name_history_before, '')

            if checkTableExists:
                last_check_out_user_before = _history_user.get_history_user_check_out(
                    table_name_history_before, user['store_id'], session['user_id'], config_const.CHECKED_OUT)
                if last_check_out_user_before:
                    lastCheckOutTime = db_handle.get_timestamp_milis_from_date(last_check_out_user_before['created']) \
                        if last_check_out_user_before['created'] else 0
            else:
                break
            count += 1

    normalUser = _user.getUser(session['user_id'])
    data['user_id']                     = int(session['user_id'])
    data['name']                        = str(normalUser['name'])
    data['last_check_out_time']         = int(lastCheckOutTime)
    data['current_seat_number']         = int(session['seat_number'])
    data['seat_list']                   = list_seat
    data['user_information_from_store'] = info_user_from_store
    data['user_memo']                   = memo_user
    data['number_of_cards']             = card_info['number_of_cards']
    data['cards']                       = card_info['cards']
    data['card_type']                   = int(service_company['card_type'])
    data['number_of_stamps_in_card']    = int(service_company['number_of_stamps_in_card'])

    dict_success = DictSuccess(data, message_const.SUCCESS_MES).response
    return jsonify(dict_success)

def get_list_card_and_stamp_of_user(service_company_id, user_id):
    _card = Card()
    _stamp = Stamp()
    list_card = _card.get_card_can_use_of_user(service_company_id, user_id,
                            1, 1, True)

    total_number_of_cards = 0
    if not list_card:
        dict_fail = DictFail(message_const.FAIL_GET_CARD).response
        return jsonify(dict_fail)

    cards = []
    card = {}

    for row in list_card:
        stamps = []
        item_stamp = {}
        card['card_id'] = row['id']
        card['current_number_of_stamps'] = row['current_number_of_stamps'] if row['current_number_of_stamps'] else 0
        card['max_number_of_stamps'] = row['max_number_of_stamps'] if row['max_number_of_stamps'] else 0
        card['card_status'] = row['status'] if row['status'] else ''
        # Stamp of card
        lst_stamp = _stamp.getStamp(card['card_id'])
        for item in lst_stamp:
            item_stamp['stamp_id']           = item['id']
            item_stamp['management_user_id'] = item['management_user_id'] if item['management_user_id'] else 0
            item_stamp['number_in_card']     = item['number_in_card'] if item['number_in_card'] else 0
            item_stamp['store_name']         = item['store_name'] if item['store_name'] else ''
            item_stamp['created']            = db_handle.get_timestamp_milis_from_date(item['created']) if item['created'] else ''
            item_stamp['star_review']        = item['star_review'] if item['star_review'] else 0.0
            item_stamp['text_review']        = item['text_review'] if item['text_review'] else ''
            item_stamp['point']              = item['point'] if item['point'] else 0
            stamps.append(copy.copy(item_stamp))

        card['stamp'] = stamps

        cards.append(copy.copy(card))
        total_number_of_cards += 1

    if total_number_of_cards == 0:
        cards = []

    response_data = {}
    response_data['number_of_cards'] = total_number_of_cards
    response_data['cards'] = cards

    return response_data

@mod_store.route('/get-store-history-today', methods = ['GET'])
@manager_user_auth_required()
def get_store_history_today(user = None):
    if not user:
        dictFail = DictFail(message_const.TOKEN_EXPIRE).response
        return jsonify(dictFail)

    try:
        page  = request.args.get('page', 1)
        limit = request.args.get('limit', 20)
    except:
        page = 1
        limit = 20

    store_id = user['store_id']

    _history_user = HistoryUser()
    _user         = User()
    name_history_table = db_handle.get_name_table_history_now()
    checkTableExists = db_handle.check_table_exists(name_history_table, '')

    if not checkTableExists:
        try:
            db_handle.create_table_history(name_history_table)
        except:
            dictFail = DictFail(message_const.MES_NONE).response
            return jsonify(dictFail)
    getTotal = True
    count_his_all = _history_user.count_history_check_out(name_history_table, store_id, config_const.CHECKED_OUT, page, limit, getTotal)

    total_page = int(math.ceil(int(count_his_all) / float(int(limit))))

    history_list = _history_user.get_history_check_out_by_store(name_history_table, store_id,
                                                                config_const.CHECKED_OUT, page, limit)
    getTotal = False
    count_his_limit = _history_user.count_history_check_out(name_history_table, store_id, config_const.CHECKED_OUT, page, limit, getTotal)
    data = {
        'page'                       : int(page),
        'total_page'                 : int(total_page),
        'limit'                      : int(limit),
        'total_number_of_histories'  : int(count_his_all),
        'number_of_history_in_page'  : int(count_his_limit),
    }
    histories   = []
    historyItem = {}
    if count_his_limit > 0 :
        for item in history_list:
            if int(item['user']>0):
                userNormal = _user.getUser(item['user'])
                user_name  = userNormal['name'] if userNormal['name'] else ''
            else:
                user_name = item['user_name'] if item['user_name'] else ''

            historyItem['history_id']          = item['id']
            historyItem['name']                = user_name if user_name else ''
            historyItem['seat_number']         = int(item['seat_number'])  if item['seat_number'] else 0
            historyItem['history_information'] = get_history_user_today_by_history_id(item['id'], name_history_table)
            histories.append(copy.copy(historyItem))
        data['history_today'] = histories
    else:
        data['history_today'] = []

    dict_success = DictSuccess(data, message_const.MES_NONE).response
    return jsonify(dict_success)

@mod_store.route('/cancel-check-in', methods = ['POST'])
@manager_user_auth_required()
def cancel_check_in(user = None):
    if not user:
        dictFail = DictFail(message_const.USER_NOT_EXISTS).response
        return jsonify(dictFail)

    result = request.json if request.json else {}
    if result == {}:
        dictFail = DictFail(message_const.JSON_NONE).response
        return jsonify(dictFail)

    session_id = result.get('session_id', 0)
    try:
        # update session to cancel
        M_Session.query.filter(M_Session.id == session_id).update({"status" : "cancel"})
        db_handle.commit_obj()
        dict_success = DictSuccess(None, message_const.MES_NONE).response
        return jsonify(dict_success)
    except:
        dictFail = DictFail(message_const.MES_NONE).response
        return jsonify(dictFail)


@mod_store.route('/cancel-check-in-and-delete-history', methods = ['POST'])
@manager_user_auth_required()
def cancel_check_in_and_delete_history(user = None):
    if not user:
        dictFail = DictFail(message_const.USER_NOT_EXISTS).response
        return jsonify(dictFail)

    result = request.json if request.json else {}
    if result == {}:
        dictFail = DictFail(message_const.JSON_NONE).response
        return jsonify(dictFail)

    session_id = result.get('session_id', 0)
    _session   = Session()
    _history_user = HistoryUser()

    session = _session.get_session_by_id(session_id)

    if not session:
        dictFail = DictFail(message_const.UNDEFINED_SESSION).response
        return jsonify(dictFail)

    if session['status'] != config_const.CHECKED_IN:
        dictFail = DictFail(message_const.MES_NONE).response
        return jsonify(dictFail)

    name_history_table = db_handle.get_name_table_history_now()

    checkTableExists = db_handle.check_table_exists(name_history_table, '')

    if checkTableExists:
        try:
            _history_user.delete_history_user_not_session(name_history_table, session['store_id'],
                                                            session['user_id'], config_const.CHECKED_IN,
                                                            db_handle.get_current_day())
        except:
            dictFail = DictFail(message_const.MES_NONE).response
            return jsonify(dictFail)

    try:
        _session.delete_session(session_id)
        dict_success = DictSuccess(None, message_const.MES_NONE).response
        return jsonify(dict_success)
    except:
        dictFail = DictFail(message_const.MES_NONE).response
        return jsonify(dictFail)


@mod_store.route('/set-store-detail', methods = ['POST'])
@manager_user_auth_required()
def set_store_detail(user = None):
    if not user:
        db_handle.close_connect()
        dictFail = DictFail(message_const.USER_NOT_EXISTS).response
        return jsonify(dictFail)

    result = request.json if request.json else {}

    if result == {}:
        db_handle.close_connect()
        dictFail = DictFail(message_const.JSON_NONE).response
        return jsonify(dictFail)

    try:
        data = {}

        name             = result.get('name', '')
        logo             = result.get('logo', '')
        sort_description = result.get('sort_description', '')
        m_store = M_Store.query.filter_by(id = user['store_id']).first()

        if not m_store:
            db_handle.close_connect()
            dictFail = DictFail(message_const.MES_NONE).response
            return jsonify(dictFail)

        m_store.name             = name
        m_store.logo             = logo
        m_store.sort_description = sort_description

        db.session.commit()

        db_handle.close_connect()
        dict_success = DictSuccess(data, message_const.MES_NONE).response
        return jsonify(dict_success)

    except Exception, e:
        db.session.rollback()
        db_handle.close_connect()
        dict = DictFail(message_const.MES_NONE).response
        return jsonify(dict)


@mod_store.route('/get-detail-history-today-by-history-id', methods = ['POST'])
@manager_user_auth_required()
def get_detail_history_today_by_history_id(user = None):
    if not user:
        dictFail = DictFail(message_const.USER_NOT_EXISTS).response
        return jsonify(dictFail)

    result = request.json if request.json else {}
    if result == {}:
        dictFail = DictFail(message_const.JSON_NONE).response
        return jsonify(dictFail)

    history_id    = result.get('history_id', 0)
    _history_user = HistoryUser()
    _store        = Store()
    _service_com  = Service_Company()
    _common       = Common()
    _user         = User()

    data          = {}

    name_history_table = db_handle.get_name_table_history_now()

    checkTableExists = db_handle.check_table_exists(name_history_table, '')

    if not checkTableExists:
        dictFail = DictFail(message_const.MES_NONE).response
        return jsonify(dictFail)

    history_user = _history_user.get_history_by_id(name_history_table, history_id)

    if not history_user:
        dictFail = DictFail(message_const.MES_NONE).response
        return jsonify(dictFail)

    if int(history_user['user'] > 0):
        userNormal = _user.getUser(history_user['user'])
        user_name = userNormal['name'] if userNormal['name'] else ''
    else:
        user_name = history_user['user_name'] if history_user['user_name'] else ''

    data['name']                        = user_name if user_name else ''
    data['store_id']                    = history_user['store_id']
    data['user_id']                     = history_user['user']
    data['seat_number']                 = history_user['seat_number']
    data['history_information']         = get_history_user_today_by_history_id(history_id, name_history_table)
    data['user_information_from_store'] = _common.get_store_memo(history_user['store_id'], history_user['user'])
    try:
        store             = _store.getStoreById(history_user['store_id'])
        service_com       = _service_com.get_service_from_service_company(store['service_company_id'])
        service_id        = service_com['service_id']
        data['card_type'] = int(service_com['card_type'])
    except:
        service_id  = 0
        data['card_type'] = config_const.NONE_TYPE
        pass

    data['user_memo'] = _common.get_user_memo(service_id, history_user['user'])

    dict_success = DictSuccess(data, message_const.MES_NONE).response
    return jsonify(dict_success)


@mod_store.route('/set-info-of-user-from-store-by-store-id-and-user-id', methods = ['POST'])
@manager_user_auth_required()
def set_info_of_user_from_store_by_store_id_and_user_id(user = None):
    if not user:
        dictFail = DictFail(message_const.USER_NOT_EXISTS).response
        return jsonify(dictFail)

    result = request.json if request.json else {}
    if result == {}:
        dictFail = DictFail(message_const.JSON_NONE).response
        return jsonify(dictFail)

    user_id          = int(result.get('user_id', 0))
    store_id         = int(result.get('store_id', 0))
    store_memo_value = result.get('store_memo_value', '')

    _common = Common()

    if store_id == 0 or user_id == 0:
        dict_fail = DictFail(message_const.GUEST).response
        return jsonify(dict_fail)

    response = _common.set_store_memo(store_id, user_id, store_memo_value)

    if response:
        dict_success = DictSuccess(None, message_const.SET_INFO_DONE).response
        return jsonify(dict_success)
    else:
        dict_fail = DictFail(message_const.MES_NONE).response
        return jsonify(dict_fail)

@mod_store.route('/feedback-scan-qr-code-store-for-guest', methods = ['POST'])
@manager_user_auth_required()
def feedback_scan_qr_code_store_for_guest(user = None):
    if not user:
        dictFail = DictFail(message_const.USER_NOT_EXISTS).response
        return jsonify(dictFail)

    result = request.json if request.json else {}
    if result == {}:
        dictFail = DictFail(message_const.JSON_NONE).response
        return jsonify(dictFail)

    user_name         = result.get('name', '')
    _session          = Session()
    _store            = Store()
    _config_app_store = Config_App_Store()
    _service_com      = Service_Company()
    _common           = Common()

    store = _store.getStoreById(user['store_id'])
    service_company = _service_com.get_service_from_service_company(store['service_company_id'])
    is_approved_check_in = service_company['is_approved_check_in']
    is_approved_check_out = service_company['is_approved_check_out']

    if int(is_approved_check_in) != 1:
        config_app_store = _config_app_store.get_config()
        if db_handle.get_current_day() != int(store['code_create_day']):
            check_in_code = config_app_store['prefix'] + '.' + str(user['id']) + db_handle.get_timestamp_now()
            _store.update_store_after_create_qr_code(user['store_id'], check_in_code, db_handle.get_current_day())

    wait_confirm_session_count = _session.count_wait_confirm_session(store['id'], config_const.WAIT_CONFIRM)

    numerical_session = _store.get_numerical_session_day(str(store['id']))
    _store.update_numerical_session(str(store['id']), numerical_session)

    expire_midnight = dt.now(tz=pytz.timezone('Asia/Tokyo')).strftime('%Y-%m-%d 23:59:59')

    key_gen = db_handle.create_key()

    if (is_approved_check_in == 1 and is_approved_check_out == 1) \
            or (is_approved_check_in == 1 and is_approved_check_out == 0):
        status = config_const.WAIT_CONFIRM
    elif is_approved_check_in == 0 and is_approved_check_out == 1:
        status = config_const.CHECKED_IN
    elif is_approved_check_in == 0 and is_approved_check_out == 0:
        status = config_const.CHECKED_OUT
    else:
        dict_fail = DictFail(message_const.UNSETTING_APPROVED_CHECK_IN_OUT_STORE).response
        return jsonify(dict_fail)

    _session.insert_session_with_key(store['id'], 0, user_name, store['check_in_code'], expire_midnight,
                                     status, numerical_session, key_gen)

    new_session = _session.get_session_by_key(key_gen)
    waiting_time = _common.get_waiting_time(int(new_session['id']), int(user['store_id']))

    if (status == config_const.CHECKED_IN) \
            or (status == config_const.CHECKED_OUT):
        save_user_history(store['id'], 0, new_session['id'], user_name,
                                         0, 0, config_const.CHECKED_IN, ' ')

        M_Session.query.filter(M_Session.key_gen == key_gen).update({
            M_Session.check_in_time: db_handle.get_full_current_jav_time()
        })
        db.session.commit()

        if status == config_const.CHECKED_OUT:
            save_user_history(store['id'], 0, new_session['id'], user_name,
                                             0, 0, config_const.CHECKED_OUT, ' ')

            _session.delete_session(str(new_session['id']))

    try:
        service_com = _service_com.get_service_from_service_company(store['service_company_id'])
        service_id  = service_com['service_id']
        company_id  = service_com['company_id']
    except:
        service_id  = 0
        company_id  = 0
        pass

    data = {
        'session_id'               : new_session['id'],
        'status_check_in'          : status,
        'store_id'                 : store['id'],
        'service_company_id'       : store['service_company_id'],
        'service_id'               : service_id,
        'company_id'               : company_id,
        'user_name'                : user_name,
        'number_of_waiting_people' : int(wait_confirm_session_count),
        'waiting_time'             : waiting_time,
        'numerical_session'        : numerical_session
    }

    dictSucess = DictSuccess(data, message_const.MES_NONE).response
    return jsonify(dictSucess)

@mod_store.route('/get-seat-and-staff', methods = ['GET'])
@manager_user_auth_required()
def get_seat_and_staff(user = None):
    if not user:
        dictFail = DictFail(message_const.USER_NOT_EXISTS).response
        return jsonify(dictFail)

    _session      = Session()
    _store        = Store()
    _manager_user = ManagerUser()

    response = get_list_seat_of_store(user['store_id'])
    seat_arr = response['data']
    message  = response['message']
    success  = response['success']

    data  = {}
    if seat_arr and success:
        store = _store.getStoreById(user['store_id'])
        session_list = _session.get_list_session_by_store(store['id'])

        for sess_item in session_list:
            for seatItem in seat_arr:
                if sess_item['seat_number'] == seatItem['number']:
                    seatItem['status'] = 1

        manager_user_list = _manager_user.get_manager_user_by_store(user['store_id'], 5)

        staff ={}
        staff_list = []
        for manager_user in manager_user_list:
            staff['staff_id']   = manager_user['id']
            staff['staff_name'] = manager_user['name']
            staff_list.append(copy.copy(staff))

        data['seat_list']  = seat_arr
        data['staff_list'] = staff_list

    else:
        data['seat_list'] = []
        data['staff_list'] = []

    dictSucess = DictSuccess(data, message).response
    return jsonify(dictSucess)

@mod_store.route('/set-seat-and-staff', methods = ['POST'])
@manager_user_auth_required()
def set_seat_and_staff(user = None):
    if not user:
        dict_fail = DictFail(message_const.USER_NOT_EXISTS).response
        return jsonify(dict_fail)

    result = request.json if request.json else {}
    if result == {}:
        dict_fail = DictFail(message_const.JSON_NONE).response
        return jsonify(dict_fail)

    if int(user['store_id'] == 0):
        dict_fail = DictFail(message_const.CAN_NOT_GET_STORE_ID_OF_USER).response
        return jsonify(dict_fail)

    new_seat_list = result.get('seat_list', '')
    _store = Store()
    _management_user = ManagerUser()
    store_id = int(user['store_id'])
    if store_id > 0:
        response = get_list_seat_of_store(store_id)
        if response['success'] == True:
            old_seat_list = response['data']

            for old_item in old_seat_list:
                for new_item in new_seat_list:
                    if int(old_item['number']) == int(new_item['number']):
                        old_item['staff_id'] = int(new_item['staff_id'])
                        if int(new_item['staff_id']) > 0:
                            management_user = _management_user.get_staff_name_by_id(new_item['staff_id'])
                            if management_user:
                                old_item['staff_name'] = str(management_user['name'].encode('utf-8'))
                            else:
                                old_item['staff_name'] = ''
                        else:
                            old_item['staff_name'] = ''

            _store.update_seat_manager_of_store(store_id, str(json.dumps(old_seat_list)))

            dictSucess = DictSuccess(None, message_const.SUCCESS_MES).response
            return jsonify(dictSucess)
        else:
            dict_fail = DictFail(message_const.FAIL_MES).response
            return jsonify(dict_fail)
    else:
        dict_fail = DictFail(message_const.FAIL_MES).response
        return jsonify(dict_fail)

def _get_memo_user(service_id, user_id):
    data = {}
    photo = {}
    _user_memo = UserMemo()

    if not service_id and not user_id:
        data['note'] = ''

        photo['photo_1'] = ''
        photo['photo_2'] = ''
        photo['photo_3'] = ''
        photo['photo_4'] = ''

        data['photo'] = photo
        data['created'] = 0

        return data

    resUserMemo = _user_memo.get_memo_by_user_service(user_id, service_id)

    if not resUserMemo:
        _user_memo.create_memo_user(user_id, service_id)
        resUserMemo = _user_memo.get_memo_by_user_service(user_id, service_id)

    if not resUserMemo['information_json']:
        data['note'] = ''

        photo['photo_1'] = ''
        photo['photo_2'] = ''
        photo['photo_3'] = ''
        photo['photo_4'] = ''

        data['photo'] = photo
        data['created'] = 0

        return data

    info = json.loads(resUserMemo['information_json'])
    photo = {}
    data['note'] = info['note'] if info['note'] else ''
    photo['photo_1'] = str(info['photo']['photo_1']) if info['photo']['photo_1'] else ''
    photo['photo_2'] = str(info['photo']['photo_2']) if info['photo']['photo_2'] else ''
    photo['photo_3'] = str(info['photo']['photo_3']) if info['photo']['photo_3'] else ''
    photo['photo_4'] = str(info['photo']['photo_4']) if info['photo']['photo_4'] else ''

    data['photo'] = photo
    data['created'] = db_handle.get_timestamp_milis_from_date(resUserMemo['first_modified']) if resUserMemo['first_modified'] else 0

    return data

def get_history_user_today_by_history_id(history_id, name_history_table):

    _history_user = HistoryUser()
    _manager_user = ManagerUser()
    data          = {}

    user_check_out_history = _history_user.get_history_by_id(name_history_table, history_id)

    user_check_in_history  = _history_user.get_history_user_by_action_code(name_history_table,
                                                                     user_check_out_history['store_id'],
                                                                     user_check_out_history['user'],
                                                                     user_check_out_history['session'],
                                                                     config_const.CHECKED_IN,
                                                                     db_handle.get_current_day())
    if user_check_in_history:
        if user_check_in_history['staff']:
            manager_user = _manager_user.getUser(user_check_in_history['staff'])
            if manager_user:
                data['staff_name'] = manager_user['name'] if manager_user['name'] else ''
        else:
            data['staff_name'] = ''
        data['check_in_time'] = db_handle.get_timestamp_milis_from_date(user_check_in_history['created']) \
            if user_check_in_history['created'] else 0
    else:
        data['staff_name']    = ''
        data['check_in_time'] = 0

    data['check_out_time'] = db_handle.get_timestamp_milis_from_date(user_check_out_history['created']) \
            if user_check_out_history['created'] else 0

    user_use_card_history = _history_user.get_history_user_by_action_code(name_history_table,
                                                                     user_check_out_history['store_id'],
                                                                     user_check_out_history['user'],
                                                                     user_check_out_history['session'],
                                                                     'use_card',
                                                                     db_handle.get_current_day())
    if user_use_card_history:
        if user_use_card_history['note']:
            try:
                used_card_info = json.loads(user_use_card_history['note'])
                data['number_of_used_card'] = int(used_card_info['number_of_used_card']) if used_card_info['number_of_used_card'] else 0
            except Exception, e:
                data['number_of_used_card'] = 0
        else:
            data['number_of_used_card'] = 0
    else:
        data['number_of_used_card'] = 0

    return data

def get_name_table_after_subtract_month(month):
    date_before_month = dt.today() - relativedelta(months=month)
    return date_before_month.strftime('%Y_%m') + '_user_history'

def check_valid_session_management_user(session_id, management_user_id):
    return ''

@mod_store.route('/get-check-in-out-setting', methods = ['GET'])
@manager_user_auth_required()
def get_check_in_out_setting(user = None):
    if not user:
        dict_fail = DictFail(message_const.USER_NOT_EXISTS).response
        return jsonify(dict_fail)

    if not (int(user['level_id']) == 3 or int(user['level_id']) == 4):
        dictFail = DictFail(message_const.PERMISSION_DENIED).response
        return jsonify(dictFail)

    _store = Store()
    _service_company = Service_Company()
    data = {}
    service_company = {}

    # company
    if int(user['level_id']) == 3:
        service_company = _service_company.get_service_from_service_company(int(user['service_company_id']))
    elif int(user['level_id']) == 4:  # store
        store = _store.getStoreById(int(user['store_id']))
        service_company = _service_company.get_service_from_service_company(int(store['service_company_id']))

    data['service_company_id'] = int(service_company['id'])
    data['is_approved_check_in'] = bool(service_company['is_approved_check_in'])
    data['is_approved_check_out'] = bool(service_company['is_approved_check_out'])

    dictSucess = DictSuccess(data, message_const.MES_NONE).response
    return jsonify(dictSucess)

@mod_store.route('/set-check-in-out-setting', methods = ['POST'])
@manager_user_auth_required()
def set_check_in_out_setting(user=None):
    if not user:
        dict_fail = DictFail(message_const.USER_NOT_EXISTS).response
        return jsonify(dict_fail)

    if not (int(user['level_id']) == 3 or int(user['level_id']) == 4):
        dictFail = DictFail(message_const.PERMISSION_DENIED).response
        return jsonify(dictFail)

    result = request.json if request.json else {}
    if result == {}:
        dictFail = DictFail(message_const.JSON_NONE).response
        return jsonify(dictFail)

    is_approved_check_in = int(result.get('is_approved_check_in', ''))
    is_approved_check_out = int(result.get('is_approved_check_out', ''))
    _store = Store()
    _service_company = Service_Company()

    # company
    if int(user['level_id']) == 3:
        _service_company.set_approve_check_in_out(int(user['service_company_id']), int(is_approved_check_in),
                                                  int(is_approved_check_out))
    elif int(user['level_id']) == 4:  # store
        store = _store.getStoreById(int(user['store_id']))
        _service_company.set_approve_check_in_out(int(store['service_company_id']), int(is_approved_check_in),
                                                  int(is_approved_check_out))

    dictSucess = DictSuccess(None, message_const.SET_CHECK_IN_OUT_SETTING_SUCCESS).response
    return jsonify(dictSucess)


def add_stamp_for_user(service_company_id, store_id, user_id, management_user_id
                       , seat_number, session_id, username, point, number_stamp_add):

    _service_company = Service_Company()
    _store = Store()
    _card = Card()
    _stamp = Stamp()
    _reminder_notification = ReminderNotification()

    service_company = _service_company.get_service_from_service_company(service_company_id)
    store           = _store.getStoreById(str(store_id))
    card            = get_card_after_create(service_company, user_id)

    if not card:
        return False

    type_create_coin = service_company['card_type']

    # insert stamp to card or update point for card
    try:
        if type_create_coin == config_const.CARD_TYPE:

            if int(number_stamp_add) == 1:
                m_stamp = M_Stamp()
                m_stamp.card_id = card['id']
                m_stamp.store_id = store_id
                m_stamp.management_user_id = management_user_id
                m_stamp.store_name = store['name']
                m_stamp.star_review = -1
                m_stamp.text_review = ''
                m_stamp.point = point

                db.session.add(m_stamp)
                db.session.commit()
                # change current number of stamp in card
                new_current_number_of_stamps = card['current_number_of_stamps'] + 1
                if new_current_number_of_stamps == card['max_number_of_stamps']:
                    can_be_used = 1
                else:
                    can_be_used = 0

                if int(card['is_used']) == 1:
                    status = 'used'
                else:
                    if can_be_used == 1:
                        status = config_const.CAN_USE
                    else:
                        status = config_const.UNUSED

                updateCardRes = _card.udpate_card(card['id'], str(new_current_number_of_stamps), can_be_used, status)

                if not updateCardRes:
                    return False

                updateStampRes = _stamp.update_number_in_card(m_stamp.id, new_current_number_of_stamps)

                if not updateStampRes:
                    return False

                _reminder_notification.create_reminder_notification(service_company['id'], store_id, user_id,
                                                                    m_stamp.id, 0, 0)
                # Save history for stamp
                save_success = save_add_stamp_history(m_stamp.id, user_id, session_id, username, management_user_id,
                                                      seat_number, config_const.ADD_STAMP, '')

                if not save_success:
                    return False

            elif int(number_stamp_add) > 1:
                i = 0
                current_number_of_stamps = int(card['current_number_of_stamps']) if card['current_number_of_stamps'] else 0
                max_number_of_stamps     = int(card['max_number_of_stamps']) if card['max_number_of_stamps'] else 0
                while i < int(number_stamp_add):
                    #Check card full
                    if current_number_of_stamps == max_number_of_stamps:

                        can_be_used = 1
                        status      = config_const.CAN_USE
                        _card.udpate_card(card['id'], str(current_number_of_stamps), can_be_used, status)
                        #create new card for remain stamp
                        _card.create_new_card(
                            service_company_id,
                            user_id,
                            0,
                            service_company['number_of_stamps_in_card'],
                            0,
                            0,
                            config_const.UNUSED
                        )
                        card = _card.get_card_by_service_company_id_and_user(service_company_id, user_id,
                                                                             config_const.UNUSED)
                        current_number_of_stamps = 0
                    current_number_of_stamps += 1
                    m_stamp = M_Stamp()

                    m_stamp.card_id            = card['id']
                    m_stamp.store_id           = store_id
                    m_stamp.management_user_id = management_user_id
                    m_stamp.store_name         = store['name']
                    m_stamp.star_review        = -1
                    m_stamp.text_review        = ''
                    m_stamp.point              = point
                    m_stamp.number_in_card     = current_number_of_stamps

                    db.session.add(m_stamp)
                    db.session.commit()

                    _reminder_notification.create_reminder_notification(service_company['id'], store_id, user_id,
                                                                        m_stamp.id, 0, 0)
                    # Save history for stamp
                    save_success = save_add_stamp_history(m_stamp.id, user_id, session_id, username, management_user_id,
                                                          seat_number, config_const.ADD_STAMP, '')

                    if not save_success:
                        return False

                    i+=1

                if current_number_of_stamps == max_number_of_stamps:
                    can_be_used = 1
                    status = config_const.CAN_USE
                else:
                    can_be_used = 0
                    status = config_const.UNUSED
                _card.udpate_card(card['id'], str(current_number_of_stamps), can_be_used, status)

        elif type_create_coin == config_const.POINT_TYPE:
            m_stamp = M_Stamp.query.filter(M_Stamp.card_id == card['id']).first()
            if m_stamp:
                old_point = m_stamp.point
                m_stamp.point = int(point) + int(old_point)
                db.session.commit()
            else:
                m_stamp = M_Stamp()

                m_stamp.card_id            = card['id']
                m_stamp.store_id           = store_id
                m_stamp.management_user_id = management_user_id
                m_stamp.store_name         = store['name']
                m_stamp.star_review        = -1
                m_stamp.text_review        = ''
                m_stamp.point              = point

                db.session.add(m_stamp)
                db.session.commit()

        elif type_create_coin == config_const.NONE_TYPE:
            return True

    except Exception,e :
        db.session.rollback()
        return False

    return True


@mod_store.route('/get-list-stamp-to-review', methods = ['POST'])
@manager_user_auth_required()
def get_list_stamp_to_review(user=None):
    if not user:
        dict_fail = DictFail(message_const.TOKEN_EXPIRE).response
        return jsonify(dict_fail)

    result = request.json if request.json else {}
    if result == {}:
        dictFail = DictFail(message_const.JSON_NONE).response
        return jsonify(dictFail)

    try:
        page  = request.args.get('page', 1)
        limit = request.args.get('limit', 20)
    except:
        page  = 1
        limit = 20

    start_at       = str(result.get('start_at', ''))
    end_at         = str(result.get('end_at', ''))
    is_all_message = int(result.get('is_all_message', 1))

    name_history_table = db_handle.get_name_table_history_now()

    checkTableExists = db_handle.check_table_exists(name_history_table, '')

    if not checkTableExists:
        try:
            db_handle.create_table_history(name_history_table)
        except:
            dictFail = DictFail(message_const.MES_NONE).response
            return jsonify(dictFail)
    none_star  = -1
    none_text  = ''

    lst_stamp  = M_Stamp.query.filter(func.DATE(M_Stamp.review_at) >= start_at).filter(func.DATE(M_Stamp.review_at) <= end_at)\
        .filter(M_Stamp.star_review != none_star).filter(M_Stamp.store_id == user['store_id'])

    if is_all_message:
        total_number_of_stamp = lst_stamp.count()
    else:
        total_number_of_stamp = lst_stamp.filter(M_Stamp.text_review != none_text).count()
        lst_stamp             = lst_stamp.filter(M_Stamp.text_review != none_text)

    startPoint = db_handle.getStartPoint(page, limit)
    lst_stamp = lst_stamp.offset(startPoint).limit(limit)

    total_page = db_handle.get_total_page(total_number_of_stamp, limit)

    data       = {}
    arr_stamps = []
    number_of_stamp_in_page = 0
    for stamp in lst_stamp:
        number_of_stamp_in_page +=1
        stamp_dict = stamp.to_dict()
        m_card  = M_Card.query.filter_by(id = stamp.card_id).first()
        m_user  = M_User.query.filter_by(id = m_card.user_id).first()

        history_stamp = M_History_User.query.filter(M_History_User.stamp_id == stamp.id).first()

        history = {}

        if history_stamp:
            history['history_id']          = history_stamp.id
            history['name']                = m_user.name if m_user.name else ''
            history['seat_number']         = int(history_stamp.seat_number) if history_stamp.seat_number else 0
            history['history_information'] = get_history_user_by_session(stamp.store_id, m_user.id, history_stamp.session)

        stamp_dict['history'] = history
        arr_stamps.append(stamp_dict)

    data['list_stamp'] = arr_stamps

    data['page']                    = int(page)
    data['total_page']              = int(total_page)
    data['limit']                   = int(limit)
    data['total_number_of_stamp']   = int(total_number_of_stamp)
    data['number_of_stamp_in_page'] = int(number_of_stamp_in_page)

    dictSucess = DictSuccess(data, message_const.MES_NONE).response
    return jsonify(dictSucess)


def get_history_user_by_session(store_id, user_id, session_id):
    data = {}

    user_check_out_history = M_History_User.query.filter(M_History_User.store_id == store_id).\
            filter(M_History_User.user == user_id).\
            filter(M_History_User.session == session_id).\
            filter(M_History_User.action_code == config_const.CHECKED_OUT).first()

    if not user_check_out_history:
        return data

    user_check_in_history = M_History_User.query.filter(M_History_User.store_id == store_id). \
        filter(M_History_User.user == user_id). \
        filter(M_History_User.session == session_id). \
        filter(M_History_User.action_code == config_const.CHECKED_IN).first()

    if user_check_in_history:
        if user_check_in_history.staff:
            manager_user = M_Manager_User.query.filter_by(id = user_check_in_history.staff).first()
            if manager_user:
                data['staff_name'] = manager_user.name if manager_user.name else ''
        else:
            data['staff_name'] = ''
        data['check_in_time'] = db_handle.get_timestamp_milis_from_date(user_check_in_history.created) \
            if user_check_in_history.created else 0
    else:
        data['staff_name'] = ''
        data['check_in_time'] = 0

    data['check_out_time'] = db_handle.get_timestamp_milis_from_date(user_check_out_history.created) \
        if user_check_out_history.created else 0

    user_use_card_history = M_History_User.query.filter(M_History_User.store_id == store_id). \
        filter(M_History_User.user == user_id). \
        filter(M_History_User.session == session_id). \
        filter(M_History_User.action_code == config_const.USE_CARD).first()

    if user_use_card_history:
        if user_use_card_history.note:
            used_card_info = json.loads(user_use_card_history.note)
            data['number_of_used_card'] = int(used_card_info['number_of_used_card']) if used_card_info['number_of_used_card'] else 0
        else:
            data['number_of_used_card'] = 0
    else:
        data['number_of_used_card'] = 0

    return data


@mod_store.route('/get-detail-stamp-to-review', methods = ['POST'])
@manager_user_auth_required()
def get_detail_stamp_to_review(user=None):
    if not user:
        dict_fail = DictFail(message_const.TOKEN_EXPIRE).response
        return jsonify(dict_fail)

    result = request.json if request.json else {}
    if result == {}:
        dictFail = DictFail(message_const.JSON_NONE).response
        return jsonify(dictFail)
    try:
        data = {}
        _common = Common()
        stamp_id = str(result.get('stamp_id', ''))
        m_stamp = M_Stamp.query.filter(M_Stamp.id == stamp_id).first()
        m_card  = M_Card.query.filter(M_Card.id == m_stamp.card_id).first()
        m_service_company = M_Service_Company.query.filter(M_Service_Company.id == m_card.service_company_id).first()

        service_id = m_service_company.service_id
        user_id    = m_card.user_id
        store_id   = m_stamp.store_id

        memo_user         = _common.get_user_memo(service_id, user_id)
        data['memo_user'] = memo_user
        store_user         = _common.get_store_memo(store_id, user_id)
        data['store_user'] = store_user

        dictSucess = DictSuccess(data, message_const.MES_NONE).response
        return jsonify(dictSucess)

    except Exception, e:
        dictFail = DictFail(message_const.CONFIG_MEMO_NOT_SET).response
        return jsonify(dictFail)

@mod_store.route('/how-use-app', methods = ['GET'])
@manager_user_auth_required()
def how_use_app(user = None):

    if not user:
        dictFail = DictFail(message_const.TOKEN_EXPIRE).response
        return jsonify(dictFail)

    data = {}
    content = FormatWeb.get_content_how_use_app().replace('\n','').replace('\t','')
    data['content'] = content

    dictSucess = DictSuccess(data, message_const.MES_NONE).response
    return jsonify(dictSucess)

@mod_store.route('/get-list-question', methods = ['POST'])
@manager_user_auth_required()
def get_list_question(user = None):
    if not user:
        dictFail = DictFail(message_const.TOKEN_EXPIRE).response
        return jsonify(dictFail)

    try:
        page  = request.args.get('page', 1)
        limit = request.args.get('limit', 20)
    except:
        page  = 1
        limit = 20

    result = request.json if request.json else {}
    if result == {}:
        dictFail = DictFail(message_const.JSON_NONE).response
        return jsonify(dictFail)

    system = result.get('system', 1)

    _question = Question()

    # Question all
    getTotal = True
    result_number_of_ques = _question.get_question(page, limit, getTotal, system, 1)
    total_number_of_ques = 0
    if not result_number_of_ques:
        dictFail = DictFail(message_const.MES_NONE).response
        return jsonify(dictFail)

    for row in result_number_of_ques:
        total_number_of_ques += 1

    total_page = db_handle.get_total_page(total_number_of_ques, limit)

    # Question in page
    getTotal = False
    result_ques_in_page = _question.get_question(page, limit, getTotal, system, 1)
    number_of_ques_in_page = 0
    if not result_ques_in_page:
        dictFail = DictFail(message_const.MES_NONE).response
        return jsonify(dictFail)

    data   = {}
    titles = []
    title  = {}

    for row in result_ques_in_page:
        number_of_ques_in_page += 1
        title['title']  = row['title']  if row['title'] else ''
        title['answer'] = row['answer'] if row['answer'] else ''
        titles.append(copy.copy(title))

    data['page']                   = int(page)
    data['total_page']             = int(total_page)
    data['limit']                  = int(limit)
    data['total_number_of_ques']   = int(total_number_of_ques)
    data['number_of_ques_in_page'] = int(number_of_ques_in_page)
    data['list_titles']            = titles

    dictSucess = DictSuccess(data, message_const.MES_NONE).response
    return jsonify(dictSucess)


@mod_store.route('/save-store-contact', methods = ['POST'])
@manager_user_auth_required()
def save_store_contact(user = None):
    if not user:
        dictFail = DictFail(message_const.TOKEN_EXPIRE).response
        return jsonify(dictFail)

    result = request.json if request.json else {}
    if result == {}:
        dictFail = DictFail(message_const.MES_NONE).response
        return jsonify(dictFail)
    user_name = str(user['name'])
    _company_email = CompanyEmail()
    _email_handle = EmailHandle()
    info = result.get('info', '')

    is_have_line_break = info.find("\n")
    if is_have_line_break >= 0:
        info = info.replace("\n", "<br>")

    store_id = user['store_id'] if user['store_id'] else 0
    try:
        m_contact = M_Contact()
        m_contact.store_id = store_id
        m_contact.info     = info
        db.session.add(m_contact)
        db_handle.commit_obj()
    except:
        db.session.rollback()
        dictFail = DictFail(message_const.MES_NONE).response
        return jsonify(dictFail)

    # get email company
    email_info = _company_email.get_company_email()

    # send email contact admin
    email_variables = {
        'email': admin_email.ADMIN_EMAIL,
        'sender': user_name,
        'content': str(info)
    }
    _email_handle.send_email(email_info['email'], email_info['password'],
                             email_info['host'], email_info['port'],
                             email_variables, email_type_const.CONTACT_ADMIN)

    email_variables = {
        'email': str(user['email']),
        'sender': user_name,
        'content': str(info)
    }
    _email_handle.send_email(email_info['email'], email_info['password'],
                             email_info['host'], email_info['port'],
                             email_variables, email_type_const.CONTACT_USER)

    dictSucess = DictSuccess(None, message_const.MES_NONE).response
    return jsonify(dictSucess)


@mod_store.route('/check-version-app-and-server-user', methods=['POST'])
def check_version_app_and_server():
    result = request.json if request.json else {}
    if result == {}:
        dict_fail = DictFail(message_const.JSON_NONE).response
        return jsonify(dict_fail)

    # get post data
    device_id = result.get('device_id')
    current_version = result.get('current_version')

    newest_version = M_Version.query.filter(M_Version.version_code > current_version, M_Version.type == 2)\
        .order_by(M_Version.version_code.desc()).first()

    server_information = M_Server_Information.query.order_by(M_Server_Information.id.asc()).first()

    if newest_version:
        version_info = {
            'version_code': newest_version.version_code,
            'version_name': newest_version.version_name,
            'is_required': True if newest_version.is_required == 1 else False,
            'is_send_notify': True if newest_version.is_send_notify == 1 else False,
            'change_log': newest_version.change_log,
            'warning': newest_version.warning
        }

        data = {
            'has_update': True
        }
    else:
        newest_version = M_Version.query.filter(M_Version.version_code == current_version, M_Version.type == 1)\
            .first()
        if not newest_version:
            dict_fail = DictFail(message_const.MES_NONE).response
            return jsonify(dict_fail)

        version_info = {
            'version_code': newest_version.version_code,
            'version_name': newest_version.version_name,
            'is_required': True if newest_version.is_required == 1 else False,
            'is_send_notify': True if newest_version.is_send_notify == 1 else False,
            'change_log': newest_version.change_log,
            'warning': newest_version.warning
        }

        data = {
            'has_update': False
        }

    server_info = {
        'status': server_information.status,
        'is_maintain': True if server_information.is_maintain == 1 else False,
        'message': server_information.message
    }

    data['version_info'] = version_info
    data['server_information'] = server_info

    dict_success = DictSuccess(data, message_const.MES_NONE).response
    return jsonify(dict_success)


def create_auth_token(user):

    new_token = str(uuid.uuid4())

    user_id = user.id
    expire  = db_handle.getExpireToken()

    try:
        m_token = M_Token()
        m_token.management_user_id = user_id
        m_token.auth_token = new_token
        m_token.expire = expire
        db.session.add(m_token)
        db.session.commit()
    except Exception, e:
        db.session.rollback()
        return False

    data = {
        'token'       : new_token,
        'expire_date' : expire,
        'username'    : user.name.encode('utf-8'),
        'email'       : user.email.encode('utf-8')
    }
    return data

#----------------------------------------------------------------------------------------------#

#HANDLE ADD, EDIT, DELETE TIMELINE
@mod_store.route('/add-timeline', methods = ['POST'])
@manager_user_auth_required()
def add_timeline(user = None):
    if not user:
        db_handle.close_connect()
        LogHandle(message_const.TOKEN_EXPIRE, message_const.WARNING)
        dictFail = DictFail(message_const.TOKEN_EXPIRE).response
        return jsonify(dictFail)

    result = request.json if request.json else {}
    if result == {}:
        db_handle.close_connect()
        dictFail = DictFail(message_const.MES_NONE).response
        return jsonify(dictFail)

    user_id          = user['id']
    app_id           = user['app_id']
    firebase_api_key = user['firebase_api_key']
    store_id         = user['store_id']

    if not app_id:
        app_id = 0

    if not firebase_api_key:
        firebase_api_key = ''

    message  = result.get('message', '')
    images   = result.get('images', '')
    img_html = result.get('img_html', '')

    data = {}
    try:
        m_timeline = M_Timeline()

        m_timeline.images             = images
        m_timeline.messages           = message
        m_timeline.management_user_id = user_id
        m_timeline.app_id             = app_id
        m_timeline.firebase_api_key   = firebase_api_key
        m_timeline.img_html           = img_html

        db.session.add(m_timeline)
        db.session.commit()

        is_send_for_user = True
        if store_id and m_timeline.id:
            list_device_token = func_get_list_user_receive_notifi_when_add_timeline(store_id)
            m_store = M_Store.query.filter_by(id = store_id).first();
            manager_post_timeline = user['name'] if user['name'] else (m_store.name if m_store.name else '')
            send_push_firebase(is_send_for_user, firebase_api_key, list_device_token, 'action_timeline', m_timeline.id, '', manager_post_timeline, 0)
        db_handle.close_connect()

        redis_store.delete('list_timeline')
        dict_success = DictSuccess(None, message_const.MES_NONE).response
        return jsonify(dict_success)

    except Exception, e:
        LogHandle(e, message_const.ERROR)
        db.session.rollback()
        db_handle.close_connect()
        dictFail = DictFail(message_const.MES_NONE).response
        return jsonify(dictFail)

@mod_store.route('/edit-timeline', methods = ['POST'])
@manager_user_auth_required()
def edit_timeline(user = None):
    if not user:
        LogHandle(message_const.TOKEN_EXPIRE, message_const.WARNING)
        dictFail = DictFail(message_const.TOKEN_EXPIRE).response
        return jsonify(dictFail)

    result = request.json if request.json else {}
    if result == {}:
        LogHandle(message_const.JSON_NONE, message_const.WARNING)
        dictFail = DictFail(message_const.MES_NONE).response
        return jsonify(dictFail)
    user_id = user['id']

    timeline_id = result.get('timeline_id', 0)
    message = result.get('message', '')
    images  = result.get('images', '')

    data = {}
    try:
        m_timeline = M_Timeline.query.filter_by(id = timeline_id).first()

        if not m_timeline:
            dictFail = DictFail(message_const.MES_NONE).response
            return jsonify(dictFail)

        m_timeline.images   = images
        m_timeline.messages = message

        db.session.commit()
        db.session.close()

        dict_success = DictSuccess(None, message_const.MES_NONE).response
        return jsonify(dict_success)

    except Exception, e:
        LogHandle(e, message_const.ERROR)
        db.session.rollback()
        dictFail = DictFail(message_const.MES_NONE).response
        return jsonify(dictFail)

@mod_store.route('/delete-timeline', methods = ['POST'])
@manager_user_auth_required()
def delete_timeline(user = None):
    if not user:
        LogHandle(message_const.TOKEN_EXPIRE, message_const.WARNING)
        dictFail = DictFail(message_const.TOKEN_EXPIRE).response
        return jsonify(dictFail)

    result = request.json if request.json else {}
    if result == {}:
        LogHandle(message_const.JSON_NONE, message_const.WARNING)
        dictFail = DictFail(message_const.MES_NONE).response
        return jsonify(dictFail)

    user_id = user['id']

    timeline_id = result.get('timeline_id', 0)

    data = {}
    try:
        M_Timeline.query.filter(M_Timeline.id == timeline_id).update({M_Timeline.is_deleted : 1})
        db.session.commit();
        db.session.close();

        dict_success = DictSuccess(None, message_const.MES_NONE).response
        return jsonify(dict_success)

    except Exception, e:
        LogHandle(e, message_const.ERROR)
        db.session.rollback()
        dictFail = DictFail(message_const.MES_NONE).response
        return jsonify(dictFail)

#HANDLE GET TIMELINE
@mod_store.route('/get-timeline-by-id', methods = ['POST'])
@manager_user_auth_required()
def get_timeline_by_id(user = None):
    if not user:
        dictFail = DictFail(message_const.TOKEN_EXPIRE).response
        return jsonify(dictFail)

    result = request.json if request.json else {}
    if result == {}:
        dictFail = DictFail(message_const.MES_NONE).response
        return jsonify(dictFail)

    user_id = user['id']

    timeline_id = result.get('timeline_id', 0)

    data = {}
    number_like = 0
    list_type_like = []
    try:
        m_timeline = M_Timeline.query.filter_by(id=timeline_id, is_deleted=0).first()
        if not m_timeline:
            dictFail = DictFail(message_const.MES_NONE).response
            return jsonify(dictFail)

        result = func_list_like_info_of_timeline_id(timeline_id);

        one_timeline = m_timeline.to_dict()
        one_timeline['number_like'] = result[0]

        data['timeline'] = one_timeline
        data['likes']    = result[1]

        dict_success = DictSuccess(data, message_const.MES_NONE).response
        return jsonify(dict_success)

    except Exception, e:
        LogHandle(e, message_const.ERROR)
        db.session.rollback()
        dictFail = DictFail(message_const.MES_NONE).response
        return jsonify(dictFail)


def func_get_img_store_by_manager(manager_user_id):
    m_manager = M_Manager_User.query.filter_by(id = manager_user_id, is_deleted = 0).first()

    if not m_manager:
        return ""

    m_store = M_Store.query.filter_by(id = m_manager.store_id).first()

    if not m_store:
        return ""

    return m_store.logo if m_store.logo else ""


def func_get_list_user_like_timeline(timeline_id, like_id):
    list_user_timeline_like = M_User_Timeline_Like.query.filter_by(timeline_id = timeline_id, like_id = like_id).all()

    if not list_user_timeline_like:
        return None

    return list_user_timeline_like


def func_get_list_user_like_comment(comment_id, like_id):
    list_user_comment_like = M_User_Comment_Like.query.filter_by(comment_id = comment_id, like_id = like_id).all()

    if not list_user_comment_like:
        return None

    return list_user_comment_like


def func_list_like_info_of_timeline_id(timeline_id):
    number_like = 0
    list_type_like = []

    result = []

    try:
        # Handle like timeline
        likes = M_Timeline_Count_Like.query.filter(M_Timeline_Count_Like.timeline_id == timeline_id, M_Timeline_Count_Like.count_like > 0).all()
        if not likes:
            number_like = 0
        else:
            # Get like type
            for one_type_like in likes:
                number_like += one_type_like.count_like
                # Get user like
                like_json = copy.copy(one_type_like.to_dict())
                list_user_like = []
                if func_get_list_user_like_timeline(timeline_id, one_type_like.like_id):
                    for user_time_like in func_get_list_user_like_timeline(timeline_id, one_type_like.like_id):
                        list_user_like.append(user_time_like.user_name)

                like_json['list_user_like'] = list_user_like
                list_type_like.append(like_json)

        result.append(number_like)
        result.append(list_type_like)
        return result

    except Exception, e:
        result.append(0)
        result.append([])
        return result


def func_list_like_info_of_comment_id(comment_id):
    number_like = 0
    list_type_like = []

    result = []

    try:
        # Handle like timeline
        likes = M_Comment_Count_Like.query.filter(M_Comment_Count_Like.comment_id == comment_id, M_Comment_Count_Like.count_like > 0).all()
        if not likes:
            number_like = 0
        else:
            # Get like type
            for one_type_like in likes:
                number_like += one_type_like.count_like
                # Get user like
                like_json = copy.copy(one_type_like.to_dict())
                list_user_like = []
                if func_get_list_user_like_comment(comment_id, one_type_like.like_id):
                    for user_time_like in func_get_list_user_like_comment(comment_id, one_type_like.like_id):
                        list_user_like.append(user_time_like.user_name)

                like_json['list_user_like'] = list_user_like
                list_type_like.append(like_json)

        result.append(number_like)
        result.append(list_type_like)
        return result

    except Exception, e:
        result.append(0)
        result.append([])
        return result


def func_get_like_type_of_current_user(user_id, timeline_id, is_normal_user):

    if is_normal_user:
        m_self_like_timeline = M_User_Timeline_Like.query.filter_by(user_id=user_id,
                                                                    timeline_id=timeline_id).first()

    else:
        m_self_like_timeline = M_User_Timeline_Like.query.filter_by(management_user_id=user_id,
                                                                    timeline_id=timeline_id).first()

    if m_self_like_timeline:
        return  m_self_like_timeline.like_id
    else:
        return  0


def func_get_like_type_of_current_user_for_comment(user_id, comment_id, is_normal_user):
    if is_normal_user:
        m_self_like_comment = M_User_Comment_Like.query.filter_by(user_id=user_id,
                                                                  comment_id=comment_id).first()

    else:
        m_self_like_comment = M_User_Comment_Like.query.filter_by(management_user_id=user_id,
                                                                  comment_id=comment_id).first()

    if m_self_like_comment:
        return  m_self_like_comment.like_id
    else:
        return  0

def func_get_name_store_by_store_id(store_id):
    m_store = M_Store.query.filter_by(id = store_id).first()
    if not m_store:
        return ""

    name_store = m_store.name if m_store.name else ''

    return name_store


@mod_store.route('/get-list-timeline-by-store', methods = ['POST'])
@manager_user_auth_required()
def get_list_timeline_by_store(user = None):
    if not user:
        dictFail = DictFail(message_const.TOKEN_EXPIRE).response
        return jsonify(dictFail)

    manager_user_id = user['id']

    result = request.json if request.json else {}
    if result == {}:
        dictFail = DictFail(message_const.JSON_NONE).response
        return jsonify(dictFail)

    last_timeline_id = result.get('last_timeline_id', 0)
    limit_timeline   = result.get('limit_timeline', 5)

    data = {}
    is_normal_user = False
    try:
        data = func_get_timeline_by_store(last_timeline_id, limit_timeline, manager_user_id, None, user['store_id'], is_normal_user)

        dict_success = DictSuccess(data, message_const.MES_NONE).response
        return jsonify(dict_success)

    except Exception, e:
        LogHandle(e, message_const.ERROR)
        db.session.rollback()
        dictFail = DictFail(message_const.MES_NONE).response
        return jsonify(dictFail)


def func_get_timeline_by_store(last_timeline_id = None, limit_timeline = None, manager_id = None, normal_user_id = None, store_id = None, is_normal_user = None):
    data = {}
    list_timeline = []

    try:
        if last_timeline_id:
            timelines = M_Timeline.query.filter(M_Timeline.management_user_id == manager_id, M_Timeline.is_deleted == 0, M_Timeline.id < last_timeline_id)\
                .order_by(M_Timeline.id.desc()).limit(limit_timeline)
        else:
            timelines = M_Timeline.query.filter(M_Timeline.management_user_id == manager_id, M_Timeline.is_deleted == 0) \
                .order_by(M_Timeline.id.desc()).limit(limit_timeline)
        #GET INFO STORE
        manager_name = ''
        img_store    = ''

        m_store = M_Store.query.filter_by(id = store_id).first()

        if m_store:
            img_store    = m_store.logo if m_store.logo else ''
            manager_name = m_store.name if m_store.name else ''

        one_timeline  = {}

        current_id_user_see_timeline = normal_user_id if is_normal_user else manager_id

        for timeline in timelines:
            result = func_list_like_info_of_timeline_id(timeline.id);
            one_timeline_json = timeline.to_dict()
            one_timeline_json['number_like'] = result[0]
            # Check like type of current store
            one_timeline_json['self_like_id'] = func_get_like_type_of_current_user(current_id_user_see_timeline, timeline.id, is_normal_user)
            one_timeline['likes']    = result[1]
            one_timeline['timeline'] = one_timeline_json

            list_timeline.append(copy.copy(one_timeline))

        data['img_store']                  = img_store
        data['manager_name']               = manager_name
        data['list_timeline']              = list_timeline

        return data

    except Exception, e:
        print e
        return data



@mod_store.route('/store-get-list-comment-by-timeline-id', methods = ['POST'])
@manager_user_auth_required()
def store_get_list_comment_by_timeline_id(user = None):
    if not user:
        dictFail = DictFail(message_const.TOKEN_EXPIRE).response
        return jsonify(dictFail)

    user_id = user['id']

    result = request.json if request.json else {}
    if result == {}:
        dictFail = DictFail(message_const.MES_NONE).response
        return jsonify(dictFail)

    timeline_id = result.get('timeline_id', 0)
    data = {}
    one_comment = {}
    list_comment = []

    is_normal_user = False

    try:
        comments = M_Comment.query.filter_by(timeline_id = timeline_id, is_deleted = 0).order_by(M_Comment.created.desc()).all()
        for comment in comments:

            result = func_list_like_info_of_comment_id(comment.id)
            one_comment_json = comment.to_dict()
            if not comment.management_user_id:
                one_comment_json['management_user_id'] = 0
            if not comment.user_id:
                one_comment_json['user_id'] = 0
            one_comment_json['number_like'] = result[0]
            one_comment_json['img_avatar']  = ''
            if one_comment_json['management_user_id'] != 0:
                m_manager = M_Manager_User.query.filter_by(id = one_comment_json['management_user_id']).first()
                if m_manager:
                    m_store = M_Store.query.filter_by(id = m_manager.store_id).first()
                    if m_store:
                        one_comment_json['img_avatar'] = m_store.logo if m_store.logo else ''

            if one_comment_json['user_id'] != 0:
                m_user = M_User.query.filter_by(id=one_comment_json['user_id']).first()
                if m_user:
                    one_comment_json['img_avatar'] = m_user.img_url if m_user.img_url else ''
                    one_comment_json['user_name']  = m_user.name if m_user.name else ''

            one_comment['comment'] = one_comment_json
            one_comment['likes'] = result[1]
            one_comment['self_like_id'] = func_get_like_type_of_current_user_for_comment(user_id, comment.id, is_normal_user)

            list_comment.append(copy.copy(one_comment))

        data['list_comment'] = list_comment
        dict_success = DictSuccess(data, message_const.MES_NONE).response
        return jsonify(dict_success)

    except Exception, e:
        LogHandle(e, message_const.ERROR)
        db.session.rollback()
        dictFail = DictFail(message_const.MES_NONE).response
        return jsonify(dictFail)

#LIKE TIMELINE
#USER LIKE TIMELINE
@mod_store.route('/store-like-timeline', methods = ['POST'])
@manager_user_auth_required()
def store_like_timeline(user = None):
    if not user:
        dictFail = DictFail(message_const.USER_NOT_EXISTS).response
        return jsonify(dictFail)

    user_id = user['id']
    result = request.json if request.json else {}

    if result == {}:
        dictFail = DictFail(message_const.JSON_NONE).response
        return jsonify(dictFail)

    like_id     = result.get('like_id', 0)
    old_like_id = result.get('old_like_id', 0)
    timeline_id = result.get('timeline_id', 0)
    type_like   = result.get('type_like', 0)

    normal_user = False
    #Like or unlike
    func_like_timeline(type_like, timeline_id, user_id, like_id, normal_user, old_like_id)

    dictSucess = DictSuccess(None, message_const.MES_NONE).response
    return jsonify(dictSucess)

def func_update_action_on_timeline(timeline_id):
    m_timeline = M_Timeline.query.filter_by(id = timeline_id).first()

    if not m_timeline:
        return False

    m_timeline.key_gen = 1
    db.session.commit()

    return True

def func_update_action_on_chat(user_id, store_id, content):
    m_user_info = M_User_Info.query.filter_by(store_id = store_id, user_id = user_id).first()
    if not m_user_info:
        return False
    m_user_info.key_gen = 1
    m_user_info.last_message      = content
    m_user_info.last_message_time = db_handle.get_now_milis()
    m_user_info.modified = db_handle.get_full_current_jav_time()
    db.session.commit()
    return True

#USER COMMENT
@mod_store.route('/store-add-comment', methods = ['POST'])
@manager_user_auth_required()
def store_add_comment(user = None):
    if not user:
        dictFail = DictFail(message_const.USER_NOT_EXISTS).response
        return jsonify(dictFail)

    user_id = user['id']
    result = request.json if request.json else {}

    if result == {}:
        dictFail = DictFail(message_const.JSON_NONE).response
        return jsonify(dictFail)

    content     = result.get('content', '')
    timeline_id = result.get('timeline_id', 0)

    normal_user = False
    #Comment
    try:
        #get timeline
        m_timeline = M_Timeline.query.filter_by(id=timeline_id).first()

        if not m_timeline:
            LogHandle('no timeline', message_const.ERROR)
            dictSucess = DictSuccess(None, message_const.MES_NONE).response
            return jsonify(dictSucess)
        # get firebase_api_key
        if m_timeline.app_id is not None and m_timeline.app_id != '':
            m_app = M_App.query.filter_by(id=m_timeline.app_id).first()

            if not m_app:
                LogHandle('no apps', message_const.ERROR)
                dictSucess = DictSuccess(None, message_const.MES_NONE).response
                return jsonify(dictSucess)
            firebase_api_key = m_app.firebase_api_key
        else:
            firebase_api_key = m_timeline.firebase_api_key
        #get store post timeline
        m_manager = M_Manager_User.query.filter_by(id=m_timeline.management_user_id).first()

        if not m_manager:
            LogHandle('no manager', message_const.ERROR)
            dictSucess = DictSuccess(None, message_const.MES_NONE).response
            return jsonify(dictSucess)
        manager_post_timeline = m_manager.name if m_manager.name else message_const.ANONYMOUS.decode('utf-8')

        #create timeline
        m_comment = func_add_comment(timeline_id, user_id, content, normal_user)

        comment_id   = m_comment.id if m_comment.id else 0
        user_comment = m_comment.user_name if m_comment.user_name else message_const.ANONYMOUS.decode('utf-8')

        is_send_for_user = True
        if comment_id:
            list_device_token = func_get_list_user_receive_notifi_when_comment(user_id, timeline_id, normal_user)
            #send push
            send_push_firebase(is_send_for_user, firebase_api_key, list_device_token, 'action_comment', int(timeline_id), user_comment, manager_post_timeline, 0)

        dictSucess = DictSuccess(None, message_const.MES_NONE).response
        return jsonify(dictSucess)

    except Exception, e:
        LogHandle(e, message_const.ERROR)
        db.session.rollback()
        db.session.close()
        dictSucess = DictSuccess(None, message_const.MES_NONE).response
        return jsonify(dictSucess)


def func_get_list_store_token_by_manager_id(manager_id):
    arr_device_token = []

    list_device_token = M_Device.query.filter_by(manager_id = manager_id).all()

    for item_device_token in list_device_token:
        arr_device_token.append(str(item_device_token.device_token))

    return arr_device_token

def func_get_list_user_receive_notifi_when_add_timeline(store_id):
    arr_device_token = []

    m_lst_user_id_check_in = M_User_Info.query.with_entities(M_User_Info.user_id.label('user_id')) \
        .filter(M_User_Info.store_id == store_id).subquery()

    list_device_token = M_Device.query.with_entities(M_Device.device_token.label('device_token')) \
        .filter(M_Device.user_id.in_(m_lst_user_id_check_in)).all()

    for item_device_token in list_device_token:
        arr_device_token.append(str(item_device_token.device_token))

    return arr_device_token


def func_get_list_user_receive_notifi_when_comment(user_id, timeline_id, normal_user):

    arr_device_token = []

    if not normal_user:
        #case store comment
        list_user_id_comment = M_Comment.query.with_entities(distinct(M_Comment.user_id).label('user_id'))\
            .filter(M_Comment.user_id != None, M_Comment.timeline_id == timeline_id).subquery()
    else:
        #case user comment
        list_user_id_comment = M_Comment.query.with_entities(distinct(M_Comment.user_id).label('user_id')) \
            .filter(M_Comment.user_id != None, M_Comment.timeline_id == timeline_id, M_Comment.user_id != user_id).subquery()

    list_device_token = M_Device.query.with_entities(M_Device.device_token.label('device_token')) \
        .filter(M_Device.user_id.in_(list_user_id_comment)).all()

    for item_device_token in list_device_token:
        arr_device_token.append(str(item_device_token.device_token))

    return arr_device_token

def func_get_user_receive_notifi_when_chat(user_id):

    arr_device_token = []

    list_device_token = M_Device.query.with_entities(M_Device.device_token.label('device_token')) \
        .filter(M_Device.user_id == user_id).all()

    for item_device_token in list_device_token:

        arr_device_token.append(str(item_device_token.device_token))

    return arr_device_token

def func_get_user_receive_notifi_when_chat_all(store_id):
    arr_device_token = []
    m_lst_user_id_check_in = M_User_Info.query.with_entities(distinct(M_User_Info.user_id).label('user_id')) \
        .filter_by(store_id=store_id).subquery()

    list_device_token = M_Device.query.with_entities(M_Device.device_token.label('device_token')) \
        .filter(M_Device.user_id.in_(m_lst_user_id_check_in)).all()

    for item_device_token in list_device_token:

        arr_device_token.append(str(item_device_token.device_token))

    return arr_device_token


def send_push_firebase(is_send_for_user, firebase_api_key, list_device_token
                       , action_push , push_noti_id, name_send
                       , manager_post_timeline, id_user_send_chat):

    body_message = ' '

    if action_push == 'action_chat':
        title_message = name_send + message_const.MESS_CHAT.decode('utf-8')
    elif action_push == 'action_comment':
        title_message = name_send + message_const.MESS_COMMENT_1.decode('utf-8') + '' + message_const.MESS_COMMENT_2.decode('utf-8')
    elif action_push == 'action_timeline':
        title_message = message_const.NEW_TIMELINE
        body_message  = manager_post_timeline + message_const.POST_TIMELINE.decode('utf-8')
    elif action_push == 'action_like':
        title_message = name_send + message_const.MESS_LIKE.decode('utf-8')

    else:
        title_message = ''
    badge = 1

    if not is_send_for_user:
        badge = 0
        # firebase_api_key = message_const.FIREBASE_API_STORE

    firebase_api_key = "key=" + firebase_api_key
    url = 'https://fcm.googleapis.com/fcm/send'

    headers = {"Content-Type" : "application/json", "Authorization" : firebase_api_key}

    body = {
        "registration_ids"  : list_device_token,
        "time_to_live"      : 108,
        "content_available" : True,
        "priority"          : "high",
    }

    json_data = {
        "body"              : body_message,
        "title"             : title_message,
        "count"             : 1,
        "push_id"           : push_noti_id,
        "notice_type"       : 0,
        "id_user_send_chat" : id_user_send_chat,
        "type"              : action_push
    }

    body["data"] = json_data

    json_notification = {
        "body"  : body_message,
        "title" : title_message,
        "sound" : "default",
        "badge" : badge,
        "icon"  : "image_notification"
    }

    body["notification"] = json_notification
    # print json.dumps(body)
    requests.post(url, data=json.dumps(body), headers=headers, verify=False)


@mod_store.route('/save-device-token-store', methods = ['POST'])
@manager_user_auth_required()
def save_device_token_store(user = None):
    if not user:
        dictFail = DictFail(message_const.USER_NOT_EXISTS).response
        return jsonify(dictFail)

    manager_id = user['id']
    result = request.json if request.json else {}
    if result == {}:
        dictFail = DictFail(message_const.MES_NONE).response
        return jsonify(dictFail)

    device_token = result.get('device_token', '')
    device_id    = result.get('device_id', '')
    app_id       = result.get('app_id', 0)

    if not app_id:
        app_id = 3

    mDevice = M_Device.query.filter(M_Device.device_id == str(device_id), M_Device.app_id == app_id).first()

    if mDevice:
        try:
            mDevice.device_token = device_token
            mDevice.manager_id   = manager_id
            db.session.commit()
            db_handle.close_connect()
            dictSucess = DictSuccess(None, message_const.MES_NONE).response
            return jsonify(dictSucess)

        except Exception, e:
            LogHandle(e, message_const.ERROR)
            db.session.rollback()
            db_handle.close_connect()
            dictFail = DictFail(message_const.MES_NONE).response
            return jsonify(dictFail)
    try:
        newDevice = M_Device()
        newDevice.device_id    = device_id
        newDevice.device_token = device_token
        newDevice.manager_id   = manager_id
        newDevice.app_id       = app_id
        db.session.add(newDevice)
        db.session.commit()

        db_handle.close_connect()

        dictSucess = DictSuccess(None, message_const.MES_NONE).response
        return jsonify(dictSucess)

    except Exception, e:
        LogHandle(e, message_const.ERROR)

        db.session.rollback()
        db_handle.close_connect()
        dictFail = DictFail(message_const.MES_NONE).response
        return jsonify(dictFail)


@mod_store.route('/store-like-comment', methods = ['POST'])
@manager_user_auth_required()
def store_like_comment(user = None):
    if not user:
        dictFail = DictFail(message_const.USER_NOT_EXISTS).response
        return jsonify(dictFail)

    user_id = user['id']
    result = request.json if request.json else {}

    if result == {}:
        dictFail = DictFail(message_const.JSON_NONE).response
        return jsonify(dictFail)

    like_id     = result.get('like_id', 0)
    comment_id  = result.get('comment_id', 0)
    old_like_id = result.get('old_like_id', 0)
    type_like   = result.get('type_like', 0)
    normal_user = False

    #Like or unlike
    try:
        func_like_comment(type_like, comment_id, user_id, like_id, normal_user, old_like_id)
    except Exception, e:
        LogHandle(e, message_const.ERROR)
        db.session.rollback()
        db.session.close()

        dictFail = DictFail(message_const.MES_NONE).response
        return jsonify(dictFail)


    dictSucess = DictSuccess(None, message_const.MES_NONE).response
    return jsonify(dictSucess)

#STORE CHAT
@mod_store.route('/store-send-chat', methods = ['POST'])
@manager_user_auth_required()
def store_send_chat(user = None):
    if not user:
        dictFail = DictFail(message_const.USER_NOT_EXISTS).response
        return jsonify(dictFail)

    manager_user_id = user['id']
    store_id        = user['store_id']
    result = request.json if request.json else {}

    if result == {}:
        dictFail = DictFail(message_const.JSON_NONE).response
        return jsonify(dictFail)

    normal_user_id = result.get('normal_user_id', 0)
    chat_info      = result.get('chat_info', '')
    img_html       = result.get('img_html', '')
    img            = result.get('img', '')
    is_user_send   = 1 # store send
    normal_user = False

    m_manager = M_Manager_User.query.filter_by(id = manager_user_id).first()

    if not m_manager:
        LogHandle('not manager', message_const.ERROR)
        dictSucess = DictSuccess(None, message_const.MES_NONE).response
        return jsonify(dictFail)

    if m_manager.app_id is not None and m_manager.app_id != '' :
        m_app = M_App.query.filter_by(id = m_manager.app_id).first()

        if not m_app:
            LogHandle('no app', message_const.ERROR)
            dictSucess = DictSuccess(None, message_const.MES_NONE).response
            return jsonify(dictSucess)

        firebase_api_key = m_app.firebase_api_key
    else:
        firebase_api_key = m_manager.firebase_api_key

    try:
        is_send_for_user = True
        chat_id = func_create_chat(normal_user_id, manager_user_id, store_id, chat_info, is_user_send, img_html, img)
        if chat_id:
            list_device_token = func_get_user_receive_notifi_when_chat(normal_user_id)
            # send push
            send_push_firebase(is_send_for_user, firebase_api_key, list_device_token, 'action_chat', chat_id, m_manager.name, '', manager_user_id)

        dictSucess = DictSuccess(None, message_const.MES_NONE).response
        db.session.close()

        return jsonify(dictSucess)

    except Exception, e:
        LogHandle(e, message_const.ERROR)
        db.session.rollback()
        db.session.close()
        dictSucess = DictSuccess(None, message_const.MES_NONE).response
        return jsonify(dictFail)



@mod_store.route('/store-send-notifi-chat', methods = ['POST'])
@manager_user_auth_required()
def store_send_notifi_chat(user = None):
    if not user:
        db_handle.close_connect()
        dictFail = DictFail(message_const.USER_NOT_EXISTS).response
        return jsonify(dictFail)

    manager_user_id = user['id']
    store_id        = user['store_id']
    result = request.json if request.json else {}

    if result == {}:
        dictFail = DictFail(
            message_const.JSON_NONE).response
        return jsonify(dictFail)

    normal_user_id = result.get('normal_user_id', 0)
    chat_id        = result.get('chat_id', 0)
    is_send_all    = result.get('is_send_all', 0)

    m_manager = M_Manager_User.query.filter_by(id=manager_user_id).first()

    if not m_manager:
        LogHandle('not manager', message_const.ERROR)
        db_handle.close_connect()
        dictSucess = DictSuccess(None, message_const.MES_NONE).response
        return jsonify(dictFail)
    if m_manager.app_id is not None and m_manager.app_id != '' :
        m_app = M_App.query.filter_by(id = m_manager.app_id).first()

        if not m_app:
            LogHandle('no app', message_const.ERROR)
            db_handle.close_connect()
            dictSucess = DictSuccess(None, message_const.MES_NONE).response
            return jsonify(dictSucess)

        firebase_api_key = m_app.firebase_api_key
    else:
        firebase_api_key = m_manager.firebase_api_key

    try:
        name_send = m_manager.name

        if not name_send:
            m_store   = M_Store.query.filter_by(id = m_manager.store_id).first()
            name_send = m_store.name

        is_send_for_user = True

        if is_send_all:
            list_device_token = func_get_user_receive_notifi_when_chat_all(m_manager.store_id)

        else:
            list_device_token = func_get_user_receive_notifi_when_chat(normal_user_id)

        send_push_firebase(is_send_for_user, firebase_api_key, list_device_token, 'action_chat', chat_id,
                           name_send, '', manager_user_id)
        db_handle.close_connect()
        dictSucess = DictSuccess(None, message_const.MES_NONE).response
        return jsonify(dictSucess)

    except Exception, e:
        print e
        LogHandle(e, message_const.ERROR)
        db.session.rollback()
        db_handle.close_connect()
        dictSucess = DictSuccess(None, message_const.MES_NONE).response
        return jsonify(dictSucess)


@mod_store.route('/store-send-chat-more-users', methods = ['POST'])
@manager_user_auth_required()
def store_send_chat_more_users(user = None):
    if not user:
        dictFail = DictFail(message_const.USER_NOT_EXISTS).response
        return jsonify(dictFail)

    manager_user_id = user['id']
    store_id        = user['store_id']
    result = request.json if request.json else {}

    if result == {}:
        dictFail = DictFail(message_const.JSON_NONE).response
        return jsonify(dictFail)

    list_normal_user_id = result.get('list_normal_user_id', '')
    chat_info           = result.get('chat_info', '')
    img_html            = result.get('img_html', '')
    img                 = result.get('img', '')

    is_user_send = 1  # store send

    list_arr_user_id = db_handle.get_list_arr(list_normal_user_id)

    if not list_arr_user_id:
        dictFail = DictFail(message_const.MES_NONE).response
        return jsonify(dictFail)
    try:
        for normal_user_id in list_arr_user_id:
            func_create_chat(normal_user_id, manager_user_id, store_id, chat_info, is_user_send, img_html, img)

        db.session.close()
        dictSucess = DictSuccess(None, message_const.MES_NONE).response
        return jsonify(dictSucess)

    except Exception, e:
        LogHandle(e, message_const.ERROR)
        db.session.rollback()
        db.session.close()
        dictFail = DictFail(message_const.MES_NONE).response
        return jsonify(dictFail)


@mod_store.route('/store-send-chat-all-users', methods = ['POST'])
@manager_user_auth_required()
def store_send_chat_all_users(user = None):
    if not user:
        dictFail = DictFail(message_const.USER_NOT_EXISTS).response
        return jsonify(dictFail)

    manager_user_id = user['id']
    store_id        = user['store_id']
    result = request.json if request.json else {}

    if result == {}:
        dictFail = DictFail(message_const.JSON_NONE).response
        return jsonify(dictFail)

    chat_info    = result.get('chat_info', '')
    img_html     = result.get('img_html', '')
    img          = result.get('img', '')

    is_user_send = 1  # store send

    try:
        func_create_chat_all_user(manager_user_id, store_id, chat_info, is_user_send, img_html, img)
        db.session.commit()
        db.session.close()
        dictSucess = DictSuccess(None, message_const.MES_NONE).response
        return jsonify(dictSucess)

    except Exception, e:
        LogHandle(e, message_const.ERROR)
        db.session.rollback()
        db.session.close()
        dictFail = DictFail(message_const.MES_NONE).response
        return jsonify(dictFail)


@mod_store.route('/get-list-user-follow', methods = ['POST'])
@manager_user_auth_required()
def get_list_user_follow(user = None):
    if not user:
        db_handle.close_connect()
        dictFail = DictFail(message_const.USER_NOT_EXISTS).response
        return jsonify(dictFail)

    manager_user_id = user['id']
    store_id        = user['store_id']
    result = request.json if request.json else {}

    if result == {}:
        db_handle.close_connect()
        dictFail = DictFail(message_const.JSON_NONE).response
        return jsonify(dictFail)

    last_modified_date = result.get('last_modified_date', '')
    limit              = result.get('limit', 5)

    list_user_follow = []

    data          = {}
    one_user_json = {}
    try:
        if not store_id:
            db_handle.close_connect()
            data['users_follow'] = list_user_follow
            dictSucess = DictSuccess(data, message_const.MES_NONE).response
            return jsonify(dictSucess)

        if last_modified_date:
            m_lst_user_id_check_in = M_User_Info.query.with_entities(M_User_Info.user_id.label('user_id'), M_User_Info.modified) \
                .filter(M_User_Info.store_id == store_id, M_User_Info.modified < str(last_modified_date)).order_by(M_User_Info.modified.desc()).limit(limit).all()
        else:
            m_lst_user_id_check_in = M_User_Info.query.with_entities(M_User_Info.user_id.label('user_id'), M_User_Info.modified) \
                .filter_by(store_id=store_id).order_by(M_User_Info.modified.desc()).limit(limit).all()

        for m_user in m_lst_user_id_check_in:
            user_follow = M_User.query.filter_by(id = m_user.user_id, is_deleted = 0).first()
            if user_follow:
                one_user_json['name']     = user_follow.name
                one_user_json['email']    = user_follow.email
                one_user_json['img_user'] = user_follow.img_url if user_follow.img_url else ''
                one_user_json['id']       = user_follow.id
                one_user_json['modified'] = db_handle.get_timestamp_milis_from_date(m_user.modified) if m_user.modified else 0

                arr = func_get_last_message(user_follow.id, store_id)
                one_user_json['last_message']      = arr[0]
                one_user_json['last_message_time'] = arr[1]


                list_user_follow.append(copy.copy(one_user_json));

        db_handle.close_connect()

        data['users_follow'] = list_user_follow
        dictSucess = DictSuccess(data, message_const.MES_NONE).response
        return jsonify(dictSucess)

    except Exception, e:
        LogHandle(e, message_const.ERROR)
        db.session.rollback()
        db_handle.close_connect()
        dictFail = DictFail(message_const.MES_NONE).response
        return jsonify(dictFail)


def func_get_last_message(user_id, store_id):
    m_chat = M_Chat.query.filter_by(user_id = user_id, store_id = store_id).order_by(M_Chat.created.desc()).first()

    arr = []
    if m_chat:
        arr.append(m_chat.content if m_chat.content else '')
        arr.append(db_handle.get_timestamp_milis_from_date(m_chat.created) if m_chat.created else 0)

        return arr

    arr.append('')
    arr.append(0)

    return arr

@mod_store.route('/get-list-chat-history-for-store', methods = ['POST'])
@manager_user_auth_required()
def get_list_chat_history_for_store(user = None):
    if not user:
        dictFail = DictFail(message_const.USER_NOT_EXISTS).response
        return jsonify(dictFail)

    management_user_id = user['id']
    store_id           = user['store_id']
    result = request.json if request.json else {}
    data = {}
    list_message = []

    if result == {}:
        dictFail = DictFail(message_const.JSON_NONE).response
        return jsonify(dictFail)

    normal_user_id = result.get('normal_user_id', 0)
    last_chat_id   = result.get('last_chat_id', 0)
    limit          = result.get('limit', 5)

    data['management_user_id'] = int(management_user_id)
    data['store_id']           = int(store_id)
    data['user_id']            = int(normal_user_id)

    try:
        if not last_chat_id:
            lst_chat = M_Chat.query.filter_by(user_id = normal_user_id, management_user_id = management_user_id)\
                .order_by(M_Chat.id.desc()).limit(limit).all()
        else:
            lst_chat = M_Chat.query.filter(M_Chat.user_id == normal_user_id, M_Chat.management_user_id == management_user_id, M_Chat.id < last_chat_id) \
                .order_by(M_Chat.id.desc()).limit(limit).all()

        if not lst_chat:
            data['list_message'] = list_message
            dictSucess = DictSuccess(data, message_const.MES_NONE).response
            return jsonify(dictSucess)

        for chat in lst_chat:
            chat_json = chat.to_dict()
            list_message.append(copy.copy(chat_json))

        data['list_message'] = list_message
        dictSucess = DictSuccess(data, message_const.MES_NONE).response
        return jsonify(dictSucess)

    except Exception, e:
        LogHandle(e, message_const.ERROR)
        db.session.rollback()
        db.session.close()
        dictFail = DictFail(message_const.MES_NONE).response
        return jsonify(dictFail)

#RICH MESSAGE
@mod_store.route('/get-list-rich-message', methods = ['POST'])
@manager_user_auth_required()
def get_list_rich_message(user = None):

    if not user:
        LogHandle(message_const.USER_NOT_EXISTS, message_const.WARNING)
        dictFail = DictFail(message_const.USER_NOT_EXISTS).response
        return jsonify(dictFail)

    manager_id = user['id']

    result = request.json if request.json else {}

    if result == {}:
        LogHandle(message_const.JSON_NONE, message_const.WARNING)
        dictFail = DictFail(message_const.JSON_NONE).response
        return jsonify(dictFail)

    last_rich_message_id = result.get('last_rich_message_id', 0)
    limit                = result.get('limit', 5)

    data = {}
    list_arr_rich = []
    try:
        if last_rich_message_id:
            list_rich_message = M_Rich_Message.query.filter(M_Rich_Message.management_user_id == manager_id,
                                                            M_Rich_Message.id < last_rich_message_id).order_by(M_Rich_Message.id.desc()).limit(limit).all()
        else:
            list_rich_message = M_Rich_Message.query.filter(M_Rich_Message.management_user_id == manager_id)\
                                                            .order_by(M_Rich_Message.id.desc()).limit(limit).all()

        for one_rich in list_rich_message:
            list_arr_rich.append(copy.copy(one_rich.to_dict()))

        data['list_rich_message'] = list_arr_rich
        dictSucess = DictSuccess(data, message_const.MES_NONE).response
        return jsonify(dictSucess)

    except Exception,e:
        LogHandle(e, message_const.ERROR)
        dictFail = DictFail(message_const.MES_NONE).response
        return jsonify(dictFail)


def func_get_manager_user_id_from_store_id(store_id):
    m_manager = M_Manager_User.query.filter_by(store_id = store_id, is_deleted = 0, level_id = 4).first();
    if not m_manager:
        return 0
    return m_manager.id


def func_create_chat(user_id, manager_user_id, store_id, content, is_user_send, img_html, img):
    func_update_action_on_chat(user_id, store_id, content)

    m_chat = M_Chat()

    m_chat.user_id              = user_id
    m_chat.management_user_id   = manager_user_id
    m_chat.store_id             = store_id
    m_chat.user_name            = func_get_name_user(user_id, True)
    m_chat.management_user_name = func_get_name_user(manager_user_id, False)
    m_chat.content              = content
    m_chat.is_user_send         = is_user_send
    m_chat.img_html             = img_html
    m_chat.img                  = img

    db.session.add(m_chat)
    db.session.commit()

    return m_chat.id


def func_create_chat_all_user(manager_user_id, store_id, content, is_user_send, img_html, img):
    m_lst_user_id_check_in = M_User_Info.query.with_entities(distinct(M_User_Info.user_id).label('user_id')) \
        .filter_by(store_id=store_id).order_by(M_User_Info.modified.desc()).all()

    for m_user in m_lst_user_id_check_in:
        m_chat = M_Chat()
        m_chat.user_id              = m_user.user_id
        m_chat.management_user_id   = manager_user_id
        m_chat.store_id             = store_id
        m_chat.user_name            = func_get_name_user(m_user.user_id, True)
        m_chat.management_user_name = func_get_name_user(manager_user_id, False)
        m_chat.content              = content
        m_chat.is_user_send         = is_user_send
        m_chat.img_html             = img_html
        m_chat.img                  = img

        db.session.add(m_chat)


def func_like_timeline(type_like, timeline_id, user_id, like_id, is_normal_user, old_like_id):
    # Update action
    func_update_action_on_timeline(timeline_id)
    try:
        if is_normal_user:
            history_user_like = M_User_Timeline_Like.query.filter_by(timeline_id = timeline_id, user_id = user_id).first()
            if type_like:
                #User like
                if not history_user_like:
                    #User never like before
                    m_user_like = M_User_Timeline_Like()
                    m_user_like.timeline_id = timeline_id
                    m_user_like.user_id = user_id
                    m_user_like.like_id = like_id
                    m_user_like.user_name = func_get_name_user(user_id, is_normal_user)
                    db.session.add(m_user_like)

                    #Increase like user
                    func_increase_like_timeline(timeline_id, like_id)
                    db.session.commit()
                else:
                    #User change like type
                    history_user_like.like_id = like_id

                    if like_id != old_like_id:
                        #Decrease count like old
                        func_decrease_like_timeline(timeline_id, old_like_id)
                        #Increase count like new
                        func_increase_like_timeline(timeline_id, like_id)

                    db.session.commit()
            else:
                #User unlike
                if history_user_like:
                    db.session.delete(history_user_like)
                    func_decrease_like_timeline(timeline_id, old_like_id)
                    db.session.commit()

        else:

            history_user_like = M_User_Timeline_Like.query.filter_by(timeline_id=timeline_id, management_user_id = user_id).first()
            if type_like:
                #User like
                if not history_user_like:
                    #User never like before
                    m_user_like = M_User_Timeline_Like()
                    m_user_like.timeline_id = timeline_id
                    m_user_like.management_user_id = user_id
                    m_user_like.like_id = like_id
                    m_user_like.user_name = func_get_name_user(user_id, is_normal_user)
                    db.session.add(m_user_like)

                    #Increase like user
                    func_increase_like_timeline(timeline_id, like_id)
                    db.session.commit()
                else:
                    #User change like type
                    history_user_like.like_id = like_id
                    if like_id != old_like_id:
                        #Decrease count like old
                        func_decrease_like_timeline(timeline_id, old_like_id)
                        #Increase count like new
                        func_increase_like_timeline(timeline_id, like_id)


                    db.session.commit()
            else:
                #User unlike
                if history_user_like:
                    db.session.delete(history_user_like)
                    func_decrease_like_timeline(timeline_id, old_like_id)
                    db.session.commit()

    except Exception, e:
        LogHandle(e, message_const.ERROR)
        db.session.rollback()


def func_get_name_user(user_id, is_normal_user):
    if is_normal_user:
        m_user = M_User.query.filter_by(id = user_id).first()
        if not m_user:
            return ''
        return m_user.name

    m_user = M_Manager_User.query.filter_by(id=user_id).first()
    if not m_user:
        return ''

    if not m_user.name:
        m_store = M_Store.query.filter_by(id = m_user.store_id).first()
        if not m_store:
            return ''
        return m_store.name


    return m_user.name


def func_increase_like_timeline(timeline_id, like_id):
    handle_count_like = M_Timeline_Count_Like.query.filter_by(timeline_id = timeline_id, like_id = like_id).first()

    first_time = 1
    if not handle_count_like:
        m_time_line_count = M_Timeline_Count_Like()
        m_time_line_count.timeline_id = timeline_id
        m_time_line_count.like_id     = like_id
        m_time_line_count.count_like  = first_time
        db.session.add(m_time_line_count)

    else:
        count_like_now = handle_count_like.count_like
        handle_count_like.count_like = count_like_now + 1

def func_decrease_like_timeline(timeline_id, like_id):

    handle_count_like = M_Timeline_Count_Like.query.filter_by(timeline_id = timeline_id, like_id = like_id).first()
    if handle_count_like:
        count_like_now = handle_count_like.count_like
        if count_like_now >= 1:
            handle_count_like.count_like = count_like_now - 1

#USER COMMENT
def func_add_comment(timeline_id, user_id, content, is_normal_user):
    # Update action
    func_update_action_on_timeline(timeline_id)
    try:
        if is_normal_user:

            #ADD COMMENT
            m_comment = M_Comment()
            m_comment.user_id = user_id
            m_comment.timeline_id = timeline_id
            m_comment.content = content
            m_comment.user_name = func_get_name_user(user_id, is_normal_user)

            db.session.add(m_comment)

            #Update count comment
            m_timeline = M_Timeline.query.filter_by(id = timeline_id).first()
            m_timeline.comment_number = m_timeline.comment_number + 1

            db.session.commit()

        else:
            # ADD COMMENT
            m_comment = M_Comment()
            m_comment.management_user_id = user_id
            m_comment.timeline_id = timeline_id
            m_comment.content = content
            m_comment.user_name = func_get_name_user(user_id, is_normal_user)

            db.session.add(m_comment)

            # Update count comment
            m_timeline = M_Timeline.query.filter_by(id=timeline_id).first()
            m_timeline.comment_number = m_timeline.comment_number + 1

            db.session.commit()
        return m_comment

    except Exception, e:
        LogHandle(e, message_const.ERROR)
        db.session.rollback()
        return 0


def func_like_comment(type_like, comment_id, user_id, like_id, is_normal_user, old_like_id):
    try:
        if is_normal_user:
            history_user_like = M_User_Comment_Like.query.filter_by(comment_id = comment_id, user_id = user_id).first()
            if type_like:
                # User like
                if not history_user_like:
                    # User never like before
                    m_user_like = M_User_Comment_Like()
                    m_user_like.comment_id = comment_id
                    m_user_like.user_id    = user_id
                    m_user_like.like_id    = like_id
                    m_user_like.user_name  = func_get_name_user(user_id, is_normal_user)
                    db.session.add(m_user_like)

                    # Increase like user
                    func_increase_like_comment(comment_id, like_id)
                    db.session.commit()
                else:
                    # User change like type
                    history_user_like.like_id = like_id
                    db.session.commit()
            else:
                # User unlike
                if history_user_like:
                    db.session.delete(history_user_like)
                    func_decrease_like_comment(comment_id, old_like_id)
                    db.session.commit()
        else:

            history_user_like = M_User_Comment_Like.query.filter_by(comment_id = comment_id,
                                                                        management_user_id=user_id).first()
            if type_like:
                # User like
                if not history_user_like:
                    # User never like before
                    m_user_like = M_User_Comment_Like()
                    m_user_like.comment_id         = comment_id
                    m_user_like.management_user_id = user_id
                    m_user_like.like_id            = like_id
                    m_user_like.user_name          = func_get_name_user(user_id, is_normal_user)
                    db.session.add(m_user_like)

                    # Increase like user
                    func_increase_like_comment(comment_id, like_id)
                    db.session.commit()
                else:
                    # User change like type
                    history_user_like.like_id = like_id
                    if like_id != old_like_id:
                        #Decrease count like old
                        func_decrease_like_comment(comment_id, old_like_id)
                        #Increase count like new
                        func_increase_like_comment(comment_id, like_id)

                    db.session.commit()
            else:
                # User unlike
                if history_user_like:
                    db.session.delete(history_user_like)
                    func_decrease_like_comment(comment_id, old_like_id)
                    db.session.commit()

    except Exception, e:
        LogHandle(e, message_const.ERROR)
        db.session.rollback()


def func_increase_like_comment(comment_id, like_id):

    handle_count_like = M_Comment_Count_Like.query.filter_by(comment_id = comment_id, like_id = like_id).first()

    first_time = 1
    if not handle_count_like:
        m_comment_count = M_Comment_Count_Like()
        m_comment_count.comment_id = comment_id
        m_comment_count.like_id    = like_id
        m_comment_count.count_like = first_time
        db.session.add(m_comment_count)
    else:
        count_like_now = handle_count_like.count_like
        handle_count_like.count_like = count_like_now + 1

def func_decrease_like_comment(comment_id, like_id):

    handle_count_like = M_Comment_Count_Like.query.filter_by(comment_id = comment_id, like_id = like_id).first()
    if handle_count_like:
        count_like_now = handle_count_like.count_like
        if count_like_now >= 1:
            handle_count_like.count_like = count_like_now - 1


@mod_store.route('/test', methods = ['GET'])
def test():
    func_update_action_on_timeline(2)



    return jsonify('aaaa')