from func.base_model import Base
from func import db, db_handle

class M_Store(Base):

    __tablename__ = 'stores'

    service_company_id              = db.Column(db.Integer, nullable=True)
    name                            = db.Column(db.String, nullable=True)
    address                         = db.Column(db.String, nullable=True)
    address2                        = db.Column(db.String, nullable=True)
    phone                           = db.Column(db.String, nullable=True)
    check_in_code                   = db.Column(db.String, nullable=True)
    number_of_seats                 = db.Column(db.Integer, nullable=True)
    seat_manager                    = db.Column(db.Text, nullable=True)
    current_number_of_staff         = db.Column(db.Integer, nullable=True)
    max_number_of_staff             = db.Column(db.Integer, nullable=True)
    current_number_of_notifications = db.Column(db.Integer, nullable=True)
    max_number_of_notifications     = db.Column(db.Integer, nullable=True)
    is_applied                      = db.Column(db.Integer, nullable=True)
    information                     = db.Column(db.Text, nullable=True)
    logo_main                       = db.Column(db.Text, nullable=True)
    sort_description                = db.Column(db.Text, nullable=True)
    review                          = db.Column(db.Text, nullable=True)
    review_rating                   = db.Column(db.String, nullable=True)
    notification_fee_per_people     = db.Column(db.String, nullable=True)
    working_time                    = db.Column(db.String, nullable=True)
    road                            = db.Column(db.String, nullable=True)
    day_off                         = db.Column(db.String, nullable=True)
    way_to_pay                      = db.Column(db.String, nullable=True)
    have_parking                    = db.Column(db.Integer, nullable=True)
    is_smoking                      = db.Column(db.Integer, nullable=True)
    logo                            = db.Column(db.Text, nullable=True)
    info_of_user                    = db.Column(db.Text, nullable=True)
    is_deleted                      = db.Column(db.Integer, nullable=True)
    code_create_day                 = db.Column(db.Integer, nullable=True)
    numerical_session_in_day        = db.Column(db.Integer, nullable=True)
    today                           = db.Column(db.Integer, nullable=True)
    latitude                        = db.Column(db.Numeric, nullable=True)
    longitude                       = db.Column(db.Numeric, nullable=True)
    secret_code                     = db.Column(db.Integer, nullable=True)
    note_json                       = db.Column(db.Text, nullable=True)
    postal_code                     = db.Column(db.String, nullable=True)
    qr_code_guide_1                 = db.Column(db.Text, nullable=True)
    qr_code_guide_2                 = db.Column(db.Text, nullable=True)
    url_store                       = db.Column(db.String, nullable=True)

    def __init__(self):
        super(M_Store, self).__init__()




