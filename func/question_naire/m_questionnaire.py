from func.base_model import Base
from func import db, db_handle

class M_Question_Naire(Base):

    __tablename__ = 'question_naires'

    form_data          = db.Column(db.Text, nullable=True)
    service_company_id = db.Column(db.Integer, nullable=True)
    title              = db.Column(db.Text, nullable=True)
    description        = db.Column(db.Text, nullable=True)
    management_user_id = db.Column(db.Integer, nullable=True)
    is_thank_custom    = db.Column(db.Text, nullable=True)
    number_day_expire  = db.Column(db.Integer, nullable=True)

    def __init__(self):
        super(M_Question_Naire, self).__init__()

    def get_question_naire_still_effect(self, id):
        sql = 'select * from question_naires where id = ' + str(id) +  ' and  now() < (created + INTERVAL number_day_expire DAY)'
        return db_handle.executeCommand(sql)

