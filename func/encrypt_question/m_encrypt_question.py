from func.base_model import Base
from func import db, db_handle

class M_Encrypt_Question(Base):
    __tablename__ = 'encrypt_questions'

    user_id           = db.Column(db.Integer, nullable=True)
    question_naire_id = db.Column(db.Integer, nullable=True)
    code              = db.Column(db.String, nullable=True)

    def __init__(self):
        super(M_Encrypt_Question, self).__init__()
