from func.base_model import Base
from func import db, db_handle

class M_App(Base):

    __tablename__ = 'apps'

    app_name         = db.Column(db.String(256), nullable=True)
    package_name     = db.Column(db.String(256), nullable=True)
    firebase_api_key = db.Column(db.Text, nullable=True)
    url_schema       = db.Column(db.Text, nullable=True)
    url_download     = db.Column(db.String(256), nullable=True)

    def __init__(self):
        super(M_App, self).__init__()


