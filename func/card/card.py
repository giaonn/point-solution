from func import db, table_name, db_handle
from datetime import datetime as dt
from func import config_const
import re
import hashlib, pytz

class Card:
    #for user
    def get_card_by_service_company_and_user(self, service_company_id = None, user_id= None,
                                             page = None, limit = None, getTotal = None):
        startPoint = db_handle.getStartPoint(page, limit)
        # Devide case to get all card or not
        if not getTotal:

            sql = db_handle.SELECT + table_name.CARD + ' where service_company_id = ' + str(service_company_id) + ' ' \
                                ' and user_id = ' + str(user_id) + '  ' \
                                ' limit ' + str(startPoint) + ', ' + str(limit)
        else:
            sql = db_handle.SELECT + table_name.CARD + ' where service_company_id = ' + str(service_company_id) + ' ' \
                                ' and user_id = ' + str(user_id) + '  '

        return db_handle.executeCommandMoreRow(sql)
    #for manager
    def get_card_can_use_of_user(self, service_company_id = None, user_id= None,
                                             page = None, limit = None, getTotal = None):
        startPoint = db_handle.getStartPoint(page, limit)
        # Devide case to get all card or not
        if not getTotal:

            sql = db_handle.SELECT + table_name.CARD + ' where service_company_id = ' + str(service_company_id) + ' ' \
                                ' and user_id = ' + str(user_id) + ' and (status = \'can_use\' or status = \'point_can_use\') order by id desc' \
                                ' limit ' + str(startPoint) + ', ' + str(limit)
        else:
            sql = db_handle.SELECT + table_name.CARD + ' where service_company_id = ' + str(service_company_id) + ' ' \
                                ' and user_id = ' + str(user_id) + ' and (status = \'can_use\' or status = \'point_can_use\') order by id desc'

        return db_handle.executeCommandMoreRow(sql)

    def get_card_unused_of_user(self, service_company_id = None, user_id= None,
                                             page = None, limit = None, getTotal = None):
        startPoint = db_handle.getStartPoint(page, limit)
        if not getTotal:

            sql = db_handle.SELECT + table_name.CARD + ' where service_company_id = ' + str(service_company_id) + ' ' \
                                ' and user_id = ' + str(user_id) + ' and status = \'unused\' order by id desc' \
                                ' limit ' + str(startPoint) + ', ' + str(limit)
        else:
            sql = db_handle.SELECT + table_name.CARD + ' where service_company_id = ' + str(service_company_id) + ' ' \
                                ' and user_id = ' + str(user_id) + ' and status = \'unused\' order by id desc'


        return db_handle.executeCommandMoreRow(sql)

    def get_card_amount(self, service_company_id,  user_id, status):

        status = "'" +status + "'"
        sql = 'select count(*) as card_amount from cards where user_id = ' + str(user_id) + ' and status = ' + status  + ' and ' + ' service_company_id = ' + str(service_company_id)
        return db_handle.executeCommand(sql)

    def get_card_by_service_company_id_and_user(self, service_company_id, user_id, status):
        status = "'" + status + "'"
        sql = 'select * from cards where user_id = ' + str(user_id) + ' and status = ' + status + ' and ' \
                ' service_company_id = ' + str(service_company_id) + ' order by created desc limit 1'
        return db_handle.executeCommand(sql)

    def create_new_card(self, service_company_id, user_id, current_number_of_stamps,
                        max_number_of_stamps, can_be_used, is_used, status):
        created = modified = db_handle.get_full_current_jav_time()

        sql = db_handle.INSERT + table_name.CARD + db_handle.LIST_CARD_ATTRIBUTE + ' values ' \
            '(%s , %s, %s , %s, %s , %s, %s, %s, %s)'

        params = (service_company_id, user_id, current_number_of_stamps, max_number_of_stamps, can_be_used, is_used, status,
                  created, modified)

        return db_handle.excuteCommandChangeRecordParams(sql, params)

    def udpate_card(self, id, current_number_of_stamps, can_be_used, status):
        modified = "'" + db_handle.get_full_current_jav_time() + "'"
        status = "'" + status + "'"

        sql = db_handle.UPDATE + table_name.CARD +' set current_number_of_stamps = ' + str(current_number_of_stamps) + ' , can_be_used = ' + \
              str(can_be_used) + ' , status = ' + status + ' , modified = ' + modified + ' where id = ' + str(id)

        return db_handle.excuteCommandChangeRecord(sql)

    def udpate_card_used(self, id, is_used, status):
        modified = "'" + db_handle.get_full_current_jav_time() + "'"
        status = "'" + status + "'"

        sql = db_handle.UPDATE + table_name.CARD +' set is_used = ' + str(is_used) + ' ,status = ' + status + '' \
                        ' , modified = ' + modified + ' where id = ' + str(id)

        return db_handle.excuteCommandChangeRecord(sql)

    def get_card_by_id(self, id):
        sql = db_handle.SELECT + table_name.CARD + ' where id = ' + str(id) + ' '
        return db_handle.executeCommand(sql)



