from func.base_model import Base
from func import db, db_handle

class M_Card(Base):
    __tablename__ = 'cards'
    service_company_id       = db.Column(db.Integer, nullable=True)
    user_id                  = db.Column(db.Integer, nullable=True)
    current_number_of_stamps = db.Column(db.Integer, nullable=True, default=0)
    max_number_of_stamps     = db.Column(db.Integer, nullable=True, default=0)
    can_be_used              = db.Column(db.Integer, nullable=True, default=0)
    is_used                  = db.Column(db.Integer, nullable=True, default=0)
    status                   = db.Column(db.String(50), nullable=True, default='unused' )

    def __init__(self):
        super(M_Card, self).__init__()

    def __repr__(self):
        return '<M_Card %r>' % (self.id)