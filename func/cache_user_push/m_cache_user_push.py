from func.base_model import Base
from func import db, db_handle

class M_Cache_User_Push(Base):

    __tablename__ = 'cache_user_push'

    user_id    = db.Column(db.Integer, nullable=True)
    manager_id = db.Column(db.Integer, nullable=True)
    status     = db.Column(db.Integer, nullable=True, default=1)

    def __init__(self):
        super(M_Cache_User_Push, self).__init__()


