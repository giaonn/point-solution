from func.base_model import Base
from func import db, db_handle

class M_Chat(Base):
    __tablename__ = 'chats'

    user_id              = db.Column(db.Integer, nullable=True)
    management_user_id   = db.Column(db.Integer, nullable=True)
    store_id             = db.Column(db.Integer, nullable=True)
    content              = db.Column(db.Text, nullable=True)
    img_html             = db.Column(db.Text, nullable=True)
    img                  = db.Column(db.Text, nullable=True)
    management_user_name = db.Column(db.String, nullable=True)
    user_name            = db.Column(db.String, nullable=True)
    is_user_send         = db.Column(db.Integer, nullable=True)

    def __init__(self):
        super(M_Chat, self).__init__()
