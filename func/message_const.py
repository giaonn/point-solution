#!/usr/bin/env python
# -*- coding: utf-8 -*-
NAME_ANONYMOUS_DEFAULT = "無名"
#MES PUBLIC
SUCCESS_MES  = "Success"
FAIL_MES     = "Fail"
UPDATE_FAIL  = "Update fail"
DELETE_FAIL  = "Delete fail"
MES_NONE     = ""
GUEST        = "guest"
UNDEFINED_STORE_ID_AND_USER_ID = "ko co store id va user id"
CAN_NOT_GET_STORE_ID_OF_USER = "user ko co store_id"

#AUTHEN USER
GET_TOKEN_FAIL    = "Can\'t get token"
TOKEN_OUT_OF_DATE = "Token out of date"
TOKEN_NO_EXISTS   = "Token doesn\'t exists"
USER_NOT_EXISTS   = "セクションが切りました。再ログインしてください"

#TOKEN
CREATE_TOKEN_FAIL = "Cant create token"

#REGISTER
JSON_NONE          = "No data from client"
IMPORT_FAIL        = "Import to database failed"
MES_NULL_INFO_USER = "おログインIDまたはパスワードが入力されていないため、入力してください。"
LENGTH_PASS_WRONG  = "パスワードを8～14桁で入力してください。"
FORMAT_EMAIL_WRONG = "メールアドレスに誤りがあります。正しく入力してください。"
USER_EXISTS        = "入力されたログインIDは既に登録されています。他のログイン IDを利用してください。"
EMAIL_EXIST        = "入力されたメールアドレスは既に登録されています。他のメールアドレスを利用してください。"
REGISTER_SUCCESS   = "アカウントを登録しました。"

#LOGIN

LOGIN_SUCCESS = "Login successfully"
LOGIN_FAIL    = "ログインイ IDまたはパスワードは正しくありません。"
WRONG_APP_ID  = 'wrong app ID'

#feedback-scan-qr-code-store

QR_CODE_NULL                = "Qrcode is null"
QR_CODE_NOT_EXISTS          = "Qrcode doesn\'t exists"
SCAN_SUCCESS                = "Scan successfully"
USER_STILL_CHECK_IN         = "User haven\'t been checked out"
UPDATE_STORE_NUMERICAL_FAIL = "Update store numerical failed"

#get-check-in-status-by-user

USER_CHECK_IN_SUCCESS     = "CHECK IN!"
USER_NOT_CHECK_IN_SUCCESS = "User haven\'t been checked in"

#get-memo-user
USER_MEMO_SUCCESS = "User\'s memo"
USER_MEMO_FAIL    = "User doesn\'t have memo"
UNSET_MEMO_SERVICE = "Service doesnt set memo"

#handleStore
STORE_NOT_FOUND = "Store not found"
SERVICE_COMPANY_NOT_FOUND = "Service company not found"
UNSETTING_APPROVED_CHECK_IN_OUT_STORE = "Store setting check in check out sai"

#get-list-card-by-store-id
FAIL_GET_CARD = "Cant get card"

#Company
FAIL_GET_COMPANY = "Cant get company"

#Service
FAIL_GET_SERVICE = "Cant get service"

#Stamp
STAMP_NOT_FOUND = "Stamp not found"

#Push_notification
FAIL_GET_PUSH = "Cant get push"

#Get_number_of_seat_in_store
NUMBER_SEAT_NULL = "Seat's number is zero"

#MES_JAV
MATCH_CHECK_IN      = '様が チェックインされました'
UNDEFINED_SESSION   = '来店中の顧客ではありません'
CHECKED_IN          = 'サービス中'
TOKEN_EXPIRE        = 'セクションが切りました。再ログインしてください'
TOO_MANY_CONNECTION = '申し訳ありません。ただ今サーバーが混み合っております。しばらく経ってからもう一度お試しください。'
QR_CODE_EXPIRE      = 'QRコードが変更しました。再度スキャンしてください'
WRONG_QR_CODE       = '店のQRコードではありません'
CHECK_OUT_SUCCESS   = 'チェックアウトされました'
UNCHECK_IN_USER     = 'チェック待ち'
STAMP_ADDED         = 'スタンプが追加できました'
CARD_NOT_FULL       = 'スタンプ獲得中のカードが複数存在しています'
PERMISSION_DENIED   = 'この画面の利用権限がありません'

#get_list_user
GET_LIST_USER_CHECK_IN_SUCCESS = "Get list checked_in user success"

#Card
WRONG_CARD_ID  = "CARD WRONG ID"
WRONG_STAMP_ID = "STAMP WRONG ID"

# Send email reset password
WRONG_USERNAME_RESET_PASSWORD = 'ログイン IDは未登録です。'
WRONG_EMAIL_RESET_PASSWORD = 'メールアドレスは未登録です。'
SUCCESS_SEND_EMAIL = 'メールを送りました。'
ERROR_SEND_EMAIL = 'メール送信の時にエラーが発生しました。'
SET_MEMO_DONE = "要望が更新されました。"
SET_INFO_DONE = "メモを保存しました。"

# Change password by code message
SUCCESS_CHANGE_PASSWORD = 'パスワードを初期化しました。'
ERROR_CHANGE_PASSWORD_BY_CODE = '認証コードは正しくありません。'
ERROR_CHANGE_PASSWORD = 'パスワードは正しくありません。'

# get-ask-to-review-info
UNKNOWN_SERVICE_COMPANY_ID = 'ko thay service company id'
UNKNOWN_SERVICE_COMPANY = 'ko thay service company'

MES_WAIT_CONFIMED = 'チェック待ち'
MES_CHECKED_IN    = 'サービス中'

# set-check-in-out-setting
SET_CHECK_IN_OUT_SETTING_SUCCESS = '設定しました。'

CONFIG_MEMO_NOT_SET = 'Memo null'

SESSION_EXPIRE = 'セッション期限が切れます。'

GOOGLE_WRONG_ISSUE = "Wrong issuer."
INVALID_TOKEN      = "Invalid token or app_id"

NOTIFICATION_STORE_FULL = "契約以上に追加できないので、ご確認してください。"

POINT_NOT_ENOUGH = 'あなたはこのカードを使うために、ポイントが足りません。'

#Close questionnaire
QUESTION_NAIRE_EXPIRE = 'このアンケートの期限が切れました。'

#LIST LOG TYPE
ERROR   = 'Error'
WARNING = 'Warning'
INFO    = 'Info'

#MESS SENDPUSH
MESS_CHAT      = 'さんからメッセージを受け取りました。'
MESS_COMMENT_1 = 'さんが'
MESS_COMMENT_2 = '投稿しました。'
MESS_COMMENT_3 = '投稿しました。'
MESS_LIKE      = 'さんがいいねをつけました。'

ANONYMOUS = '匿名'

NAME_SAVE = ''

NEW_TIMELINE  = 'New Timeline'
POST_TIMELINE = 'が新しい投稿をしました。'

FIREBASE_API_STORE = 'AAAA1AwvYdc:APA91bFKHFrOUFKqOuAIBr62cYxPMD_VoxvIgSwNW2qqOEAjDtxPyBkyZ3OkNknMuLU5MwSsoShFJyBbbL9iSxM8M7QqaM4cXb_P0JIMxJac-zXj7qUskSnn9V4hYJIft2RYkLJoAud4'
