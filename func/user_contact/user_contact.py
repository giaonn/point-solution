from func import db, table_name, db_handle
from datetime import datetime as dt
from func import config_const
import  pytz


class UserContact():
    def create_user_contact(self, user_id, info):
        created = modified = db_handle.get_full_current_jav_time()

        sql = db_handle.INSERT + table_name.USER_CONTACT + db_handle.LIST_USER_CONTACT_ATTRIBUTE + \
              ' values(%s, %s, %s, %s)  '

        params = (user_id, info, created, modified)

        return db_handle.excuteCommandChangeRecordParams(sql, params)