from func import db, table_name, db_handle
from datetime import datetime as dt
from func import config_const
import re
import hashlib, pytz

class Device:
    def create_device(self, device_id, device_token, user_id):
        created = db_handle.get_full_current_jav_time()
        sql = db_handle.INSERT + table_name.DEVICE + db_handle.LIST_DEVICE_ATTRIBUTE + ' values ' \
                 '(%s , %s, %s, %s)'

        params = (device_id, device_token, user_id, created)

        return db_handle.excuteCommandChangeRecordParams(sql, params)