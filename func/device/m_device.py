from func.base_model import Base
from func import db, db_handle

class M_Device(Base):
    __tablename__ = 'devices'

    device_id    = db.Column(db.String, nullable=True)
    device_token = db.Column(db.Text, nullable=True)
    user_id      = db.Column(db.Integer, nullable=True)
    manager_id   = db.Column(db.Integer, nullable=True)
    app_id       = db.Column(db.Integer, nullable=True, default=3)

    def __init__(self):
        super(M_Device, self).__init__()
