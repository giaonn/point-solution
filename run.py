import os
os.environ['ENV'] = 'development'

from func import app

port = 8080
print 'Server listening on http://localhost:{} Ctrl+C to stop'.format(port)
app.run(host='0.0.0.0', port=port, debug=True)